<?php
include_once 'config/validarsessao.php';
include_once 'config/funcoesgerais.php';
include_once 'config/tabela_auxiliar.php';

/* ################################################ */
/* ################################################ */
	include_once 'config/Fun.php';
	include_once 'config/conn_novo.php';

	date_default_timezone_set('america/manaus');

	// ARUQIVO COM FUNÇÕES COMUNS PARA OS CONTROL
	$fun = new FUN();

	// CONNECTIONS
	$sisel = new ConnectionSISEL();

	// IMPORTAÇÃO DE CLASSES
	function my_autoload ($pClassName) {
	    include_once('./php'.DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.'M'.substr($pClassName, 1).".class.php");
	    include_once('./php'.DIRECTORY_SEPARATOR.'control'.DIRECTORY_SEPARATOR.'C'.substr($pClassName, 1).".class.php");
	}
	spl_autoload_register("my_autoload");

/* ################################################ */
/* ################################################ */

$funcoes  = new Funcoes;
$auxiliar = new Auxiliar;
?>

<html>
	<head>
		<title>SISEL</title>
		<meta charset="UTF-8">

		<!-- <link href="css/bootstrap/css/bootstrap.css" rel="stylesheet" media="all"/> -->
		<link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="all"/>
		<link href="css/font/css/font-awesome_ccbr.css" rel="stylesheet" />
		<link href="css/custom/style.css" rel="stylesheet" media="all" />

		<link href="css/minify.css" rel="stylesheet" />
		<!-- <link href="css/metisMenu.css" rel="stylesheet" />
		<link href="css/sb-admin-2.css" rel="stylesheet" /> -->

		<script src="js/jquery.min.js"></script>
		<script src="js/jquery-ui.min.js"></script>
	</head>
	<body>
		<div class="fundopreto"></div>
		<!-- DIV PADRÃO PARA EXIBIR MENSAGEM PARA O USUÁRIO ATRAVÉS DA FUNÇÃO alert2 QUE ESTÁ NO ARQUIVO padrao.js -->
		<div class="alert-msg-pai"></div>
		<div class="loading text-center" style="display: none">
			<div class="cssload-thecube">
				<div class="cssload-cube cssload-c1"></div>
				<div class="cssload-cube cssload-c2"></div>
				<div class="cssload-cube cssload-c4"></div>
				<div class="cssload-cube cssload-c3"></div>
			</div>
			<br><h2>Carregando...</h2>
		</div>

		<!-- CONTEUDO -->
		<div id="wrapper" class="events">
			<?php
			echo "<nav class='navbar navbar-default navbar-static-top hidden-print' role='navigation' style='margin-bottom: 0'>";
				// CARREGA AS BARRAS DE MENU
				include_once "php/topbar.php";
				include_once "php/sidebar.php";
			echo "</nav>";

			// RECEBE PARAMETRO PARA CARREGAR A PÁGINA
			$folder = (isset($_GET["p"])) ? $_GET["p"] : "START";

			if(isset($_GET["page"])){
				$file = $_GET["page"];
			}elseif(isset($_POST["page"])){
				$file = $_POST["page"];
			}else{
				$file = "index";
			}

			$acesso = "?".$_SERVER["QUERY_STRING"];

			echo "<div id='page-wrapper'>";
				echo "<div class='container-fluid'>";
					echo "<div class='row'>";

						if(isset($_GET['p'])){
							$pasta = $_GET['p'];
							$page  = (isset($_GET['page'])) ? $_GET['page'] : "index";

							include_once "php/$pasta/$page.php";
						}else{
							include_once "php/inicio.php";
						}

					echo "</div>";
				echo "</div>";
			echo "</div>";
			?>
		</div>
	</body>
</html>

<script src="js/minify/minify.js"></script>
<!-- <script src="js/maskedinput.js"></script>
<script src="js/maskMoney.js"></script>
<script src="js/funcao.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/sb-admin-2.js"></script>
<script src="js/metisMenu.min.js"></script> -->
<script src="js/padrao.js?0003"></script>