$(function(){
	$("#tipoata").on('change', function(){
		var sigla = $(this).find('option[value='+$(this).val()+']').attr('data-sigla');

		$(".processo-compra").val(sigla);
	});

	$(".btn-inserir").on('click', function(){

		var tipoata 		= $("#tipoata").val();
		var processoCompra 	= $(".processo-compra").val();
		var nata 			= $(".num-ata").val();
		var contratado 		= $("#contratado").val();
		var processoLic 	= $(".processo-licitatorio").val();
		
		var usuario_id 	= $("#usuario_id").val();
		var usuario_ano_exercicio 	= $("#usuario_ano_exercicio").val();

		var mes = $("#mes").val();
		var ano = $("#anoexercicio").val();	

		var serial = $(this).closest('form').serialize();

		if( validarForm(".formAdesao") ){
			$.ajax({
				url: 'php/adesao/ajax_adesao.php',
				type: 'POST',
				data: serial+"&acao=inserir",
				success: function(result){
					if(result == 1){
						alert('Adesão inserido com sucesso!');
						location.href='?p=adesao&page=adesao_cadastrar&processocompras='+processoCompra;
					}else{
						alert('Não foi possível salvar sua Ata. Verifique se o Nº Proceso de Compras já está cadastrado no sistema OU entre em contato com o Administrador.');
					}
		      	}
		  	});
		}

	});

	$(".btn-editar").on('click', function(){

		var get_processoCompra = $(this).attr('data-adesao');

		var tipoata 		= $("#tipoata").val();
		var processoCompra 	= $(".processo-compra").val();
		var nata 			= $(".num-ata").val();
		var contratado 		= $("#contratado").val();
		var processoLic 	= $(".processo-licitatorio").val();

		var mes = $("#mes").val();
		var ano = $("#anoexercicio").val();

		if(tipoata == ''){
			alert('O campo Tipo de Adesão não pode ser vazio.');
			return false;
		}		

		if(processoCompra == ''){
			alert('O campo Nº Proceso de Compras não pode ser vazio.');
			return false;
		}
		
		if(nata == ''){
			alert('O campo Nº Ata não pode ser vazio.');
			return false;
		}		

		if(contratado == ''){
			alert('O campo Fornecedor não pode ser vazio.');
			return false;
		}

		if(processoLic == ''){
			alert('O campo Processo Licitatório não pode ser vazio.');
			return false;
		}

		if(mes == ''){
			alert('O campo Mês Empenho não pode ser vazio.');
			return false;
		}		

		if(ano == ''){
			alert('O campo Ano Empenho não pode ser vazio.');
			return false;
		}

		var serial = $(this).closest('form').serialize();

		$.ajax({
			url: 'php/adesao/ajax_adesao.php',
			type: 'POST',
			data: serial+"&getAdesao="+get_processoCompra+"&acao=editar",
			success: function(result){
				console.log(result);
				if(result == 1){
					alert('Adesão editado com sucesso!');
					location.href='?p=adesao&page=adesao_cadastrar&processocompras='+processoCompra;
				}else{
					console.log(result);
					alert('Não foi possível alterar sua Ata. Verifique se o Nº Proceso de Compras já está cadastrado no sistema OU entre em contato com o Administrador.');	
				}
	      	}
	  	});
	});

	$(".btn-excluir").on('click', function(){
		var nprocesso = $(this).attr('data-codigo');
		
		var c = confirm('Deseja excluir o processo de compras '+nprocesso+" ?");

		if(c == true){

			$.ajax({
				url: 'php/adesao/ajax_adesao.php',
				type: 'POST',
				data: {nprocesso: nprocesso, acao: 'excluir'},
				success: function(result){
					// console.log(result);
					if(result == 1){
						alert('Excluído com sucesso!');
						location.href='?p=adesao&page=adesao';
					}else{
						alert('Não foi possível excluir Ata.');	
					}
		      	}
		  	});

		}

	});

	$(".btn-inserir-item").on('click', function(){
		var qtd 	= $(".qtd").val();
		var vlunit 	= $(".vlunit").val();
		var produto = $("#produto").val();
		var processocompras = $("#processocompras").val();
		var idprocessocompras = $("#idprocessocompras").val();
		
		var t = $(this);

		if(qtd == ''){
			alert('O campo quantidade não pode ser vazio!');
			return false;
		}

		if(vlunit == ''){
			alert('O campo valor unitário não pode ser vazio!');
			return false;
		}

		if(produto == ''){
			alert('O campo produto não pode ser vazio!');
			return false;
		}
		
		$(this).attr("disabled","true");

		$.ajax({
			url: 'php/adesao/ajax_adesao.php',
			type: 'POST',
			data: {qtd: qtd, vlunit: vlunit, produto: produto, processocompras: processocompras, idprocessocompras: idprocessocompras, acao: 'inserirItem'},
			success: function(result){
				// console.log(result);
				if(result == 1){
					alert('Inserido com sucesso!');
					location.href='?p=adesao&page=adesao_cadastrar&processocompras='+processocompras;
				}else{
					alert('Não foi possível Inserir Item.');	
				}
			}
		});

	});

	$(".btn-excluir-item").on('click', function(){
		var id = $(this).attr('data-id');
		// alert(id);
		$.ajax({
			url: 'php/adesao/ajax_adesao.php',
			type: 'POST',
			data: {id: id, acao: 'excluirItem'},
			success: function(result){
				// console.log(result);
				if(result == 1){
					alert('Excluído com sucesso!');
					location.reload();
				}else{
					alert('Não foi possível excluir Item.');	
				}
			}
		});
	});

	$(".btn-cancelar-item").on('click', function(){
		$(".btn-editar-item").hide();
		$(".btn-inserir-item").show();

		$(".qtd").val('');
		$(".vlunit").val('');
		$("#produto option[value='']").attr('selected');


	});
})