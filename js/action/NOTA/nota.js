$(function(){
	$(".btn-excluir").on("click", function(){
		var v = confirm("Deseja deletar Nota Fiscal ?");
		if(v == true){
			$(this).closest('form').submit();
		}
	});

	$(".btnSalvar").on("click", function(){
		if(validarForm(".formNotaFiscal") == true){
			$(".formNotaFiscal").submit();
		}
	});

	$("#contratado").on("change", function(){
		var cpfcnpj    = $(this).find(":selected").attr('cpfcnpj');
		var imunicipal = $(this).find(":selected").attr('imunicipal');
		var iestadual  = $(this).find(":selected").attr('iestadual');
		if(cpfcnpj != ""){
			$("input[name=cpfcnpj]").val(cpfcnpj);
			$("#notafiscalInscmunicipal_emitente").val(imunicipal);
			$("#notafiscalInscestadual_emitente").val(iestadual);
		}
	});

	$("#notafiscalValor_liquido").on("change", function(){
		
		var desconto = parseFloat($("#notafiscalValor_desconto").val());
		var bruto = parseFloat($("#notafiscalValor_bruto").val());
		var liquido = parseFloat($("#notafiscalValor_liquido").val());

		if(bruto != ""){
			var valor = bruto-liquido;
			$("#notafiscalValor_desconto").val(valor.toFixed(2));
		}
	});

	$("#notafiscalValor_desconto").on("change", function(){
		
		var desconto = parseFloat($("#notafiscalValor_desconto").val());
		var bruto = parseFloat($("#notafiscalValor_bruto").val());
		var liquido = parseFloat($("#notafiscalValor_liquido").val());

		if(bruto != ""){
			var valor = bruto-desconto;
			$("#notafiscalValor_liquido").val(valor.toFixed(2));
		}
	});
})