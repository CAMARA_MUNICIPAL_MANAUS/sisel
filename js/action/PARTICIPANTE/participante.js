$(function(){

	$(".cadastrar").on("click", function(){
		var v = $("#licitacaoparticipante").val();
		if(v != ""){
			$("#form-publicacao").submit();
		}
	});

	$(".certidao, .certidao-data-emissao, .certidao-data-validade").on('change', function(){
		
		var id = $(this).attr('data-id');
		var certidao = $(this).closest('.bloco').find('.certidao').val();
		var certidao_emissao = $(this).closest('.bloco').find('.certidao-data-emissao').val();
		var certidao_validade = $(this).closest('.bloco').find('.certidao-data-validade').val();

		$.ajax({
			url: 'php/participante/ajax_participante.php',
			type: 'POST',
			data: {id: id, certidao: certidao, certidao_emissao: certidao_emissao, certidao_validade: certidao_validade, acao: 'atualizarCertidao'},
			success: function(data){
				console.log(data);
				if(data != 1){
					alert('Falha ao atualizar certidão. Entre em contato com o Administrador!');
				}
			}
		});
	});

	$(".btn-inserir").on('click', function(){

		if($("#tipoparticipante").val() == ""){
			alert('O campo Tipo Participante é obrigatório!');
			return false;
		}

		if($("#tipopessoa").val() == ""){
			alert('O campo Tipo Pessoa é obrigatório!');
			return false;
		}

		if($(".razaosocial").val() == ""){
			alert('O campo Razão Social/Nome é obrigatório!');
			return false;
		}

		var tipoparticipante 	= $("#tipoparticipante").val();
		var tipopessoa 			= $("#tipopessoa").val();
		var razaosocial 		= $(".razaosocial").val();
		var licitacaoparticipante = $("#licitacaoparticipante").val();

		$.ajax({
			url: 'php/participante/ajax_participante.php',
			type: 'POST',
			data: {tipoparticipante: tipoparticipante, tipopessoa: tipopessoa, razaosocial: razaosocial, licitacaoparticipante: licitacaoparticipante, acao: 'inserir'},
			success: function(data){
				if(data == 1){
					alert('Inserido com Sucesso!');
					location.reload();
				}else{
					alert('Falha ao inserir participante');
					location.reload();
				}
			}
		});

	});

	$("#tipopessoa").on('change', function(){
		var licitacao = $("#licitacaoparticipante").val();
		var tipopessoa = $(this).val();

		if(tipopessoa != ''){

			$.ajax({
				url: 'php/participante/ajax_participante.php',
				type: 'POST',
				data: {licitacao: licitacao, tipopessoa: tipopessoa, acao: 'listarFornecedores'},
				success: function(data){
					$(".razaosocial").html(data);
				}
			});

		}else{
			$(".razaosocial").html('<option value="">Selecione um Participante</option>');
		}
	});

	$(".razaosocial").on('change', function(){
		var cpfcnpj = $(this).find(":selected").attr('cpfcnpj');
		$("#cpfcnpj").val(cpfcnpj);
	});

	$(".btn-excluir-fornecedor").on('click', function(){
		var main = $(this).closest('.main');
		var fornecedor = $(main).data('fornecedor');
		var c = confirm("Deseja realmente excluir participante "+fornecedor+" da licitação ?");

		if(c == true){

			var fornecedor = $(this).attr("data-fornecedor");
			var licitacao = $(this).attr("data-licitacao");

			$.ajax({
				url: 'php/participante/ajax_participante.php',
				type: 'POST',
				data: {licitacao: licitacao, fornecedor: fornecedor, acao: 'removerParticipante'},
				success: function(data){
					
					if(data == 1){
						alert2('success', 'Participante removido com sucesso!', 5);
						$(main).remove();
					}else{
						alert2('danger','Falha ao remover!', 5);
					}
					
				}
			});

		} // fim if
	});

	$(".btn-excluir-todos-participantes").on('click', function(){
		
		var licitacao = $(this).attr('data-licitacao');

		var c = confirm("Deseja realmente excluir todos os participantes do processo Nº "+licitacao);

		if(c == true){

			$.ajax({
				url: 'php/participante/ajax_participante.php',
				type: 'POST',
				data: {licitacao: licitacao, acao: 'excluirTodosParticipantes'},
				success: function(data){

					// console.log(data);
					
					if(data == 1){
						alert('Participantes removidos com sucesso!');
						location.reload();
					}else{
						alert('Falha ao remover!');
						location.reload();
					}
					
				}
			});
		}
	});
})