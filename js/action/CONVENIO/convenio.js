$(function(){
	$("#tipoconvenio").on('change', function(){
		var sigla = $("#tipoconvenio option:selected").attr('sigla');
		$(".numconvenio").val(sigla);
	});

	$(".numconvenio").on('change', function(){
		$(".arqtexto").val($(this).val()+".pdf");
	});

	$(".btn-inserir, .btn-editar").on('click', function(){

		var tipoconvenio = $("#tipoconvenio").val();
		var numconvenio = $(".numconvenio").val();
		var tipoesfera = $("#tipoesfera").val();
		var objeto = $(".objeto").val();
		var mes = $("#mes").val();
		var ano = $("#anoexercicio").val();

		var acao = $(this).attr('acao');

		if(tipoconvenio == ""){
			alert('O campo TIPO DE CONVÊNIO não pode ser vazio!');
			return false;
		}

		if(numconvenio == ""){
			alert('O campo Nº CONVÊNIO não pode ser vazio!');
			return false;
		}

		if(tipoesfera == ""){
			alert('O campo ESFERA não pode ser vazio!');
			return false;
		}

		if(objeto == ""){
			alert('O campo OBJETO não pode ser vazio!');
			return false;
		}

		if(mes == ""){
			alert('O campo MÊS EMPENHO não pode ser vazio!');
			return false;
		}

		if(ano == ""){
			alert('O campo ANO EMPENHO não pode ser vazio!');
			return false;
		}

		if(acao == 'editar'){
			var dados = $("form").serialize()+"&acao="+acao+"&coigo="+$(this).attr('codigo');
		}else{
			var dados = $("form").serialize()+"&acao="+acao;
		}

		$.ajax({
			url: 'php/convenio/ajax_convenio.php',
			type: 'POST',
			data: dados,
			success: function(data){
				if(data == 1){
					alert('Efetuado com sucesso!');
					location.href="?p=convenio&page=convenio";
				}else{
					console.log(data);
					alert('Falha na execução!');
				}
			}
		});
		
	})
})