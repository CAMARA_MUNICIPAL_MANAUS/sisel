$(function(){
	$("#licitacao").on('change', function(){
		var licitacao = $(this).val();

		if(licitacao != ""){
			$.ajax({
		        url: 'php/cotacao/ajax_cotacao.php',
		        type: 'POST',
		        data: {licitacao:licitacao, acao: 'buscarItens'},
		        success: function(result) {
		        	$(".itemLicitacao").html(result);
		        }
		    });

			$.ajax({
		        url: 'php/cotacao/ajax_cotacao.php',
		        type: 'POST',
		        data: {licitacao:licitacao, acao: 'listarCotacao'},
		        success: function(result) {
		        	// console.log(result);
		        	$(".listarCotacao").html(result);
		        }
		    });

		}
	});

	$(".itemLicitacao").on('change', function(){

		var value = $(this).val();
		var licitacao = $("#licitacao").val();

		if(value != ""){

			var option = $(this).find("option[value="+value+"]");

			$(".quantidade").val($(option).attr('data-qtd'));
			$(".controleItemLote").val($(option).attr('data-controle'));
			$(".unidade").val($(option).attr('data-unidade'));

			$.ajax({
		        url: 'php/cotacao/ajax_cotacao.php',
		        type: 'POST',
		        data: {licitacao: licitacao, item: value, acao: 'buscarParticipante'},
		        success: function(result) {
		        	// console.log(result);
		        	$(".participante").html(result);
		        }
		    });

		}else{

			$(".quantidade").val('');
			$(".controleItemLote").val('');
			$(".unidade").val('');

		}
	});

	$(".participante").on('change', function(){
		var value = $(this).val();

		if(value != ""){

			var tipopessoa = $(this).find("option:selected").attr('data-pessoa');

			$(".tipopessoa").val(tipopessoa);
		}else{
			$(".tipopessoa").val('');
		}
	});

	$(".vltotal").on('keyup', function(){

		var qtd = $(".quantidade").val();
		var vltotal = $(this).val();

		calculoValorUnitario(qtd, vltotal);
	});


	$(".vltotal").on('keydown', function(){
		var qtd = $(".quantidade").val();
		var vltotal = $(".vltotal").val();

		calculoValorUnitario(qtd, vltotal);
	});

	$(".btn-inserir").on('click', function(){
		
		if($("#licitacao").val() == ''){
			alert("Por favor, selecione o Nº do Proceso Licitatório.");
			return false;
		}

		if($(".itemLicitacao").val() == ''){
			alert("O campo Item da Licitação deve ser selecionado.");
			return false;
		}

		if($(".participante").val() == ''){
			alert("O campo participante deve ser selecionado.");
			return false;
		}

		if($(".vltotal").val() == ''){
			alert("O  campo Valor Total deve ser preenchido.");
			return false;
		}

		if($("#tipovalor").val() == ''){
			alert("O campo Tipo Valor deve ser selecionado.");
			return false;
		}

		if($("#tiporesultado").val() == ''){
			alert("O campo Tipo Resultado deve ser selecionado.");
			return false;
		}

		$.ajax({
	        url: 'php/cotacao/ajax_cotacao.php',
	        type: 'POST',
	        data: $("form").serialize()+"&acao=inserirCotacao",
	        success: function(result) {
	        	if(result == 1){
	        		alert('Cotação adicionado com sucesso!');
	        		location.reload();
	        	}else{
	        		alert('Falha ao cadastrar a cotação!');
	        		location.reload();
	        	}
	        }
	    });
		

	});

	$(document).on('click', ".excluir-licitacao", function(){

		var licitacao = $(this).attr('data-licitacao');
		var c = confirm("Deseja realmente excluir essa cotação ?");

		if(c == true){
			$.ajax({
		        url: 'php/cotacao/ajax_cotacao.php',
		        type: 'POST',
		        data: {licitacao: licitacao, acao: 'excluirCotacao'},
		        success: function(result) {

		        	// console.log(result);
		        	
		        	if(result == 1){
		        		alert('Cotação removida com sucesso!');
		        		location.reload();
		        	}else{
		        		alert('Falha ao remover cotação!');
		        		location.reload();
		        	}
		        }
		    });
		}

	});
});

function calculoValorUnitario(qtd, vltotal){
	if(qtd == "" || qtd == 0){
		$(".vltotal").val('');
		alert('O campo QTD deve ser preenchido e maior que zero.');
		return false;
	}else{

		qtd = parseFloat(qtd);

		var calculo = (convertStringToFloat(vltotal)/qtd).formatMoney("2",",",".");

		$(".vlunit").val(calculo);
	}
}