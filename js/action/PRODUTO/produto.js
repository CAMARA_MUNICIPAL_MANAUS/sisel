$(function(){
	$(".btn-cadastrar").on('click', function(){

		if(validarForm(".formProduto")){
			$("#acao").val('cadastrar');
			$(this).closest('form').submit();
		}

	});

	$(".btn-excluir").on('click', function(){

		var produto = $(this).closest("tr").find("td:eq(1)").text();
		var c = confirm("Deseja excluir o produto: "+produto);

		if( c === true ){
			var codigo = $(this).closest("td").data('codigo');

			$.ajax({
				url: 'php/produto/ajax_cadastar_editar_produto.php',
				type: "POST",
				data: {codigo:codigo, acao: 'excluir'},
				success: function(data){
					if(data == 1){
						alert('Excluido com Sucesso!');
						location.reload();
					}else{
						alert2('danger', 'Falha ao excluir produto!', 8);
					}
				}
			});
		} // fim if
	});
})