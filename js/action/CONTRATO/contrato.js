$(function(){
  $("#tipopessoa").on('change', function(){
    var value = $("#tipopessoa").val();
    if(value != ''){
      $.ajax({
        url: 'php/contrato/ajax_contrato.php',
        type: 'POST',
        data: 'tipopessoa='+value,
        success: function(result) {

          //  console.log(result);
          $("#contratado").html(result);
          $("#cpfcnpj").val('');
        }
     });
    }else{
      $("#contratado").html("<option value=''>Selecione o campo Tipo Pesoa</option>");
    }
  });

  $(document).on('change', "#contratado", function(){
    var cpfcnpj = $(this).find(":selected").attr('cpfcnpj');
    $("#cpfcnpj").val(cpfcnpj);
  });

  $("#tipocontrato").on('change', function(){
    var sigla = $(this).find(":selected").attr('sigla');
    $("#numcontrato").val(sigla);

    var value = $(this).val();

    if(value == 2){
      $(".divAdtivo").show();
    }else{
      $(".divAdtivo").hide();
    }
  });

  $("#numcontrato").on('blur', function(){
    var valor = $("#numcontrato").val();
    if(valor != ''){
      $("#arqtexto").val(valor+'.pdf');
    }
  });

  $(".btn-excluir").on('click', function(){
    var contrato = $(this).closest('tr').find("td:eq(1)").text();
    var c = confirm("Deseja excluir o contrato: "+contrato);
    if(c == true){
      var codigo = $(this).data('codigo');
      $.ajax({
        url: 'php/contrato/ajax_contrato.php',
        type: 'POST',
        data: {codigo: codigo, acao: 'excluir'},
        success: function(result){
          if(result){
            alert('Excluído com Sucesso!');
            location.reload();
          }else{
            alert('Falha ao excluir contrato!');
          }
        }
      });
    } // fim if
  });

  $(".btn-inserir, .btn-editar").on('click', function(){
    
    if($("#tipopessoa").val() == ""){
      alert('O campo Tipo Pessoa é obrigatório.');
      return false;
    }

    if($("#contratado").val() == ""){
      alert('O campo Contrato é obrigatório.');
      return false;
    }

    if($("#tipocontrato").val() == ""){
      alert('O campo Tipo Contrato é obrigatório.');
      return false;
    }

    if($("#numcontrato").val() == ""){
      alert('O campo Nº Contrato é obrigatório.');
      return false;
    }

    if($("#recebeValor").val() == ""){
      alert('O campo Receber Valor é obrigatório.');
      return false;
    }

    if($("#obj").val() == ""){
      alert('O campo Objetivo é obrigatório.');
      return false;
    }

    if($("#ordernador").val() == ""){
      alert('O campo Ordenador é obrigatório.');
      return false;
    }

    if($("#dtcontrato").val() == ""){
      alert('O campo Dt. Ass. Contrato é obrigatório.');
      return false;
    }

    if($("#dtvencimento").val() == ""){
      alert('O campo Dt. Venc. é obrigatório.');
      return false;
    }

    if($("#mes").val() == ""){
      alert('O campo Competência(Mês) é obrigatório.');
      return false;
    }

    if($("#anoexercicio").val() == ""){
      alert('O campo Competência(Ano) é obrigatório.');
      return false;
    }

    var acao = $(this).val();

    $(".formContrato").submit();

  });

$("#licitacao, #contratado").on('change', function(){

  var fornecedor = "";
  var licitacao = "";

  if(licitacao != "" && fornecedor != ""){
    $.ajax({
      url: 'php/contrato/ajax_contrato.php',
      type: 'POST',
      data: {fornecedor: fornecedor, licitacao: licitacao, acao: 'buscarCertidoes'},
      success: function(data){

        var c = $.parseJSON(data);
        var len = c.length;

        // console.log(c[1]);

        // return false;

        if(len > 0){

          for (var i = 0; i < len; i++){

            switch (c[i].tipocertidao_fk) {
              
              case '1': // INSS

                $("#inss").val(c[i].num_certidao);
                $("#dte1").val(c[i].dt_emissao);
                $("#dtv1").val(c[i].dt_validade);

              break;

              case '2': // FEDERAL

                $("#sff").val(c[i].num_certidao);
                $("#dte5").val(c[i].dt_emissao);
                $("#dtv5").val(c[i].dt_validade);

              break;

              case '3': // ESTADUAL

                $("#sfe").val(c[i].num_certidao);
                $("#dte3").val(c[i].dt_emissao);
                $("#dtv3").val(c[i].dt_validade);

              break;

              case '4': // MUNICIPAL

                $("#sfm").val(c[i].num_certidao);
                $("#dte4").val(c[i].dt_emissao);
                $("#dtv4").val(c[i].dt_validade);

              break;

              case '5': // FGTS

                $("#fgts").val(c[i].num_certidao);
                $("#dte2").val(c[i].dt_emissao);
                $("#dtv2").val(c[i].dt_validade);

              break;

              /*case '6': // CAM

                $("#inss").val(c[i].num_certidao);
                $("#dte1").val(c[i].dt_emissao);
                $("#dtv1").val(c[i].dt_validade);

              break;*/

              case '7': // CNDT

                $("#tst").val(c[i].num_certidao);
                $("#dte8").val(c[i].dt_emissao);
                $("#dtv8").val(c[i].dt_validade);

              break;

              case '99': // OUTRAS CERTIDOES

                $("#oc").val(c[i].num_certidao);
                $("#dte6").val(c[i].dt_emissao);
                $("#dtv6").val(c[i].dt_validade);

              break;

            } // fim switch

          }; // fim for

        } // fim if

      } // fim success

    });
  }
});

})