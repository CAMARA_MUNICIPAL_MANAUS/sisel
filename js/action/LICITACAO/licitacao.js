$(function(){

        var valorTipoModalide = $("#tipomodalidade").val();

        if(valorTipoModalide != 8 && valorTipoModalide != 9 && valorTipoModalide != 20){
            $("#tiponaturezaprocedimento").removeAttr("disabled");
        }else{
            $("#tiponaturezaprocedimento").attr("disabled", "true");
            $("#tiponaturezaprocedimento option").removeAttr("selected");
        }

        if(valorTipoModalide == 10){ // SE FOR CONCURSO

            $("#tipolicitacao").attr("disabled", "true");
            $("#tiponaturezalicitacao").attr("disabled", "true");

            $("#tipolicitacao option").removeAttr("selected");
            $("#tiponaturezalicitacao option").removeAttr("selected");
        }else{                                                      

            if(valorTipoModalide == 8 || valorTipoModalide == 9 || valorTipoModalide == 20){
                $("#tipolicitacao").attr("disabled", "true");
                $("#tiponaturezalicitacao").removeAttr("disabled");
                $("#razao").removeAttr("disabled");

                $("#tiponaturezalicitacao option[value=4]").css("display", "none");
                $("#tiponaturezalicitacao option[value=5]").css("display", "none");
                $("#tiponaturezalicitacao option[value=6]").css("display", "none");
            }else{
                $("#tipolicitacao").removeAttr("disabled");
                $("#tiponaturezalicitacao").removeAttr("disabled");

                $("#tiponaturezalicitacao option[value=4]").css("display", "block");
                $("#tiponaturezalicitacao option[value=5]").css("display", "block");
                $("#tiponaturezalicitacao option[value=6]").css("display", "block");
            }

        }

    /* ### */

    $("#numprocesso").on('change', function(){
        if($("#numprocesso").val() != ''){
            $("#arq").val($("#numprocesso").val()+'-'+$("#anolicitacao").val()+".pdf");
        }
    });

    $("#tiponaturezalicitacao").on("change", function(){
        let valor = $(this).val();
        if(valor == 1){
            $("#tiporegimeexecucao").removeAttr("disabled");
        }else{ 
            $("#tiporegimeexecucao").attr("disabled", "true");
            $("#tiporegimeexecucao option").removeAttr("selected", "selected");
        }
    });

    $("#tipomodalidade").on('change', function(){
        let sigla = $(this).find(":selected").attr('sigla');
        let valor = $(this).val();

        $("#numprocesso").val(sigla).focus();

        if(valor != 8 && valor != 9 && valor != 20){
            $("#tiponaturezaprocedimento").removeAttr("disabled");
        }else{
            $("#tiponaturezaprocedimento").attr("disabled", "true");
            $("#tiponaturezaprocedimento option").removeAttr("selected");
        }

        if(valor == 10){ // SE FOR CONCURSO
            $("#tipolicitacao").attr("disabled", "true");
            $("#tiponaturezalicitacao").attr("disabled", "true");
            $("#tipolicitacao option").removeAttr("selected");
            $("#tiponaturezalicitacao option").removeAttr("selected");
        }else{

            if(valor == 8 || valor == 9 || valor == 20){
                $("#tipolicitacao").attr("disabled", "true");
                $("#tiponaturezalicitacao").removeAttr("disabled");
                $("#razao").removeAttr("disabled");

                $("#tiponaturezalicitacao option[value=4]").css("display", "none");
                $("#tiponaturezalicitacao option[value=5]").css("display", "none");
                $("#tiponaturezalicitacao option[value=6]").css("display", "none");
            }else{
                $("#tipolicitacao").removeAttr("disabled");
                $("#tiponaturezalicitacao").removeAttr("disabled");

                $("#tiponaturezalicitacao option[value=4]").css("display", "block");
                $("#tiponaturezalicitacao option[value=5]").css("display", "block");
                $("#tiponaturezalicitacao option[value=6]").css("display", "block");
            }

        }

    });

    $('.novoitem').on('click', function(){

        var cont = $(".item-licitacao").find('tbody').find('tr').length+1;

        $(".cadastrar-item").data('id','');
        $(".formItemLicitacao #descricao").val('');
        $(".formItemLicitacao #dtpubhomologa").val('');
        $(".formItemLicitacao #dtassinatura").val('');
        $(".formItemLicitacao #controle").val('');
        $(".formItemLicitacao #sequencial").val(cont);
        $(".formItemLicitacao #qtd").val('');
        $(".formItemLicitacao #tipounidade").val('');
        $(".formItemLicitacao #statusitem").val('');

        $(".cadastrar-item").html('Cadastrar');
    });

    $(document).on('click', ".cadastrar-item", function(){
        // ESSA FUNÇÃO SERVE CADASTRAR E EDITAR ITEM DA LICITAÇÃO

        var txtbutton = $(this).html();
        var acao = '';

        if(txtbutton == 'Cadastrar'){
            acao = 'cadastraritem';
            id = '';
        }else{
            acao = 'editaritem';
            id = $(this).data('id');
        }// fim if


            var numprocesso   = $("#numprocesso").val()+'-'+$("#anolicitacao").val();
            var sequencial    = $("#sequencial").val();
            var quantidade    = $("#qtd").val();
            var tipounidade   = $("#tipounidade").val();
            var dtassinatura  = $("#dtassinatura").val();
            var dtpubhomologa = $("#dtpubhomologa").val();
            var controle      = $("#controle").val();
            var statusitem    = $("#statusitem").val();
            var descricao     = $("#descricao").val();
            
            $.ajax({
                url: 'php/licitacao/ajax_licitacao.php',
                type: 'POST',
                data: "numprocesso="+numprocesso+
                      "&sequencial="+sequencial+
                      "&quantidade="+quantidade+
                      "&tipounidade="+tipounidade+
                      "&dtassinatura="+dtassinatura+
                      "&dtpubhomologa="+dtpubhomologa+
                      "&controle="+controle+
                      "&statusitem="+statusitem+
                      "&descricao="+descricao+
                      "&idItem="+id+
                      "&"+acao+"="+acao,

                success: function(result) {
                    if(result == '1'){
                        if(id != ""){
                            alert('Item inserido com sucesso!');
                        }else{
                            alert('Item alterado com sucesso!');
                        }
                        location.reload();
                    }else{
                        alert2('danger', 'Desulpe, não foi possível armazenar sua informação. Verifique se todos os campos foram preenchidos corretamnte!', 8);
                    }
                }
            });


    });

    $("input.ui-autocomplete-input").on('blur', function(){
        if($(this).val() != ''){
           $(".cadastrar-item") .removeAttr('disabled');
        }
    });

    $(document).on('click', ".editar-item", function(){
        // O FORMULÁRIO DE EDIÇÃO DO ITEM ESTÁ NO FINAL DO ARQUIVO AJAX_LICITACAO.PHP
        var tr = $(this).closest('tr');

        var iditem        = $(tr).data("id");
        var licitacao     = $(tr).data("licitacao");
        var sequencial    = $(tr).find("td:eq(0)").data("sequencial");
        var produto       = $(tr).find("td:eq(1)").data("produto");
        var qtd           = $(tr).find("td:eq(2)").data("qtd");
        var unidade       = $(tr).find("td:eq(3)").data("unidade");
        var dt_publicacao = $(tr).find("td:eq(4)").data("dt_publicacao");
        var dt_assinatura = $(tr).find("td:eq(5)").data("dt_assinatura");
        var itemlote      = $(tr).find("td:eq(6)").data("itemlote");
        var status        = $(tr).find("td:eq(7)").data("status");

        $(".cadastrar-item").data('id',iditem);
        $(".formItemLicitacao #descricao").val(produto).trigger('change');
        $(".formItemLicitacao #dtpubhomologa").val(dt_publicacao);
        $(".formItemLicitacao #dtassinatura").val(dt_assinatura);
        $(".formItemLicitacao #controle").val(itemlote);
        $(".formItemLicitacao #sequencial").val(sequencial);
        $(".formItemLicitacao #qtd").val(qtd);
        $(".formItemLicitacao #tipounidade").val(unidade);
        $(".formItemLicitacao #statusitem").val(status);

        $(".cadastrar-item").html('Editar');
        $(".cadastrar-item").data('id',iditem);

        $("#myModalItemLicitacao").modal("show");

    });

    $(document).on('click', ".excluir-item", function(){
        
        var produto = $(this).closest('tr').find("td:eq(1)").text();
        var c = confirm("Deseja realmente deletar o item: "+produto+"?");

        if(c == true){
            
            var id = $(this).closest("tr").data("id");

            $.ajax({
                url: 'php/licitacao/ajax_licitacao.php',
                type: 'POST',
                data: {iditem: id, exluiritem: 'exluiritem'},
                success: function(result){

                    console.log(result);

                    if(result == 1){
                        alert('Item Excluído com sucesso!');
                        location.reload();
                    }else{
                        alert('Erro ao deletar item. Entre em contato com o Administrador!');
                    }
                }
            }); // fim ajax
        }
        
    });


    $("#descricao").on('change', function(){

        var value = $(this).val();

        if(value != ''){
            
            var unidade = $(this).find('option[value='+value+']').attr('unidade');

            $("#tipounidade").find("option[value="+unidade+"]").attr('selected','selected');

        }

    });


    $("#cancelar-edicao").on('click', function(){
        // CANCELAR EDIÇÃO DE DOTAÇÃO
        $("#inserir-dotacao").css('display', 'block');
        $("#salvar-dotacao").css('display', 'none');

        var idcotacao = $(this).attr('iddotacao');

        $("#cancelar-edicao").attr('disabled','disabled');
        $("#cancelar-edicao").removeAttr('iddotacao');

        var numprocesso = $("#numprocesso").val()+'-'+$("#anolicitacao").val();

        $.ajax({
            url: "php/licitacao/ajax_licitacao.php",
            type: "POST",
            data: "numprocesso="+numprocesso+"&cancelardotacao=cancelardotacao",
            success: function(result){

                $("#c1").val('');
                $("#c2").val('');
                $("#c3").val('');
                $("#c4").val('');
                $("#c5").val('');
                $("#c6").val('');
                $("#c7").val('');
                $("#c8").val('');
                $("#c9").val('');
                $("#c10").val('');

                $(".tbody-dotacao").html(result);
            }
        });
    });

    $(".inserir-licitacao").on('click', function(){

        var molidade        = $("#tipomodalidade").val();
        var numprocesso     = $("#numprocesso").val()+'-'+$("#anolicitacao").val();
        var tipo_itemlote   = $("#tipo_itemlote").val();

        var tipolicitacao         = $("#tipolicitacao").val();
        var tiponaturezalicitacao = $("#tiponaturezalicitacao").val();
        var tiporegimeexecucao    = $("#tiporegimeexecucao").val();
        var tiponaturezaprocedimento = $("#tiponaturezaprocedimento").val();

        var valor_bloqueado = $(".dinheiro").val();
        var objetivo        = $("#obj").val();

        var razao           = $("#razao").val();

        var ordernador      = $("#ordenador").val();
        var numero_edital   = $("#numEdital").val();
        var numero_diario   = $("#numdiario").val();
        var dt_publicacao   = $("#dtpublic").val();
        var dias            = $("#dias").val();
        var dt_limite       = $("#dtLimite").val();
        var mesEmpenho      = $("#mes").val();
        var anoEmpenho      = $("#anoEmpenho").val();
        var arq             = $("#arq").val();  
        var empenho1        = $("#e1").val();
        var empenho2        = $("#e2").val();
        var empenho3        = $("#e3").val();
        var empenho4        = $("#e4").val();
        var empenho5        = $("#e5").val();
        var empenho6        = $("#e6").val();
        var empenho7        = $("#e7").val();
        var empenho8        = $("#e8").val();
        var empenho9        = $("#e9").val();
        var empenho10       = $("#e10").val();
        var empenho11       = $("#e11").val();
        var empenho12       = $("#e12").val();

        if(molidade == ''){ alert('O campo Modalidade não pode ser vazio'); return false; }
        if(numprocesso == ''){ alert('O campo Nº Processo não pode ser vazio'); return false; }
        if(tipo_itemlote == ''){ alert('O campo Tipo Licitação não pode ser vazio'); return false; }
        if(valor_bloqueado == ''){ alert('O campo Valor Bloqueado não pode ser vazio'); return false; }
        if(objetivo == ''){ alert('O campo Objetivo não pode ser vazio'); return false; }
        if(ordernador == ''){ alert('O campo Ordenador não pode ser vazio'); return false; }
        /*if(numero_edital == ''){ alert('O campo Número do Edital não pode ser vazio'); return false; }
        if(numero_diario == ''){ alert('O campo Número do Diário não pode ser vazio'); return false; }*/
        /*if(dt_publicacao == ''){ alert('O campo Data de Publicação não pode ser vazio'); return false; }
        if(dt_limite == ''){ alert('O campo Data Limite não pode ser vazio'); return false; }*/
        if(arq == ''){ alert('O campo Arquivo Texto não pode ser vazio'); return false; }

        var dados = "molidade="+molidade+
                    "&numprocesso="+numprocesso+
                    "&tipo_itemlote="+tipo_itemlote+
                    "&tipolicitacao="+tipolicitacao+
                    "&tiponaturezalicitacao="+tiponaturezalicitacao+
                    "&tiporegimeexecucao="+tiporegimeexecucao+
                    "&tiponaturezaprocedimento="+tiponaturezaprocedimento+
                    "&valor_bloqueado="+valor_bloqueado+
                    "&objetivo="+objetivo+
                    "&razao="+razao+
                    "&ordernador="+ordernador+
                    "&numero_edital="+numero_edital+
                    "&numero_diario="+numero_diario+
                    "&dt_publicacao="+dt_publicacao+
                    "&dias="+dias+
                    "&dt_limite="+dt_limite+
                    "&mesEmpenho="+mesEmpenho+
                    "&anoEmpenho="+anoEmpenho+
                    "&arq="+arq+
                    "&empenho1="+empenho1+
                    "&empenho2="+empenho2+
                    "&empenho3="+empenho3+
                    "&empenho4="+empenho4+
                    "&empenho5="+empenho5+
                    "&empenho6="+empenho6+
                    "&empenho7="+empenho7+
                    "&empenho8="+empenho8+
                    "&empenho9="+empenho9+
                    "&empenho10="+empenho10+
                    "&empenho11="+empenho11+
                    "&empenho12="+empenho12;

        $.ajax({
            url: 'php/licitacao/ajax_licitacao.php',
            type: "post",
            data: dados+"&cadastrarlicitacao=cadastrarlicitacao",
            success: function(result){
                if(result == 1){
                    alert('Licitação inserida com Sucesso! \nContinue para inserir a dotação e item da licitação');
                    location.href="?p=licitacao&page=licitacao_cadastrar&licitacao="+numprocesso;
                }

                if(result == 2){
                    alert('Esta Licitação já está cadsatrada no sitema.');
                    return false;
                }

                if(result != 1 && result != 2){
                    alert('Ops, encontramos um erro ao inserir esta solicitação!\nVerifique se todos os campos foram preenchidos corretamente ou se já existe uma licitação com este número cadastrado no sistema.');
                    console.log(result);
                    return false;
                }
            }
        });
    
    });

    $(document).on('click', ".editar-licitacao", function(){

        var molidade        = $("#tipomodalidade").val();
        var numprocesso     = $("#numprocesso").val()+"-"+$("#anolicitacao").val();
        var tipo_itemlote   = $("#tipo_itemlote").val();

        var tipolicitacao         = $("#tipolicitacao").val();
        var tiponaturezalicitacao = $("#tiponaturezalicitacao").val();
        var tiporegimeexecucao    = $("#tiporegimeexecucao").val();
        var tiponaturezaprocedimento = $("#tiponaturezaprocedimento").val();

        var valor_bloqueado = $(".dinheiro").val();
        var objetivo        = $("#obj").val();

        var razao           = $("#razao").val();

        var ordernador      = $("#ordenador").val();
        var numero_edital   = $("#numEdital").val();
        var numero_diario   = $("#numdiario").val();
        var dt_publicacao   = $("#dtpublic").val();
        var dias            = $("#dias").val();
        var dt_limite       = $("#dtLimite").val();
        var mesEmpenho      = $("#mes").val();
        var anoEmpenho      = $("#anoEmpenho").val();
        var arq             = $("#arq").val();  
        var empenho1        = $("#e1").val();
        var empenho2        = $("#e2").val();
        var empenho3        = $("#e3").val();
        var empenho4        = $("#e4").val();
        var empenho5        = $("#e5").val();
        var empenho6        = $("#e6").val();
        var empenho7        = $("#e7").val();
        var empenho8        = $("#e8").val();
        var empenho9        = $("#e9").val();
        var empenho10       = $("#e10").val();
        var empenho11       = $("#e11").val();
        var empenho12       = $("#e12").val();
        var getlicitacao    = $("#getlicitacao").val();

        if(molidade == ''){ alert('O campo Modalidade não pode ser vazio'); return false; }
        if($("#numprocesso").val() == ''){ alert('O campo Nº Processo não pode ser vazio'); return false; }
        if(tipo_itemlote == ''){ alert('O campo Tipo Licitação não pode ser vazio'); return false; }
        if(valor_bloqueado == ''){ alert('O campo Valor Bloqueado não pode ser vazio'); return false; }
        if(objetivo == ''){ alert('O campo Objetivo não pode ser vazio'); return false; }
        if(ordernador == ''){ alert('O campo Ordenador não pode ser vazio'); return false; }
        /*if(numero_edital == ''){ alert('O campo Número do Edital não pode ser vazio'); return false; }
        if(numero_diario == ''){ alert('O campo Número do Diário não pode ser vazio'); return false; }*/
        /*if(dt_publicacao == ''){ alert('O campo Data de Publicação não pode ser vazio'); return false; }
        if(dt_limite == ''){ alert('O campo Data Limite não pode ser vazio'); return false; }*/
        if(arq == ''){ alert('O campo Arquivo Texto não pode ser vazio'); return false; }
        if(mesEmpenho == ''){ alert('O campo Mês Empenho não pode ser vazio'); return false; }
        if(anoEmpenho == ''){ alert('O campo Ano Empenho não pode ser vazio'); return false; }

        var dados = "molidade="+molidade+
                    "&numprocesso="+numprocesso+
                    "&tipo_itemlote="+tipo_itemlote+
                    "&tipolicitacao="+tipolicitacao+
                    "&tiponaturezalicitacao="+tiponaturezalicitacao+
                    "&tiporegimeexecucao="+tiporegimeexecucao+
                    "&tiponaturezaprocedimento="+tiponaturezaprocedimento+
                    "&valor_bloqueado="+valor_bloqueado+
                    "&objetivo="+objetivo+
                    "&razao="+razao+
                    "&ordernador="+ordernador+
                    "&numero_edital="+numero_edital+
                    "&numero_diario="+numero_diario+
                    "&dt_publicacao="+dt_publicacao+
                    "&dias="+dias+
                    "&dt_limite="+dt_limite+
                    "&mesEmpenho="+mesEmpenho+
                    "&anoEmpenho="+anoEmpenho+
                    "&arq="+arq+
                    "&empenho1="+empenho1+
                    "&empenho2="+empenho2+
                    "&empenho3="+empenho3+
                    "&empenho4="+empenho4+
                    "&empenho5="+empenho5+
                    "&empenho6="+empenho6+
                    "&empenho7="+empenho7+
                    "&empenho8="+empenho8+
                    "&empenho9="+empenho9+
                    "&empenho10="+empenho10+
                    "&empenho11="+empenho11+
                    "&empenho12="+empenho12+
                    "&getlicitacao="+getlicitacao;

        $.ajax({
            url: 'php/licitacao/ajax_licitacao.php',
            type: "post",
            data: dados+"&editarlicitacao=editarlicitacao",
            success: function(result){
                
                console.log(result);
                // return false;

                if(result == 1){
                    alert('Licitação editada com Sucesso!');
                    location.href="?p=licitacao&page=licitacao_cadastrar&licitacao="+numprocesso;
                }

                if(result == 2){
                    alert('Esta Licitação já está cadsatrada no sitema.');
                    return false;
                }

                if(result == 0){
                    alert('Ops, encontramos um erro ao editar esta solicitação!\nVerifique se todos os campos foram preenchidos corretamente OU se existe outra Licitação com o mesmo número.');
                    return false;
                }
            }
        });
    
    });

    $(document).on('click', ".excluir-licitacao", function(){
        // EXCLUIR LICITAÇÃO
        var licitacao = $(this).attr('data-licitacao');

        var c = confirm('Deseja deletar a licitação '+licitacao+' ?');
        
        if(c == true){

            $.ajax({
                url: 'php/licitacao/ajax_licitacao.php',
                type: 'POST',
                data: {licitacao: licitacao, excluirlicitacao: 'excluirlicitacao'},
                success: function(result){

                    /*console.log(result);
                    return false;*/

                    if(result == 1){
                        alert('Licitação '+licitacao+' deletado com sucesso!');
                        location.reload();
                    }else{
                        alert('Não conseguimos deletar sua Licitação, entre em contato com Administrador!');
                        location.reload();
                    }
                }
            });
        }
        
    });

    $("#dtpublic, #dtLimite").on('change', function(){

        var ini = $("#dtpublic").val();
        var fim = $("#dtLimite").val();

        var date1 = ini;
        var date2 = fim;
        
        date1 = date1.split("/");   
        date2 = date2.split("/");
        var sDate = new Date(date1[1]+"/"+date1[0]+"/"+date1[2]);
        var eDate = new Date(date2[1]+"/"+date2[0]+"/"+date2[2]);
        var daysApart = Math.abs(Math.round((sDate-eDate)/86400000));
        
        if(isNaN(daysApart)){
            // console.log('000');
            $("#dias").val('');
        }else{  
            // console.log(daysApart);
            $("#dias").val(daysApart);

        } 

    });

    $(document).on('click', "#inserir-dotacao", function(){

        if($("#c1").val() == '' || $("#c2").val() == '' || $("#c3").val() == '' || $("#c4").val() == '' || $("#c5").val() == '' || $("#c6").val() == '' || $("#c7").val() == '' || $("#c8").val() == '' || $("#c9").val() == '' || $("#c10").val() == ''){
            alert('Todos os campos devem ser preechidos.');
            return false;
        }

        var c1  = padding_right($("#c1").val(), '0', 1);
        var c2  = padding_right($("#c2").val(), '0', 1);
        var c3  = padding_right($("#c3").val(), '0', 2);
        var c4  = padding_right($("#c4").val(), '0', 2);
        var c5  = padding_right($("#c5").val(), '0', 6);
        var c6  = padding_right($("#c6").val(), '0', 10);
        var c7  = padding_right($("#c7").val(), '0', 7);
        var c8  = padding_right($("#c8").val(), '0', 3);
        var c9  = padding_right($("#c9").val(), '0', 2);
        var c10 = padding_right($("#c10").val(), '0', 4);
        var numprocesso = $("#numprocesso").val()+'-'+$("#anolicitacao").val();

        var dados = "c1="+c1+"&c2="+c2+"&c3="+c3+"&c4="+c4+"&c5="+c5+"&c6="+c6+"&c7="+c7+"&c8="+c8+"&c9="+c9+"&c10="+c10+"&numprocesso="+numprocesso+"&dotacao=dotacao";

        $.ajax({
            url: "php/licitacao/ajax_licitacao.php",
            type: "POST",
            data: dados,
            success: function(result){

                // console.log(result);

                if(result == 0){
                    alert('Erro ao Cadastrar Dotação!');
                }else{

                    $("#c1").val('');
                    $("#c2").val('');
                    $("#c3").val('');
                    $("#c4").val('');
                    $("#c5").val('');
                    $("#c6").val('');
                    $("#c7").val('');
                    $("#c8").val('');
                    $("#c9").val('');
                    $("#c10").val('');

                    $(".tbody-dotacao").html(result);

                    alert('Dotação inserida com Sucesso!');
                }
            }
        });
    });

    $(".buscarempenho").on('change', function(){
        var value = $(this).val();

        $.ajax({
            url: "php/licitacao/ajax_licitacao.php",
            type: "POST",
            data: "empenho="+value+"&anoexercicio="+$("#anoExercicio").val()+"&pesLic=pesLic",
            success: function(result){

                /*console.log(result);
                return false;*/

                $("tbody").html(result);

            }
        });
    });

$(document).on('click', ".remover-dotacao", function(){
        var id = $(this).attr('idDotacao');
        var licitacao = $(this).attr('data-licitacao');

        $.ajax({
            url: "php/licitacao/ajax_licitacao.php",
            type: "POST",
            data: "iddotacao="+id+"&numprocesso="+licitacao+"&deletardotacao=deletardotacao",
            success: function(result){

                if(result == 0){
                    alert('Erro ao Deletar Dotação!');
                }else{

                    $(".tbody-dotacao").html(result);

                    alert('Dotação deletada com Sucesso!');
                }
            }
        });
    });

});

$(document).on('click',".editar-dotacao", function(){
    // console.log('Editar');
    $("#inserir-dotacao").css('display', 'none');
    $("#salvar-dotacao").css('display', 'block');
    
    var idcotacao = $(this).attr('iddotacao');

    $("#cancelar-edicao").removeAttr('disabled');
    $("#candelar-edicao").attr('iddotacao', idcotacao);

    var c1  = $("tr#"+idcotacao).find('td:eq(0)').text();
    var c2  = $("tr#"+idcotacao).find('td:eq(1)').text();
    var c3  = $("tr#"+idcotacao).find('td:eq(2)').text();
    var c4  = $("tr#"+idcotacao).find('td:eq(3)').text();
    var c5  = $("tr#"+idcotacao).find('td:eq(4)').text();
    var c6  = $("tr#"+idcotacao).find('td:eq(5)').text();
    var c7  = $("tr#"+idcotacao).find('td:eq(6)').text();
    var c8  = $("tr#"+idcotacao).find('td:eq(7)').text();
    var c9  = $("tr#"+idcotacao).find('td:eq(8)').text();
    var c10 = $("tr#"+idcotacao).find('td:eq(9)').text();

    $("#c1").val(c1);
    $("#c2").val(c2);
    $("#c3").val(c3);
    $("#c4").val(c4);
    $("#c5").val(c5);
    $("#c6").val(c6);
    $("#c7").val(c7);
    $("#c8").val(c8);
    $("#c9").val(c9);
    $("#c10").val(c10);

    $("tr#"+idcotacao).css('display','none');

    $(".editar-dotacao").attr('disabled','disabled');
    $(".remover-dotacao").attr('disabled','disabled');

});