$(function(){

   $("#dqwuserPassword, #dqwuserPassword2").on("keyup", function(){
      var p1 = $("#dqwuserPassword").val();
      var p2 = $("#dqwuserPassword2").val();

      // VERIFICA SE OS DOIS CAMPOS INFORMADOS PELO USUARIO AO ALTERAR SENHA SÃO IGUAIS
      if(p1 && p2){
         if(p1 == p2){
            $(".btnSave").removeClass("disabled");
         }else{
            alert2("danger", "Different passwords.");
            $(".btnSave").addClass("disabled");
         }
      }else{
         $(".btnSave").addClass("disabled");
      }
   });

   $(".btnSave").on("click", function(){
      if(validarForm(".formUserProfile") == true){
         var s = $(".formUserProfile").serializeArray();
         $.ajax({
            url: "webservice.php?",
            data: {funcao: "CDQWUSER::savee", dados: {s: s} },
            type: 'POST',
            success: function(data){
               // console.log(data);
               if(data == 1){
                  $(this).closest('.modal-footer').find("button:eq(1)").click();
                  alert2("success", "Successfully.", 3);
                  location.reload();
               }else{
                  alert2("danger", "Fail.", 3);
               }
            }
         });
      }
   });

})