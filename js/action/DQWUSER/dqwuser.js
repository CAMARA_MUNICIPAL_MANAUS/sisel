$(function(){

	$(".btnAddUser").on("click", function(){
		$('.formInsertUser input[name="DQWUSER[PASSWORD]"]').addClass('required').attr("disabled", "true");
		$('.formInsertUser input[name="DQWUSER[PASSWORD2]"]').addClass('required').attr("disabled", "true");
		$('.formInsertUser input[name="DQWUSER[EMP_NAME]"]').attr("disabled", "true");
		$('.formInsertUser input[name="DQWUSER[EMAIL]"]').attr("disabled", "true");
		$('.formInsertUser input[name="ID"]').val("");
		$('.formInsertUser input').val("");
		$('.formInsertUser select option').removeAttr('selected');
		$('.formInsertUser select').attr("disabled", "true");
		$(".btnSaveInsertUser").addClass("disabled");
	});

	$(".btnSaveInsertUser").on("click", function(){
		// INSERIR USUARIO NO SISTEMA
		if(validarForm(".formInsertUser") == true){
			// $(".formInsertUser").submit();
			var s = $(".formInsertUser").serializeArray();
			$.ajax({
				url: "webservice.php?",
				data: {funcao: "CDQWUSER::savee", dados: {s: s} },
				type: 'POST',
				success: function(data){
					console.log(data);
				 	if(data == 1){
				 		location.reload();
				 	}else{
				 		alert2("danger", "Insert/Edit user error.");
				 	}
				}
			});
		}
	});

	$(".btnRemoveUser").on("click", function(){
		// REMOVE USUARIO
		var nome      = $(this).closest('tr').find("td:eq(1)").html();
		var matricula = $(this).closest('tr').find("td:eq(0)").html();

		// mensagem de confirmação
		var c = confirm("Want to delete user: "+nome);

		if(c == true){
			$.ajax({
				url: 'webservice.php?',
				data: {funcao: "CDQWUSER::remove", dados: {id: matricula} },
				type: 'POST',
				success: function(data){
				 	if(data == 1){
				 		location.reload();
				 	}else{
				 		alert2("danger", "Error deleting user.");
				 	}
				}
			});
		}
	});

	$(".btnEditUser").on("click", function(){

		$(".formInsertUser input").removeAttr('disabled');
		$(".formInsertUser select").removeAttr('disabled');

		$('.formInsertUser input[name="DQWUSER[EMP_NO]"]').val($(this).closest('tr').find("td:eq(0)").html());
		$('.formInsertUser input[name="DQWUSER[EMP_NAME]"]').val($(this).closest('tr').find("td:eq(1)").html());
		$('.formInsertUser input[name="DQWUSER[EMAIL]"]').val($(this).closest('tr').find("td:eq(2)").html());
		$('.formInsertUser select option[value='+$(this).attr("profile")+']').attr("selected", "selected");
		$('.formInsertUser input[name="ID"]').val($(this).attr("code"));
		
		$('.formInsertUser input[name="DQWUSER[PASSWORD]"]').removeClass('required');
		$('.formInsertUser input[name="DQWUSER[PASSWORD2]"]').removeClass('required');

		$(".btnSaveInsertUser").removeClass("disabled");
	});

	$(".formInsertUser .btnFindNo").on("click", function(){
		// AO CLIAR NA LUPA DO FORMULÁRIO O CÓDIGO VERIFICA SE O USUÁRIO JÁ ESTÁ CADASTRADO NO SISTEMA
		var no = $('.formInsertUser input[name="DQWUSER[EMP_NO]"]').val();

		if(no == ""){
			alert2("danger", "Enter the registration number.");
		}else{
			// PESQUISA NA TABELA DQWUSER PARA SABE SE O REGISTRO JÁ ESTÁ CADASTRADO NO SISTEMA
			$.post('webservice.php?', {funcao: "CDQWUSER::findNo", dados: {no: no} }, function(data){
				if(data == "[]"){
					// SE O USUARIO NAO ESTIVER NO SISTEMA, BUSCA O NOME DO FUNCIONARIO EM VPR01
					$.post('webservice.php?', {funcao: "CVPR01::findNo", dados: {no: no} }, function(data){
						var nome = jQuery.parseJSON(data);
						nome = nome[0].EMP_NAME;
						$(".formInsertUser input").removeAttr('disabled');
						$(".formInsertUser select").removeAttr('disabled');
						$('.formInsertUser input[name="DQWUSER[EMP_NAME]"]').val(nome);
					});	
				}else{
					if(data == "[]"){
						alert2("danger", "No found register.");
					}else{
						var nome = jQuery.parseJSON(data);
						alert2("warning", "This record is in the database as "+nome[0].EMP_NAME+".");
					}
				}
			});
		}
	});

	$('.formInsertUser input[name="DQWUSER[PASSWORD]"], .formInsertUser input[name="DQWUSER[PASSWORD2]"]').on("keyup", function(){
		var p1 = $('.formInsertUser input[name="DQWUSER[PASSWORD]"]').val();
		var p2 = $('.formInsertUser input[name="DQWUSER[PASSWORD2]"]').val();
		// console.log(p1+" - "+p2);

		// VERIFICA SE OS DOIS CAMPOS INFORMADOS PELO USUARIO AO ALTERAR SENHA SÃO IGUAIS
		if(p1 && p2){
			if(p1 == p2){
				$(".btnSaveInsertUser").removeClass("disabled");
			}else{
				// alert2("danger", "Different passwords.");
				$(".btnSaveInsertUser").addClass("disabled");
			}
		}else{
			$(".btnSaveInsertUser").addClass("disabled");
		}
	});
})