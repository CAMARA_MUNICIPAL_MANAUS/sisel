$(function(){

	$(document).on("click", ".btnNewObra", function(){
		$("form.formObras").find("input[type=text],input[type=date],input[type=hidden]").val("");
		$("form.formObras").find("select").val("").trigger("change");
	});

	$(document).on("click", ".btnSalvar", function(){
		if( validarForm(".formObras") ){
			let dados = $(".formObras").serializeArray();

			setTimeout(function() {
				showLoading("");
				$.ajax({
					url: "webservice.php?",
					data: {funcao: "CObras::savee", dados: {d: dados} },
					type: "post",
					success: function(data){
						if(data == 1){
							alert2("success", "Inserido com sucesso!", 5);
							setTimeout(function() {
								location.reload();
							}, 1000);
						}else{
							hideLoading();
							alert2("danger", "Falha ao salvar obra!", 5);
						}
					}
				});
			}, 100);
		}
	});

	$(document).on("click", ".btnRemoverObra", function(){
		let id = $(this).closest('td').data("id");
		let tr = $(this).closest('tr');
		let c = confirm("Deseja remover obra ?");
		if(c){
			$.ajax({
				url: "webservice.php?",
				data: {funcao: "CObras::remove", dados: {d: id} },
				type: "post",
				success: function(data){
					if(data == 1){
						alert2("success", "Removido com sucesso!", 5);
						$(tr).remove();
					}else{
						hideLoading();
						alert2("danger", "Falha ao remover obra!", 5);
					}
				}
			});
		}
	});

	$("#obrasNumero_convenio").on("change", function(){
		if( $(this).val() != "" ){
			let v = $(this).val();
			let ano = $(this).find("option[value='"+v+"']").data("ano");
			let cnpj = $(this).find("option[value='"+v+"']").data("convenente");

			$("#obrasAno_convenio").val(ano);
			$("#obrasCnpj_concedente").val(cnpj);
		}else{
			$("#obrasAno_convenio").val("");
			$("#obrasCnpj_concedente").val("");
		}
	});

	/*$("#obrasContrato").on("change", function(){

		if( $(this).val() != "" ){
			let v = $(this).val();
			let licitacao = $(this).find("option[value='"+v+"']").data("licitacao");
			$("#obrasLicitacao").val(licitacao);
		}else{
			$("#obrasLicitacao").val("");
		}
	});*/

	$(document).on("focusout", "#obrasValor_estimado, #obrasCusto_obra", function(){
		let contrato = $("#obrasContrato").val();
		// let custo = $("#obrasCusto_obra").val();
		let custo = parseFloat(replaceAll(replaceAll($("#obrasCusto_obra").val(), ".", ""), ",", "."));
		let valor = parseFloat(replaceAll(replaceAll($("#obrasValor_estimado").val(), ".", ""), ",", "."));

		if (isNaN(custo)){ custo = 0; }
		if (isNaN(valor)){ valor = 0; }

		if(contrato == "" && (custo > 0 || valor > 0)){
			$("#obrasCusto_obra").val("");
			$("#obrasValor_estimado").val("");
			alert2("warning", "Selecione primeiro o Nº do Contrato.");
		}else{

			let valor_limit = parseFloat($("#obrasContrato").find("option[value='"+contrato+"']").data("valor"));
			
			if(valor > valor_limit){
				$("#obrasValor_estimado").val("");
				alert2("warning", "O valor da obra não pode ser maior que o valor estabelecido em contrato <b>R$ "+numberToReal(valor_limit)+"</b>");
			}

			if(custo > 100){
				$("#obrasCusto_obra").val("");
				alert2("warning", "O custo da obra não pode ser maior que 100%", 6);
			}
		}
	});

	$(document).on("click", ".btnEditarObra", function(){
		let id = $(this).closest('td').data("id");
		$("form.formObras").find("input[name=codigo]").val(id);

		$.ajax({
			url: "webservice.php?",
			data: {funcao: "CObras::obras", dados: {d: id}},
			type: "post",
			success: function(data){
				if(data != "[]"){
					let d = $.parseJSON(data);
					$("#obrasNome_publico").val(d[0].nome_publico);
					$("#obrasContrato").val(d[0].contrato).trigger("change");
					$("#obrasLicitacao").val(d[0].licitacao).trigger("change");
					$("#obrasArt").val(d[0].art);
					$("#obrasTipoobra").val(d[0].tipoobra).trigger("change");
					$("#obrasNaturezaobra").val(d[0].naturezaobra).trigger("change");
					$("#obrasSituacao_obra").val(d[0].situacao_obra).trigger("change");
					$("#obrasOrigem_recurso").val(d[0].origem_recurso).trigger("change");
					$("#obrasCusto_obra").val(d[0].custo_obra);
					$("#obrasValor_estimado").val(d[0].valor_estimado);
					$("#obrasNumero_convenio").val(d[0].numero_convenio).trigger("change");
					$("#obrasAno_convenio").val(d[0].ano_convenio);
					$("#obrasCnpj_concedente").val(d[0].cnpj_concedente);
					$("#obrasEndereco").val(d[0].endereco);
					$("#obrasBairro").val(d[0].bairro);
					$("#obrasMunicipio").val(d[0].municipio);
					$("#obrasLatitude").val(d[0].latitude);
					$("#obrasLongitude").val(d[0].longitude);
					$("#obrasGeoreferenciado").val(d[0].georeferenciado);
					$("#obrasResponsavel_nome_1").val(d[0].responsavel_nome_1);
					$("#obrasResponsavel_registro_1").val(d[0].responsavel_registro_1);
					$("#obrasResponsavel_nome_2").val(d[0].responsavel_nome_2);
					$("#obrasResponsavel_registro_2").val(d[0].responsavel_registro_2);
					$("#obrasResponsavel_nome_3").val(d[0].responsavel_nome_3);
					$("#obrasResponsavel_registro_3").val(d[0].responsavel_registro_3);
					$("#obrasResponsavel_nome_4").val(d[0].responsavel_nome_4);
					$("#obrasResponsavel_registro_4").val(d[0].responsavel_registro_4);
					$("#obrasResponsavel_nome_5").val(d[0].responsavel_nome_5);
					$("#obrasResponsavel_registro_5").val(d[0].responsavel_registro_5);
					$("#obrasMesempenho").val(pad(d[0].mesempenho,2));
					$("#obrasAnoempenho").val(d[0].anoempenho);

					$("#modalObra").modal("show");
					/*alert2("success", "Inserido com sucesso!", 5);
					setTimeout(function() {
						location.reload();
					}, 1000);*/
				}else{
					hideLoading();
					alert2("danger", "Falha ao carregar dados!", 5);
				}
			}
		});

	});

	/* MEDIÇÃO */
	/* MEDIÇÃO */
	/* MEDIÇÃO */

	$(".btnMedicao").on("click", function(){
		let obra = $(this).closest('tr').find("td:eq(3)").text();
		let codigo = $(this).closest('td').data("id");
		$("#modalObraMedicao .modal-title").text(obra.toUpperCase());
		$("#modalObraMedicao").find("input[name='obras_medicao[obra_fk]']").val(codigo);
		listObraMedicao(codigo);
		$("#modalObraMedicao").modal("show");
	});

	$(document).on("click", ".btnAddMed", function(){
		if( validarForm(".formObrasMedicao") ){
			let dados = $(".formObrasMedicao").serializeArray();
			
			setTimeout(function() {
				showLoading("");
				$.ajax({
					url: "webservice.php?",
					data: {funcao: "CObras_medicao::savee", dados: {d: dados} },
					type: "post",
					success: function(data){
						// console.log(data);
						if(data == 1){
							alert2("success", "Inserido com sucesso!", 5);
							limpar();
							listObraMedicao($("input[name='obras_medicao[obra_fk]']").val());
						}else{
							alert2("danger", "Falha ao salvar obra!");
						}
						hideLoading();
					}
				});
			}, 100);
		}
	});

	$(document).on("click", ".btnEditarMed", function(){
		var tr = $(this).closest("tr");

		let codigo         = $(tr).data("id");
		let medicao        = $(tr).find("td:eq(0)").data("medicao");
		let situacao       = $(tr).find("td:eq(1)").data("situacao");
		let nota           = $(tr).find("td:eq(2)").data("nota");
		let inicio_medicao = $(tr).find("td:eq(3)").text();
		let final_medicao  = $(tr).find("td:eq(4)").text();
		let data_medicao   = $(tr).find("td:eq(5)").text();
		let valor_medicao  = $(tr).find("td:eq(6)").text();
		let observacao     = $(tr).find("td:eq(7)").text();
		let anoempenho     = $(tr).find("td:eq(8)").text().substr(0, 4);
		let mesempenho     = $(tr).find("td:eq(8)").text().substr(4);

		$("select[name='obras_medicao[tipo_medicao]']").val("0"+medicao).trigger('change');
		$("select[name='obras_medicao[situacao_obra]']").val(situacao).trigger('change');
		$("select[name='obras_medicao[nota_fiscal]']").val(nota).trigger('change');
		$("input[name='obras_medicao[inicio_medicao]']").val(inicio_medicao);
		$("input[name='obras_medicao[final_medicao]']").val(final_medicao);
		$("input[name='obras_medicao[data_medicao]']").val(data_medicao);
		$("input[name='obras_medicao[valor_medicao]']").val(numberToReal(parseFloat(valor_medicao),""));
		$("input[name='obras_medicao[observacao]']").val(observacao);
		$("select[name='obras_medicao[anoempenho]']").val(anoempenho);
		$("select[name='obras_medicao[mesempenho]']").val(pad(mesempenho,2));
		$("input[name='codigo']").val(codigo);

		$(tr).remove();

	});

	$(document).on("click", ".btnRemoverMed", function(){
		var tr = $(this).closest("tr");
		var c = confirm("Deseja remover medição ?");
		var codigo = $(this).closest('tr').data("id");
		if(c){
			$.ajax({
				url: "webservice.php?",
				data: {funcao: "CObras_medicao::excluirr", dados: {d: codigo} },
				type: "post",
				success: function(data){
					if(data == 1){
						$(tr).remove();
						listObraMedicao($("input[name='obras_medicao[obra_fk]']").val());
						alert2("success", "Removido com sucesso!", 4);
					}else{
						alert2("danger", "Falha ao remover medição!", 4);
					}
				}
			});
		}

	});

})

function listObraMedicao(obra){
	$.ajax({
		url: "webservice.php?",
		data: {funcao: "CObras_medicao::medicao", dados: {d: obra} },
		type: "post",
		success: function(data){
			if(data != "[]"){
				let tr = "";
				let d = $.parseJSON(data);
				$(d).each(function(){
					var tipo_medicao = $("select[name='obras_medicao[tipo_medicao]'] option[value='0"+$(this)[0].tipo_medicao+"']").text();
					var situacao_obra = $("select[name='obras_medicao[situacao_obra]'] option[value='"+$(this)[0].situacao_obra+"']").text();

					tr += `<tr data-id='${$(this)[0].id}'>
								<td data-medicao='${$(this)[0].tipo_medicao}'>${tipo_medicao}</td>
								<td data-situacao='${$(this)[0].situacao_obra}'>${situacao_obra}</td>
								<td data-nota='${$(this)[0].idnf}'>${$(this)[0].nf}</td>
								<td>${$(this)[0].inicio_medicao}</td>
								<td>${$(this)[0].final_medicao}</td>
								<td>${$(this)[0].data_medicao}</td>
								<td>${$(this)[0].valor_medicao}</td>
								<td>${$(this)[0].observacao}</td>
								<td>${$(this)[0].anoempenho}${pad($(this)[0].mesempenho,2)}</td>
								<td class='text-right'>
									<button type='button' class='btn btn-warning btn-circle btnEditarMed' title='Editar'><i class='fa fa-pencil'></i></button>
									<button type='button' class='btn btn-danger btn-circle btnRemoverMed' title='Remover'><i class='fa fa-remove'></i></button>
								</td>
							</tr>`;
				});

				$("#modalObraMedicao").find("tbody").html(tr);
			}else{
				$("#modalObraMedicao").find("tbody").html("NENHUM REGISTRO ENCONTRADO.");
			}
		}
	});
}

function limpar(){
	$("select[name='obras_medicao[tipo_medicao]']").val("").trigger('change');
	$("select[name='obras_medicao[situacao_obra]']").val("").trigger('change');
	$("select[name='obras_medicao[nota_fiscal]']").val("").trigger('change');
	$("input[name='obras_medicao[inicio_medicao]']").val("");
	$("input[name='obras_medicao[final_medicao]']").val("");
	$("input[name='obras_medicao[data_medicao]']").val("");
	$("input[name='obras_medicao[valor_medicao]']").val("");
	$("input[name='obras_medicao[observacao]']").val("");
	$("select[name='obras_medicao[anoempenho]']").val("");
	$("select[name='obras_medicao[mesempenho]']").val("");
	$("input[name='codigo']").val("");
}