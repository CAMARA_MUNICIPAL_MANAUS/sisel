$(function(){

	if($("#tipopessoa").val() != ""){

		var value = $("#tipopessoa").val();

		if(value == 1){ /* FISICO */
			$(".cpf").show();
			$(".cnpj").val('');
			$(".nomeFantasia").attr('readonly','readonly');
		}

		if(value == 2){ /* JURIDICO */
			$(".cnpj").show();
			$(".cpf").val('');
			$(".nomeFantasia").removeAttr('readonly');
		}
	}



	$("#tipopessoa").on('change', function(){
		var value = $(this).val();

		if(value == 1){ /* FISICO */
			CpfCnpjPontoInicial();
			$(".cpf").show();
			$(".nomeFantasia").attr('readonly','readonly');
			$(".nomeFantasia").val('');
		}

		if(value == 2){ /* JURIDICO */
			CpfCnpjPontoInicial();
			$(".cnpj").show();
			$(".nomeFantasia").removeAttr('readonly');
		}

		if(value == ""){
			CpfCnpjPontoInicial();
		}
	});

	$(".razaosocial").on('keyup', function(){
		if($("#tipopessoa").val() == 1){
			$(".nomeFantasia").val($(this).val());
		}
	});

	$(".cpfcnpj").on('change', function(){
		var value = $(this).val();

		$.ajax({
			url: 'php/fornecedores/ajax_fornecedores.php',
			type: 'POST',
			data: {cpfcnpj: value, buscarCpfCnpj: 'true'},
			success: function(data){
				console.log(data)
				if(data > 0){
					alert('CPF/CNPJ já cadastrado no Sistema.');
				}
			}
		});

	});

/* ######################################################################################################### */
/* ######################################################################################################### */

	$(".btn-inserir, .btn-salvar").on('click', function(){

		var value = $(this).val();
		var alertt = $(this).attr('data-alert');

		if($("#tipopessoa").val() == ""){
			alert('O campo Pessoa é obrigatório!');
			return false;
		}

		if($(".razaosocial").val() == ""){
			alert('O campo Razão Social é obrigatório!');
			return false;
		}

		if( ($(".cpf").val() == "") && ($(".cnpj").val() == "") ){
			alert('O campo CPF/CNPJ é obrigatório!');
			return false;
		}

		$.ajax({
			url: 'php/fornecedores/ajax_fornecedores.php',
			type: 'POST',
			data: $("form").serialize()+"&acao="+value,
			success: function(data){

				// console.log(data);

				if(data == 1){
					alert("Fornecedor "+alertt+" com sucesso.");
					location.href="?p=fornecedores&page=fornecedores";
				}else{
					alert('Falha ao inserir fornecedor, verifique se os campos foram preenchidos corretamente.');
				}
			}
		});
	});

	$(".btn-excluir").on('click', function(){
        var forname = $(this).closest('tr').find("td:eq(1)").text();
        var c = confirm("Deseja realmente deletar o fornecedor "+forname+"? \nTodos os contratos, certidões e cotações associados a ele serão excluídos.");

        if(c == true){
            
            var id = $(this).attr('data-codigo');

            $.ajax({
                url: 'php/fornecedores/ajax_fornecedores.php',
                type: 'POST',
                data: {codigo: id, acao: 'excluir'},
                success: function(data){

                   	// console.log(data);
                    if(data == 1){
                        alert('Item Excluído com sucesso!');
                        location.reload();
                    }else{
                        alert2('danger', 'Erro ao deletar item. Entre em contato com o Administrador!', 8);
                    }
                }
            }); // fim ajax
        }
     
    });
})

function CpfCnpjPontoInicial(){
	$(".cpfcnpj").hide();
	$(".cpfcnpj").val('');
}