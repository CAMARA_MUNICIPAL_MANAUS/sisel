$(function(){
	$(".state-menu").on("click", function(){
		var state = $(this).attr("data-value");
		var id = $(this).closest('tr').attr("code");
		var c = confirm("Do you want change the menu state ?");
		
		if(c == true){
			$.ajax({
				url: "webservice.php?",
				data: {funcao: "CDQWMENU::changeStatus", dados: { d: {state: state, id: id}} },
				type: "post",
				success: function(data){
					console.log(data);
					if(data == 1){
						alert2("success", 'Successfully');
					}else{
						alert2("danger", 'Fail');
					}
				}
			});
		} //fim if
	});

	$(".liMenu > input:checkbox").on("click", function(){
		let li = $(this).closest('li');

		if( $(li).find("li").length > 0 ){
			if($(this).is(":checked")){
				$(li).find("input").prop('checked', true);
			}else{
				$(li).find("input").prop('checked', false);
			}
		}
	});
})