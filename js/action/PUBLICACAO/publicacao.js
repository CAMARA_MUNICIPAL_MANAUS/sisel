$(function(){

	$(".novo").on('click', function(){
		$("#licitacao").val('').trigger("change");
		$("#dtpublicacao").val('');
		$("#veiculo").val('');
		$(".cadastrar").html('Cadastrar');
		$(".cadastrar").attr('data-id', '');
	});


	$(".visualizar").on('click', function(){
		var tr = $(this).closest('tr');
		var id = $(this).attr('id');

		var licitacao     = $(tr).find("td:eq(1)").data('licitacao');
		var dt_publicacao = $(tr).find("td:eq(2)").data('dt_publicacao');
		var veiculo       = $(tr).find("td:eq(3)").data('veiculo');

		$("#licitacao").val(licitacao).trigger("change");
		$("#dtpublicacao").val(dt_publicacao);
		$("#veiculo").val(veiculo);
		$(".cadastrar").html("Editar");
		$(".cadastrar").data('id',id);

		$("#myModalPublicacao").modal("show");
	});

	$(".cadastrar").on('click', function(){
		// ESTA FUNÇÃO SERVE PARA CADASTRAR E EDITAR
		var licitacao 	 = $("#licitacao").find(':selected').val();
		var dtpublicacao = $("#dtpublicacao").val().split('-');
		var veiculo 	 = $("#veiculo").val();
		var idpub 		 = $(".cadastrar").data('id');

		if(licitacao == ''){
			alert('Selecione uma Licitação');
			return false;
		}

		if(dtpublicacao == ''){
			alert('Prencha o campo Data de Publicação');
			return false;
		}

		if(veiculo == ''){
			alert('Prencha o campo Veículo de Comunicação');
			return false;
		}


		$.ajax({
			url: 'php/publicacao/publicacao_cadastrar.php',
			type: 'POST',
			data: "licitacao="+licitacao+"&dtpublicacao="+dtpublicacao+"&idpub="+idpub+"&veiculo="+veiculo+"&"+$(this).html()+"=acao",
			success: function(result){

				if(result == 1){
					alert('Efetuado com sucesso!');
					location.reload();
				}else{
					alert('Falha ao executar a ação');
					location.reload();
				}
			}
		});
		
	});

	$(".excluir").on('click', function(){

		var licitacao = $(this).closest('tr').find('td:eq(1)').text();

		var excluir = confirm("Deseja Excluir esta Publicação da Licitação "+licitacao+"?");
		
		if(excluir == true){
			var idpub = $(this).attr('data-id');

			$.ajax({
				url: 'php/publicacao/publicacao_cadastrar.php',
				type: 'POST',
				data: "publicacao="+idpub+"&excluir=excluir",
				success: function(result){
					
					if(result == 1){
						alert('Publicacao Excluída com Sucesso');
						location.reload();
					}else{
						alert('Falha ao excluir');
						location.reload();
					}
				} // fim success
			}); // fim ajax
		} // fim if
	});
})