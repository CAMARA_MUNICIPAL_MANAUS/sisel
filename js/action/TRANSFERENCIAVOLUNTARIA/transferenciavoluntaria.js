$(function(){

	if($("input[name=ntransferenciavoluntariasuperior]").val() != ""){
		carregarEmpenho();
	}

	$("#tipotransferenciavoluntaria").on('change', function(){
		var option = $(this).find(":selected").attr("sigla");
		// console.log(option);

		$("input[name=ntransferenciavoluntaria]").val(option);
	});

	$(".btn-inserir").on('click', function(){

		var datainicio  = $("input[name=inicio-convenentetv]").val();
		var cnae 		= $("#tipocnae").val();
		var tipoesfera 	= $("#tipoesfera").val();
		var ntransf 	= $("input[name=ntransferenciavoluntaria]").val();

		if(datainicio == ""){
			setFlash("O campo Inicio da Atividade não pode ser vazio.");
			return false;
		}

		if(cnae == ""){
			setFlash("O campo CNAE não pode ser vazio.");
			return false;
		}

		if(tipoesfera == ""){
			setFlash("O campo Esfera não pode ser vazio.");
			return false;
		}

		if(validarForm("#cadastrar_transferenciavoluntaria") == true){

			var serial = $("#cadastrar_transferenciavoluntaria").serialize()+"&acao=inserir";

			var serial2 = "ntransf="+ntransf+"&data="+datainicio+"&cnae="+cnae+"&esfera="+tipoesfera+"&acao=inserirConvenenteTv";

			$.ajax({
				url: 'php/transferenciavoluntaria/ajax_transferenciavoluntaria.php',
				type: 'POST',
				data: serial,
				success: function(data){
					if(data > 0){

						$.ajax({
							url: 'php/transferenciavoluntaria/ajax_transferenciavoluntaria.php',
							type: 'POST',
							data: serial2,
							success: function(data){
								
							}
						});
						alert("Cdastro Efetuado com Sucesso! n Continue para cadastrar o empenho.");
						location.href="?p=transferenciavoluntaria&page=transferenciavoluntaria_cadastrar&transv="+ntransf;
					}else{
						alert("Falha no Cadastro, tente novamente mais tarde ou entre em contato com o Administrador!");
					}
				}
			});
		} // fim if
	});

	$(".btn-alterar").on('click', function(){

		var datainicio  = $("input[name=inicio-convenentetv]").val();
		var cnae 		= $("#tipocnae").val();
		var tipoesfera 	= $("#tipoesfera").val();
		var ntransf 	= $("input[name=ntransferenciavoluntaria]").val();

		if(datainicio == ""){
			setFlash("O campo Inicio da Atividade não pode ser vazio.");
			return false;
		}

		if(cnae == ""){
			setFlash("O campo CNAE não pode ser vazio.");
			return false;
		}

		if(tipoesfera == ""){
			setFlash("O campo Esfera não pode ser vazio.");
			return false;
		}

		if(validarForm("#cadastrar_transferenciavoluntaria") == true){

			var serial = $("#cadastrar_transferenciavoluntaria").serialize()+"&acao=editar&codigo="+$(this).attr('codigo');

			var serial2 = "codigo="+$(this).attr('codigo')+"ntransf="+ntransf+"&data="+datainicio+"&cnae="+cnae+"&esfera="+tipoesfera+"&acao=editarConvenenteTv";

			$.ajax({
				url: 'php/transferenciavoluntaria/ajax_transferenciavoluntaria.php',
				type: 'POST',
				data: serial,
				success: function(data){

					if(data != 0){

						$.ajax({
							url: 'php/transferenciavoluntaria/ajax_transferenciavoluntaria.php',
							type: 'POST',
							data: serial2,
							success: function(data){
							}
						});

						alert("Alteração Efetuada com Sucesso!");
						location.href="?p=transferenciavoluntaria&page=transferenciavoluntaria_cadastrar&transv="+data;
					}else{
						alert2("danger", "Falha na Alteração, tente novamente mais tarde ou entre em contato com o Administrador!");
					}
				}
			});

		} // fim if
		
	});

	$(document).on('click', ".btn-inserir-empenho, .btn-alterar-empenho", function(){

		if(validarForm("#formempenho") == true){
			var serial = $("#formempenho").serialize()+"&acao="+$(this).attr('acao')+"&codigo="+$(this).attr('codigo')+"&pk="+$(this).attr("pk");

			$.ajax({
				url: 'php/transferenciavoluntaria/ajax_transferenciavoluntaria.php',
				type: 'POST',
				data: serial,
				success: function(data){
					if(data == 1){
						alert("Ação efetuada com Suceso!");
						carregarEmpenho();
						limparForm("formempenho");
					}else{
						console.log(data);
					}
				}
			});
		} // fim if
	});

	$(document).on('click', ".btn-editar-empenho", function(){
		$(".btn-alterar-empenho, .btn-inserir-empenho").html("Alterar");
		$(".btn-alterar-empenho, .btn-inserir-empenho").attr("acao", "editarEmpenho");
		$(".btn-alterar-empenho, .btn-inserir-empenho").attr("pk", $(this).attr('codigo'));
		$(".btn-alterar-empenho, .btn-inserir-empenho").attr("class", "btn btn-success btn-alterar-empenho");
		$(".btn-alterar-empenho, .btn-cancelar-empenho").removeAttr('disabled');

		var dados = "codigo="+$(this).attr('codigo')+"&acao=buscarEmpenho";

		$.ajax({
			url: 'php/transferenciavoluntaria/ajax_transferenciavoluntaria.php',
			type: 'POST',
			data: dados,
			success: function(data){
				var ln = $.parseJSON(data);

				// console.log(ln[0]);

				$("input[name=notaempenho]").val(ln[0].notaempenho);
				$("input[name=anoempenho]").val(ln[0].anoempenho);
				$("input[name=anotransferencia]").val(ln[0].anotransferencia);
				$("input[name=cnpjconcedente]").val(ln[0].cnpjconcedente);

				$("#tipoparticipacaotransferenciavol option[value="+ln[0].tipoparticipacaotransferencia_pk+"]").attr("selected","selected");
			}
		});
	});

	$(document).on('click', ".btn-cancelar-empenho", function(){
		$(".btn-alterar-empenho").html("Inserir");
		$(".btn-alterar-empenho").attr("acao", "inserirEmpenho");
		$(".btn-alterar-empenho").attr("pk", '');
		$(".btn-alterar-empenho").attr("class", "btn btn-success btn-inserir-empenho");
		$(".btn-cancelar-empenho").attr('disabled','disabled');
		limparForm("formempenho");
	});

	$(document).on('click', ".btn-excluir-empenho", function(){

		var c = confirm("Deseja excluir empenho ?");

		if(c == true){

			var codigo = $(this).attr('codigo');

			var dados = "codigo="+codigo+"&acao=excluirEmpenho";
			$.ajax({
				url: 'php/transferenciavoluntaria/ajax_transferenciavoluntaria.php',
				type: 'POST',
				data: dados,
				success: function(data){
					console.log(data);
					alert("Excluído com Suceso!");
					carregarEmpenho();
				}
			});
		}
	});

	$(document).on('change', '#tipoparticipacaotransferenciavol', function(){
		
	});

	/* ##############  CONVENENTETV.REM  ######################### */

	$("input[name=cnpjconvenente]").on("change", function(){

		$("input[name=cnpj-convenentetv]").val($(this).val());

		var dados = "cnpj="+$(this).val()+"&acao=buscarConvenenteTv";
		$.ajax({
			url: 'php/transferenciavoluntaria/ajax_transferenciavoluntaria.php',
			type: 'POST',
			data: dados,
			success: function(data){
				var data = $.parseJSON(data);
				if(data != ""){
					if(data[0].nome_fantasia == "" || data[0].razao_social == ""){
						alert("Por favor antes de continuar, verifique se o Nome Fantasia ou Razão Social está vazio na opção Configurações > Fornecedores.\nEssa informação servirá para o envio do arquivo CONVENENTETV.REM");
						$(".btn-alterar").attr("disabled","disabled");
	 					$(".btn-inserir").attr("disabled","disabled");
					}else{
						$("input[name=razaosocial-convenentetv]").val(data[0].razao_social);
						$("input[name=nomefantaia-convenentetv]").val(data[0].nome_fantasia);
						$(".btn-alterar").removeAttr('disabled');
	 					$(".btn-inserir").removeAttr('disabled');
					}
				}else{
					alert("Por favor antes de continuar, verifique se o Nome Fantasia ou Razão Social está vazio na opção Configurações > Fornecedores.\nEssa informação servirá para o envio do arquivo CONVENENTETV.REM");
					$(".btn-alterar").attr("disabled","disabled");
 					$(".btn-inserir").attr("disabled","disabled");
				}
			}
		});

	});


	//CERTIDAO
	$(document).on('click', ".btn-inserir-certidao, .btn-alterar-certidao", function(){
		
		if(validarForm("#formcertidao") == true){

			var serial = $("#formcertidao").serialize()+"&acao="+$(this).attr('acao')+"&codigo="+$(this).attr('codigo')+"&pk="+$(this).attr("pk");

			$.ajax({
				url: 'php/transferenciavoluntaria/ajax_transferenciavoluntaria.php',
				type: 'POST',
				data: serial,
				success: function(data){
					if(data == 1){
						alert("Ação efetuada com Sucesso!");
						limparForm("formcertidao");
						$(".btn-cancelar-certidao").click();
						carregarCertidao();
					}else{
						//console.log(data);
					}
				}
			});
		} // fim if
	});

	$(document).on('click', ".btn-editar-certidao", function(){
		$(".btn-alterar-certidao, .btn-inserir-certidao").html("Alterar");
		$(".btn-alterar-certidao, .btn-inserir-certidao").attr("acao", "editarCertidao");
		$(".btn-alterar-certidao, .btn-inserir-certidao").attr("pk", $(this).attr('codigo'));
		$(".btn-alterar-certidao, .btn-inserir-certidao").attr("class", "btn btn-success btn-alterar-certidao");
		$(".btn-alterar-certidao, .btn-cancelar-certidao").removeAttr('disabled');

		var dados = "codigo="+$(this).attr('codigo')+"&acao=buscarCertidao";

		$.ajax({
			url: 'php/transferenciavoluntaria/ajax_transferenciavoluntaria.php',
			type: 'POST',
			data: dados,
			success: function(data){
				var ln = $.parseJSON(data);
				// console.log(ln);
				
				$("input[name=numero_certidao]").val(ln[0].numero_certidao);
				$("input[name=data_certidao]").val(ln[0].data_certidao);
				$("input[name=data_validade]").val(ln[0].data_validade);

				$("#tipocertidao option[value="+ln[0].tipocertidao_fk+"]").attr("selected","selected");

			}
		});
	});

	$(document).on('click', ".btn-cancelar-certidao", function(){
		$(".btn-alterar-certidao").html("Inserir");
		$(".btn-alterar-certidao").attr("acao", "inserirCertidao");
		$(".btn-alterar-certidao").attr("pk", '');
		$(".btn-alterar-certidao").attr("class", "btn btn-success btn-inserir-certidao");
		$(".btn-cancelar-certidao").attr('disabled','disabled');
		limparForm("formcertidao");
	});

	/* ##############  FIM - CONVENENTETV.REM  ######################### */

})

function carregarEmpenho(){
	$(".empenho").load("php/transferenciavoluntaria/_empenho.php?ntransferenciavoluntaria="+$("input[name=ntransferenciavoluntaria]").val());
}

function carregarCertidao(){
	$(".certidao").load("php/transferenciavoluntaria/_certidaotv.php?ntransferenciavoluntaria="+$("input[name=ntransferenciavoluntaria]").val());	
}

function limparForm(form){
	$("#"+form+" input[type=text]").val('');
	$("#"+form+" select").find("option:eq(0)").attr("selected","selected");
}