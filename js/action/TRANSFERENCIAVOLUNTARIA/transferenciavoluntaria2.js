$(function(){
	$(".excluir-transferencia").on('click', function(){
		var transf = $(this).closest('tr').find("td:eq(1)").text();
		var c = confirm("Deseja excluir transferência "+transf+" ?");

		if(c == true){
			var codigo = $(this).attr('data-licitacao');

			var dados = "codigo="+codigo+"&acao=excluirTransferencia";
			$.ajax({
				url: 'php/transferenciavoluntaria/ajax_transferenciavoluntaria.php',
				type: 'POST',
				data: dados,
				success: function(data){
					// console.log(data);
					alert("Excluído com Suceso!");
					location.reload();
				}
			});
		}
	});
})