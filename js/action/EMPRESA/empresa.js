$(function(){

	$(".btn-editar").on('click', function(){
		$(".formempresa input[type=text]").each(function() {
			$(this).removeAttr('readonly');
		});

		$(".cnpj").attr('readonly', 'readonly');

		$(".btn-editar").hide();
		$(".btn-salvar").show();
	});

	$(".btn-cancelar").on('click', function(){
		$(".formempresa input[type=text]").each(function() {
			$(this).attr('readonly', 'readonly');
		});

		$(".cnpj").attr('readonly', 'readonly');

		$(".btn-editar").show();
		$(".btn-salvar").hide();
	});

	$(".btn-salvar").on('click', function(){
		$(".formempresa").submit();
	});

})