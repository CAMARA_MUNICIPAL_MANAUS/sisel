$(function(){

	$('input').attr('autocomplete','off');

	//CLASSE PARA FORMATAÇÃO DE DINHEIRO AO DIGITAR
	$(".dinheiro").maskMoney({
		showSymbol: false, 	//deverá mostrar o símbolo monetário? (Padrão: false ou true)
		symbol:"R$ ", 		// Qual o símbolo? (US$, R$, etc) (Padrão: US$)
		decimal:",", 		// Qual sinal para decimal? ( Nos EUA é ponto(.) e no Brasil é vírgula(,) )
		thousands:".", 		// Qual sinal para casa de milhar? (Nos EUA é vírgula(,) e no Brasil é ponto(.))
		precision: 2, 		// Qual o nível de precisão? (Padrão: 2)
	});

	// CLASSE QUE ACEITA SOMENTE NÚMEROS
	$('.sonums').on('keypress', function(event) {
		var tecla = (window.event) ? event.keyCode : event.which;
		if ((tecla > 47 && tecla < 58)) return true;
		else {
			if (tecla != 8) return false;
			else return true;
		}
	});

	// MASCARAS
	$(".celular").mask("(92)99999-9999");
	$(".cpf").mask("999.999.999-99");
	$(".cep").mask("99.999-999");
	$(".rg").mask("999999-99");
	$(".cnpj").mask("99.999.999/9999-99");

})

	function padding_left(s, c, n) {

		// alert(padding_left('eureka', '*', 10));
		/* padding_right(c1, '0', 1); */

		if (/*! s ||*/ ! c || s.length >= n) {
			return s;
		}
		var max = (n - s.length)/c.length;
		for (var i = 0; i < max; i++) {
			s = c + s;
		}
		return s;
	}

	function padding_right(s, c, n) {

		//alert(padding_right('eureka', '*', 10));
		/* padding_right(c1, '0', 1); */

		if (/*! s ||*/ ! c || s.length >= n) {
			return s;
		}
		var max = (n - s.length)/c.length;
		for (var i = 0; i < max; i++) {
			s += c;
		}
		return s;
	}

	function convertStringToFloat(valor){

		var resultado = valor;

		resultado = resultado.replaceAll(".","",true);
		resultado = parseFloat(resultado.replace(",",".").trim());

		return resultado;
	}

	String.prototype.replaceAll = function(str1, str2, ignore){
	    return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
	}

	Number.prototype.formatMoney = function(c, d, t){

		// formatMoney("0",".",".")

		var n = this, 
		c = isNaN(c = Math.abs(c)) ? 2 : c, 
		d = d == undefined ? "." : d, 
		t = t == undefined ? "," : t, 
		s = n < 0 ? "-" : "", 
		i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
		j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};

	function errorFocus(element) {
	
		var tabPai = element.closest('.tab-pane').attr('id');
		$('a[href="#'+tabPai+'"]').trigger('click');
		element.css({
			border: '1px solid #ED3737',
		});
		element.focus();
	}