function wizard(eModal, eWizard, progress_status, progress_class){

	/*  eModal = id ou classe do elemento, exemplo: #modalTeste OU .modalTeste
	    eWizard = id ou classe do elemento, exemplo: #wizard1 OU .wizard1
	    progress_status = S,N
	    progress_class = primary, success, warning, danger

	    Chamando a função:
	    	wizard("#modalInspection", "#wizard", "S", "warning");

		Exemplo HTML:
			<div class="modal fade" id="modalInspection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				...
				<div class="modal-body">
					<div id="wizard">
						<div class="navbar">
		    				<ul class="nav nav-tabs" role="tablist">
		    					...
		    				</ul>
		    			</div>
            			<div class="tab-content">
            				...
						</div>
					</div>
				</div>
				<div class="modal-footer">
					...
				</div>
			</div>
	*/

	/* ADD STRIBUTE STEP */
	var contador_step = 1;
	$(eWizard+" li[role=presentation]").each(function(){
		$(this).find("a").attr("data-step", contador_step);
		contador_step++;
	});

	// TOTAL DE ABAS NO MENU
	var totalLi = $(eWizard+" li[role=presentation]").length;

	/* CRIANDO O PROGRESS BAR */
	if(progress_status == 'S'){
		var percent = (1/totalLi)*100;
		var htmlProgress = "<div class='progress'>";
			htmlProgress += "<div class='progress-bar progress-bar-"+progress_class+"' role='progressbar' aria-valuenow='1' aria-valuemin='1' aria-valuemax='"+totalLi+"' style='width: 20%;'>Step 1 of "+totalLi;
			htmlProgress += "</div></div>";
		$(eWizard).prepend(htmlProgress);
	}

	/* CRIANDO BOTÕES NO FOOTER */
	var htmlFooter = $(eModal).find(".modal-footer").html();

	var newHtmlFooter = "<div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left'>";
      		newHtmlFooter += "<button type='button' class='btn btn-default prev'><i class='fa fa-chevron-left'></i> Previous</button>";
      		newHtmlFooter += "<button type='button' class='btn btn-default next'>Next <i class='fa fa-chevron-right'></i></button>";
    	newHtmlFooter += "</div>";
    	newHtmlFooter += "<div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right'>";
    		newHtmlFooter += htmlFooter;
    	newHtmlFooter += "</div>";

	$(eModal).find(".modal-footer").html(newHtmlFooter);

	// CONTROLANDO BOTÕES DE CONTROLE DO WIZARD
	$(document).on("click", ".next", function(){
		// quando clica no voltar
		var total = $(eWizard+" li[role=presentation]").length;
		var active = $(eWizard+" li[role=presentation].active");
		var step = $(active).next().find("a").data("step");
		$(active).next().find("a").click();
		
		if(step == total){ $('.next').attr("disabled",""); }else{ $('.next').removeAttr("disabled"); }
		if(step == 1){ $('.prev').attr("disabled",""); }else{ $('.prev').removeAttr("disabled"); }
	});

	$(document).on("click", ".prev", function(){
		// quando clica no avançar
		var total = $(eWizard+" li[role=presentation]").length;
		var active = $(eWizard+" li[role=presentation].active");
		var step = $(active).prev().find("a").data("step");
		$(active).prev().find("a").click();
		
		if(step == total){ $('.next').attr("disabled",""); }else{ $('.next').removeAttr("disabled"); }
		if(step == 1){ $('.prev').attr("disabled",""); }else{ $('.prev').removeAttr("disabled"); }
	});

	// CONTROLANDO O PROGRESS BAR
	$(eModal).on('shown.bs.modal', function () {
		// quando o modal abre
		$(eWizard+" li[role=presentation]").find("a[data-step=1]").click();
		$('.prev').attr("disabled","");
	})

	$(eWizard+" a[data-toggle='tab']").on('shown.bs.tab', function (e) {
		// quando clica na aba
		var step = $(e.target).data('step');
		var percent = (parseInt(step) / $("li[role=presentation]").length) * 100;
		
		$(eWizard+" .progress-bar").css({width: percent + '%'});
  		$(eWizard+" .progress-bar").text("Step " + step + " of "+$("li[role=presentation]").length);

		if(step == 1){
			$('.prev').attr("disabled","");
			$('.next').removeAttr("disabled");
		}else if(step == $("li[role=presentation]").length){
			$('.prev').removeAttr("disabled");
			$('.next').attr("disabled","");
		}else{
			$('.next').removeAttr("disabled");
			$('.prev').removeAttr("disabled");
		}
	});
}