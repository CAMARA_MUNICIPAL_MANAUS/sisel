$(function(){
	$(".btnSign").on("click", function(){
		if(validarForm(".formLogin") == true){
			$(".formLogin").submit();
		}
		/*else{
			if($("input[type=text]").val() == ""){ $(".formLogin .divlogin").effect("shake", { times: 2 }, 500); }

			if($("input[type=password]").val() == ""){ $(".formLogin .divsenha").effect("shake", { times: 2 }, 500); }
		}*/
	});

	$(".formLogin").on("keypress", function(e){
		// var k = e.key;

		// if(k == "Enter"){
		if(e.keyCode == 13){
			$(".btnSign").trigger('click');
		}
	});
})

function validarForm(form){
	// VALIDA OS CAMPOS OBRIGATÓRIOS DE UM FORMULÁRIO
	var erro = 0;
	$(form+" .required").each(function(){
		// console.log("Valor: "+$(this).val());
		if($(this).val() == "" || $(this).val() == "0"){
			$(this).closest('div').addClass('has-error');
			erro++;
		}else{
			$(this).closest('div').removeClass('has-error');
		}
	});

	if(erro > 0){
		alert2("danger", "The highlighted fields are required!", 5);
		return false;
	}else{
		return true;
	}
}

function alert2(type, msg, time){

	localStorage.setItem(getDateActual().replace('/', '').replace('/', '').replace('/', '').replace(':', '').replace(':', '').replace(':', '').replace(' ', ''), msg);

	// ALERT COMO DIV NO CANTO DA TELA
	// SEGUE EXEMPLO ABAIXO DE COMO CHAMAR A FUNÇÃO:
	// TYPE: danger (vermelho), success (verde), warning (amarelo)

	if( typeof time !== 'undefined' ){
		time = (time*1000);
		$(".alert-msg-pai").append('<div class="alert alert-msg alert-dismissable alert-'+type+'" style="display: none"><div class="msg">'+msg+'</div></div>');

			$(".alert-msg").toggle("slow");
			setTimeout(function() {
				$(".alert-msg").toggle("slow");
				setTimeout(function() {
					$(".alert-msg-pai").html("");
				}, 1000);
			}, time);
	}else{
		// console.log("0");
		$(".alert-msg-pai").append('<div class="alert alert-msg alert-dismissable alert-'+type+'"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><div class="msg">'+msg+'</div></div>');
	}

}

function getDateActual(){
	var data 	= new Date();
	var dia     = data.getDate() < 10 ? "0"+data.getDate() : data.getDate();
	var mes     = data.getMonth() < 10 ? "0"+(data.getMonth()+1) : (data.getMonth()+1);
	var ano     = data.getFullYear();
	var hora    = data.getHours() > 12 ? data.getHours() - 12 : (data.getHours() < 10 ? "0" + data.getHours() : data.getHours());
	var minuto  = data.getMinutes() < 10 ? "0" + data.getMinutes() : data.getMinutes();
	var segundo = data.getSeconds() < 10 ? "0" + data.getSeconds() : data.getSeconds();

	var timestamp = dia+"/"+mes+"/"+ano+" "+hora+":"+minuto+":"+segundo;
	// console.log(dia+"/"+mes+"/"+ano+" "+hora+":"+minuto+":"+segundo);

	return timestamp;
}