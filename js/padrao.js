$(function(){

	// CHAMA FUNÇÃO PARA VERIFICAR O STATUS DA BARRA LATERAL
	checkSideBar(window.localStorage.getItem('menu'));

	// ABRIR O MODAL DE ABERTURA DE CHAMADOS PELA URL
	// FONTE: https://gist.github.com/MrDys/3512455
	if(window.location.href.indexOf('#bug') != -1) {
		$('#bug').modal('show');
	}

	// ACRESCENTA UM * EM TODAS AS LABEL ONDE O CAMPO FOR OBRIGATÓRIO COM A CLASSE REQUIRED 
	$(".required").each(function(){
		if($(this).attr("type") != "hidden"){
			var html = $(this).closest('div').find("label:eq(0)").text();
			$(this).closest('div').find("label:eq(0)").html("<span class='red'>*</span> "+html);
		}
	});

	// CLICA NO INPUT FILE QUE ESTÁ OCULTO
	$(document).on("click", ".file", function(){
		// UTILIZADO PARA O BOTÃO FILE
		var btnFile = $(this);
		var inputfile = $(this).next("input");
		inputfile.click();

		$(inputfile).on("change", function(){
			btnFile.html($(inputfile)[0].files[0].name);
		})

	});

	// FUNÇÃO PRA RECEBER SOMENTE NÚMEROS
	$(document).on('keypress', 'input[type=number], .onlynumber', function(e){
		var key = (window.event) ? event.keyCode : e.which;
		if((key > 47 && key < 58) || key == 46 || key == 44){
			return true;
		}else if(key == 13){
			return false;
		}else{
			alert2('danger', "Digite somente números.", 4);
		}
	});

	// FUNÇÃO PRA RECEBER SOMENTE LETRAS
	$(document).on('keypress', '.onlyletter', function(e){
		var key = (window.event) ? event.keyCode : e.which;
		if((key > 47 && key < 58) || key == 46 || key == 44){
			alert2('danger', "Digite somente letras.", 4);
			return false;
		}else{
			return true;
		}
	});

	// AÇÃO AO CLICA NO BOTÃO HIDE/SHOW DO SIDEBAR
	$(document).on("click", ".btnMenuHide", function(){
		// QUANDO CLICA NO BOTÃO PARA ESCONDER O MENU
		// FAZ VALIDAÇÃO PARA SABER SE A BARRA_LATERAL ESTÁ VISIVEL NA TELA OU NAO
		if($(".sidebar").is(':visible')){
			window.localStorage.setItem('menu', '0');
			// console.log("Set 0");
			$(".sidebar").hide('slide', {}, 400);
			$("#page-wrapper").animate({"margin": "0px 0px 0px 0px"}, 400 );
		}else{
			window.localStorage.setItem('menu', '1');
			// console.log("Set 1");
			$(".sidebar").show('slide', {}, 400);
			$("#page-wrapper").animate({"margin": "0px 0px 0px 250px"}, 400 );
			setTimeout(function() {
				$("#page-wrapper").removeAttr('style');
			}, 1000);
		}
	});

	// AÇÃO DO BOTÃO HIDE/SHOW SIDEBAR QUANDO O SISTEMA FOR ACESSADO POR UM DISPOTIVIO MÓVEL
   	$(".btnMenuMobile").on("click", function(){
   		$(".sidebarmobile").toggle( "slide", "", 200, "");
   	});

   	// AÇÃO DE MOSTRAR O MENU QUANDO DESLIZAR O DEDO
	$("html, #wrapper").bind("swipeone", function(event, obj) {
		var direction=obj.description.split(":")[2];
	   	if(direction=="left"){
	    	// console.log("left");
			$(".sidebarmobile").hide( "slide", "", 200, "");
		}else if(direction=="right"){
			$(".sidebarmobile").show( "slide", "", 200, "");

		}
    });
});

function validarForm(form){
	// VALIDA OS CAMPOS OBRIGATÓRIOS DE UM FORMULÁRIO
	var erro = 0;
	$(form+" .required").each(function(){
		// console.log("Valor: "+$(this).val());
		if($(this).val() == "" || $(this).val() == "0"){
			$(this).closest('div').addClass('has-error');
			erro++;
			alert2("danger", "O campo "+$(this).prev("label").html()+" é obrigatório!", 4);
			if(erro == 1){
				$(this).focus();
			}
		}else{
			$(this).closest('div').removeClass('has-error');
		}
	});

	if(erro > 0){ return false; }else{ return true; }
}

function validarFormTable(form){
	// Validação de campos em tabela
	var erro = 0;
	$(form+" .required").each(function(){
		// console.log("Valor: "+$(this).val());
		if($(this).val() == "" || $(this).val() == "0"){
			$(this).closest('td').addClass('has-error');
			erro++;
			alert2("danger", "O campo "+$(this).attr("data-name")+" é obrigatório!", 4);
			if(erro == 1){
				$(this).focus();
			}
		}else{
			$(this).closest('div').removeClass('has-error');
		}
	});

	if(erro > 0){ return false; }else{ return true; }
}

function alert2(type, msg, time){

	localStorage.setItem(getDateActual().replace('/', '').replace('/', '').replace('/', '').replace(':', '').replace(':', '').replace(':', '').replace(' ', ''), msg);

	// ALERT COMO DIV NO CANTO DA TELA
	// SEGUE EXEMPLO ABAIXO DE COMO CHAMAR A FUNÇÃO:
	// TYPE: danger (vermelho), success (verde), warning (amarelo)

	if( typeof time !== 'undefined' ){

		var idnwe = Math.floor(Math.random() * 256) + 1;

		time = (time*1000);
		$(".alert-msg-pai").append('<div class="alert alert-msg alert-dismissable alert-'+type+'" id="'+idnwe+'" style="display: none"><div class="msg">'+msg+'</div></div>');

			$(".alert-msg#"+idnwe).toggle("slow");
			setTimeout(function() {
				$(".alert-msg#"+idnwe).toggle("slow");
				setTimeout(function() {
					// $(".alert-msg-pai").html("");
					$(".alert-msg#"+idnwe).remove();
				}, 1000);
			}, time);
	}else{
		// console.log("0");
		$(".alert-msg-pai").append('<div class="alert alert-msg alert-dismissable alert-'+type+'"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><div class="msg">'+msg+'</div></div>');
	}

}

function getDateActual(){
	var data 	= new Date();
	var dia     = data.getDate() < 10 ? "0"+data.getDate() : data.getDate();
	var mes     = data.getMonth() < 10 ? "0"+(data.getMonth()+1) : (data.getMonth()+1).toString();
	var ano     = data.getFullYear();
	var hora    = data.getHours() > 12 ? data.getHours() - 12 : (data.getHours() < 10 ? "0" + data.getHours() : data.getHours());
	var minuto  = data.getMinutes() < 10 ? "0" + data.getMinutes() : data.getMinutes();
	var segundo = data.getSeconds() < 10 ? "0" + data.getSeconds() : data.getSeconds();

	var timestamp = dia+"/"+mes+"/"+ano+" "+hora+":"+minuto+":"+segundo;

	return timestamp;
}

function checkSideBar(statusMenu){
	if(statusMenu){
		// console.log(statusMenu);
		if(statusMenu == 1){
			window.localStorage.setItem('menu', '1');
		}else{
			$(".sidebar").hide();
			$("#page-wrapper").css("margin", "0px 0px 0px 0px");
			window.localStorage.setItem('menu', '0');
		}
	}else{
		// console.log("> ");
		// console.log(statusMenu);
		window.localStorage.setItem('menu', '1');
		$(".btnMenuHide").click();
	}
}

// LOADING //
function showLoading(msg){
	msg = (msg == "") ? "Carregando..." : msg;
	$(".loading").find("h2").text(msg);
	$(".loading").show();
}

function hideLoading(){
	$(".loading").hide();
}

function replaceAll(str, needle, replacement){
    return str.split(needle).join(replacement);
}

function nl2br (str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

function numberToReal(numero, prefix){
    var numero = numero.toFixed(2).split('.');
    numero[0] = prefix + numero[0].split(/(?=(?:...)*$)/).join('.');
    return numero.join(',');
}

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}