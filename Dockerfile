FROM ubuntu

#oracle client instalation
RUN apt-get update
RUN export DEBIAN_FRONTEND=noninteractive

RUN apt-get install -y tzdata
RUN ln -fs /usr/share/zoneinfo/America/Manaus /etc/localtime
RUN dpkg-reconfigure --frontend noninteractive tzdata
RUN apt-get install alien -y
RUN apt-get install -y wget 
RUN apt-get install libaio1
RUN apt-get install zip unzip
RUN apt-get install curl -y

#php / apache instalion
RUN apt-get update
RUN apt install build-essential -y
RUN apt install apache2 -y
RUN apt install php php-dev libapache2-mod-php -y
# RUN apt-get install php-pear -y
# RUN apt-get install php-bcmath -y
# RUN apt-get install php-bz2 -y
# RUN apt-get install php-intl -y
# RUN apt-get install php-gd -y
# RUN apt-get install php-mbstring -y
RUN apt-get install php-zip -y
RUN apt-get install php-ldap -y
# RUN apt-get install php-odbc -y
RUN apt-get install php-mysql -y
# RUN apt-get install php-pgsql -y
RUN apt-get install php-curl -y

#PHP 7.4.3
RUN wget https://github.com/php/php-src/archive/PHP-7.4.3.zip
RUN unzip PHP-7.4.3.zip

# Update the PHP.ini file, enable <? ?> tags and quieten logging.
RUN sed -i "s/short_open_tag = Off/short_open_tag = On/" /etc/php/7.4/apache2/php.ini

# Apache2 config 
RUN mkdir -p /var/www/html/sisel
RUN chmod -rw -R /var/www/html/sisel/

RUN rm -f /var/www/html/index.html 
# RUN touch /var/www/html/index.php
# RUN echo "<?php header('Location: eip/'); ?>" > /var/www/html/index.php


COPY . /var/www/html/sisel
WORKDIR /var/www/html/sisel

EXPOSE 80

#now start the server
CMD ["apachectl", "-D", "FOREGROUND"]
