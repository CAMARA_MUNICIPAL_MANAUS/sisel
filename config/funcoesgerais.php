<?php

class Funcoes extends Connection{

	#######################################################
	#######################################################
	// Acoes de Banco

	function listartabela($tabela){
		$sql = "select * from :tabela";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue(':tabela',$tabela);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $retorno;
	}

	function buscardados($tabela, $campo){

		$sql = "select * from $tabela WHERE id = :id";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue(':id',$campo);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $retorno;
	}

	function buscardados_campo($tabela, $campo, $dado){
		$sql = "select * from $tabela WHERE $campo = :dado";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue(':dado',$dado);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $retorno;
	}

	#######################################################
	#######################################################
	// FUNCOES GERAIS

	function dateUSparaBR($data){
		## Converter data de 2014-12-31 para 31/12/2014

		if($data != ''){
			$data = explode("-", $data);
			$data = $data[2].'/'.$data[1].'/'.$data[0];
		}

		return $data;

	}

	function dateBRparaUS($data){
		## Converter data de 31/12/2014 para 2014-12-31
		if($data != ''){
			$data = explode("/", $data);
			$data = $data[2].'-'.$data[1].'-'.$data[0];
		}else{
			$data = trim($data);
		}
		return $data;

	}

	function dateBRparaUSMySql($data){
		## Converter data de 31/12/2014 para 2014-12-31
		if($data != ''){
			$data = explode("/", $data);
			$data = $data[2].'-'.$data[1].'-'.$data[0];
		}else{
			$data = null;
		}
		return $data;

	}

	function validaValorRetornaNulo($data){
		if($data == ""){
			$data = null;
		}

		return $data;
	}

	function validaValorRetornaNuloo($data){
		if(empty($data)){
			return 'null';
		}else{
			return "'".$data."'";
		}

	}

	function formatarValorParaBanco($valor){
		if($valor != "" || $valor != null){
			$valor = str_replace(',', '.', str_replace('.', '', $valor));
		}

		return $valor;
	}

	function deixarSomenteNumeros($data){
		$data = str_replace('.', '', str_replace('-', '', str_replace('/', '', str_replace(',', '', $data))));

		return $data;
	}

	function descricaoGrande($descricao){
		# Verificar se a quantidade de caracteres na descrição é maior que 38.
		# Se for maior ele escreve assim: Exemplo de Descri...

		if(strlen($descricao) > 38){
			$desc = substr($descricao, 0,38).'...';
			return $desc;
		}else{
			return $descricao;
		}
	}

    function formatarDoc($string, $tipo=null){

		/* FORMATAR DE NÉMERO COMO: TELEFONE, CEP, CPF e CNPJ*/

		@$string = preg_replace("/[^0-9]/", "", $string);
	
        if (!$tipo){
            switch (strlen($string)){
                case 10: $tipo = 'fone';  break;
                case 8:  $tipo = 'cep';   break;
                case 11: $tipo = 'cpf';   break;
                case 14: $tipo = 'cnpj';  break;
            }
		}
        
		switch ($tipo){
	            case 'fone':
	                    $string = '(' . substr($string, 0, 2) . ') ' . substr($string, 2, 4) . '-' . substr($string, 6);
	            break;
	            case 'cep':
	                    $string = substr($string, 0, 5) . '-' . substr($string, 5, 3);
	            break;
	            case 'cpf':
	                    $string = substr($string, 0, 3) . '.' . substr($string, 3, 3) . '.' . substr($string, 6, 3) . '-' . substr($string, 9, 2);
	            break;
	            case 'cnpj':
	                    $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) . '.' . substr($string, 5, 3) . '/' . substr($string, 8, 4) . '-' . substr($string, 12, 2);
	            break;
	            case 'rg':
	                    $string = substr($string, 0, 7) .'-' . substr($string, 7, 1);
	            break;
		}
		return $string;
    }

    function mes($mes, $tipo = 'completo'){
    	
    	// Abreviação
    	$nome_mes = '';
    	if($tipo == 'abr'){
    		switch ($mes) {
    			case '01': $nome_mes = 'Jan'; break;
    			case '02': $nome_mes = 'Fev'; break;
    			case '03': $nome_mes = 'Mar'; break;
    			case '04': $nome_mes = 'Abr'; break;
    			case '05': $nome_mes = 'Mai'; break;
    			case '06': $nome_mes = 'Jun'; break;
    			case '07': $nome_mes = 'Jul'; break;
    			case '08': $nome_mes = 'Ago'; break;
    			case '09': $nome_mes = 'Set'; break;
    			case '10': $nome_mes = 'Out'; break;
    			case '11': $nome_mes = 'Nov'; break;
    			case '12': $nome_mes = 'Dez'; break;
    		} // fim swich
    	}else{
			switch ($mes) {
    			case '01': $nome_mes = 'Janeiro'; break;
    			case '02': $nome_mes = 'Fevereiro'; break;
    			case '03': $nome_mes = 'Marco'; break;
    			case '04': $nome_mes = 'Abril'; break;
    			case '05': $nome_mes = 'Maio'; break;
    			case '06': $nome_mes = 'Junho'; break;
    			case '07': $nome_mes = 'Julho'; break;
    			case '08': $nome_mes = 'Agosto'; break;
    			case '09': $nome_mes = 'Setembro'; break;
    			case '10': $nome_mes = 'Outubro'; break;
    			case '11': $nome_mes = 'Novembro'; break;
    			case '12': $nome_mes = 'Dezembro'; break;
    		} // fim swich
    	}

    	return $nome_mes;
    }

    function menuDynamic($array, $level, $id="0"){

		$callback = function($v) use ($id, $level){
		    return $v['FATHER'] == $id && $v['LEVEL'] == $level;
		};

		$m = array_filter($array, $callback);
	
		/*$m = array_filter($array, function($v, $k) use ($id, $level){
    			return $v['FATHER'] == $id && $v['LEVEL'] == $level;
			}, ARRAY_FILTER_USE_BOTH);*/

		foreach ($m as $menu){
			
			$arrow = ($menu["QTYSUB"] > 0) ? "<span class='glyphicon glyphicon-chevron-left arrow'></span>" : "";
			$title = ($level == 1) ? "<b>".$menu["TITLE"]."</b>" : $menu["TITLE"];

			echo "<li>";
				echo "<a href='".$menu["HREF"]."' target='_SELF' class='black'><i class='".$menu["CLASS"]." ".$menu["ICON"]."' style='font-size: 18px;'></i>&nbsp;$title$arrow</a>";

				if($menu["QTYSUB"] > 0){
					$newLevel = $level+1;
					switch ($newLevel) {
						case 2: $ulClass = "nav nav-second-level"; break;
						case 3: $ulClass = "nav nav-third-level"; break;
						case 4: $ulClass = "nav nav-fourth-level"; break;
						case 5: $ulClass = "nav nav-fifth-level"; break;
						default: $ulClass = ""; break;
					}

					echo "<ul class='$ulClass'>";
						self::menuDynamic($array, ($newLevel), $menu["ID"]);
					echo "</ul>";
				}
			echo "</li>";

		} // fim foreach*/

	}

}
?>