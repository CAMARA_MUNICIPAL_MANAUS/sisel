-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           5.5.8 - MySQL Community Server (GPL)
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para novo_sisel2
CREATE DATABASE IF NOT EXISTS `novo_sisel2` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `novo_sisel2`;


-- Copiando estrutura para tabela novo_sisel2.adesao
DROP TABLE IF EXISTS `adesao`;
CREATE TABLE IF NOT EXISTS `adesao` (
  `id` int(10) NOT NULL,
  `tipoadesao` int(2) NOT NULL,
  `numprocessocompra` varchar(18) NOT NULL,
  `numata` varchar(18) NOT NULL,
  `numprocesso_licitatorio` varchar(18) NOT NULL,
  `numdoe` varchar(6) NOT NULL,
  `dt_pulblicacaodoe` date NOT NULL,
  `dt_validade` date NOT NULL,
  `dt_adesao` date NOT NULL,
  `numempenho` varchar(10) DEFAULT NULL,
  `numempenho2` varchar(10) DEFAULT NULL,
  `numempenho3` varchar(10) DEFAULT NULL,
  `numempenho4` varchar(10) DEFAULT NULL,
  `numempenho5` varchar(10) DEFAULT NULL,
  `numempenho6` varchar(10) DEFAULT NULL,
  `numempenho7` varchar(10) DEFAULT NULL,
  `numempenho8` varchar(10) DEFAULT NULL,
  `numempenho9` varchar(10) DEFAULT NULL,
  `numempenho10` varchar(10) DEFAULT NULL,
  `numempenho11` varchar(10) DEFAULT NULL,
  `numempenho12` varchar(10) DEFAULT NULL,
  `ano_empenho` int(4) DEFAULT NULL,
  `usuario` int(11) NOT NULL,
  `data_criacao` datetime NOT NULL,
  `ano_exercicio` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipoadesao_adesaolicitac_tb_tipo_ata_cod_tipoata` (`tipoadesao`),
  KEY `fk_adesao_usuario1_idx` (`usuario`),
  KEY `fk_adesao_ano_exercicio1_idx` (`ano_exercicio`),
  CONSTRAINT `fk_adesao_ano_exercicio1` FOREIGN KEY (`ano_exercicio`) REFERENCES `ano_exercicio` (`ano`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_adesao_usuario1` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tipoadesao_adesaolicitac_tb_tipo_ata_cod_tipoata` FOREIGN KEY (`tipoadesao`) REFERENCES `tipoata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.adesao: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `adesao` DISABLE KEYS */;
/*!40000 ALTER TABLE `adesao` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.ano_exercicio
DROP TABLE IF EXISTS `ano_exercicio`;
CREATE TABLE IF NOT EXISTS `ano_exercicio` (
  `ano` int(11) NOT NULL,
  PRIMARY KEY (`ano`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.ano_exercicio: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `ano_exercicio` DISABLE KEYS */;
INSERT INTO `ano_exercicio` (`ano`) VALUES
	(2014),
	(2015);
/*!40000 ALTER TABLE `ano_exercicio` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.contrato
DROP TABLE IF EXISTS `contrato`;
CREATE TABLE IF NOT EXISTS `contrato` (
  `idcontrato` int(4) NOT NULL AUTO_INCREMENT,
  `numcontrato` varchar(16) NOT NULL,
  `tipocontrato_fk` int(10) NOT NULL,
  `fornecedor_fk` int(11) NOT NULL,
  `objetivo` varchar(300) NOT NULL,
  `responsavel_juridico` varchar(100) NOT NULL,
  `valor` float DEFAULT NULL,
  `recebevalor` enum('S','N') NOT NULL,
  `moeda_fk` int(11) NOT NULL,
  `licitacao_fk` varchar(18) DEFAULT NULL,
  `dtcontrato` date DEFAULT NULL,
  `dtvencimento` date DEFAULT NULL,
  `dtpublicacao` date DEFAULT NULL,
  `num_doe` varchar(6) DEFAULT NULL,
  `nomearquivotexto` varchar(50) DEFAULT NULL,
  `certificadoinss` varchar(60) DEFAULT NULL COMMENT 'CERTIFICADO INSS',
  `dtcertidaoinss` date DEFAULT NULL,
  `dtvalidadeinss` date DEFAULT NULL,
  `numcertidaofgts` varchar(60) DEFAULT NULL COMMENT 'CERTIFICADO FGTS',
  `dtcertidaofgts` date DEFAULT NULL,
  `dtvalidadefgts` date DEFAULT NULL,
  `numcertestmuncarat` varchar(60) DEFAULT NULL COMMENT 'SEC. DA FEZENDA EST.',
  `dtcertestmuncarat` date DEFAULT NULL,
  `dtvalestmuncarat` date DEFAULT NULL,
  `numcertidaofm` varchar(60) DEFAULT NULL COMMENT 'SEC. DA FAZENDA MUN.',
  `dtcertidaofm` date DEFAULT NULL,
  `dtvalidadefm` date DEFAULT NULL,
  `numcertidaoff` varchar(60) DEFAULT NULL COMMENT 'SEC. DA FAZENDA FED.',
  `dtcertidaoff` date DEFAULT NULL,
  `dtvalidadeff` date DEFAULT NULL,
  `numcertidaotst` varchar(60) DEFAULT NULL,
  `dtcertidaotst` date DEFAULT NULL,
  `dtvalidadetst` date DEFAULT NULL,
  `numcertidaooutros` varchar(60) DEFAULT NULL COMMENT 'CERTIFICADO OUTROS',
  `dtcertidaooutros` date DEFAULT NULL,
  `dtvalidadeoutros` date DEFAULT NULL,
  `numempenho` varchar(10) DEFAULT NULL,
  `numempenho2` varchar(10) DEFAULT NULL,
  `numempenho3` varchar(10) DEFAULT NULL,
  `numempenho4` varchar(10) DEFAULT NULL,
  `numempenho5` varchar(10) DEFAULT NULL,
  `numempenho6` varchar(10) DEFAULT NULL,
  `numempenho7` varchar(10) DEFAULT NULL,
  `numempenho8` varchar(10) DEFAULT NULL,
  `numempenho9` varchar(10) DEFAULT NULL,
  `numempenho10` varchar(10) DEFAULT NULL,
  `numempenho11` varchar(10) DEFAULT NULL,
  `numempenho12` varchar(10) DEFAULT NULL,
  `ano_exercicio` int(11) NOT NULL,
  `usuario` int(11) NOT NULL,
  `data_criacao` datetime NOT NULL,
  PRIMARY KEY (`idcontrato`),
  KEY `fk_contrato_fornecedor1_idx` (`fornecedor_fk`),
  KEY `fk_contrato_tipomoeda1_idx` (`moeda_fk`),
  KEY `fk_contrato_ano_exercicio1_idx` (`ano_exercicio`),
  KEY `fk_contrato_tipocontrato1_idx` (`tipocontrato_fk`),
  KEY `fk_contrato_usuario1_idx` (`usuario`),
  CONSTRAINT `fk_contrato_ano_exercicio1` FOREIGN KEY (`ano_exercicio`) REFERENCES `ano_exercicio` (`ano`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contrato_fornecedor1` FOREIGN KEY (`fornecedor_fk`) REFERENCES `fornecedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contrato_tipocontrato1` FOREIGN KEY (`tipocontrato_fk`) REFERENCES `tipocontrato` (`idtipocontrato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contrato_tipomoeda1` FOREIGN KEY (`moeda_fk`) REFERENCES `tipomoeda` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contrato_usuario1` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.contrato: ~31 rows (aproximadamente)
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
INSERT INTO `contrato` (`idcontrato`, `numcontrato`, `tipocontrato_fk`, `fornecedor_fk`, `objetivo`, `responsavel_juridico`, `valor`, `recebevalor`, `moeda_fk`, `licitacao_fk`, `dtcontrato`, `dtvencimento`, `dtpublicacao`, `num_doe`, `nomearquivotexto`, `certificadoinss`, `dtcertidaoinss`, `dtvalidadeinss`, `numcertidaofgts`, `dtcertidaofgts`, `dtvalidadefgts`, `numcertestmuncarat`, `dtcertestmuncarat`, `dtvalestmuncarat`, `numcertidaofm`, `dtcertidaofm`, `dtvalidadefm`, `numcertidaoff`, `dtcertidaoff`, `dtvalidadeff`, `numcertidaotst`, `dtcertidaotst`, `dtvalidadetst`, `numcertidaooutros`, `dtcertidaooutros`, `dtvalidadeoutros`, `numempenho`, `numempenho2`, `numempenho3`, `numempenho4`, `numempenho5`, `numempenho6`, `numempenho7`, `numempenho8`, `numempenho9`, `numempenho10`, `numempenho11`, `numempenho12`, `ano_exercicio`, `usuario`, `data_criacao`) VALUES
	(3, 'CT0016-2014', 1, 76, 'AQUISIÇÃO DE GENEROS ALIMENTÍCIOS', 'JOÃO BOSCO GOMES SARAIVA', 6148250, 'S', 1, 'RP0007-2014', '2014-07-01', '2015-07-01', '2014-07-21', '188', 'CT0016-2014.pdf', '1739F00E56F880C5 ', '0000-00-00', '0000-00-00', '06015837482480 ', '0000-00-00', '0000-00-00', '15469395 ', '0000-00-00', '0000-00-00', '48943/2014 ', '0000-00-00', '0000-00-00', '017142017488888226 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '544', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(4, 'CT0001-2014', 1, 77, 'SERVIÇO E MANUT.PREV.CORRETIVA EM INFRAESTRUTURA PREDIAL ', '', 30904900, 'S', 1, 'DL 2437/2013', '2014-01-02', '2014-06-30', '2014-01-28', '106', 'CT0001-2014.pdf', '4D9A9941D64353A5', '0000-00-00', '0000-00-00', '12364482455363', '0000-00-00', '0000-00-00', '14588161', '0000-00-00', '0000-00-00', '137922/2013', '0000-00-00', '0000-00-00', '00092201303001338', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00001', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(5, 'CT0017-2014', 1, 77, 'SERVIÇO DE MANUT PREV CORRET EM INFRAESTRUTURA PREDIAL', 'JOÃO BOSCO GOMES SARAIVA', 37085900, 'S', 1, 'DL1146-2014', '2014-07-01', '2014-12-27', '2014-07-29', '192', 'CT0017-2014.pdf', '8485B9EC8A74E929 ', '0000-00-00', '0000-00-00', '05202183247017 ', '0000-00-00', '0000-00-00', '15867536 ', '0000-00-00', '0000-00-00', '66716-2014 ', '0000-00-00', '0000-00-00', '173862014 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '000630', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(6, 'CT0029-2014', 1, 77, 'SERVI?O DE MANUT PREV CORRET EM INFRAESTRUTURA PREDIAL', 'JOAO BOSCO GOMES SARAIVA', 37085900, 'S', 1, 'DL2415-2014', '2014-12-29', '2015-06-27', '2014-12-30', '263', 'CT0029-2014.pdf', '', '0000-00-00', '0000-00-00', '01313669023507 ', '0000-00-00', '0000-00-00', '16901126 ', '0000-00-00', '0000-00-00', '11032015 ', '0000-00-00', '0000-00-00', '238052014888338 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '01257', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(7, 'CT0002-2014', 1, 85, 'SERVIÇO DE CONSERVAÇÃO E LIMPEZA NO PRÉDIO SEDE', '', 19200000, 'S', 1, 'DL0247-2014', '2014-02-24', '2014-04-25', '2014-03-11', '125', 'CT0002-2014.pdf', '48cff0df0d40628f', '0000-00-00', '0000-00-00', '20531606954500', '0000-00-00', '0000-00-00', '15031802', '0000-00-00', '0000-00-00', '22368-2014', '0000-00-00', '0000-00-00', '7920303001157', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00171', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(8, 'CT0014-2014', 1, 85, 'SERVIÇO DE LIMPEZA E CONSERVAÇÃO,JARDINAGEM, COPEIRAGEM', 'JOÃO BOSCO GOMES SARAIVA', 70410600, 'S', 1, 'DL0712-2014', '2014-04-25', '2014-10-21', '2014-05-05', '155', 'CT0014-2014.pdf', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '0440', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(9, 'CT0023-2014', 1, 85, 'SERVI?O DE LIMPEZA E CONSERVA??O,JARDINAGEM,COPEIRAGEM DO PREDIO SEDE DA CMM', '', 70410600, 'S', 1, 'DL1898-2014', '2014-10-22', '2015-04-22', '2014-11-03', '237', 'CT0023-2014.pdf', '1A2A7E15B6676D39', '0000-00-00', '0000-00-00', '02413281758209', '0000-00-00', '0000-00-00', '16533429', '0000-00-00', '0000-00-00', '1072922014', '0000-00-00', '0000-00-00', '2421720148888157', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00933', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(10, 'TCU0001-2014', 6, 88, 'PERMISSÃO DE USO REMUNERADO P EXPLORAÇÃO E ADM. DO RESTAURANTE', '', 310000, 'S', 1, 'CO0002-2013', '2014-02-10', '2016-02-10', '2014-04-16', '147', 'TCU0001-2014.pdf', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(11, 'CT0003-2014', 1, 97, 'SERVIÇO DE PIANISTA PROFISSIONAL', '', 800000, 'S', 1, 'DL 0246-2014', '2014-03-18', '2014-07-18', '2014-04-16', '147', 'CT0003-2014.pdf', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00252', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(12, 'CT0004-2014', 1, 99, 'SERVIÇO DE GERENCIAMENTO SUPORTE TECNICO MANUT PREV.CORRET.SISTEMA TELEFONICO', '', 7986000, 'S', 1, 'CC0003-2014', '2014-04-01', '2015-04-01', '2014-04-24', '150', 'CT0004-2014.pdf', 'E672AA49B56208A', '0000-00-00', '0000-00-00', '07422009808789', '0000-00-00', '0000-00-00', '15198723 ', '0000-00-00', '0000-00-00', '27306/2014', '0000-00-00', '0000-00-00', '8520133001756', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00275', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(13, 'CT0010-2014', 1, 100, 'SERV. DE MANUT. PREV.CORRET.DE COMPUTADORES,NOTEBOOKS,TABLETS E SERVIDORES', '', 11520000, 'S', 1, 'PP0001-2014', '2014-04-16', '2014-10-16', '2014-04-29', '153', 'CT0010-2014.pdf', '0a174f82a65cfd5e ', '0000-00-00', '0000-00-00', '08221497865790 ', '0000-00-00', '0000-00-00', '15440532 ', '0000-00-00', '0000-00-00', '34351/2014 ', '0000-00-00', '0000-00-00', '1009201488888482 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00349', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(14, 'CT0011-2014', 1, 100, 'SERV. DE MANUT. PREVT.CORRET DO LINK PONTO A PONTO ', '', 11940000, 'S', 1, 'PP0001-2014', '2014-04-16', '2014-10-16', '2014-04-29', '153', 'CT0011-2014.pdf', '0a174f82a65cfd5e ', '0000-00-00', '0000-00-00', '08221497865790 ', '0000-00-00', '0000-00-00', '15440532 ', '0000-00-00', '0000-00-00', '34351/2014 ', '0000-00-00', '0000-00-00', '1009201488888482 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00350', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(15, 'TACT1º0010-2014', 2, 100, 'PROROGAR PRAZO POR MAIS 06 MESES', '', 11520000, 'S', 1, 'RP0001-2014', '2014-10-16', '2015-04-16', '2014-10-30', '236', 'TACT1º0010-2014.pdf', ' a38404af23852303d', '0000-00-00', '0000-00-00', '007121168870607', '0000-00-00', '0000-00-00', '16471644', '0000-00-00', '0000-00-00', '95824/2014', '0000-00-00', '0000-00-00', '1979720148888482', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '859', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(16, 'TACT1º0011-2014', 2, 100, 'PROROGAR PRAZO POR MAIS 06 MESES', 'JOÃO BOSCO GOMES SARAIVA', 11940000, 'S', 1, 'RP0001-2014', '2014-10-16', '2015-04-16', '2014-10-30', '236', 'TACT1º0011-2014.pdf', 'a38404af2385303d', '0000-00-00', '0000-00-00', '072121168870607', '0000-00-00', '0000-00-00', '16471644', '0000-00-00', '0000-00-00', '958242014', '0000-00-00', '0000-00-00', '1979720148888482', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(17, 'CT0007-2014', 1, 101, 'SERVIÇO DE MANUT.PREV.CORRET.DA INFRA DE REDE FISICA LÓGICA', '', 19800000, 'S', 1, 'PP0001-2014', '2014-04-16', '2014-10-16', '2014-04-29', '153', 'CT0007-2014.pdf', '4f91976f249fb716 ', '0000-00-00', '0000-00-00', '05571578735350 ', '0000-00-00', '0000-00-00', '15405842 ', '0000-00-00', '0000-00-00', '49633/2014 ', '0000-00-00', '0000-00-00', '124302014 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00346', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(18, 'CT0012-2014', 1, 101, 'SERV. DE MANUT. PREVET.CORRET, DOS PONTOS DIGITAIS DE TVS', '', 7770000, 'S', 1, 'PP0001-2014', '2014-04-16', '2014-10-16', '2014-04-29', '153', 'CT0012-2014.pdf', '4f91976f249fb716 ', '0000-00-00', '0000-00-00', '05571578735350 ', '0000-00-00', '0000-00-00', '15405842 ', '0000-00-00', '0000-00-00', '49633/2014 ', '0000-00-00', '0000-00-00', '124302014 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00351', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(19, 'CT0013-2014', 1, 101, 'SERV. DE MANUT. CORRET.PREVENT. EM SISTEMA GERENCIADORES DE BANCO DE DADOS', '', 12480000, 'S', 1, 'PP0001-2014', '2014-04-16', '2014-10-16', '2014-04-29', '153', 'CT0013-2014.pdf', '4f91976f249fb716 ', '0000-00-00', '0000-00-00', '05571578735350 ', '0000-00-00', '0000-00-00', '15405842 ', '0000-00-00', '0000-00-00', '49633/2014 ', '0000-00-00', '0000-00-00', '124302014 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00352', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(20, 'CT0008-2014', 1, 102, 'SERV. MANUT.PREVET.CORRET.DOS SERVIDORES LINUX E SISTEMA DE INF', '', 22800000, 'S', 1, 'PP0001-2014', '2014-04-16', '2014-10-16', '2014-04-29', '153', 'CT0008-2014.pdf', '', '0000-00-00', '0000-00-00', '12241684250502 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '23753/2014 ', '0000-00-00', '0000-00-00', '0010420133001037 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00347', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(21, 'CT0009-2014', 1, 102, 'SERV. DE MANUT. PREVT.CORRET. DE EQUIPAMENTOS DE INFORMATICA COMO IMP.SCA.NOBREAKS', '', 11940000, 'S', 1, 'PP0001-2014', '2014-04-16', '2014-10-16', '2014-04-29', '153', 'CT0009-2014.pdf', '', '0000-00-00', '0000-00-00', '12241684250502 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '23753/2014 ', '0000-00-00', '0000-00-00', '0010420133001037 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00348', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(22, 'TACT1º0008-2014', 2, 102, 'PROROGAR PRAZO POR MAIS 06 MESES', '', 22800000, 'S', 1, 'RP0001-2014', '2014-10-15', '2015-04-15', '2014-11-03', '237', 'TACT1º0008-2014.pdf', '6d861d631e155876', '0000-00-00', '0000-00-00', '11083986748659', '0000-00-00', '0000-00-00', '16474449', '0000-00-00', '0000-00-00', '94301/2014', '0000-00-00', '0000-00-00', '1642420148888037', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '856', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(23, 'CT0006-2014', 1, 103, 'SERVIÇO FORNECIMENTO DE ÁGUA MINERAL', '', 2509000, 'S', 1, 'PP0003-2014', '2014-04-14', '2014-10-14', '2014-04-29', '153', 'CT0006-2014.pdf', '1FCB8FFD19E04194', '0000-00-00', '0000-00-00', '06121274678706', '0000-00-00', '0000-00-00', '15306453', '0000-00-00', '0000-00-00', '31600/2014', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00354', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(24, 'TACT1º0006-2014', 2, 103, 'PROROGAR PRAZO POR MAIS 06 MESES', '', 0, 'N', 1, 'RP0003-2014', '2014-10-14', '2015-04-14', '2014-10-15', '227', 'TACT1º0006-2014.pdf', '85d5fd68657c8193', '0000-00-00', '0000-00-00', '07255744679599', '0000-00-00', '0000-00-00', '16428056', '0000-00-00', '0000-00-00', '81897/2014', '0000-00-00', '0000-00-00', '222072014888168', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(25, 'CT0005-2014', 1, 104, 'SERVIÇO DE AGENCIAMENTO DE PASSAGENS AÉÁREAS', '', 9720000, 'S', 1, 'PP0004-2014', '2014-04-01', '2015-04-01', '2014-04-30', '154', 'CT0005-2014.pdf', '5E9FD01B4095ADA1 ', '0000-00-00', '0000-00-00', '04551348815714', '0000-00-00', '0000-00-00', '15407713', '0000-00-00', '0000-00-00', '45011/2014', '0000-00-00', '0000-00-00', '052042014888888083 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00365', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(26, 'TACT7º0007-2009', 2, 105, 'PRORROGAR PRAZO DE VIRGÊNCIA POR 04 MESES', '', 16020000, 'S', 1, 'tp0002-2009', '2014-04-04', '2014-08-04', '2014-04-24', '150', 'TACT7º0007-2009.pdf', '94DA13426C181B70', '0000-00-00', '0000-00-00', '03573869327233', '0000-00-00', '0000-00-00', '15327051', '0000-00-00', '0000-00-00', '40754/2014', '0000-00-00', '0000-00-00', '01552201488888239', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00320', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(27, 'TACT1º0005-2013', 2, 106, 'PRORROGAR PRAZO DE VIRGÊNCIA POR 12 MESES', '', 18720000, 'S', 1, 'IL0517-2013', '2014-04-03', '2015-04-03', '2014-04-30', '154', 'TACT1º0005-2013.pdf', '342787BC69BFACDA', '0000-00-00', '0000-00-00', '01420048591432', '0000-00-00', '0000-00-00', '068888807', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00123201319029400', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00319', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(28, 'CT0015-2014', 1, 112, 'LOCAÇÃO DE QUATRO VEÍCULOS AUTOMOTORES', 'JOÃO BOSCO GOMES SARAIVA', 7980000, 'S', 1, 'CC0006-2014', '2014-05-06', '2014-11-06', '2014-05-23', '164', 'CT0015-2014.pdf', '4DB7A5C2CFE38939 ', '0000-00-00', '0000-00-00', '13392160596023', '0000-00-00', '0000-00-00', '15430200 ', '0000-00-00', '0000-00-00', '34277-2014 ', '0000-00-00', '0000-00-00', '131132014888888557 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '000457', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(29, 'TACT1º0010-2013', 2, 115, 'PRORROGAR PRAZO POR MAIS 12 MESES', '', 8100000, 'S', 1, '', '2014-06-10', '2015-06-10', '2014-06-12', '175', 'TACT1º0010-2013.pdf', 'B130F6849F32F3', '0000-00-00', '0000-00-00', '02001314505405', '0000-00-00', '0000-00-00', '10262105', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '0038620323001316', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '00503', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(30, 'CT0018-2014', 1, 116, 'LOCAÇÃO DE EQUIPAMENTOS DE SONORIZAÇÃO', 'JOÃO BOSCO GOMES SARAIVA', 7899300, 'S', 1, 'CC0007-2014', '2014-07-02', '2015-07-02', '2014-07-30', '193', 'CT0018-2014.pdf', 'A7F04EFDADDF4122 ', '0000-00-00', '0000-00-00', '03464827735625 ', '0000-00-00', '0000-00-00', '15427277 ', '0000-00-00', '0000-00-00', '34802-2014 ', '0000-00-00', '0000-00-00', '00142038888658 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '000632', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(31, 'CT0019-2014', 1, 119, 'SERVIÇOS GRAFICOS  PARA EVENTOS,FESTIVIDADES E HOMENAGENS', 'JOÃO BOSCO GOMES SARAIVA', 39710000, 'S', 1, 'RP0006-2014', '2014-07-17', '2015-07-17', '2014-08-05', '195', 'CT0019-2014.pdf', 'F1CA8854DDA5A2AD ', '0000-00-00', '0000-00-00', '07065306896094 ', '0000-00-00', '0000-00-00', '15471985 ', '0000-00-00', '0000-00-00', '52184/2014 ', '0000-00-00', '0000-00-00', '1252022014 ', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '640', '641', '642', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(32, 'TACT3º0007-2011', 2, 120, 'PRORROGAR PRAZO POR MAIS 12 MESES', 'JOÃO BOSCO GOMES SARAIVA', 11196600, 'S', 1, '', '2014-08-12', '2015-08-12', '2014-08-25', '205', 'TACT3º0007-2011.pdf', 'D564DED44999E136', '0000-00-00', '0000-00-00', '02143546528119', '0000-00-00', '0000-00-00', '16137140', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '1094320148888920', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '725', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48'),
	(33, 'TACT1º0009-2013', 2, 120, 'PROROGAR PRAZO POR MAIS 12 MESES', 'JOAO BOSCO GOMES SARAIVA', 2239550, 'S', 1, '', '2014-11-24', '2015-05-24', '2014-12-04', '253', 'TACT1º0009-2013.pdf', '2360EDF6A1979C48', '0000-00-00', '0000-00-00', '04154620562806', '0000-00-00', '0000-00-00', '16587286', '0000-00-00', '0000-00-00', '115785-2014', '0000-00-00', '0000-00-00', '199312014888920', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '000975', '', '', '', '', '', '', '', '', '', '', '', 2014, 19, '2015-02-17 19:21:48');
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.convenio
DROP TABLE IF EXISTS `convenio`;
CREATE TABLE IF NOT EXISTS `convenio` (
  `id` int(16) NOT NULL,
  `numconvenio` varchar(16) NOT NULL,
  `tipoconvenio_fk` int(10) NOT NULL,
  `esfera_fk` int(1) NOT NULL,
  `recebervalor` enum('S','N') DEFAULT NULL,
  `objconvenio` varchar(300) DEFAULT NULL,
  `respjuridico` varchar(100) DEFAULT NULL,
  `moeda` int(11) NOT NULL,
  `valor` float DEFAULT NULL,
  `esferagoverno` int(11) NOT NULL,
  `dtassinatura` date DEFAULT NULL,
  `dtvencimento` date DEFAULT NULL,
  `numdoem` varchar(6) DEFAULT NULL,
  `dtpublicacao` date DEFAULT NULL,
  `numoficio` varchar(6) DEFAULT NULL,
  `dtoficio` date DEFAULT NULL,
  `nomearquivotexto` varchar(50) DEFAULT NULL,
  `numempenho` varchar(10) DEFAULT NULL,
  `numempenho2` varchar(10) DEFAULT NULL,
  `numempenho3` varchar(10) DEFAULT NULL,
  `numempenho4` varchar(10) DEFAULT NULL,
  `numempenho5` varchar(10) DEFAULT NULL,
  `numempenho6` varchar(10) DEFAULT NULL,
  `numempenho7` varchar(10) DEFAULT NULL,
  `numempenho8` varchar(10) DEFAULT NULL,
  `numempenho9` varchar(10) DEFAULT NULL,
  `numempenho10` varchar(10) DEFAULT NULL,
  `numempenho11` varchar(10) DEFAULT NULL,
  `numempenho12` varchar(10) DEFAULT NULL,
  `ano_exercicio` int(11) NOT NULL,
  `usuario` int(11) NOT NULL,
  `data_criacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_convenio_tipoconvenio1_idx` (`tipoconvenio_fk`),
  KEY `fk_convenio_ano_exercicio1_idx` (`ano_exercicio`),
  KEY `fk_convenio_tipomoeda1_idx` (`moeda`),
  KEY `fk_convenio_usuario1_idx` (`usuario`),
  KEY `fk_convenio_tipoesferaconveniado1_idx` (`esfera_fk`),
  CONSTRAINT `fk_convenio_ano_exercicio1` FOREIGN KEY (`ano_exercicio`) REFERENCES `ano_exercicio` (`ano`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_convenio_tipoconvenio1` FOREIGN KEY (`tipoconvenio_fk`) REFERENCES `tipoconvenio` (`idtipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_convenio_tipoesferaconveniado1` FOREIGN KEY (`esfera_fk`) REFERENCES `tipoesferaconveniado` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_convenio_tipomoeda1` FOREIGN KEY (`moeda`) REFERENCES `tipomoeda` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_convenio_usuario1` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.convenio: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `convenio` DISABLE KEYS */;
/*!40000 ALTER TABLE `convenio` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.cotacao
DROP TABLE IF EXISTS `cotacao`;
CREATE TABLE IF NOT EXISTS `cotacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `participante_fk` int(11) NOT NULL,
  `itemlicitacao_fk` int(11) NOT NULL,
  `valor_unitario` float NOT NULL,
  `valor_total` float NOT NULL,
  `tipovalor_fk` varchar(1) NOT NULL,
  `tiporesultado_ifk` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cotacao_itemlicitacao1_idx` (`itemlicitacao_fk`),
  KEY `fk_cotacao_tipovalor1_idx` (`tipovalor_fk`),
  KEY `fk_cotacao_tiporesultado1_idx` (`tiporesultado_ifk`),
  KEY `fk_cotacao_participante_certidao1_idx` (`participante_fk`),
  CONSTRAINT `fk_cotacao_itemlicitacao1` FOREIGN KEY (`itemlicitacao_fk`) REFERENCES `itemlicitacao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cotacao_participante_certidao1` FOREIGN KEY (`participante_fk`) REFERENCES `fornecedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cotacao_tiporesultado1` FOREIGN KEY (`tiporesultado_ifk`) REFERENCES `tiporesultado` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cotacao_tipovalor1` FOREIGN KEY (`tipovalor_fk`) REFERENCES `tipovalor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.cotacao: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `cotacao` DISABLE KEYS */;
INSERT INTO `cotacao` (`id`, `participante_fk`, `itemlicitacao_fk`, `valor_unitario`, `valor_total`, `tipovalor_fk`, `tiporesultado_ifk`) VALUES
	(1, 137, 47, 5, 1110, 'E', 'V');
/*!40000 ALTER TABLE `cotacao` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.dotacao
DROP TABLE IF EXISTS `dotacao`;
CREATE TABLE IF NOT EXISTS `dotacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `licitacao_fk` varchar(18) NOT NULL,
  `categoria_economica` varchar(1) DEFAULT NULL,
  `grupo_natureza` varchar(1) DEFAULT NULL,
  `modalidade_aplicacao` varchar(2) DEFAULT NULL,
  `elemento` varchar(2) DEFAULT NULL,
  `unidade_orcamentaria` varchar(6) DEFAULT NULL,
  `fonte_recurso` varchar(10) DEFAULT NULL,
  `acao_governo` varchar(7) DEFAULT NULL,
  `sub_funcao` varchar(3) DEFAULT NULL,
  `funcao` varchar(2) DEFAULT NULL,
  `programa` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dotacao_licitacao1_idx` (`licitacao_fk`),
  CONSTRAINT `fk_dotacao_licitacao1` FOREIGN KEY (`licitacao_fk`) REFERENCES `licitacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.dotacao: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `dotacao` DISABLE KEYS */;
INSERT INTO `dotacao` (`id`, `licitacao_fk`, `categoria_economica`, `grupo_natureza`, `modalidade_aplicacao`, `elemento`, `unidade_orcamentaria`, `fonte_recurso`, `acao_governo`, `sub_funcao`, `funcao`, `programa`) VALUES
	(11, 'CC0003-2014', '4', '5', '45', '45', '456000', '4564560000', '5460000', '546', '64', '5645');
/*!40000 ALTER TABLE `dotacao` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.empresa
DROP TABLE IF EXISTS `empresa`;
CREATE TABLE IF NOT EXISTS `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_empresa` varchar(100) DEFAULT NULL,
  `cnpj` varchar(20) DEFAULT NULL,
  `logradouro` varchar(150) DEFAULT NULL,
  `cep` int(8) DEFAULT NULL,
  `numero` int(5) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `ordenador` varchar(100) DEFAULT NULL,
  `presidente` varchar(100) DEFAULT NULL,
  `funcao_presidente` varchar(100) DEFAULT NULL,
  `vice_presidente` varchar(100) DEFAULT NULL,
  `funcao_vicepresidente` varchar(100) DEFAULT NULL,
  `membro1` varchar(100) DEFAULT NULL,
  `membro2` varchar(100) DEFAULT NULL,
  `membro3` varchar(100) DEFAULT NULL,
  `mapa_titulo` varchar(50) DEFAULT NULL,
  `logo_empresa` varchar(300) DEFAULT NULL,
  `unidade_orcamentaria` int(10) DEFAULT NULL,
  `data_expiracao` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.empresa: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` (`id`, `nome_empresa`, `cnpj`, `logradouro`, `cep`, `numero`, `telefone`, `ordenador`, `presidente`, `funcao_presidente`, `vice_presidente`, `funcao_vicepresidente`, `membro1`, `membro2`, `membro3`, `mapa_titulo`, `logo_empresa`, `unidade_orcamentaria`, `data_expiracao`) VALUES
	(2, 'CÂMARA MUNICIPAL DE MANAUS', '04.503.504/0001-85', 'Rua Padre Agostinho Caballero Martin', NULL, 850, '(92)3303-2736', 'JOÃO BOSCO GOMES SARAIVA', 'WANDECY GOMES CAMPOS', 'PRESIDENTE DA COMISSÃO DE LICITAÇÃO-CCCL-1', 'KELLY CRISTINA SANTOS COSTA', 'SECRETÁRIO DA COMISSÃO DE LICITAÇÃO, FCL-1', 'LILIAN DA FROTA MOREIRA ALEIXO', 'WALDER BARBOSA DOS REIS JÚNIOR', 'TANIA MARA DOS SANTOS TORRES', NULL, 'brasao.png', 1100, '2015-02-28');
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.fornecedor
DROP TABLE IF EXISTS `fornecedor`;
CREATE TABLE IF NOT EXISTS `fornecedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_fantasia` varchar(100) DEFAULT NULL,
  `razao_social` varchar(100) DEFAULT NULL,
  `tipopessoa_fk` int(11) NOT NULL,
  `logradouro` varchar(80) DEFAULT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `cpfcnpj` varchar(20) DEFAULT NULL,
  `telefone` varchar(30) DEFAULT NULL,
  `insc_estadual` varchar(15) DEFAULT NULL,
  `insc_municipal` varchar(20) DEFAULT NULL,
  `ramoatividade` varchar(100) DEFAULT NULL,
  `nomeresponsavel` varchar(50) DEFAULT NULL,
  `cpfresponsavel` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_fornecedor_tipopessoa1_idx` (`tipopessoa_fk`),
  CONSTRAINT `fk_fornecedor_tipopessoa1` FOREIGN KEY (`tipopessoa_fk`) REFERENCES `tipopessoa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.fornecedor: ~72 rows (aproximadamente)
/*!40000 ALTER TABLE `fornecedor` DISABLE KEYS */;
INSERT INTO `fornecedor` (`id`, `nome_fantasia`, `razao_social`, `tipopessoa_fk`, `logradouro`, `numero`, `complemento`, `cep`, `cpfcnpj`, `telefone`, `insc_estadual`, `insc_municipal`, `ramoatividade`, `nomeresponsavel`, `cpfresponsavel`) VALUES
	(75, 'PLANORTE INSTALAÇÃO E MANUTENÇÃO ELETRICA LTDA', 'PLANORTE INSTALAÇÃO E MANUTENÇÃO ELETRICA LTDA', 2, 'RUA CORONEL CONRADO NIEMEYER, 1422', NULL, NULL, '69.063-550', '08395214000116', '', '04.230.699-0', '11596901', 'SERVIÇOS DE ENGENHARIA', 'PAULO NERY GOMES DA SILVA', '076.256.342-72'),
	(76, 'AMAZOMARTE COMERCIO E SERVIÇOS LTDA', 'AMAZOMARTE COMERCIO E SERVIÇOS LTDA', 2, 'RUA XI Nº 19 QUADRA 12 CONJUNTO MUNDO NOVO', NULL, NULL, '69.090-450', '04781226000128', '(92)3232-2241', '04.149.611-6', '10272501', 'COMERCIO AYTACADISTA DE ARTIGOS DE ESCRITORIO E PA', 'RAFAELLE LEINA CAVALCANTE TORRES', '651.752.632-34'),
	(77, 'MARKA REFORMAS LTDA', 'MARKA REFORMAS LTDA', 2, 'AV SENADOR RAIMUNDO PARENTE, 70 CONJ. GUAIANAS BL-01 APT 203', NULL, NULL, '69.048-661', '05764338000133', '(92)3234-7882', '', '10678801', 'SERVIÇO DE ENGENHARIA', 'MARCELLO MUSSA COSTA', '348.061.482-53'),
	(78, 'AMAZONAS ENERGIA S.A', 'AMAZONAS ENERGIA S.A', 2, '', NULL, NULL, '', '02341467000120', '', '', '', '', '', ''),
	(79, 'TERRA EDITORA COMERCIO SERVIÇOS GRÁFICOS LTDA', 'TERRA EDITORA COMERCIO SERVIÇOS GRÁFICOS LTDA', 2, '', NULL, NULL, '', '08951434000189', '', '', '', '', '', ''),
	(80, 'EDITORA ANA CASSIA LTDA', 'EDITORA ANA CASSIA LTDA', 2, '', NULL, NULL, '', '04816658000127', '', '', '', '', '', ''),
	(81, 'EMPRESA JORNAL DO COMÉRCIO LTDA', 'EMPRESA JORNAL DO COMÉRCIO LTDA', 2, '', NULL, NULL, '', '04561791000180', '', '', '', '', '', ''),
	(82, 'MANAUS AMBIENTAL S.A', 'MANAUS AMBIENTAL S.A', 2, '', NULL, NULL, '', '03264927000127', '', '', '', '', '', ''),
	(83, 'EMPRESA DE JORNAIS CALDERARO LTDA', 'EMPRESA DE JORNAIS CALDERARO LTDA', 2, '', NULL, NULL, '', '04354908000154', '', '', '', '', '', ''),
	(84, 'EDITORA NDJ LTDA', 'EDITORA NDJ LTDA', 2, '', NULL, NULL, '', '54102785000132', '', '', '', '', '', ''),
	(85, 'TECNELETRICA DA AMAZÔNIA LTDA', 'TECNELETRICA DA AMAZÔNIA LTDA', 2, '', NULL, NULL, '', '05531157000167', '', '', '', '', '', ''),
	(86, 'NP CAPACITAÇÃO E SOLUÇÕES TECNOLOGICAS LTDA', 'NP CAPACITAÇÃO E SOLUÇÕES TECNOLOGICAS LTDA', 2, '', NULL, NULL, '', '07797967000195', '', '', '', '', '', ''),
	(87, 'ZENITE INFORMAÇÃO E CONSULTORIA', 'ZENITE INFORMAÇÃO E CONSULTORIA', 2, '', NULL, NULL, '', '86781069000115', '', '', '', '', '', ''),
	(88, 'IGOR DA S. MENDONÇA', 'IGOR DA S. MENDONÇA', 2, '', NULL, NULL, '', '16799056000198', '', '', '', '', 'IGOR DA SILVA MENDONÇA', '786.197.182-15'),
	(89, 'ANDRE DE VASCONCELOS GITIRANA-DB INFORMATICA', 'ANDRE DE VASCONCELOS GITIRANA-DB INFORMATICA', 2, '', NULL, NULL, '', '10855056000181', '', '', '', '', '', ''),
	(90, 'ANA CAROLINE SOUZA LOPES', 'ANA CAROLINE SOUZA LOPES', 2, 'BECO JOSE CASEMIRO Nº 36', NULL, NULL, '69.010-108', '17660392000118', '', '', '', '', '', ''),
	(91, 'IANE PRSICILA NEVES DA SILVA', 'IANE PRSICILA NEVES DA SILVA', 2, 'RUA LUIS ANTONY Nº 15', NULL, NULL, '69.010-100', '18055275000198', '', '', '', '', '', ''),
	(92, 'D V DA SILVA COMERCIO', 'D V DA SILVA COMERCIO', 2, 'RUA JOAQUIM SERRA, 573', NULL, NULL, '69.035-290', '18525935000157', '', '', '', '', '', ''),
	(93, 'G DA S BESSA', 'G DA S BESSA', 2, 'RUA NAZARTH MESQUITA Nº 4', NULL, NULL, '69.054-501', '08735744000166', '', '', '', '', '', ''),
	(94, 'P A VILAÇA NETO', 'P A VILAÇA NETO', 2, 'AV DUQUE DE CAXIAS Nº 1771', NULL, NULL, '69.020-520', '02704436000197', '', '', '', '', '', ''),
	(95, 'H M GARCIA-EPP', 'H M GARCIA-EPP', 2, 'RUA 05, 189 QUADRA 1-CONJ VILAR CAMARA ', NULL, NULL, '69.083-390', '03662747000101', '', '', '', '', '', ''),
	(96, 'JOÃO TORRES DE MELO', 'JOÃO TORRES DE MELO', 2, '', NULL, NULL, '', '34577379000119', '', '', '', '', '', ''),
	(97, 'JHONNY MEURER CARVALHO', 'JHONNY MEURER CARVALHO', 1, '', NULL, NULL, '', '95861440204', '', '', 'MANAUS', '', '', '958.614.402-04'),
	(98, 'EMPRESA BRASILEIRA DE TELECOMUNICAÇÃO', 'EMPRESA BRASILEIRA DE TELECOMUNICAÇÃO', 2, '', NULL, NULL, '', '33.530.486/0001-29', '', '', '', '', 'WILLIAM CARVALHO CUNHA', '174.695.278-42'),
	(99, 'P & G COMERCIO E SERVIÇO DE INFORMATICA', 'P & G COMERCIO E SERVIÇO DE INFORMATICA', 2, '', NULL, NULL, '', '11347756000128', '', '', '', '', '', ''),
	(100, 'M. DO S. FURTADO MONTEIRO', 'M. DO S. FURTADO MONTEIRO', 2, '', NULL, NULL, '', '12887482000122', '', '', '', '', '', ''),
	(101, 'R 2 COM. SERVIÇOS E REPRESENTAÇÃO DE PROD. INF. LTDA', 'R 2 COM. SERVIÇOS E REPRESENTAÇÃO DE PROD. INF. LTDA', 2, '', NULL, NULL, '', '10176902000137', '', '', '', '', '', ''),
	(102, 'CINTIA ALVES VIEIRA', 'CINTIA ALVES VIEIRA', 2, '', NULL, NULL, '', '11903037000146', '', '', '', '', '', ''),
	(103, 'P S ALMEIDA SERVIÇOS E REPRESENTAÇÕES', 'P S ALMEIDA SERVIÇOS E REPRESENTAÇÕES', 2, '', NULL, NULL, '', '09598168000115', '', '', '', '', '', ''),
	(104, 'TREVO TURISMO LTDA', 'TREVO TURISMO LTDA', 2, '', NULL, NULL, '', '03176083000162', '', '', '', '', '', ''),
	(105, 'COSTA RICA SERVIÇOS TECNICOS LTDA', 'COSTA RICA SERVIÇOS TECNICOS LTDA', 2, '', NULL, NULL, '', '01756239000159', '', '', '', '', '', ''),
	(106, 'IMPLY TECNOLOGIA ELETRÔNICA LTDA', 'IMPLY TECNOLOGIA ELETRÔNICA LTDA', 2, '', NULL, NULL, '', '05681400000123', '', '', '', '', '', ''),
	(107, 'CASSIANO JOSE ALVES E GUEDES DE OLIVEIRA', 'CASSIANO JOSE ALVES E GUEDES DE OLIVEIRA', 2, 'AV AUTAZ MIRIN, 45', NULL, NULL, '69.088-244', '02942797000171', '(92)8143-7666', '04.140.276-6', '8827401', '', '', ''),
	(108, 'B.E.M SERVIÇOS E INTALAÇÕES LTDA', 'B.E.M SERVIÇOS E INTALAÇÕES LTDA', 2, 'BECO SAO BENEDITO, 218', NULL, NULL, '69.057-495', '12096451000153', '(92)3236-3358', '', '', '', '', ''),
	(109, 'INFORTEL-COMERCIO E SERVIÇOS DE TELECOMUNICAÇÃO', 'INFORTEL-COMERCIO E SERVIÇOS DE TELECOMUNICAÇÃO', 2, 'AV TIMBIRAS,1254 NUCLEO 3', NULL, NULL, '69.090-010', '05820963000155', '', '', '', '', '', ''),
	(110, 'POLO TELECOM COMERCIO E SERVIÇOS LTDA', 'POLO TELECOM COMERCIO E SERVIÇOS LTDA', 2, 'RUA SILVA RAMOS, 66', NULL, NULL, '69.010-180', '01676099000109', '', '', '', '', '', ''),
	(111, 'ERIDATA COMERCIO DE MATERIAS ELETRONICOS LTDA', 'ERIDATA COMERCIO DE MATERIAS ELETRONICOS LTDA', 2, 'RUA MAJOR GABRIEL, 530', NULL, NULL, '69.020-060', '15816937000107', '', '', '', '', '', ''),
	(112, 'ROSIANE SILVA E SILVA', 'ROSIANE SILVA E SILVA', 2, 'AV. D. PEDRO Nº 29', NULL, NULL, '69.040-040', '14906557000109', '(92)9235-6410', '', '', '', '', ''),
	(113, 'TUPI LOCAÇÕES E SERVIÇOS TECNICOS EIRELI', 'TUPI LOCAÇÕES E SERVIÇOS TECNICOS EIRELI', 2, 'RUA SÃO LUIZ Nº 80', NULL, NULL, '69.057-050', '08840355000109', '(92)9136-5590', '', '', '', '', ''),
	(114, 'ELIVALDO DA SILVA PENA', 'ELIVALDO DA SILVA PENA', 2, 'AV COSME FERREIRA Nº 3468', NULL, NULL, '69.083-000', '03965386000164', '(92)9138-1672', '', '', '', '', ''),
	(115, 'EMPRESA BRASILEIRA DE CORREIOS E TELEGRAFOS', 'EMPRESA BRASILEIRA DE CORREIOS E TELEGRAFOS', 2, 'RUA PARA Nº 885 ED. JOSE FROTA II', NULL, NULL, '', '34028316000375', '(92)3621-8495', '', '', '', 'LUQUESIA MARIA DAS DORES MUSTAFA PAES DE LEMOS', '119.478.102-00'),
	(116, 'SOL AZUL COMERCIO E SERVIÇOS ', 'SOL AZUL COMERCIO E SERVIÇOS ', 2, 'RUA MARISOL Nº 99 ', NULL, NULL, '69.020-400', '02088658000122', '(92)9114-8332', '04.135.434-6', '7880101', '', '', ''),
	(117, 'MAILSON LIMA DE ALMEIDA', 'MAILSON LIMA DE ALMEIDA', 2, 'RUA JOSE AUGUSTO DE QUEIROZ Nº 234', NULL, NULL, '', '17428783000101', '(92)8833-7172', '', '20858701', '', '', ''),
	(118, 'L S C DA SILVA PRADO', 'L S C DA SILVA PRADO', 2, 'RUA FLORIANO PEIXOTO Nº 133', NULL, NULL, '', '09308343000192', '(92)3622-3706', '04.224.819-1', '12371901', '', '', ''),
	(119, 'MARCA BRASIL COMERCIO E SERV. GRAFICOS', 'MARCA BRASIL COMERCIO E SERV. GRAFICOS', 2, 'RUA BELO HORIZONTE Nº 903', NULL, NULL, '69.057-060', '06889405000109', '', '', '', '', '', ''),
	(120, 'PRODAM PROCESSAMENTO DE DADOS DO AMAZONAS', 'PRODAM PROCESSAMENTO DE DADOS DO AMAZONAS', 2, 'RUA JONATHAS PEDROSA Nº 1937', NULL, NULL, '69.020-110', '04407920000180', '', '05.341.162-5', '', '', '', ''),
	(121, 'A. J. DE SOUZA ALMEIDA E CIA', 'A. J. DE SOUZA ALMEIDA E CIA', 2, 'RUA CAREIRO Nº 32', NULL, NULL, '', '18173135000114', '(92)3213-4857', '', '', '', '', ''),
	(122, 'RESULT COMÉRCIO DE MATERIAL DE EXPEDIENTE', 'RESULT COMÉRCIO DE MATERIAL DE EXPEDIENTE', 2, 'RUA NEVES DA FONTOURA Nº 95', NULL, NULL, '69.057-495', '05120773000125', '(92)9172-0322', '', '', '', '', ''),
	(123, 'D. Q. DUTRA', 'D. Q. DUTRA', 2, 'AV H Nº 28 TERREO', NULL, NULL, '69.042-190', '05006594000161', '(92)3238-4543', '', '10314301', '', '', ''),
	(124, 'CICLO SERVIÇOS DE TRATAMENTO DE RES E SOLUÇÕES AMBIENTAIS', 'CICLO SERVIÇOS DE TRATAMENTO DE RES E SOLUÇÕES AMBIENTAIS', 2, 'RUA MAGALHÃES BARATA Nº 91', NULL, NULL, '', '14024953000102', '(92)9102-8564', '', '', '', '', ''),
	(125, 'INTERNEXT  REDE DIGITAL AMAZONICA LTDA', 'INTERNEXT  REDE DIGITAL AMAZONICA LTDA', 2, 'AV DJALMA BATISTA Nº 1661', NULL, NULL, '69.050-010', '00957830000101', '', '04.107.101-8', '7396501', '', '', ''),
	(126, 'CONTEÚDO AGENCIA DE PUBLICIDADE LTDA', 'CONTEÚDO AGENCIA DE PUBLICIDADE LTDA', 2, 'RUA SALVADOR Nº 120 SL 1208 ED.VIEIRALVES BUSINESS', NULL, NULL, '69.057-040', '20248960000182', '', '', '21402001', '', '', ''),
	(127, ' E P  MONTEIRO COMÉRCIO E SERVIÇOS', ' E P  MONTEIRO COMÉRCIO E SERVIÇOS', 2, 'RUA SALDANHA MARINHO Nº 765 SALA 03', NULL, NULL, '69.010-040', '14634236000194', '(92)9166-7259', '', '', '', '', ''),
	(128, 'E MOTA DA COSTA', 'E MOTA DA COSTA', 2, 'RUA COSTA AZEVEDO Nº09 SALA  301', NULL, NULL, '69.010-230', '19338980000165', '', '', '', '', '', ''),
	(129, 'DILSON MARCOS KOVALKI-ME', 'DILSON MARCOS KOVALKI-ME', 2, 'RUA SANTA HELENA Nº 18', NULL, NULL, '', '04017759000138', '', '', '', '', '', ''),
	(130, 'A DE C VENTURELLI-EPP', 'A DE C VENTURELLI-EPP', 2, 'RUA SÃO JOSÉ Nº 31', NULL, NULL, '69.075-451', '02595192000151', '', '', '', '', '', ''),
	(131, 'JONAS SABINO DA COSTA-ME', 'JONAS SABINO DA COSTA-ME', 2, 'RUA TERRA NOVA Nº 81', NULL, NULL, '69.085-080', '13127077000179', '', '', '13712001', '', '', ''),
	(132, 'LEANDRO OLIVEIRA SILVA-ME', 'LEANDRO OLIVEIRA SILVA-ME', 2, 'RUA JOÃO ALFREDO Nº 457 C ANDAR 1 SALA 115', NULL, NULL, '', '19041289000115', '', '', '20990301', 'COMERCIO VAREGISTA', '', ''),
	(133, 'M DE A MARQUES E CIA LTDA', 'M DE A MARQUES E CIA LTDA', 2, 'AV 7 DE SETEMBRO Nº 1768 SALA  C2', NULL, NULL, '', '07884579000141', '', '04.227.594-6', '113807041', '', '', ''),
	(134, 'M M C COMERCIO E SERVIÇOS DE IMPRESSÕES LTDA', 'M M C COMERCIO E SERVIÇOS DE IMPRESSÕES LTDA', 2, 'AV DJALMA BATISTA Nº 39', NULL, NULL, '69.050-010', '09279345000109', '(92)3642-0086', '04.223.180-9', '12134101', '', '', ''),
	(135, 'AMAZONAS COPIADORAS LTDA ', 'AMAZONAS COPIADORAS LTDA ', 2, 'AV TEFÉ Nº 315', NULL, NULL, '', '01657353000121', '(92)2127-6154', '', '', '', '', ''),
	(136, 'PODIUM COMERCIO DE PNEUS AUTO CENTER LTDA', 'PODIUM COMERCIO DE PNEUS AUTO CENTER LTDA', 2, 'AV DJALMA BATISTA Nº 535', NULL, NULL, '69.085-000', '07153962000120', '(92)3248-7153', '', '11235802', '', '', ''),
	(137, ' F B BARRETO COMÉRCIO DE ALIMENTOS', ' F B BARRETO COMÉRCIO DE ALIMENTOS', 2, 'RUA NATAL Nº 300', NULL, NULL, '69.057-090', '11536156000108', '', '04.226.622-0', '20118401', '', '', ''),
	(138, 'LIMA E GOMES LTDA', 'LIMA E GOMES LTDA', 2, 'RUA SOLIMÕES Nº 439 A SALA 02', NULL, NULL, '', '17962811000176', '', '05.338.308-7', '20622201', '', '', ''),
	(139, 'JONAS UCHOA SALUSTIANO', 'JONAS UCHOA SALUSTIANO', 1, 'RUA SANTO ANTENEDORO Nº 500 COD. ELEG. VILLAGE', NULL, NULL, '', '03137929253', '', '', '', '', '', ''),
	(140, 'P D CARNEIRO EPP', 'P D CARNEIRO EPP', 2, 'AV AUTAZ MIRIM Nº 5483', NULL, NULL, '69.085-000', '02651415000150', '(92)3249-1626', '', '9225601', '', '', ''),
	(141, 'MAPEMI BRASIL MATERIAIS MÉDICOS E ODONTOLOGICOS LTDA', 'MAPEMI BRASIL MATERIAIS MÉDICOS E ODONTOLOGICOS LTDA', 2, 'AV PEDRO TEIXEIRA Nº 2204', NULL, NULL, '69.040-000', '84487131000135', '', '', '6347401', '', '', ''),
	(142, 'A R RODRIGUEZ & CIA LTDA', 'A R RODRIGUEZ & CIA LTDA', 2, 'AV JOAQUIM NABUCO Nº 2235', NULL, NULL, '69.020-031', '04562591000141', '', '', '469901', '', '', ''),
	(143, 'EMPRESA BRASILEIRA DE TELECOMUNICAÇOES', 'EMPRESA BRASILEIRA DE TELECOMUNICAÇOES', 2, 'RUA EMILIO MOREIRA Nº 605', NULL, NULL, '69.020-040', '33530486000129', '', '', '', '', '', ''),
	(144, 'NUBIA GOMES EIRELI-ME', 'NUBIA GOMES EIRELI-ME', 2, 'RUA NEVES DA FONTOURA Nº 95', NULL, NULL, '69.057-495', '19945996000136', '', '', '', '', '', ''),
	(145, 'SUPLEX SERV DE MANT DE EQUIP DE REFRIG. LTDA', 'SUPLEX SERV DE MANT DE EQUIP DE REFRIG. LTDA', 2, 'AV ALVARO MAIA Nº 346 2º ANDAR SALA 06', NULL, NULL, '69.025-070', '04465383000124', '', '', '', '', '', ''),
	(146, 'DISTRIBUIDORA E COMERCIO ECLIPS LTDA', 'DISTRIBUIDORA E COMERCIO ECLIPS LTDA', 2, 'RUA MONSENHOR COUTINHO Nº 126 SALA 202', NULL, NULL, '69.010-110', '00689453000177', '(92)3308-7840', '', '', '', '', '');
/*!40000 ALTER TABLE `fornecedor` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.itemadesaoata
DROP TABLE IF EXISTS `itemadesaoata`;
CREATE TABLE IF NOT EXISTS `itemadesaoata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adesao_fk` int(10) NOT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `sequencial_item` int(11) DEFAULT NULL,
  `valor_unitario` float DEFAULT NULL,
  `unidade` int(11) NOT NULL,
  `produto_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `processo_compra_fk` (`adesao_fk`),
  KEY `fk_itemadesaoata_tipounidade1_idx` (`unidade`),
  KEY `fk_itemadesaoata_produto1_idx` (`produto_fk`),
  CONSTRAINT `fk_itemadesaoata_produto1` FOREIGN KEY (`produto_fk`) REFERENCES `produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_itemadesaoata_tipounidade1` FOREIGN KEY (`unidade`) REFERENCES `tipounidade` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `num_processo_compra` FOREIGN KEY (`adesao_fk`) REFERENCES `adesao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ITEMADESAOATA.REM';

-- Copiando dados para a tabela novo_sisel2.itemadesaoata: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `itemadesaoata` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemadesaoata` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.itemlicitacao
DROP TABLE IF EXISTS `itemlicitacao`;
CREATE TABLE IF NOT EXISTS `itemlicitacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `licitacao_fk` varchar(18) NOT NULL,
  `sequencial` int(16) NOT NULL,
  `quantidade` float NOT NULL,
  `produto_fk` int(11) NOT NULL,
  `dt_publicacao` date NOT NULL,
  `dt_assinatura` date NOT NULL,
  `unidade_fk` int(11) NOT NULL,
  `status_item_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_itemlicitacao_licitacao` (`licitacao_fk`),
  KEY `fk_itemlicitacao_status_item1_idx` (`status_item_fk`),
  KEY `fk_itemlicitacao_tipounidade1_idx` (`unidade_fk`),
  KEY `fk_itemlicitacao_produto1_idx` (`produto_fk`),
  CONSTRAINT `FK_itemlicitacao_licitacao` FOREIGN KEY (`licitacao_fk`) REFERENCES `licitacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_itemlicitacao_produto1` FOREIGN KEY (`produto_fk`) REFERENCES `produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_itemlicitacao_status_item1` FOREIGN KEY (`status_item_fk`) REFERENCES `status_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_itemlicitacao_tipounidade1` FOREIGN KEY (`unidade_fk`) REFERENCES `tipounidade` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.itemlicitacao: ~42 rows (aproximadamente)
/*!40000 ALTER TABLE `itemlicitacao` DISABLE KEYS */;
INSERT INTO `itemlicitacao` (`id`, `licitacao_fk`, `sequencial`, `quantidade`, `produto_fk`, `dt_publicacao`, `dt_assinatura`, `unidade_fk`, `status_item_fk`) VALUES
	(3, 'DL2424-2013', 1, 1, 3, '2014-01-22', '2014-01-02', 21, 1),
	(4, 'DL2437-2013', 1, 1, 4, '2014-01-02', '2014-01-02', 21, 1),
	(5, 'IL2418-2013', 1, 1, 5, '2014-01-22', '2014-01-02', 22, 1),
	(6, 'IL2420-2013', 1, 1, 6, '2014-01-22', '2014-01-02', 22, 1),
	(7, 'IL2421-2013', 1, 1, 7, '2014-01-22', '2014-01-02', 22, 1),
	(8, 'IL2422-2013', 1, 1, 8, '2014-01-22', '2014-01-02', 21, 1),
	(9, 'IL2423-2013', 1, 1, 9, '2014-01-22', '2014-01-02', 21, 1),
	(10, 'IL2425-2013', 1, 1, 10, '2014-01-22', '2014-01-02', 21, 1),
	(11, 'IL2427-2013', 1, 1, 11, '2014-01-22', '2014-01-02', 21, 1),
	(12, 'IL2428-2013', 1, 1, 12, '2014-01-22', '2014-01-02', 21, 1),
	(13, 'IL2429-2013', 1, 1, 13, '2014-01-22', '2014-01-02', 22, 1),
	(14, 'DL0247-2014', 1, 1, 14, '2014-02-24', '2014-02-24', 21, 1),
	(15, 'DL0159-2014', 1, 12, 15, '2014-02-17', '2014-02-14', 22, 1),
	(16, 'IL0189-2014', 1, 1, 16, '2014-02-17', '2014-02-14', 22, 1),
	(20, 'DL0246-2014', 1, 4, 29, '2014-03-26', '2014-03-18', 21, 1),
	(21, 'RP0003-2014', 1, 800, 41, '2014-04-08', '2014-04-08', 22, 1),
	(22, 'RP0003-2014', 2, 800, 46, '2014-04-08', '2014-04-08', 22, 1),
	(23, 'RP0003-2014', 3, 3000, 45, '2014-04-08', '2014-04-08', 22, 1),
	(24, 'RP0004-2014', 1, 12, 42, '2014-04-01', '2014-03-31', 21, 1),
	(25, 'CC0004-2014', 1, 7, 47, '2014-04-22', '2014-04-10', 22, 1),
	(26, 'CC0004-2014', 2, 1, 48, '2014-04-22', '2014-04-10', 22, 1),
	(27, 'CC0004-2014', 3, 1, 49, '2014-04-22', '2014-04-10', 22, 1),
	(28, 'CC0004-2014', 4, 1, 50, '2014-04-22', '2014-04-10', 22, 1),
	(29, 'CC0004-2014', 5, 1, 51, '2014-04-22', '2014-04-10', 22, 1),
	(30, 'CC0004-2014', 6, 1, 52, '2014-04-22', '2014-04-10', 22, 1),
	(31, 'CC0004-2014', 7, 1, 53, '2014-04-22', '2014-04-10', 22, 1),
	(32, 'CC0004-2014', 8, 1, 54, '2014-04-22', '2014-04-10', 22, 1),
	(33, 'CC0004-2014', 9, 4, 55, '2014-04-22', '2014-04-10', 22, 1),
	(34, 'CC0004-2014', 10, 2, 56, '2014-04-22', '2014-04-10', 22, 1),
	(35, 'CC0004-2014', 11, 1, 57, '2014-04-22', '2014-04-10', 22, 1),
	(36, 'CC0004-2014', 12, 20, 58, '2014-04-22', '2014-04-10', 22, 1),
	(37, 'RP0001-2014', 1, 3, 34, '2014-04-14', '2014-04-08', 21, 1),
	(38, 'RP0001-2014', 2, 3, 35, '2014-04-14', '2014-04-08', 21, 1),
	(39, 'RP0001-2014', 3, 3, 36, '2014-04-14', '2014-04-08', 21, 1),
	(40, 'RP0001-2014', 4, 3, 37, '2014-04-14', '2014-04-08', 21, 1),
	(41, 'RP0001-2014', 5, 3, 38, '2014-04-14', '2014-04-08', 21, 1),
	(42, 'RP0001-2014', 6, 3, 39, '2014-04-14', '2014-04-08', 21, 1),
	(43, 'RP0001-2014', 7, 3, 40, '2014-04-14', '2014-04-08', 21, 1),
	(44, 'IL0594-2014', 1, 1, 43, '2014-04-24', '2014-04-01', 22, 1),
	(45, 'DL0712-2014', 1, 1, 30, '2014-04-28', '2014-04-25', 21, 1),
	(47, 'CC0003-2014', 1, 222, 265, '2015-03-05', '2015-03-02', 8, 1),
	(48, 'CC0003-2014', 2, 223, 203, '2015-03-23', '2015-03-01', 22, 3);
/*!40000 ALTER TABLE `itemlicitacao` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.licitacao
DROP TABLE IF EXISTS `licitacao`;
CREATE TABLE IF NOT EXISTS `licitacao` (
  `id` varchar(18) NOT NULL,
  `modalidade_fk` int(11) NOT NULL,
  `tipo_itemlote_fk` int(11) NOT NULL,
  `objetivo` varchar(300) NOT NULL,
  `valor_bloqueado` float NOT NULL,
  `numero_edital` varchar(16) DEFAULT NULL,
  `numero_diario` varchar(16) DEFAULT NULL,
  `dt_publicacao` date DEFAULT NULL,
  `dias` int(11) DEFAULT NULL,
  `dt_limite` date DEFAULT NULL,
  `responsavel` varchar(50) NOT NULL,
  `ano_empenho` int(4) DEFAULT NULL,
  `nome_arquivo` varchar(60) NOT NULL,
  `numempenho` varchar(10) DEFAULT NULL,
  `numempenho2` varchar(10) DEFAULT NULL,
  `numempenho3` varchar(10) DEFAULT NULL,
  `numempenho4` varchar(10) DEFAULT NULL,
  `numempenho5` varchar(10) DEFAULT NULL,
  `numempenho6` varchar(10) DEFAULT NULL,
  `numempenho7` varchar(10) DEFAULT NULL,
  `numempenho8` varchar(10) DEFAULT NULL,
  `numempenho9` varchar(10) DEFAULT NULL,
  `numempenho10` varchar(10) DEFAULT NULL,
  `numempenho11` varchar(10) DEFAULT NULL,
  `numempenho12` varchar(10) DEFAULT NULL,
  `ano_exercicio` int(11) NOT NULL,
  `mes_empenho` enum('01','02','03','04','05','06','07','08','09','10','11','12','') NOT NULL,
  `usuario` int(11) NOT NULL,
  `data_criacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_licitacao_tipolicitacao1_idx` (`modalidade_fk`),
  KEY `fk_licitacao_tipo_itemlote1_idx` (`tipo_itemlote_fk`),
  KEY `fk_licitacao_usuario1_idx` (`usuario`),
  KEY `fk_licitacao_ano_exercicio1_idx` (`ano_exercicio`),
  CONSTRAINT `fk_licitacao_ano_exercicio1` FOREIGN KEY (`ano_exercicio`) REFERENCES `ano_exercicio` (`ano`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_licitacao_tipolicitacao1` FOREIGN KEY (`modalidade_fk`) REFERENCES `tipomodalidade` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_licitacao_tipo_itemlote1` FOREIGN KEY (`tipo_itemlote_fk`) REFERENCES `tipo_itemlote` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_licitacao_usuario1` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.licitacao: ~46 rows (aproximadamente)
/*!40000 ALTER TABLE `licitacao` DISABLE KEYS */;
INSERT INTO `licitacao` (`id`, `modalidade_fk`, `tipo_itemlote_fk`, `objetivo`, `valor_bloqueado`, `numero_edital`, `numero_diario`, `dt_publicacao`, `dias`, `dt_limite`, `responsavel`, `ano_empenho`, `nome_arquivo`, `numempenho`, `numempenho2`, `numempenho3`, `numempenho4`, `numempenho5`, `numempenho6`, `numempenho7`, `numempenho8`, `numempenho9`, `numempenho10`, `numempenho11`, `numempenho12`, `ano_exercicio`, `mes_empenho`, `usuario`, `data_criacao`) VALUES
	('CC0003-2014', 1, 1, 'SERVIÇO DE GERENCIAMENTO TECNICO E MANUT. PREV.CORRET.NO SISTEMA TELEFÔNICO INTERNO', 79860, 'PC2399/2014', '135', '2011-05-11', 1411, '2015-03-22', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'CC0003-2014.PDF', '111', '222222', '33333', '44444', '55555', '66666', '7777', '8888', '99999', '10101010', '1111', '12121212', 2014, '03', 19, '2015-02-17 14:50:49'),
	('CC0004-2014', 1, 1, 'AQUISIÇÃO DE EQUIPAMENTOS DE CLIMATIZAÇÃO E OUTROS EQUIPAMENTOS PARA MODERNIZAÇÃO DA SALA DE CINEMA', 78721.8, 'PC 0149/2014', '148', '2014-04-22', 21, '2014-04-01', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'CC0004-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '04', 19, '2015-02-17 14:50:49'),
	('CC0006-2014', 1, 1, 'LOCAÇÃO DE QUATRO VEÍCULOS AUTOMOTORES', 79800, 'PC2462-2013', '156', '2014-05-06', 21, '2014-04-15', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'CC0006-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '05', 19, '2015-02-17 14:50:49'),
	('CC0007-2014', 1, 1, 'LOCAÇÃO DE EQUIPAMENTOS DE SONORIZAÇÃO', 78993, 'PC0363-2014', '180', '2014-07-02', 58, '2014-05-05', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'CC0007-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '07', 19, '2015-02-17 14:50:49'),
	('CC0010-2014', 1, 1, 'AQUISIÇÃO DE MATERAIL ELETRICO E ELETRONICO', 52428.2, 'PC0050-2014', '181', '2014-07-03', 34, '2014-05-30', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'CC0010-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '07', 19, '2015-02-17 14:50:49'),
	('CC0011-2014', 1, 1, 'CONTRATAR PESSOA JURÍDICA ESP. EM CONTROLE PATRIMONIAL PUBLICO P IMPL DE SIST DE AVALIAÇÃO,REAVALIAÇÃO, CORRE.MONET.', 75000, 'PC 088-2014', '218', '2014-09-24', 54, '2014-08-01', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'CC0011-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '09', 19, '2015-02-17 14:50:49'),
	('CC0012-2014', 1, 1, 'AQUISIÇÃO DE MATERIAL ODONTOLOGICO', 15810.7, 'PC 205-2014', '208', '2014-09-03', 44, '2014-07-21', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'CC0012-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '09', 19, '2015-02-17 14:50:49'),
	('CC0016-2014', 1, 1, 'LOCAÇÃO DE SOFTWARE RENOV.DE 350 LICENÇAS DO USO DE ANTIVIRUS', 18900, 'PC 0569-2014', '214', '2014-09-18', 15, '2014-09-03', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'CC0016-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '09', 19, '2015-02-17 14:50:49'),
	('CC0019-2014', 1, 1, 'AQUISIÇÃO DE MATERIAL DE EXPEDIENTE', 79065.9, 'PC 0843/2014', '216', '2014-09-22', 19, '2014-09-03', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'CC0019-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '09', 19, '2015-02-17 14:50:49'),
	('CC0020-2014', 1, 1, 'AQUISIÇÃO DE TONNER P IMPRESSORAS', 71800, 'PC1363-2014', '233', '2014-10-27', 2, '2014-10-29', 'JOAO BOSCO GOMES SARAIVA', 2014, 'CC0020-2014.PDF', '00945', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '10', 19, '2015-02-17 14:50:49'),
	('CC0021-2014', 1, 1, 'LOCAÇÃO VEÍCULOS UTILITARIOS COM CONDUTOR P DESLOCAMENTO DE EQUP CINEMA', 51000, 'PC1472 2014', '230', '2014-10-21', 6, '2014-10-15', 'JOAO BOSCO GOMES SARAIVA', 2014, 'CC0021-2014.PDF', '00938', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '10', 19, '2015-02-17 14:50:49'),
	('CC0023-2014', 1, 1, 'AQUISIÇÃO DE EQUIPAMENTOS P DEPARTAMENTO DE SAUDE,SEG E MED TRABALHO', 19207.4, 'PC1814-2014', '245', '2014-11-17', 11, '2014-11-06', 'JOAO BOSCO GOMES SARAIVA', 2014, 'CC0023-2014.PDF', '00976', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '11', 19, '2015-02-17 14:50:49'),
	('CC0027-2014', 1, 1, 'SERVIÇO DE ENGENHARIA MANUTENÇAO PREV E CORRET DO SISTEMA DE COMBATE A INCENDIO E POÇO ARTESIANO', 137880, 'PC1989-2014', 'DIÁRIO', '2014-12-10', 12, '2014-11-28', 'JOAO BOSCO GOMES SARAIVA', 2014, 'CC0027-2014.PDF', '01004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '12', 19, '2015-02-17 14:50:49'),
	('CO0001-2014', 5, 1, 'asdf afasdf', 1231230, 'wer', '23', '2015-02-01', 13, '2015-02-14', 'JOÃO BOSCO GOMES SARAIVA', 2015, 'CO0001-2014.pdf', '234', '', '', '', '', '23', '', '', '', '', '', '', 2014, '02', 19, '2015-02-28 17:18:37'),
	('DL0107-2014', 8, 1, 'SERVIÇLO TELEFONICO FIXO COMUTADO STFC RECEBER CHAMADAS LOCAIS E INTERNACIONAIS', 103902, 'PC0107-2014', 'DIÁRIO', '2014-12-23', 31, '2014-11-22', 'JOAO BOSCO GOMES SARAIVA', 2014, 'DL0107-2014.PDF', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '12', 19, '2015-02-17 14:50:49'),
	('DL0159-2014', 8, 1, 'ASSINATURA PARA ACESSO AOS SERVIÇOS DO SISTEMA BANCO DE PREÇOS-FERRAMENTA PESQ.', 7990, 'pc159-2014', '115', '2014-02-17', 3, '2014-02-14', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'DL0159-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '02', 19, '2015-02-17 14:50:49'),
	('DL0246-2014', 8, 1, 'SERVIÇO DE PIANISTA PROFISSIONAL', 8000, 'PC 0246-2014', '135', '2014-03-26', 8, '2014-03-18', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'DL0246-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '03', 19, '2015-02-17 14:50:49'),
	('DL0247-2014', 8, 1, 'SERVIÇO DE CONSERVAÇÃO E LIMPEZA NO PRÉDIO SEDE', 192000, 'pc247-2014', '119', '2014-02-24', 0, '2014-02-24', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'DL0247-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '02', 19, '2015-02-17 14:50:49'),
	('DL0712-2014', 8, 1, 'SERVIÇO DE LIMPEZA E CONSERVAÇÃO, JARDINAGEM, COPEIRAGEM', 704106, 'PC0712/2014', '152', '2014-04-28', 3, '2014-04-25', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'DL0712-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '04', 19, '2015-02-17 14:50:49'),
	('DL1146-2014', 8, 1, 'SERVIÇO DE MANUT PREV CORRET EM INFRAESTRUTURA PREDIAL', 370859, 'PC1146-2014', '179', '2014-07-01', 0, '2014-07-01', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'DL1146-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '07', 19, '2015-02-17 14:50:49'),
	('DL1898-2014', 8, 1, 'SERVIÇO DE LIMPEZA E CONSERVAÇÃO,JARDINAGEM,COPEIRAGEM DO PREDIO SEDE DA CMM', 704106, 'PC 1898-2014', '234', '2014-10-22', 0, '2014-10-22', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'DL1898-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '10', 19, '2015-02-17 14:50:49'),
	('DL2415-2014', 8, 1, 'SERVIÇO DE MANUT PREV CORRET EM INFRAESTRUTURA PREDIAL', 370859, 'PC2415-2014', 'DIÁRIO', '2014-12-29', 0, '2014-12-29', 'JOAO BOSCO GOMES SARAIVA', 2014, 'DL2415-2014.PDF', '01257', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '12', 19, '2015-02-17 14:50:49'),
	('DL2424-2013', 8, 1, 'CONSUMO DE ENERGIA ELETRICA', 600000, 'PC 2424/2013', '104', '2014-01-22', 0, '0000-00-00', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'DL2424-2013.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '01', 19, '2015-02-17 14:50:49'),
	('DL2437-2013', 8, 1, 'SERVIÇO MANUT. PREV.CORRETIVA EM INFRESTRUTURA PREDIAL', 309049, 'PC 2437/2013', '098', '2014-01-02', 0, '2014-01-02', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'DL2437-2013.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '01', 19, '2015-02-17 14:50:49'),
	('FR0010-2014', 17, 1, 'objetivo testeobjetivo', 1500, '333', '2222', '2015-02-01', 12, '2015-02-13', 'JOÃO BOSCO GOMES SARAIVA', 2015, 'FR0010-2014.pdf', '123', '321', '', '', '', '', '', '', '', '', '', '', 2014, '02', 19, '2015-02-28 17:59:01'),
	('IL0189-2014', 9, 1, 'ASSINATURA DE ORIENTAÇÃO POR ESCRITO EM LICITAÇÕES', 8211, 'pc189-2014', '115', '2014-02-17', 3, '2014-02-14', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'IL0189-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '02', 19, '2015-02-17 14:50:49'),
	('IL0594-2014', 9, 1, 'ASSINATURA DO JORNAL A CRITICA', 26400, 'PC0594/2014', '150', '2014-04-24', 23, '2014-04-01', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'IL0594-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '04', 19, '2015-02-17 14:50:49'),
	('IL1746-2014', 9, 1, 'SERVIÇO DE PUBLICAÇÃO NO JORNAL AGORA', 15000, 'PC1746-2014', '220', '2014-09-29', 6, '2014-09-23', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'IL1746-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '09', 19, '2015-02-17 14:50:49'),
	('IL2418-2013', 9, 1, 'ASSINATURA DO JORNAL AMAZONAS EM TEMPO', 26400, 'PC 2418/2013', '104', '2014-01-22', 0, '0000-00-00', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'IL2418-2013.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '01', 19, '2015-02-17 14:50:49'),
	('IL2420-2013', 9, 1, 'ASSINATURA DO JORNAL DIÁRIO DO AMAZONAS', 20400, 'PC 2420/2013', '104', '2014-01-22', 0, '0000-00-00', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'IL2420-2013.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '01', 19, '2015-02-17 14:50:49'),
	('IL2421-2013', 9, 1, 'ASSINATURA DO JORNAL DO COMMERCIO LTDA', 23100, 'PC 2421/2013', '104', '2014-01-22', 0, '0000-00-00', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'IL2421-2013.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '01', 19, '2015-02-17 14:50:49'),
	('IL2422-2013', 9, 1, 'FORNECIMENTO DE ÁGUA PARA AS DEPENDÊNCIAS DA CMM', 15000, 'PC 2422/2013', '104', '2014-01-22', 0, '0000-00-00', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'IL2422-2013.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '01', 19, '2015-02-17 14:50:49'),
	('IL2423-2013', 9, 1, 'PUBLICAÇÃO NO JORNAL AMAZONAS EM TEMPO', 25000, 'PC 2423/2013', '104', '2014-01-22', 0, '0000-00-00', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'IL2423-2013.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '01', 19, '2015-02-17 14:50:49'),
	('IL2425-2013', 9, 1, 'PUBLICAÇÃO NO JORNAL A CRITICA', 25000, 'PC 2425/2013', '104', '2014-01-22', 0, '0000-00-00', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'IL2425-2013.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '01', 19, '2015-02-17 14:50:49'),
	('IL2427-2013', 9, 1, 'PUBLICAÇÃO NO JORNAL DIÁRIO DO AMAZONAS', 25000, 'PC 2427/2013', '104', '2014-01-22', 0, '0000-00-00', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'IL2427-2013.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '01', 19, '2015-02-17 14:50:49'),
	('IL2428-2013', 9, 1, 'PUBLICAÇÃO NO JORNAL DO COMMERCIO', 25000, 'PC 2428/2013', '104', '2014-01-22', 0, '0000-00-00', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'IL2428-2013.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '01', 19, '2015-02-17 14:50:49'),
	('IL2429-2013', 9, 1, 'ASSINATURA DOS BOLETINS DE DIREITO MUNICIPAL ADMINISTRATIVO', 25350, 'PC 2429/2013', '104', '2014-01-22', 0, '0000-00-00', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'IL2429-2013.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '01', 19, '2015-02-17 14:50:49'),
	('RP0001-2014', 12, 1, 'CONTRATAÇÃO DE PESSOAS JURIDICAS ESP. NA PRESTAÇÃO DE SERVIÇOS TECNICOS DE TECNOLOGIA DA INFORMAÇÃO E TELECOMUNICAÇÃO', 982500, 'PC 0072/2014', '145', '2014-04-14', 6, '2014-04-08', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'RP0001-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '04', 19, '2015-02-17 14:50:49'),
	('RP0003-2014', 12, 1, 'SERVIÇO DE FORNECIMENTO DE ÁGUA MINERAL', 25090, 'PC2446/2013', '141', '2014-04-08', 18, '2014-03-21', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'RP0003-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '04', 19, '2015-02-17 14:50:49'),
	('RP0004-2014', 12, 1, 'SERVIÇO DE AGENCIAMENTO DE PASSAGENS AÉREAS', 79000, 'PC 2439/2013', '139', '2014-04-01', 4, '2014-03-28', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'RP0004-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '04', 19, '2015-02-17 14:50:49'),
	('RP0005-2014', 12, 1, 'PRESTAÇÃO DE SERVIÇOS EM GERENCIAMENTO OPERACIONAL DA ESTAÇÃO DE TRATAMENTO DE EFLUENTES', 83400, 'PC0085-2014', '195', '2014-08-05', 18, '2014-07-18', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'RP0005-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '08', 19, '2015-02-17 14:50:49'),
	('RP0006-2014', 12, 1, 'SERVIÇOS GRÁFICOS PARA EVENTOS, FESTIVIDADES,HOMENAGENS', 397100, 'PC2469-2013', '186', '2014-07-16', 16, '2014-06-30', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'RP0006-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '07', 19, '2015-02-17 14:50:49'),
	('RP0007-2014', 12, 1, 'AQUISIÇÃO DE GENEROS ALIMENTICIOS', 61482.5, 'PC0160-2014', '159', '2014-06-10', 18, '2014-05-23', 'JOÃO BOSCO GOMES SARAIVA', 2014, 'RP0007-2014.PDF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '06', 19, '2015-02-17 14:50:49'),
	('RP0008-2014', 12, 1, 'AQUISIÇÃO DE GASOLINA COMUM, ÓLEO DIESEL E GÁS DE COZINHA', 55260, 'PC2373-2013', '224', '2014-10-08', 0, '2014-10-08', 'JOAO BOSCO GOMES SARAIVA', 2014, 'RP0008-2014.PDF', '00911', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '10', 19, '2015-02-17 14:50:49'),
	('RP0009-2014', 12, 1, 'SERVIÇO DE REPOGRAFIA E LOCAÇÃO DE IMPRESSORAS MULTIFUNCIONAIS', 213240, 'PC1226-2014', '230', '2014-10-21', 1, '2014-10-22', 'JOAO BOSCO GOMES SARAIVA', 2014, 'RP0009-2014.PDF', '00935', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '10', 19, '2015-02-17 14:50:49'),
	('RP0010-2014', 12, 1, 'SERVIÇO MANUT PREVENT E CORRET E HIGIENIZAÇÃO DO SISTEMA DE AR CONDICIONADOS', 583200, 'PC0037-2014', 'DIÁRIO', '2014-12-03', 6, '2014-12-09', 'JOAO BOSCO GOMES SARAIVA', 2014, 'RP0010-2014.PDF', '01002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014, '12', 19, '2015-02-17 14:50:49');
/*!40000 ALTER TABLE `licitacao` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.participanteconvenio
DROP TABLE IF EXISTS `participanteconvenio`;
CREATE TABLE IF NOT EXISTS `participanteconvenio` (
  `id` varchar(16) NOT NULL,
  `usuario_fk` int(11) NOT NULL,
  `esfera_fk` int(1) NOT NULL,
  `valorparticipacao` float DEFAULT NULL,
  `percentual` float DEFAULT NULL,
  `numinss` varchar(16) DEFAULT NULL,
  `dtinss` date DEFAULT NULL,
  `dtvalidadeinss` date DEFAULT NULL,
  `numfgts` varchar(60) DEFAULT NULL,
  `dtfgts` date DEFAULT NULL,
  `dtvalidadefgts` date DEFAULT NULL,
  `numcertidaofe` varchar(60) DEFAULT NULL,
  `dtemissaofe` date DEFAULT NULL,
  `dtvencimentofe` date DEFAULT NULL,
  `numcertidaofm` varchar(60) DEFAULT NULL,
  `dtemissaofm` date DEFAULT NULL,
  `dtvencimentofm` date DEFAULT NULL,
  `numcertidaoff` varchar(60) DEFAULT NULL,
  `dtemissaoff` date DEFAULT NULL,
  `dtvencimentoff` date DEFAULT NULL,
  `numcertidaoutras` varchar(60) DEFAULT NULL,
  `dtemissaoutras` date DEFAULT NULL,
  `dtvencimentoutras` date DEFAULT NULL,
  `numcertidaocndt` varchar(60) DEFAULT NULL COMMENT 'CERTIFICADO CNDT',
  `dtemissaocndt` date DEFAULT NULL COMMENT 'DATA REGULARIDADE CNDT',
  `dtvencimentocndt` date DEFAULT NULL COMMENT 'DATA VALIDADE CNDT',
  PRIMARY KEY (`id`),
  KEY `fk_participanteconvenio_tipoesferaconveniado1_idx` (`esfera_fk`),
  KEY `fk_participanteconvenio_usuario1_idx` (`usuario_fk`),
  CONSTRAINT `fk_participanteconvenio_tipoesferaconveniado1` FOREIGN KEY (`esfera_fk`) REFERENCES `tipoesferaconveniado` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_participanteconvenio_usuario1` FOREIGN KEY (`usuario_fk`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.participanteconvenio: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `participanteconvenio` DISABLE KEYS */;
/*!40000 ALTER TABLE `participanteconvenio` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.participante_certidao
DROP TABLE IF EXISTS `participante_certidao`;
CREATE TABLE IF NOT EXISTS `participante_certidao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `licitacao_fk` varchar(18) NOT NULL,
  `fornecedor_fk` int(11) NOT NULL,
  `tipoparticipante_fk` int(11) NOT NULL,
  `num_certidao` varchar(60) DEFAULT NULL,
  `dt_emissao` date DEFAULT NULL,
  `dt_validade` date DEFAULT NULL,
  `dias` int(11) DEFAULT NULL,
  `tipocertidao_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_itemlicitacao_licitacao` (`licitacao_fk`),
  KEY `fk_participante_tipoparticipante1_idx` (`tipoparticipante_fk`),
  KEY `fk_participante_fornecedor1_idx` (`fornecedor_fk`),
  KEY `fk_participante_certidao_tipocertidao1_idx` (`tipocertidao_fk`),
  CONSTRAINT `fk_participante_certidao_tipocertidao1` FOREIGN KEY (`tipocertidao_fk`) REFERENCES `tipocertidao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_participante_fornecedor1` FOREIGN KEY (`fornecedor_fk`) REFERENCES `fornecedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_participante_licitacao` FOREIGN KEY (`licitacao_fk`) REFERENCES `licitacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_participante_tipoparticipante1` FOREIGN KEY (`tipoparticipante_fk`) REFERENCES `tipoparticipante` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.participante_certidao: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `participante_certidao` DISABLE KEYS */;
/*!40000 ALTER TABLE `participante_certidao` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.perfil
DROP TABLE IF EXISTS `perfil`;
CREATE TABLE IF NOT EXISTS `perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.perfil: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` (`id`, `perfil`) VALUES
	(1, 'Administrador'),
	(2, 'Operador'),
	(3, 'Consulta');
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.produto
DROP TABLE IF EXISTS `produto`;
CREATE TABLE IF NOT EXISTS `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(300) NOT NULL,
  `tipoproduto_fk` int(11) NOT NULL,
  `unidade_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_produto_tipoproduto1_idx` (`tipoproduto_fk`),
  KEY `fk_produto_tipounidade1_idx` (`unidade_fk`),
  CONSTRAINT `fk_produto_tipoproduto1` FOREIGN KEY (`tipoproduto_fk`) REFERENCES `tipoproduto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_produto_tipounidade1` FOREIGN KEY (`unidade_fk`) REFERENCES `tipounidade` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=267 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.produto: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
INSERT INTO `produto` (`id`, `descricao`, `tipoproduto_fk`, `unidade_fk`) VALUES
	(1, 'SERVIÇO DE CONSUMO DE ENERGIA ELETRICA', 2, 21),
	(2, 'SERVIÇO DE MANUT. EM  INFRAESTRUTURA PREDIAL', 2, 21),
	(3, 'ASSINATURA NO JORNAL AMAZONAS EM TEMPO', 2, 21),
	(4, 'ASSINATURA NO JORNAL DIÁRIO DO AMAZONAS', 2, 21),
	(5, 'ASSINATURA NO JORNAL DO COMERCIO', 2, 21),
	(6, 'SERVIÇO FORNECIMENTO DE ÁGUA', 2, 21),
	(7, 'PUBLICAÇÃO NO JORNAL AMAZONAS EM TEMPO', 2, 21),
	(8, 'PUBLICAÇÃO NO JORNAL A CRITICA', 2, 21),
	(9, 'PUBLICAÇÃO NO JORNAL DIÁRIO DO AMAZONAS', 2, 21),
	(10, 'PUBLICAÇÃO NO JORNAL DO COMÉRCIO', 2, 21),
	(11, 'ASSINATURA DOS BOLETINS DE DIREITO  MUNICIPAL ADM', 2, 21),
	(12, 'SERVIÇO DE CONSERVAÇÃO E LIMPEZA NO PRÉDIO SEDE', 2, 21),
	(13, 'ASSINATURA PARA ACESSO AOS SERVIÇOS DO SISTEMA BANCO DE PREÇOS-FERRAMENTA', 2, 21),
	(14, 'ASSINATURA DE ORIENTAÇÃO POR ESCRITO EM LICITAÇÕES', 2, 21),
	(15, 'SERVIÇOS DE ADEQ. INTERNAS NA ÁREA DA EXPANSÃO NO 2º ANDAR DO PREDIO SEDE DA CMM', 2, 21),
	(16, 'TELEVISÃO DE 55 TELA PLANA', 1, 22),
	(17, 'SUPORTE PARA TV DE 55 RETRATIL', 1, 22),
	(18, 'SERVIÇO DE PIANISTA PROFISSIONAL', 2, 21),
	(19, 'SERVIÇO DE LIMPEZA E CONSERVAÇÃO,JARDINAGEM,COPEIRAGEM ', 2, 21),
	(20, 'SERVIÇO DE GERENCIA SUPORTE TEC.MANUT.SIST TELEFÔNICO', 2, 21),
	(21, 'AQUISIÇÃO DE MATERIAIS E EQUIPAMENTOS PARA MANUTENÇÃO FUNCIONAL ', 1, 22),
	(22, 'SERVIÇO DE MANUT.PRETEV E CORRETIV. INFRAESTRUTURA DE REDE LÓGICA', 2, 21),
	(23, 'SERVIÇO DE MANUT.PREV E CORRETIV DOS SERVIDORES LINUX E SISTEMA', 2, 21),
	(24, 'SERVÇO DE MANUT. PREV.CORRETIV.EM EQUIPAMENTOS DE INFORMATICA', 2, 21),
	(25, 'SERVIÇO DE MANUTENÇÃO CORRET.PREVENT. EM COMPUTADORES,NOTEBOOKS,TABLETS', 2, 21),
	(26, 'MANUTENÇÃO PREVENT.E CORRETIVA DO LINK PONTO A PONTO DA CMM', 2, 21),
	(27, 'SERVIÇO DE MANUT.PREVE. E CORRET. PONTOS DE TV DA CMM', 1, 22),
	(28, 'SERVIÇO DE MANUT.PREV.CORRET. EM SISTEMA GERENCIAMENTO DE BANCO DE DADOS', 1, 22),
	(29, 'AGUA MINERAL POTAVEL COM GÁS DE 350ML', 1, 22),
	(30, 'SERVIÇOS DE AGENCIAMENTO DE PASSAGENS ÁEREAS', 2, 21),
	(31, 'ASSINATURAS DO JORNAL A CRITICA', 1, 22),
	(32, 'AGUA MINERAL POTAVEL GALÃO DE 20 LITROS', 1, 22),
	(33, 'AGUA MINERAL POTAVEL SEM GÁS DE 350ML', 1, 22),
	(34, 'UNIDADE DE REFRIGERAÇÃO DE AMBIENTE TIPO CONSOLE PISO', 1, 22),
	(35, 'TELA DE PROJEÇÃO RETRÁTIL', 1, 22),
	(36, 'PROJETOR COM ENTRADA HDMI', 1, 22),
	(37, 'RACK PISO 19 40X800MM', 1, 22),
	(38, 'CAIXA PARA 08 DISJUNTORES PEQUENA', 1, 22),
	(39, 'APARELHO DE BLUE- RAY COM HDMI', 1, 22),
	(40, 'SUPORTE DE TETO PARA PROJETOR', 1, 22),
	(41, 'SWITCH HDMI DE 04 ENTRADAS', 1, 22),
	(42, 'ESTOFADO DE 03 LUGARES', 1, 22),
	(43, 'MESA DE CENTRO EM MDF', 1, 22),
	(44, 'MESA RETANGULAR DE REUNIÃO', 1, 22),
	(45, 'CADEIRAS EXECUTIVAS SEM BRAÇO', 1, 22),
	(46, 'LOCAÇÃO DE QUATRO VEÍCULOS AUTOMOTORES', 2, 21),
	(47, 'LOCAÇÃO DE EQUIPAMENTOS DE SONORIZAÇÃO', 2, 21),
	(48, 'CABO PP 2X1,50MM FLEXIVEL 450/750V START', 1, 22),
	(49, 'CABO DE REDE DE 2M PACIFIC NET', 1, 22),
	(50, 'CONECTOR XLR MACHO LINHA 3 POLOS S.ANGELO', 1, 22),
	(51, 'CABO P/MICROFONE STEREO PRETO SC30 S.ANGELO', 1, 22),
	(52, 'CAIXA DE SOM ATIVA 100WATTS RMS', 1, 22),
	(53, 'MESA DE SOM DIGITAL 32 CANAIS', 1, 22),
	(54, 'PROCESSADOR DE SINAL 3 VIAS ESTÉREO', 1, 22),
	(55, 'ROTEADOR WIRELES 3G 2.4GHZ', 1, 22),
	(56, 'TABLET 10 C SITEMA OPERACIONAL IOS', 1, 22),
	(57, 'AÇUCAR CRISTAL EMB DE 30 x 1KG', 1, 22),
	(58, 'CEFÉ TORRADO MOIDO  EMB 20 X 250G', 1, 22),
	(59, 'PLACA COMEMORATIVA EM AÇO INOX ASSI 304 MED  28X19CM', 1, 22),
	(60, 'MEDALHAS COR OURO EM LATÃO ESTAMPADO FRENTE E VERSO MED 55MM', 1, 22),
	(61, 'MEDALHAS COR OURO EM METAL ESTAMPADO FRENTE E VERSO FORMATO ESTRELA 45MMT MED 55MM', 1, 22),
	(62, 'DIPLOMA EM AÇO INOX ASI 304X100MM', 1, 22),
	(63, 'PLAQUETAS DE METAL DOURADO ESTAM RECORT FORMATO BRASÃO DO MUNICIPIO', 1, 22),
	(64, 'DIPLOMA EM PAPEL COLLOR PLUS 300G MAJORCA CAPA DURA        E HOT STAM ', 1, 22),
	(65, 'DIPLOMA EM PAPEL COLLOR PLUS 300G MAJORCA  HOT STAM', 1, 22),
	(66, 'BANNER EM LONA VINILICA FOSCA 440G 090X120M RETO E SECO', 1, 22),
	(67, 'BANNER EM LONA VINILICA FOSCA 440G 080X100M RETO E SECO', 1, 22),
	(68, 'FAIXA EM LONA FOSCA 440G MED 300MMX100M ', 1, 22),
	(69, 'CAMISA EM MEIA MALHA GOLA CARECA MANGA CURTA FIO 301 CORES E TAMAN', 1, 22),
	(70, 'PRESTAÇÃO DE SERVIÇOS EM GERENCIAMENTO OP DA ESTAÇÃO DE TRATAMENTO DE EFLUENTES', 2, 21),
	(71, 'COPO DESCARTÁVEL MATERIAL PLÁSTICO CAPACIDADE 200ML', 1, 22),
	(72, 'SERVIÇO DE PUBLICAÇÃO NO JORNAL AGORA', 2, 21),
	(73, 'ÁCIDO PARA CONDICIONAMENTO CLOREXIDINA', 1, 22),
	(74, 'ADESIVO DENTÁRIO PARA DENTINA E ESMALTE FOTOPOLIMERISÁVEL COM FLUOR', 1, 22),
	(75, 'ÁGUA OXIGENADA 10 VOLUMES', 1, 22),
	(76, 'AGULHA GENGIVAL CURTA', 1, 22),
	(77, 'ALCOOL A 70º LITRO', 1, 22),
	(78, 'ANESTÉSICO LOCAL LIDOCAÍNA 2% C EPNEFRINA', 1, 22),
	(79, 'ANESTESICO LOCAL LIDOCAÍNA 3% C EPNEFRINA', 1, 22),
	(80, 'ANESTESICO LOCAL LIDOCAÍNA 4% C EPINEFRINA', 1, 22),
	(81, 'ANESTESICO LOCAL LIDOCAINA 3% C OCTAPRESSIN', 1, 22),
	(82, 'BABADOR DESCARTAVEL', 1, 22),
	(83, 'BROCA DIAMANTADA Nº 1111', 1, 22),
	(84, 'BROCA DIAMANTADA Nº 3118', 1, 22),
	(85, 'BROCA DIAMANTADA Nº 3168', 1, 22),
	(86, 'BROCA DIAMANTADA Nº 2143', 1, 22),
	(87, 'BROCA DIAMANTADA Nº 3145', 1, 22),
	(88, 'BROCA DIAMANTADA Nº 3101', 1, 22),
	(89, 'BROCA DIAMANDATA Nº 4102', 1, 22),
	(90, 'BROCA DIAMANTADA Nº 1011', 1, 22),
	(91, 'BROCA DIAMANTADA Nº 1013', 1, 22),
	(92, 'BROCA DIAMANTADA Nº 1015', 1, 22),
	(93, 'BROCA DIAMANTADA Nº 1012 HL', 1, 22),
	(94, 'BROCA DIAMANTADA Nº 1014 HL', 1, 22),
	(95, 'BROCA DIAMANTADA Nº 1016 HL', 1, 22),
	(96, 'BROCA DIAMANTADA Nº 1090', 1, 22),
	(97, 'BROCA DIAMANTADA Nº 1091', 1, 22),
	(98, 'BROCA DIAMANTADA Nº 1093', 1, 22),
	(99, 'BROCA DIAMANDATA Nº 1190', 1, 22),
	(100, 'BROCA DIAMANTADA Nº 2200', 1, 22),
	(101, 'BRODA DIAMANTADA Nº 3038', 1, 22),
	(102, 'BROCA DIAMANTADA Nº 1334', 1, 22),
	(103, 'BROCA DIAMANTADA Nº 1302', 1, 22),
	(104, 'BROCA DIAMANTADA Nº 1342', 1, 22),
	(105, 'BROCA SHO-FU ESFERICA', 1, 22),
	(106, 'BROCA SHO-FU CILINDRICA', 1, 22),
	(107, 'BROCA SHO-FU CHAMA', 1, 22),
	(108, 'BROCA MAXCUT', 1, 22),
	(109, 'BROCA MINICUT', 1, 22),
	(110, 'BROCA GATTES GLINDEN 1,2,3', 1, 22),
	(111, 'BROCA DE ACABAMENTO KIT FINO  E EXTRAFINO', 1, 22),
	(112, 'CARBONO', 1, 22),
	(113, 'CIMENTO DE HIDROXIDO DE CALCIO FORRADOR', 1, 22),
	(114, 'CLOREXIDINE SOLUÇÃO AQUOSA A 2%', 1, 22),
	(115, 'CLOREXIDINE SOLUÇÃO AQUOSA A 0,12%', 1, 22),
	(116, 'COLGADURA', 1, 22),
	(117, 'DEGERMANTE COM CLOREXIDINE 2%', 1, 22),
	(118, 'DISCO DE LIXA DE PAPELÃO SORTIDO 12MM', 1, 22),
	(119, 'DISCO FELTRO SORTIDO PARA ACABAMENTO DE RESINA', 1, 22),
	(120, 'ESCOVA TIPO ROBSON', 1, 22),
	(121, 'ESPELHO BUCAL', 1, 22),
	(122, 'EUGENOL', 1, 22),
	(123, 'FIO PARA SUTURA 4/0 NAYLON', 1, 22),
	(124, 'FIO PARA SUTURA 3.0 CAT GUT', 1, 22),
	(125, 'FIO PARA SUTURA 3.0 SEDA', 1, 22),
	(126, 'FITA MATRIZ POLIESTER', 1, 22),
	(127, 'FITA TESTE PARA AUTOCLAVE', 1, 22),
	(128, 'FLUOR TOPICO GEL 1 MINUTO', 1, 22),
	(129, 'GASE 13 FIOS COM 500 UNIDADES', 1, 22),
	(130, 'GORRO COM ELASTICO COM 100 UNIDADES', 1, 22),
	(131, 'IONOMETRO DE VIDRO COM FLUOR', 1, 22),
	(132, 'JACARÉ (FIXADOR PARA BABADOR)', 1, 22),
	(133, 'LAMINA DE BISTURI Nº 15', 1, 22),
	(134, 'LUVA DE PROCEDIMENTO TAM P C 100 UNID', 1, 22),
	(135, 'LUVA DE PROCEDIEMNTO TAM M C 100 UND', 1, 22),
	(136, 'LUVA DE PROCEDIMENTO TAM G COM 100 UND', 1, 22),
	(137, 'MASCARA DESCARTAVEIS COM ELASTICO', 1, 22),
	(138, 'MATRIZ DE AÇO Nº 05', 1, 22),
	(139, 'OCULOS DE PROTEÇÃO', 1, 22),
	(140, 'OLEO SILICONIZADOR PARA ALTA/BAIXA ROTAÇÃO', 1, 22),
	(141, 'OXIDO DE ZINCO', 1, 22),
	(142, 'PASTA PROFILATICA TUTI-FRUTTI', 1, 22),
	(143, 'PELICULA RODIOGRAFICA RX', 1, 22),
	(144, 'POMADA ANESTESICA BENZACAINA', 1, 22),
	(145, 'PORTA DETRITOS', 1, 22),
	(146, 'RESINA FOOTOPOLIMERIZAVEL COM FLUOR A1 ESMALTE', 1, 22),
	(147, 'RESINA FOTOPOLIMERIZAVEL COM FLUOR A2 ESMALTE', 1, 22),
	(148, 'RESINA FOTOPOLIMERIZAVEL COM FLUOR A3 ESMALTE', 1, 22),
	(149, 'RESINA FOTOPOLIMERIZAVEL COM FLUOR A3,5 ESMALTE', 1, 22),
	(150, 'RESINA FOTOPOLIMERIZAVEL COM FLUOR B1 ESMALTE', 1, 22),
	(151, 'RESINA FOTOPOLIMERIZAVEL COM FLUOR B2 ESMALTE', 1, 22),
	(152, 'RESINA FOTÓPOLIMERIZAVEL COM FLUOR  B3 ESMALTE', 1, 22),
	(153, 'RESINA FOTOPOLIMERIZAVEL COM FLUOR A1 DENTINA', 1, 22),
	(154, 'RESINA FOTOPOLIMERIZAVEL COM FLUOR A2 DENTINA', 1, 22),
	(155, 'RESINA FOTOPOLIMERIZAVEL FLOW A2', 1, 22),
	(156, 'ROLO DE FIO DENTAL DE 500 METROS', 1, 22),
	(157, 'SOLUÇÃO A BASE DE GLUTARALDEIDO A 2%', 1, 22),
	(158, 'SUGADOR DESCARTAVEL COM 100 UNID', 1, 22),
	(159, 'TIRA ABRASIVAS', 1, 22),
	(160, 'TIRA DE AÇO ABRASIVAS PARA AMALGAMA 6 MM', 1, 22),
	(161, 'APONTADOR PLASTICO RETANGULAR TAM PEQ', 1, 22),
	(162, 'BARBANTE DE ALGODÃO 8 FIOS', 1, 22),
	(163, 'BORRACHA EM LÁTEX NATURAL', 1, 22),
	(164, 'CAIXA ARQUIVO MORTO EM POLIONDA', 1, 22),
	(165, 'CANETA ESFEROGRAFICA NA COR AZUL', 1, 22),
	(166, 'CANETA ESFEROGRAFICA NA COR PRETA', 1, 22),
	(167, 'CANETA ESFEROGRAFICA  NA COR VERMELHA', 1, 22),
	(168, 'CANETA MARCADORA PERMANENTE PARA CD/DVD NA COR AZUL', 1, 22),
	(169, 'CANETA MARCADORA PERMANENTE PARA CD/DVD NA COR PRETA', 1, 22),
	(170, 'CANETA MARCADORA PERMANENTE PARA CD/DVD NA COR VERMELHA', 1, 22),
	(171, 'CAPA PARA ENCARDERNAÇÃO EM PVC CRISTAL OU FUME', 1, 22),
	(172, 'CD-R OPTICA GRAVAVEL', 1, 22),
	(173, 'CLIPES DE METAL NIQUELADO Nº 3/0 EM AÇO', 1, 22),
	(174, 'CLIPES DE METAL NIQUELADO Nº 4/0 EM AÇO', 1, 22),
	(175, 'CLIPES DE METAL NIQUELADO Nº8/0 EM AÇO', 1, 22),
	(176, 'COLA BRANCA TUBO DE 90GR', 1, 22),
	(177, 'COLA EM BASTÃO 8GR', 1, 22),
	(178, 'COLA P/ISOPOR 40GR NÃO TÓXICA', 1, 22),
	(179, 'CLIPES DE METAL NIQUELADO Nº 2/0 EM AÇO', 1, 22),
	(180, 'COLCHETE LATONIANO Nº 08', 1, 22),
	(181, 'CONTRA CAPA PARA ENCADERNAÇÃO EM PVC ROGOSO', 1, 22),
	(182, 'CORRETIVO LIQUIDO A BASE DÁGUA', 1, 22),
	(183, 'DVD R ÓPTICA GRAVAVEL', 1, 22),
	(184, 'ETIQUETA AUTO-ADESIVA EM FOLHA A-4', 1, 22),
	(185, 'ETIQUETA AUTO-ADESIVA EM FOLHA A-4 CX COM 25 FL 30P FOLHA', 1, 22),
	(186, 'EXTRATOR DE GRAMPO TIPO ESPATULA EM AÇO INOXIDAVEL', 1, 22),
	(187, 'FITA ADESIVA DUREX TRANSPARENTE', 1, 22),
	(188, 'FITA ADESIVA GOMADA TIPO PAPEL', 1, 22),
	(189, 'GARRAFA TERMICA CAP DE 1 LITRO', 1, 22),
	(190, 'GARRAFA TERMICA DE PRESSÃO CAP. 1,8 LITROS', 1, 22),
	(191, 'GRAMPEADOR DE PAPEL PEQUENO', 1, 22),
	(192, 'GRAMPO DE METAL PARA GRAMPEADOR  26/6', 1, 22),
	(193, 'LAPIS DE MADEIRA', 1, 22),
	(194, 'PAPEL SULFITE TAM A-4', 1, 22),
	(195, 'PASTA A-Z LOMBO ESTREITO', 1, 22),
	(196, 'PASTA A-Z LOMBO LARGO', 1, 22),
	(197, 'PASTA ESCACELA COM ELASTICO', 1, 22),
	(198, 'PASTA ESCARELA COM TRILHO', 1, 22),
	(199, 'PASTA ENRUGADA POLIONDA COM ABAS', 1, 22),
	(200, 'PASTA SUSPENSA C/PRENDEDOR TRILHO', 1, 22),
	(201, 'PINCEL MARCA TEXTO', 1, 22),
	(202, 'PILHA ALCALINA PEQUENA TAM AA', 1, 22),
	(203, 'PILHA ALCALINA PEQUENA TAM AAA', 1, 22),
	(204, 'RÉGUA PLÁSTICA ', 1, 22),
	(205, 'CONTRATAR PESSOA JURÍDICA ESP. EM CONTROLE PATRIMONIAL PUBLICO P IMPL DE SIST DE AVALIAÇÃO,REAVALIAÇÃO, CORRE.MONET.', 1, 22),
	(206, 'LOCAÇÃO DE SOFTWARE RENOV.DE 350 LICENÇAS DO USO DE ANTIVIRUS', 1, 22),
	(207, 'SERVIÇO DE ASSISTENCIA TECNICA, MANUTENÇÃO PREV CORRETIOCA EM QUATRO ELEVADORES UMA PLATAFORMA', 2, 21),
	(208, 'SERVIÇO DE REPOGRAFIA E LOCAÇÃO DE IMPRESSORAS MULTIFUNCIONAIS', 2, 21),
	(209, 'GASOLINA COMUM', 1, 22),
	(210, 'ÓLEO DIESEL S-10', 1, 22),
	(211, 'CARGA DE GÁS GLP 13', 1, 22),
	(212, 'SERVIÇO DE LOCAÇÃO DE VEÍCULO TIPO UTILITARIO COM CONDUTOR P DESLOC DOS EQUIP CINEMA', 2, 21),
	(213, 'TONNER 285A', 1, 22),
	(214, 'TONNER 12A', 1, 22),
	(215, 'TONNER 6000A', 1, 22),
	(216, 'TONNER 6001A', 1, 22),
	(217, 'TONNER 6002A', 1, 22),
	(218, 'TONNER 6003A', 1, 22),
	(219, 'TONNER 540A', 1, 22),
	(220, 'TONNER 541A', 1, 22),
	(221, 'TONNER 542A', 1, 22),
	(222, 'TONNER 543A', 1, 22),
	(223, 'TONNER 116S', 1, 22),
	(224, 'CARTUCHO 45A', 1, 22),
	(225, 'DIVÃ PARA EXAME CLINICO', 1, 22),
	(226, 'MARTELO PARA REFLEXO', 1, 22),
	(227, 'FORNO DE BIER COM TERMOSTATO', 1, 22),
	(228, 'BOLA PARA FISIOTERAPIA', 1, 22),
	(229, 'THERA BAND AZUL (medio forte)', 1, 22),
	(230, 'THERA BANDE CINZA (super forte)', 1, 22),
	(231, 'THERA BAND LARANJA (extra forte)', 1, 22),
	(232, 'THERA BAND ROXO (forte)', 1, 22),
	(233, 'NEGASTOCOPIO P/VISUALIZAR RAIO X', 1, 22),
	(234, 'INFRA VERMELHO C/PEDESTAL 110VOLTS', 1, 22),
	(235, 'ELETROESTIMULADOR FES+TENS 4 CANAIS', 1, 22),
	(236, 'AMALGAMADOR', 1, 22),
	(237, 'PESOS ALTERES 1KG', 1, 22),
	(238, 'COMPRESSOR DE AR 40 LITROS', 1, 22),
	(239, 'SELADOR PARA GRAU CIRURGICO', 1, 22),
	(240, 'COLCHONETE PARA FISIOTERAPIA', 1, 22),
	(241, 'SERVIÇO TELEFONICO FIXO COMUTADO STFC CHAMADAS LOCAIS NACIONAIS E INTERNACIONAIS', 2, 21),
	(242, 'SERVIÇO DE ENGENHARIA PARA MANT PREV CORRET SIST COMBATE INCENDIO E POÇO ARTESIANO', 2, 21),
	(243, 'SERVIÇO DE MANUT PREVET CORRET E HIGIENIZAÇÃO DO SISTEMA DE AR CONDICIONADOS', 2, 21),
	(263, 'PILHA ALCALINA PEQUENA TAM AABCDEFGHIJKLM', 1, 22),
	(264, 'ANA CAROLINA DONATO SALAZAR', 1, 22),
	(265, 'LUIZ EDUARDO DONATO SALAZAR WANDERLEY', 1, 22),
	(266, 'TV DE LED DE NUM SEI QUANTAS POLEGADAS', 1, 22);
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.publicacao
DROP TABLE IF EXISTS `publicacao`;
CREATE TABLE IF NOT EXISTS `publicacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `licitacao_fk` varchar(18) NOT NULL,
  `sequencial` int(11) NOT NULL,
  `dt_publicacao` date NOT NULL,
  `veiculo_comunicacao` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_publicacao_licitacao` (`licitacao_fk`),
  CONSTRAINT `FK_publicacao_licitacao` FOREIGN KEY (`licitacao_fk`) REFERENCES `licitacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.publicacao: ~44 rows (aproximadamente)
/*!40000 ALTER TABLE `publicacao` DISABLE KEYS */;
INSERT INTO `publicacao` (`id`, `licitacao_fk`, `sequencial`, `dt_publicacao`, `veiculo_comunicacao`) VALUES
	(1, 'DL2424-2013', 1, '2014-01-22', 'DIÁRIO ELETRÔNICO DA CMM Nº 104'),
	(2, 'DL2437-2013', 1, '2014-01-02', 'DIÁRIO ELETRÔNICO DA CMM nº 098'),
	(3, 'IL2418-2013', 1, '2014-01-22', 'DIÁRIO ELETRÔNICO DA CMM Nº 104'),
	(4, 'IL2420-2013', 1, '2014-01-22', 'DIÁRIO ELETRÔNICO DA CMM nº 104'),
	(5, 'IL2421-2013', 1, '2014-01-22', 'DIÁRIO ELETRÔNICO DA CMM Nº 104'),
	(6, 'IL2422-2013', 1, '2014-01-22', 'DIÁRIO ELETRÔNICO DA CMM Nº 104'),
	(7, 'IL2423-2013', 1, '2014-01-22', 'DIÁRIO ELETRÔNICO DA CMM Nº 104'),
	(8, 'IL2425-2013', 1, '2014-01-22', 'DIÁRIO ELETRÔNICO DA CMM Nº 104'),
	(9, 'IL2427-2013', 1, '2014-01-22', 'DIÁRIO ELETRÔNICO DA CMM Nº 104'),
	(10, 'IL2428-2013', 1, '2014-01-22', 'DIÁRIO ELETRÔNICO DA CMM Nº 104'),
	(11, 'IL2429-2013', 1, '2014-01-22', 'DIÁRIO ELETRÔNICO DA CMM Nº 104'),
	(12, 'DL0247-2014', 1, '2014-02-24', 'DIÁRIO ELETRÔNICO DA CMM Nº 119'),
	(13, 'DL0159-2014', 1, '2014-02-17', 'DIÁRIO ELETRÔNICO DA CMM Nº 115'),
	(14, 'IL0189-2014', 1, '2014-02-17', 'DIÁRIO ELETRÔNICO DA CMM Nº 115'),
	(15, 'CC0002-2015', 1, '2014-02-07', 'DIÁRIO ELETRÔNICO DA CMM Nº 112'),
	(16, 'CC0002-2014', 1, '2014-02-26', 'DIÁRIO ELETRÔNICO DA CMM Nº 121'),
	(17, 'DL0246-2014', 1, '2014-03-26', 'DIÁRIO ELETRÔNICO DA CMM Nº 135'),
	(18, 'RP0003-2014', 1, '2014-04-08', 'DIÁRIO ELETRÔNICO DA CMM Nº 141'),
	(19, 'RP0004-2014', 1, '2014-04-01', 'DIÁRIO ELETRÔNICO DA CMM Nº 139'),
	(20, 'CC0004-2014', 1, '2014-04-22', 'DIÁRIO ELETRÔNICO DA CMM Nº 148'),
	(21, 'RP0001-2014', 1, '2014-04-14', 'DIÁRIO ELETRÔNICO DA CMM Nº 145'),
	(22, 'IL0594-2014', 1, '2014-04-24', 'DIÁRIO ELETRÔNICO DA CMM Nº 150'),
	(23, 'CC0003-2014', 1, '2014-04-01', 'DIÁRIO ELETRÔNICO DA CMM Nº 135'),
	(24, 'DL0712-2014', 1, '2014-04-28', 'DIÁRIO ELETRÔNICO DA CMM Nº 152'),
	(26, 'CC0006-2014', 1, '2014-05-06', 'DIÁRIO ELETRÔNICO DA CMM'),
	(27, 'CC0007-2014', 1, '2014-07-02', 'DIÁRIO ELETRÔNICO DA CMM Nº 180'),
	(28, 'DL1146-2014', 1, '2014-07-01', 'DIÁRIO ELETRÔNICO DA CMM Nº 179'),
	(29, 'CC0010-2014', 1, '2014-07-03', 'DIÁRIO ELETRÔNICO DA CMM Nº 181'),
	(30, 'RP0006-2014', 1, '2014-07-16', 'DIÁRIO ELETRÔNICO DA CMM Nº 186'),
	(31, 'RP0007-2014', 1, '2014-06-10', 'DIÁRIO ELETRÔNICO DA CMM Nº 174'),
	(32, 'RP0005-2014', 1, '2014-08-05', 'DIÁRIO ELETRÔNICO DA CMM Nº 195'),
	(33, 'CC0012-2014', 1, '2014-09-03', 'DIÁRIO ELETRÔNICO DA CMM Nº 208'),
	(34, 'CC0016-2014', 1, '2014-09-18', 'DIÁRIO ELETRÔNICO DA CMM Nº 214'),
	(35, 'CC0011-2014', 1, '2014-09-10', 'DIÁRIO ELETRÔNICO DA CMM Nº 211'),
	(36, 'CC0019-2014', 1, '2014-09-22', 'DIÁRIO ELETRÔNICO DA CMM Nº 216'),
	(37, 'CC0021-2014', 1, '2014-10-21', 'DIÁRIO ELETRÔNICO DA CMM Nº230'),
	(38, 'CC0020-2014', 1, '2014-10-27', 'DIÁRIO ELETRÔNICO DA CMM Nº 233'),
	(39, 'RP0009-2014', 1, '2014-10-21', 'DIÁRIO ELETRÔNICO DA CMM Nº 230'),
	(40, 'RP0008-2014', 1, '2014-10-08', 'DIÁRIO ELETRÔNICO DA CMM Nº 224'),
	(41, 'CC0023-2014', 1, '2014-11-17', 'DIÁRIO ELETRÔNICO DA CMM Nº 245'),
	(42, 'DL2415-2014', 1, '2014-12-29', 'DIÁRIO ELETRÔNICO DA CMM Nº 262'),
	(43, 'DL0107-2014', 1, '2014-12-23', 'DIÁRIO ELETRÔNICO DA CMM Nº 261'),
	(44, 'CC0027-2014', 1, '2014-12-10', 'DIÁRIO ELETRÔNICO DA CMM Nº 255'),
	(45, 'RP0010-2014', 1, '2014-12-03', 'DIÁRIO ELETRÔNICO DA CMM Nº 252');
/*!40000 ALTER TABLE `publicacao` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.status_item
DROP TABLE IF EXISTS `status_item`;
CREATE TABLE IF NOT EXISTS `status_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) DEFAULT NULL,
  `descricao` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.status_item: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `status_item` DISABLE KEYS */;
INSERT INTO `status_item` (`id`, `status`, `descricao`) VALUES
	(1, 'Homologado', NULL),
	(2, 'Deserto', NULL),
	(3, 'Fracassado', NULL),
	(4, 'Cancelado', NULL),
	(5, 'Anulado/Revogado', '(TODA A LICITAÇÃO FOI ANULADA)');
/*!40000 ALTER TABLE `status_item` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tipoata
DROP TABLE IF EXISTS `tipoata`;
CREATE TABLE IF NOT EXISTS `tipoata` (
  `id` int(2) NOT NULL,
  `descricao` varchar(50) DEFAULT NULL,
  `sigla` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.tipoata: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipoata` DISABLE KEYS */;
INSERT INTO `tipoata` (`id`, `descricao`, `sigla`) VALUES
	(1, 'Adesão ata própria (PARTICIPANTE)', 'AAP'),
	(2, 'Adesão ata externa (CARONA)', 'AAE');
/*!40000 ALTER TABLE `tipoata` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tipocertidao
DROP TABLE IF EXISTS `tipocertidao`;
CREATE TABLE IF NOT EXISTS `tipocertidao` (
  `id` int(11) NOT NULL,
  `certidao` varchar(50) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela novo_sisel2.tipocertidao: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `tipocertidao` DISABLE KEYS */;
INSERT INTO `tipocertidao` (`id`, `certidao`) VALUES
	(1, 'INSS'),
	(2, 'Federal'),
	(3, 'Estadual'),
	(4, 'Municipal'),
	(5, 'FGTS'),
	(6, 'CAM'),
	(7, 'CNDT'),
	(99, 'Outras Certidões');
/*!40000 ALTER TABLE `tipocertidao` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tipocontrato
DROP TABLE IF EXISTS `tipocontrato`;
CREATE TABLE IF NOT EXISTS `tipocontrato` (
  `idtipocontrato` int(10) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(61) DEFAULT NULL,
  `sigla` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`idtipocontrato`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela novo_sisel2.tipocontrato: ~44 rows (aproximadamente)
/*!40000 ALTER TABLE `tipocontrato` DISABLE KEYS */;
INSERT INTO `tipocontrato` (`idtipocontrato`, `descricao`, `sigla`) VALUES
	(1, 'TERMO DE CONTRATO', 'CT'),
	(2, 'TERMO ADITIVO AO CONTRATO', 'TACT'),
	(3, 'TERMO DE RE-RATIFICADO DE COTNRATO', 'TRRT'),
	(4, 'TERMO DE DISTRATO DE CONTRATO', 'TDCT'),
	(5, 'TERMO DE RESCISAO DE CONTRATO', 'TRCT'),
	(6, 'TERMO DE CONCESSAO DE USO', 'TCU'),
	(7, 'TERMO DE ADITIVO DE CONCESSAO DE USO', 'TACU'),
	(8, 'TERMO DE PERMISSAO DE USO', 'TPU'),
	(9, 'TERMO ADITIVO DE PERMISSAO DE USO', 'TAPU'),
	(10, 'TERMO DE AUTORIZACAO DE USO', 'TAU'),
	(11, 'TERMO ADITIVO DE AUTORIZACAO DE USO', 'TAAU'),
	(12, 'TERMO DE CESSAO', 'TC'),
	(13, 'TERMO ADITIVO A CESSAO', 'TAC'),
	(14, 'TERMO DE COMPROMISSO', 'TCO'),
	(15, 'TERMO ADITIVO AO COMPROMISSO', 'TACO'),
	(16, 'TERMO DE DIREITO REAL DE USO', 'TDRU'),
	(17, 'TERMO ADITIVO AO DIREITO REAL DE USO', 'TADU'),
	(18, 'TERMO DE DOACAO', 'TD'),
	(19, 'CARTA CONTRATO', 'CACT'),
	(20, 'ORDEM DE SERVICOS', 'OS'),
	(21, 'TERMO ADITIVO A ORDEM DE SERVICOS', 'TAOS'),
	(22, 'TERMO DE REVOGACAO DE AUTORIZACAO DE USO', 'TRTA'),
	(23, 'TERMO DE ADESAO AO CONTRATO', 'TA'),
	(24, 'TERMO DE OUTORGA', 'TOU'),
	(25, 'TERMO ADITIVO DE OUTORGA', 'TAOU'),
	(26, 'TERMO DE EX-OFICIO', 'TEXO'),
	(27, 'TERMO ADITITVO DE CARTA CONTRATO', 'TACC'),
	(28, 'TERMO DE COOPERACAO TECNICA', 'TCT'),
	(29, 'TERMO ADITIVO DE COOPERACAO TECNICA', 'ATCT'),
	(30, 'TERMO DE ORDEM DE SERVICOS', 'TOS'),
	(31, 'TERMO DE RECEBIMENTO DE AUXILIO ALUGUEL', 'TRAA'),
	(32, 'TERMO DE RECEBIMENTO DE CHEQUE MORADIA', 'TRCM'),
	(33, 'TERMO DE RECEBIMENTO DE INDENIZACAO', 'TRIN'),
	(34, 'TERMO DE QUITACAO DE CONTRATO', 'TQCT'),
	(35, 'PROTOCOLO DE INTENCOES', 'PINT'),
	(36, 'TERMO ADITIVO DE PROTOCOLO DE INTENCOES', 'TAPI'),
	(37, 'TERMO ADITIVO DE DOACAO', 'TADO'),
	(38, 'APOSTILA DE RETIFICACAO DE CONTRATO', 'ARCT'),
	(39, 'TERMO DE CONTRATO DE GESTAO', 'TCGE'),
	(40, 'TERMO ADITIVO DE CONTRATO DE GESTAO', 'TACG'),
	(41, 'TERMO DE RESCISAO DE CESSAO', 'TREC'),
	(42, 'TERMO DE APOSTILAMENTO DE CONTRATO', 'TAPC'),
	(43, 'APOLICE DE CONTRATACAO DE SERVICOS DE SEGURO', 'ACSS'),
	(44, 'TERMO ADITIVO DE APOLICE DE CONTRATACAO DE SERVICOS DE SEGURO', 'TACS');
/*!40000 ALTER TABLE `tipocontrato` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tipoconvenio
DROP TABLE IF EXISTS `tipoconvenio`;
CREATE TABLE IF NOT EXISTS `tipoconvenio` (
  `idtipo` int(10) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) DEFAULT NULL,
  `sigla` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`idtipo`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela novo_sisel2.tipoconvenio: ~15 rows (aproximadamente)
/*!40000 ALTER TABLE `tipoconvenio` DISABLE KEYS */;
INSERT INTO `tipoconvenio` (`idtipo`, `descricao`, `sigla`) VALUES
	(1, 'DELEGACAO DE RECURSOS E ENCARGOS', 'DRE'),
	(2, 'TRANSFERENCIA VOLUNTARIA', 'TV'),
	(3, 'TERMO DE CONVENIO', 'TCONV'),
	(4, 'TERMO DE DENUNCIA', 'TD'),
	(5, 'TERMO DE COOPERACAO TECNICO E CIENTIFICO', 'TCTC'),
	(6, 'TERMO DE COOPERACAO TECNICO E FINANCEIRO', 'TCTF'),
	(7, 'TERMO DE PARCERIA', 'TP'),
	(8, 'TERMO ADITIVO DE CONVENIO', 'TACONV'),
	(9, 'TERMO ADITIVO DE COOPERACAO TECNICO E CIENTIFICO', 'TACTC'),
	(10, 'TERMO ADITIVO DE COOPERACAO TECNICO E FINANCEIRO', 'TACTF'),
	(11, 'TERMO ADITIVO DE PARCERIA', 'TAP'),
	(12, 'CESSAO', 'CES'),
	(13, 'ADITIVO DE CESSAO', 'ADCES'),
	(14, 'TERMO DE RESPONSABILIDADE', 'TERE'),
	(15, 'TERMO ADITIVO DE RESPONSABILIDADE', 'TARES');
/*!40000 ALTER TABLE `tipoconvenio` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tipoesferaconveniado
DROP TABLE IF EXISTS `tipoesferaconveniado`;
CREATE TABLE IF NOT EXISTS `tipoesferaconveniado` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.tipoesferaconveniado: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `tipoesferaconveniado` DISABLE KEYS */;
INSERT INTO `tipoesferaconveniado` (`id`, `descricao`) VALUES
	(1, 'Federal'),
	(2, 'Estadual'),
	(3, 'Municipal'),
	(4, 'ONGs'),
	(5, 'Outros');
/*!40000 ALTER TABLE `tipoesferaconveniado` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tipomodalidade
DROP TABLE IF EXISTS `tipomodalidade`;
CREATE TABLE IF NOT EXISTS `tipomodalidade` (
  `id` int(11) NOT NULL,
  `descricao` varchar(60) DEFAULT NULL,
  `sigla` char(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.tipomodalidade: ~19 rows (aproximadamente)
/*!40000 ALTER TABLE `tipomodalidade` DISABLE KEYS */;
INSERT INTO `tipomodalidade` (`id`, `descricao`, `sigla`) VALUES
	(0, 'DISPENSA PARA COMPRAS  E SERVICOS (COMPRA DIRETA)', 'DC'),
	(1, 'CONVITE PARA COMPRAS E SERVICOS', 'CC'),
	(2, 'CONVITE PARA OBRAS E SERVICOS DE ENGENHARIA', 'CC'),
	(3, 'TOMADA DE PRECOS PARA COMPRAS E SERVICOS', 'TP'),
	(4, 'TOMADA DE PRECOS PARA OBRAS E SERVICOS DE ENGENHARIA', 'TP'),
	(5, 'CONCORRENCIA  PARA COMPRAS E SERVICOS', 'CO'),
	(6, 'CONCORRENCIA PARA OBRAS E SERVICOS DE ENGENHARIA', 'CO'),
	(7, 'LEILAO', 'LE'),
	(8, 'DISPENSA DE LICITACAO', 'DL'),
	(9, 'INEXIGIBILIDADE DE LICITACAO', 'IL'),
	(10, 'CONCURSO', 'CP'),
	(11, 'PREGAO ELETRONICO', 'PE'),
	(12, 'PREGAO PRESENCIAL', 'PP'),
	(13, 'CONCORRENCIA PARA CONCESSAO ADMINISTRATIVA DE USO ', 'CA'),
	(14, 'CONCORRENCIA PARA CONCESSAO DE DIREITO DE USO', 'CD'),
	(15, 'ANULADA', 'AN'),
	(16, 'DESERTA', 'DS'),
	(17, 'FRACASSADA', 'FR'),
	(18, 'INTERNACIONAL', 'IN');
/*!40000 ALTER TABLE `tipomodalidade` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tipomoeda
DROP TABLE IF EXISTS `tipomoeda`;
CREATE TABLE IF NOT EXISTS `tipomoeda` (
  `id` int(11) NOT NULL,
  `moeda` varchar(2) NOT NULL,
  `nome` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.tipomoeda: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tipomoeda` DISABLE KEYS */;
INSERT INTO `tipomoeda` (`id`, `moeda`, `nome`) VALUES
	(1, 'R$', 'Real'),
	(3, 'U$', 'Dólar'),
	(9, '-', 'Outra Moeda');
/*!40000 ALTER TABLE `tipomoeda` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tipoparticipante
DROP TABLE IF EXISTS `tipoparticipante`;
CREATE TABLE IF NOT EXISTS `tipoparticipante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipoparticipante` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.tipoparticipante: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tipoparticipante` DISABLE KEYS */;
INSERT INTO `tipoparticipante` (`id`, `tipoparticipante`) VALUES
	(1, 'Participante Comum'),
	(2, 'Consórcio'),
	(3, 'Consorciado');
/*!40000 ALTER TABLE `tipoparticipante` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tipopessoa
DROP TABLE IF EXISTS `tipopessoa`;
CREATE TABLE IF NOT EXISTS `tipopessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipopessoa` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.tipopessoa: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipopessoa` DISABLE KEYS */;
INSERT INTO `tipopessoa` (`id`, `tipopessoa`) VALUES
	(1, 'Física'),
	(2, 'Jurídica');
/*!40000 ALTER TABLE `tipopessoa` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tipoproduto
DROP TABLE IF EXISTS `tipoproduto`;
CREATE TABLE IF NOT EXISTS `tipoproduto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipoproduto` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.tipoproduto: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipoproduto` DISABLE KEYS */;
INSERT INTO `tipoproduto` (`id`, `tipoproduto`) VALUES
	(1, 'Produto'),
	(2, 'Serviço');
/*!40000 ALTER TABLE `tipoproduto` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tiporesultado
DROP TABLE IF EXISTS `tiporesultado`;
CREATE TABLE IF NOT EXISTS `tiporesultado` (
  `id` varchar(1) NOT NULL,
  `tiporesultado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.tiporesultado: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tiporesultado` DISABLE KEYS */;
INSERT INTO `tiporesultado` (`id`, `tiporesultado`) VALUES
	('P', 'Perdedor'),
	('V', 'Vencedor');
/*!40000 ALTER TABLE `tiporesultado` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tipounidade
DROP TABLE IF EXISTS `tipounidade`;
CREATE TABLE IF NOT EXISTS `tipounidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.tipounidade: ~22 rows (aproximadamente)
/*!40000 ALTER TABLE `tipounidade` DISABLE KEYS */;
INSERT INTO `tipounidade` (`id`, `unidade`) VALUES
	(1, 'BB'),
	(2, 'BL'),
	(3, 'CJTO'),
	(4, 'CT'),
	(5, 'CX'),
	(6, 'FD'),
	(7, 'FL'),
	(8, 'GL'),
	(9, 'JG'),
	(10, 'KG'),
	(11, 'LT'),
	(12, 'MIL'),
	(13, 'MT'),
	(14, 'PCT'),
	(15, 'PCTO'),
	(16, 'PEÇA'),
	(17, 'PÉS'),
	(18, 'PR'),
	(19, 'RL'),
	(20, 'RM'),
	(21, 'SERVIÇOS'),
	(22, 'UNID');
/*!40000 ALTER TABLE `tipounidade` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tipovalor
DROP TABLE IF EXISTS `tipovalor`;
CREATE TABLE IF NOT EXISTS `tipovalor` (
  `id` varchar(1) NOT NULL,
  `tipovalor` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.tipovalor: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipovalor` DISABLE KEYS */;
INSERT INTO `tipovalor` (`id`, `tipovalor`) VALUES
	('E', 'Espécie'),
	('P', 'Percentual');
/*!40000 ALTER TABLE `tipovalor` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.tipo_itemlote
DROP TABLE IF EXISTS `tipo_itemlote`;
CREATE TABLE IF NOT EXISTS `tipo_itemlote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(5) DEFAULT NULL,
  `sigla` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.tipo_itemlote: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_itemlote` DISABLE KEYS */;
INSERT INTO `tipo_itemlote` (`id`, `tipo`, `sigla`) VALUES
	(1, 'Item', 'I'),
	(2, 'Lote', 'L');
/*!40000 ALTER TABLE `tipo_itemlote` ENABLE KEYS */;


-- Copiando estrutura para tabela novo_sisel2.usuario
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `login` varchar(20) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `email` varchar(45) NOT NULL,
  `perfil_fk` int(11) NOT NULL,
  `ano_exercicio` int(11) NOT NULL,
  `status` enum('Ativo','Inativo') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuario_perfil1_idx` (`perfil_fk`),
  KEY `fk_usuario_ano_exercicio1_idx` (`ano_exercicio`),
  CONSTRAINT `fk_usuario_ano_exercicio1` FOREIGN KEY (`ano_exercicio`) REFERENCES `ano_exercicio` (`ano`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_perfil1` FOREIGN KEY (`perfil_fk`) REFERENCES `perfil` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela novo_sisel2.usuario: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id`, `nome`, `login`, `senha`, `email`, `perfil_fk`, `ano_exercicio`, `status`) VALUES
	(19, 'Adriano de Abreu Wanderley', 'adriano', '123', 'adrianoabreu.wy@gmail.com', 1, 2014, 'Ativo');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;


-- Copiando estrutura para view novo_sisel2.vw_licitacao
DROP VIEW IF EXISTS `vw_licitacao`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_licitacao` (
	`id` VARCHAR(18) NOT NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;


-- Copiando estrutura para view novo_sisel2.vw_produto
DROP VIEW IF EXISTS `vw_produto`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_produto` (
	`descricao` VARCHAR(300) NOT NULL COLLATE 'utf8_general_ci',
	`tipoproduto` VARCHAR(45) NOT NULL COLLATE 'utf8_general_ci',
	`unidade` VARCHAR(45) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;


-- Copiando estrutura para procedure novo_sisel2.sp_deletarLicitacao
DROP PROCEDURE IF EXISTS `sp_deletarLicitacao`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deletarLicitacao`(IN `_licitacao` VARCHAR(50))
    COMMENT 'DELETAR LICITACAO'
BEGIN
	SET foreign_key_checks = 0;
	
		DELETE FROM itemlicitacao  WHERE licitacao_fk = _licitacao;
		
		DELETE FROM licitacao WHERE id = _licitacao;
	
	SET foreign_key_checks = 1;
END//
DELIMITER ;


-- Copiando estrutura para procedure novo_sisel2.sp_gastos_por_fornecedor
DROP PROCEDURE IF EXISTS `sp_gastos_por_fornecedor`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `sp_gastos_por_fornecedor`(IN `_ano` INT)
BEGIN
	select f.nome_fantasia, ROUND(SUM(c.valor_total),2) as 'valor'
	from cotacao c
	inner join fornecedor f 	ON f.id= c.participante_fk
	inner join itemlicitacao i ON i.id = c.itemlicitacao_fk
	inner join licitacao l 		ON l.id = i.licitacao_fk
	WHERE YEAR(l.dt_publicacao) = _ano
	group by c.participante_fk
	order by ROUND(SUM(c.valor_total),2) DESC
	limit 12;
END//
DELIMITER ;


-- Copiando estrutura para procedure novo_sisel2.sp_gastos_por_mes
DROP PROCEDURE IF EXISTS `sp_gastos_por_mes`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `sp_gastos_por_mes`(IN `_ano` INT)
BEGIN
	select SUBSTR(fc_nomeMes(date_format(l.dt_publicacao, '%m')),1,3) as 'mes', ROUND(SUM(l.valor_bloqueado),2) as 'valor'
	from licitacao l
	where l.valor_bloqueado is not null
	and year(l.dt_publicacao) = _ano
	group by date_format(l.dt_publicacao, '%m-%Y')
	order by date_format(l.dt_publicacao, '%m');
END//
DELIMITER ;


-- Copiando estrutura para procedure novo_sisel2.sp_gastos_por_modalidade
DROP PROCEDURE IF EXISTS `sp_gastos_por_modalidade`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `sp_gastos_por_modalidade`(IN `_ano` INT)
BEGIN
	select tm.descricao, ROUND(SUM(l.valor_bloqueado),2) as 'valor'
	from licitacao l
	inner join tipomodalidade tm ON tm.id = l.modalidade_fk
	where year(l.dt_publicacao) = _ano
	group by l.modalidade_fk
	order by ROUND(SUM(l.valor_bloqueado),2) asc;
END//
DELIMITER ;


-- Copiando estrutura para função novo_sisel2.fc_nomeMes
DROP FUNCTION IF EXISTS `fc_nomeMes`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `fc_nomeMes`(`mes` INT) RETURNS varchar(20) CHARSET latin1
    COMMENT 'Traduz os nomes dos meses.'
BEGIN
    DECLARE nomeMes VARCHAR(20);
 
	SELECT 	CASE 
				WHEN `mes` = 1	THEN 'Janeiro'
				WHEN `mes` = 2	THEN 'Fevereiro'
				WHEN `mes` = 3	THEN 'Março'
				WHEN `mes` = 4	THEN 'Abril'
				WHEN `mes` = 5	THEN 'Maio'
				WHEN `mes` = 6	THEN 'Junho'
				WHEN `mes` = 7	THEN 'Julho'
				WHEN `mes` = 8	THEN 'Agosto'
				WHEN `mes` = 9  THEN 'Setembro'
				WHEN `mes` = 10	THEN 'Outubro'
				WHEN `mes` = 11	THEN 'Novembro'
				WHEN `mes` = 12	THEN 'Dezembro'
			END INTO nomeMes;
 
    RETURN nomeMes;
END//
DELIMITER ;


-- Copiando estrutura para trigger novo_sisel2.cotacao_AFUP
DROP TRIGGER IF EXISTS `cotacao_AFUP`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `cotacao_AFUP` BEFORE UPDATE ON `cotacao` FOR EACH ROW BEGIN
	DECLARE qtd INT;
	
	/* ATUALIZA O VALOR TOTAL SE O VALOR UNITÁRIO FOR ALTERADO*/
	IF (old.valor_unitario != new.valor_unitario) THEN
		SET qtd = (SELECT i.quantidade FROM itemlicitacao i WHERE i.id = new.itemlicitacao_fk);
		SET new.valor_total = (new.valor_unitario * qtd);	
	END IF;

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Copiando estrutura para trigger novo_sisel2.itemlicitacao
DROP TRIGGER IF EXISTS `itemlicitacao`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `itemlicitacao` BEFORE UPDATE ON `itemlicitacao` FOR EACH ROW BEGIN
	
	/* SE A QUANTIDADE DO ITEM FOR ALTERADA, VAI NA TABELA COTAÇÃO E ALTERA TODOS OS VALORES DA COTAÇÃO REFERENTE A ESTE ITEM*/
	IF (old.quantidade <> new.quantidade) THEN
		UPDATE cotacao SET cotacao.valor_total = (new.quantidade * cotacao.valor_unitario) WHERE cotacao.itemlicitacao_fk = new.id;
	END IF;
	
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Copiando estrutura para view novo_sisel2.vw_licitacao
DROP VIEW IF EXISTS `vw_licitacao`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_licitacao`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `vw_licitacao` AS SELECT l.id
FROM licitacao l
INNER JOIN tipomodalidade tm ON tm.id= l.modalidade_fk ;


-- Copiando estrutura para view novo_sisel2.vw_produto
DROP VIEW IF EXISTS `vw_produto`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_produto`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `vw_produto` AS select p.descricao, tp.tipoproduto, und.unidade
from produto p
inner join tipoproduto tp ON tp.id = p.tipoproduto_fk
inner join tipounidade und ON und.id = p.unidade_fk
order by p.descricao asc ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
