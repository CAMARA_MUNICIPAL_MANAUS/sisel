<?php

class Auxiliar extends Connection{

	function tipomodalidade($codigo=null){


		$sql = "SELECT * FROM tipomodalidade WHERE modalidade is not null ORDER BY descricao ASC";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipomodalidade' id='tipomodalidade'class='form-control'>";
			echo "<option value=''>SELECIONE UMA MODALIDADE</option>";
			foreach ($retorno as $m) {

				if($codigo != ''){
					if($codigo == $m['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}                                                                                                                                                                                                                                                                

				}else{
					$selected = '';
				}

				echo "<option value='{$m['id']}' sigla='{$m['sigla']}' $selected>".$m['descricao']."</option>";
			}
		echo "</select>";
	}

	function tipolicitacao($codigo=null){

		$sql = "SELECT * FROM tipolicitacao";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$disabled = ($codigo != "") ? "" : "disabled=true";

		echo "<select name='tipolicitacao' id='tipolicitacao' class='form-control' $disabled>";
			echo "<option value=''>SELECIONE</option>";
			foreach ($retorno as $tc) {

				if($codigo != ''){
					if($codigo == $tc['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tc['id']}' $selected>".$tc['licitacao']."</option>";
			}
		echo "</select>";

	}

	function tiponaturezalicitacao($codigo=null){

		$sql = "SELECT * FROM tipo_naturezalicitacao";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$disabled = ($codigo != "") ? "" : "disabled=true";

		echo "<select name='tiponaturezalicitacao' id='tiponaturezalicitacao'class='form-control' $disabled>";
			echo "<option value=''>SELECIONE</option>";
			foreach ($retorno as $tc) {

				if($codigo != ''){
					if($codigo == $tc['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tc['id']}' $selected>".$tc['natureza']."</option>";
			}
		echo "</select>";
		
	}

	function tipocontrato($codigo=null){


		$sql = "SELECT * FROM tipocontrato";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipocontrato' id='tipocontrato'class='form-control'>";
			echo "<option value=''>SELECIONE UM CONTRATO</option>";
			foreach ($retorno as $tc) {

				if($codigo != ''){
					if($codigo == $tc['idtipocontrato']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tc['idtipocontrato']}' sigla='{$tc['sigla']}' $selected>".$tc['descricao']."</option>";
			}
		echo "</select>";
	}

	function tiporegimeexecucao($codigo=null){


		$sql = "SELECT * FROM tiporegimeexecucao";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$disabled = ($codigo != "") ? "" : "disabled=true";

		echo "<select name='tiporegimeexecucao' id='tiporegimeexecucao' class='form-control' $disabled>";
			echo "<option value=''>SELECIONE</option>";
			foreach ($retorno as $tc) {

				if($codigo != ''){
					if($codigo == $tc['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tc['id']}' $selected>".$tc['regime']."</option>";
			}
		echo "</select>";
	}

	function tiponaturezaprocedimento($codigo=null){


		$sql = "SELECT * FROM tipo_naturezaprocedimento";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$disabled = ($codigo != "") ? "" : "disabled=true";

		echo "<select name='tiponaturezaprocedimento' id='tiponaturezaprocedimento' class='form-control' $disabled>";
			echo "<option value=''>SELECIONE</option>";
			foreach ($retorno as $tc) {

				if($codigo != ''){
					if($codigo == $tc['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tc['id']}' $selected>".$tc['natureza']."</option>";
			}
		echo "</select>";
	}

	function tipoesfera($codigo=null){


		$sql = "select * from tipoesferaconveniado;";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipoesfera' id='tipoesfera' class='form-control'>";
			echo "<option value=''>SELECIONE UMA ESFERA</option>";
			foreach ($retorno as $tc) {

				if($codigo != ''){
					if($codigo == $tc['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tc['id']}' $selected>".$tc['descricao']."</option>";
			}
		echo "</select>";
	}



	function tipoconvenio($codigo=null){


		$sql = "SELECT * FROM tipoconvenio";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipoconvenio' id='tipoconvenio' class='form-control'>";
			echo "<option value=''>SELECIONE O TIPO DE CONVÊNIO</option>";
			foreach ($retorno as $tc) {

				if($codigo != ''){
					if($codigo == $tc['idtipo']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tc['idtipo']}' sigla='{$tc['sigla']}' $selected>".$tc['idtipo']." - ".$tc['descricao']."</option>";
			}
		echo "</select>";
	}

	function tipovaloraditivo($codigo=null){

		$sql = "SELECT * FROM tipoaditivo";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipoaditivo' id='tipoaditivo'class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tc) {

				if($codigo != ''){
					if($codigo == $tc['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tc['id']}' $selected>".$tc['aditivo']."</option>";
			}
		echo "</select>";
	}

	function tipoaditivo($codigo=null){

		$sql = "SELECT * FROM tipoaditivo2 order by id ASC";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipoaditivo2' id='tipoaditivo2'class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tc) {

				if($codigo != ''){
					if($codigo == $tc['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tc['id']}' $selected>".$tc['tipoaditivo']."</option>";
			}
		echo "</select>";
	}

	function tipocontratodecorrente($codigo=null){

		$sql = "SELECT * FROM tipocontratodecorrente ORDER BY id ASC";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipocontratodecorrente' id='tipocontratodecorrente'class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tc) {

				if($codigo != ''){
					if($codigo == $tc['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tc['id']}' $selected>".$tc['contrato']."</option>";
			}
		echo "</select>";
	}

	function tiponatureza($codigo=null){

		$sql = "SELECT * FROM tiponatureza ORDER BY id ASC";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tiponatureza' id='tiponatureza'class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tc) {

				if($codigo != ''){
					if($codigo == $tc['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tc['id']}' $selected>".$tc['natureza']."</option>";
			}
		echo "</select>";
	}

	function tipoata($codigo=null){


		$sql = "SELECT * FROM tipoata";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipoata' id='tipoata' class='form-control required'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tc) {

				if($codigo != ''){
					if($codigo == $tc['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tc['id']}' $selected data-sigla='".$tc['sigla']."'>".$tc['descricao']."</option>";
			}
		echo "</select>";
	}


	function tipoitemlote($codigo=null){

		$sql = "SELECT * FROM tipo_itemlote;";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipo_itemlote' id='tipo_itemlote'class='form-control'>";
			
			foreach ($retorno as $il) {

				if($codigo != ''){
					if($codigo == $il['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$il['id']}' $selected>".$il['tipo']."</option>";
			}
		echo "</select>";
	}

	function tipounidade($codigo=null){
		$sql = "SELECT * FROM tipounidade ORDER BY unidade ASC";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipounidade' id='tipounidade' class='form-control required'>";
			foreach ($retorno as $und) {

				if($codigo != ''){
					if($codigo == $und['id']){
						$selected = 'selected=selected';
					}else{
						$selected = '';
					}

				}else{
					if($und['id'] == 22){ $selected = 'selected=selected'; }
				}

				echo "<option value='{$und['id']}' ".@$selected.">".$und['unidade']."</option>";
			}
		echo "</select>";
	}

	function tipoproduto($codigo=null){
		$sql = "select * from tipoproduto";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipoproduto' id='tipoproduto' class='form-control required'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tp) {

				if($codigo != ''){
					if($codigo == $tp['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tp['id']}' $selected>".$tp['tipoproduto']."</option>";
			}
		echo "</select>";
	}

	function tipopessoa($codigo=null){
		$sql = "SELECT * FROM tipopessoa";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipopessoa' id='tipopessoa' class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tp) {

				if($codigo != ''){
					if($codigo == $tp['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tp['id']}' $selected>".$tp['tipopessoa']."</option>";
			}
		echo "</select>";
	}

	function tipotransferenciavoluntaria($codigo=null){
		$sql = "SELECT * FROM tipotransferenciavoluntaria ORDER BY id ASC";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipotransferenciavoluntaria' id='tipotransferenciavoluntaria' class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tp) {

				if($codigo != ''){
					if($codigo == $tp['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tp['id']}' sigla='{$tp['sigla']}' $selected>".$tp['descricao']."</option>";
			}
		echo "</select>";
	}

	function tipoatividadeprincipal($codigo=null){
		$sql = "SELECT * FROM tipoatividadeprincipal";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipoatividadeprincipal' id='tipoatividadeprincipal' class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tp) {

				if($codigo != ''){
					if($codigo == $tp['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tp['id']}' $selected>".$tp['descricao']."</option>";
			}
		echo "</select>";
	}

	function tipocontrapartida($codigo=null){
		$sql = "SELECT * FROM tipocontrapartida ORDER BY id ASC";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipocontrapartida' id='tipocontrapartida' class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tp) {

				if($codigo != ''){
					if($codigo == $tp['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tp['id']}' $selected>".$tp['id']." - ".$tp['contrapartida']."</option>";
			}
		echo "</select>";
	}

	function tipoparticipacaotransferenciavol($codigo=null){
		$sql = "SELECT * FROM tipoparticipacaotransferenciavol ORDER BY id ASC";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipoparticipacaotransferenciavol' id='tipoparticipacaotransferenciavol' class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tp) {

				if($codigo != ''){
					if($codigo == $tp['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tp['id']}' $selected>".$tp['descricao']."</option>";
			}
		echo "</select>";
	}

	function tipoparticipante($codigo=null){
		$sql = "SELECT * FROM tipoparticipante";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipoparticipante' id='tipoparticipante' class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tp) {

				if($codigo != ''){
					if($codigo == $tp['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tp['id']}' $selected>".$tp['tipoparticipante']."</option>";
			}
		echo "</select>";
	}

	function statusitem($codigo=null){
		$sql = "select * from status_item";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='statusitem' id='statusitem' class='form-control'>";
			echo "<option value=''>Selecione o Status</option>";
			foreach ($retorno as $st) {

				if($codigo != ''){
					if($codigo == $st['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$st['id']}' $selected>".$st['status']."</option>";
			}
		echo "</select>";
	}

	function moeda($codigo=null){
		$sql = "SELECT * FROM tipomoeda";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipomoeda' id='tipomoeda'class='form-control'>";
			// echo "<option value=''>Selecione</option>";
			foreach ($retorno as $moeda) {

				if($codigo != ''){
					if($codigo == $moeda['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$moeda['id']}' $selected>{$moeda['moeda']} - {$moeda['nome']}</option>";
			}
		echo "</select>";
	}

	function produto($codigo=null){
		$sql = "SELECT * FROM produto p ORDER BY trim(descricao) ASC";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='produto' id='produto' class='form-control'>";
			echo "<option value=''>Selecione um Produto</option>";
			foreach ($retorno as $produto) {

				if($codigo != ''){
					if($codigo == $produto['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$produto['id']}' $selected>".$produto['descricao']."</option>";
			}
		echo "</select>";
	}

	function tipovalor($codigo=null){
		$sql = "SELECT * FROM tipovalor";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipovalor' id='tipovalor' class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tipovalor) {

				if($codigo != ''){
					if($codigo == $tipovalor['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tipovalor['id']}' $selected>".$tipovalor['tipovalor']."</option>";
			}
		echo "</select>";
	}

	function tiporesultado($codigo=null){
		$sql = "SELECT * FROM tiporesultado";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tiporesultado' id='tiporesultado' class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $tiporesultado) {

				if($codigo != ''){
					if($codigo == $tiporesultado['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$tiporesultado['id']}' $selected>{$tiporesultado['tiporesultado']}</option>";
			}
		echo "</select>";
	}

	function licitacao($ano, $codigo=null){
		$sql = "select id from licitacao WHERE ano_exercicio = $ano";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='licitacao' id='licitacao' class='form-control select2' data-anoexercicio='$ano'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $lic) {

				if($codigo != ''){
					if($codigo == $lic['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$lic['id']}' $selected>{$lic['id']}</option>";
			}
		echo "</select>";
	}

	function licitacaoParticipante($anoexercicio){
		$sql = "SELECT l.id
				FROM licitacao l
				WHERE l.id NOT IN (SELECT DISTINCT p.licitacao_fk
											FROM participante_certidao p
											INNER JOIN licitacao l ON l.id = p.licitacao_fk
											WHERE l.ano_exercicio = ".$anoexercicio."
											ORDER BY p.licitacao_fk ASC)
				AND l.ano_exercicio = ".$anoexercicio."
				ORDER BY l.id ASC";

		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='licitacaoparticipante' id='licitacaoparticipante' class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $lic) {

				if($codigo != ''){
					if($codigo == $lic['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$lic['id']}' $selected>{$lic['id']}</option>";
			}
		echo "</select>";
	}

	function numContrato($codigo=null){
		$sql = "SELECT idcontrato, numcontrato FROM contrato ORDER BY numcontrato ASC";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='contratoAdtivo' id='contratoAdtivo' class='form-control'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $lic) {

				if($codigo != ''){
					if($codigo == $lic['idcontrato']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$lic['idcontrato']}' $selected>{$lic['numcontrato']}</option>";
			}
		echo "</select>";
	}

	function cnae($codigo=null){
		$sql = "select * from tipocnae order by nome asc;";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipocnae' id='tipocnae' class='form-control input-sm select2'>";
			echo "<option value=''>Selecione</option>";
			foreach ($retorno as $cn) {

				if($codigo != ''){
					if($codigo == $cn['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$cn['id']}' $selected>{$cn['codigo']} - {$cn['nome']}</option>";
			}
		echo "</select>";
	}

	function fornecedor($codigo=null){

		$sql = "select * from fornecedor f ORDER BY trim(f.razao_social) ASC"; 
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);
		// insc_estadual, insc_municipal
		echo "<select name='contratado' id='contratado' class='form-control input-sm select2 required'>";
			echo "<option value=''>Selecione o Fornecedor</option>";
			foreach ($retorno as $m) {

				if($codigo != ''){
					if($codigo == $m['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$m['id']}' cpfcnpj='{$m['cpfcnpj']}' iestadual='{$m['insc_estadual']}' imunicipal='{$m['insc_municipal']}' $selected>".$m['razao_social']."</option>";
			}
		echo "</select>";
	}

	function tipobanco($codigo=null){

		$sql = "SELECT * FROM tipobanco ORDER BY codigo ASC";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='banco' id='tipobanco' class='form-control input-sm select2'>";
			echo "<option value=''>Selecione o Banco</option>";
			foreach ($retorno as $b) {

				if($codigo != ''){
					if($codigo == $b['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$b['id']}' $selected>".$b['codigo']." - ".$b['banco']."</option>";
			}
		echo "</select>";
	}

	function tipocertidao($codigo=null){

		$sql = "SELECT * FROM tipocertidao2 ORDER BY descricao ASC";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='tipocertidao' id='tipocertidao' class='form-control required'>";
			echo "<option value=''>Selecione o Tipo da Certidão</option>";
			foreach ($retorno as $c) {

				if($codigo != ''){
					if($codigo == $c['id']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$c['id']}' $selected>".$c['descricao']."</option>";
			}
		echo "</select>";
	}

	function anoexercicio($codigo=null){

		$sql = "SELECT * FROM ano_exercicio ORDER BY ano ASC";												
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo "<select name='anoexercicio' id='anoexercicio' class='form-control required'>";
			echo "<option value=''>Selecione o ano</option>";
			foreach ($retorno as $m) {

				if($codigo != ''){
					if($codigo == $m['ano']){
						$selected = 'selected';
					}else{
						$selected = '';
					}

				}else{
					$selected = '';
				}

				echo "<option value='{$m['ano']}' $selected>".$m['ano']."</option>";
			}
		echo "</select>";
	}

	function mes($mes=null){

		if($mes != ''){
			if($mes == '01'){ $selected1 = "selected=selected"; }else{ $selected1 = ''; }
			if($mes == '02'){ $selected2 = "selected=selected"; }else{ $selected2 = ''; }
			if($mes == '03'){ $selected3 = "selected=selected"; }else{ $selected3 = ''; }
			if($mes == '04'){ $selected4 = "selected=selected"; }else{ $selected4 = ''; }
			if($mes == '05'){ $selected5 = "selected=selected"; }else{ $selected5 = ''; }
			if($mes == '06'){ $selected6 = "selected=selected"; }else{ $selected6 = ''; }
			if($mes == '07'){ $selected7 = "selected=selected"; }else{ $selected7 = ''; }
			if($mes == '08'){ $selected8 = "selected=selected"; }else{ $selected8 = ''; }
			if($mes == '09'){ $selected9 = "selected=selected"; }else{ $selected9 = ''; }
			if($mes == '10'){ $selected10 = "selected=selected"; }else{ $selected10 = ''; }
			if($mes == '11'){ $selected11 = "selected=selected"; }else{ $selected11 = ''; }
			if($mes == '12'){ $selected12 = "selected=selected"; }else{ $selected12 = ''; }
		}

		echo "<select name='mes' id='mes' class='form-control required'>";
			echo "<option value='' selected=selected>Selecione um Mês</option>";
			echo "<option value='01' $selected1>01 - JANEIRO</option>";
			echo "<option value='02' $selected2>02 - FEVEREIRO</option>";
			echo "<option value='03' $selected3>03 - MARÇO</option>";
			echo "<option value='04' $selected4>04 - ABRIL</option>";
			echo "<option value='05' $selected5>05 - MAIO</option>";
			echo "<option value='06' $selected6>06 - JUNHO</option>";
			echo "<option value='07' $selected7>07 - JULHO</option>";
			echo "<option value='08' $selected8>08 - AGOSTO</option>";
			echo "<option value='09' $selected9>09 - SETEMBRO</option>";
			echo "<option value='10' $selected10>10 - OUTUBRO</option>";
			echo "<option value='11' $selected11>11 - NOVEMBRO</option>";
			echo "<option value='12' $selected12>12 - DEZEMBRO</option>";
		echo "</select>";
	}

}
?>