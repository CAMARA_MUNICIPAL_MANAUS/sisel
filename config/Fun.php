<?php
class FUN{
	public static function serializeToArray($data){
		foreach ($data as $d) {

			if( substr($d["name"], -1) == "]" ){

				$d["name"] = explode("[", str_replace("]", "", $d["name"]));
				
				switch (sizeof($d["name"])) {
					case 2: $a[$d["name"][0]][$d["name"][1]] = $d["value"]; break;
					case 3: $a[$d["name"][0]][$d["name"][1]][$d["name"][2]] = $d["value"]; break;
					case 4: $a[$d["name"][0]][$d["name"][1]][$d["name"][2]][$d["name"][3]] = $d["value"]; break;
				}
			}else{
				$a[$d["name"]] = $d["value"];
			} // if
		} // foreach

		return $a;
	}

	public static function erroDataBase($error){
		print_r($error);
		// $error = explode('ORA', str_replace("OCIStmtExecute: ", "", $error[2]));
		// return $error = "ORA".$error[1]." | ORA".$error[2];
	}

	public static function dateMySql($date, $days_ago=false, $time=false){
		$date = explode(" ", $date);
		
		$date_final = explode("-", $date[0]);
		$date_final = $date_final["1"]."/".$date_final["2"]."/".$date_final["0"];

		if($time == true && isset($date[1])){
			$date_final = $date_final." ".substr(trim($date[1]), 0,5);
		}

		if($days_ago == true){
			$data1 = new DateTime($date[0]);
			$data2 = new DateTime(date('Y-m-d'));

			$intervalo = $data1->diff($data2);

			$date_final .= " (".$intervalo->d." days ago)";
		}

		return $date_final;
	}

	public static function greetings(){
		$hr = date("H");
		if($hr >= 12 && $hr < 18) {
			$resp = "home_greetingsMorning";
		}else if ($hr >= 0 && $hr <12 ){
			$resp = "home_greetingsAfternoon";
		}else{
			$resp = "home_greetingsNight";
		}
		return $resp;
	}

	public static function getTextZplCode($string){
		// $start  = $data["start"];
		// $end    = $data["end"];

		// $my_string = "I am with a [id]123[/id] and [id]456[/id]";
		preg_match_all("~\^FD(.*?)\^FS~",$string,$m);
		return json_encode($m[1]);
		// print_r($m[1]);
	}

	public static function checkMobile(){
		$useragent = $_SERVER['HTTP_USER_AGENT'];

		$iphone  = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$ipad    = strpos($_SERVER['HTTP_USER_AGENT'],"iPad");
		$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
		$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
		$berry   = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
		$ipod    = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
		$symbian = strpos($_SERVER['HTTP_USER_AGENT'],"Symbian");

		if ($iphone || $ipad || $android || $palmpre || $ipod || $berry || $symbian == true) {
			$mobile = true;
		}else{
			$mobile = false;
		}

		return $mobile;
	}

	public static function array_column($input = null, $columnKey = null, $indexKey = null){
		
		$argc = func_num_args();
		$params = func_get_args();

		if ($argc < 2) {
			trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
			return null;
		}

		if (!is_array($params[0])) {
			trigger_error(
				'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
				E_USER_WARNING
			);
			return null;
		}

		if (!is_int($params[1]) && !is_float($params[1])
			&& !is_string($params[1])
			&& $params[1] !== null
			&& !(is_object($params[1]) && method_exists($params[1], '__toString'))
		){
			trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
			return false;
		}

		if (isset($params[2])
			&& !is_int($params[2])
			&& !is_float($params[2])
			&& !is_string($params[2])
			&& !(is_object($params[2]) && method_exists($params[2], '__toString'))
		) {
			trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
			return false;
		}

		$paramsInput = $params[0];
		$paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

		$paramsIndexKey = null;
		if (isset($params[2])) {
			if (is_float($params[2]) || is_int($params[2])) {
				$paramsIndexKey = (int) $params[2];
			} else {
				$paramsIndexKey = (string) $params[2];
			}
		}

		$resultArray = array();

		foreach ($paramsInput as $row) {
			$key = $value = null;
			$keySet = $valueSet = false;

			if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
				$keySet = true;
				$key = (string) $row[$paramsIndexKey];
			}

			if ($paramsColumnKey === null) {
				$valueSet = true;
				$value = $row;
			} elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
				$valueSet = true;
				$value = $row[$paramsColumnKey];
			}

			if ($valueSet) {
				if ($keySet) {
					$resultArray[$key] = $value;
				} else {
					$resultArray[] = $value;
				}
			}

		}

		return $resultArray;
	}

	public static function diferencahora($horario1, $horario2){
		$entrada = $horario1.":00";
		$saida = $horario2.":00";
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);

		$hora2[0] = ($hora2[0] < $hora1[0]) ? (24+$hora2[0]) : $hora2[0];

		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		$resultado = $acumulador2 - $acumulador1;
		$hora_ponto = floor($resultado / 3600);
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = floor($resultado / 60);
		$resultado = $resultado - ($min_ponto * 60);
		$secs_ponto = $resultado;
		
		//Grava na variável resultado final
		// $tempo = $hora_ponto.":".$min_ponto.":".$secs_ponto;
		$tempo = ($hora_ponto*60)+$min_ponto;

		return $tempo;
	}

	public function hourToMinute($time, $lunch=0){

		$time = explode(":", $time);
		// estou tirando uma hora pois é o desconto do almoço
		$time = (($time[0]-$lunch)*60)+$time[1];

		return $time;
	}

	public static function weekday($day){
		switch ($day) {
			case 0:
				// DOMINGO
				$weekday["name"] = "SUNDAY";
				$weekday["abv"] = "SUN";
			break;

			case 1:
				// SEGUNDA-FEIRA
				$weekday["name"] = "MONDAY";
				$weekday["abv"] = "MON";
			break;

			case 2:
				// TERÇA-FEIRA
				$weekday["name"] = "TUESDAY";
				$weekday["abv"] = "TUE";
			break;

			case 3:
				// QUARTA-FEIRA
				$weekday["name"] = "WEDNESDAY";
				$weekday["abv"] = "WED";
			break;

			case 4:
				// QUINTA-FEIRA
				$weekday["name"] = "THURSDAY";
				$weekday["abv"] = "THU";
			break;

			case 5:
				// SEXTA-FEIRA
				$weekday["name"] = "FRIDAY";
				$weekday["abv"] = "FRI";
			break;

			case 6:
				// SÁBADO
				$weekday["name"] = "SATURDAY";
				$weekday["abv"] = "SAT";
			break;

			default:
				return "ERRO";
			break;
		}

		return $weekday;
	}
	
	public static function multiSearch($parents, $searched) { 
		if (empty($searched) || empty($parents)) { 
			return false; 
		} 

		foreach ($parents as $key => $value) { 
			$exists = true; 
			foreach ($searched as $skey => $svalue) { 
				$exists = ($exists && IsSet($parents[$key][$skey]) && $parents[$key][$skey] == $svalue); 
			} 
			if($exists){ return $key; } 
		} 

		return false; 
	}

	public static function icon($icon){
		$ext = strtolower(pathinfo($icon, PATHINFO_EXTENSION));

		switch ($ext) {
			case "txt":
				$icon = "img/file/txt.svg";
			break;

			case "jpg":
			case "jpeg":
				$icon = "img/file/jpg.svg";
			break;

			case "png":
				$icon = "img/file/png.svg";
			break;

			case "pdf":
				$icon = "img/file/pdf.svg";
			break;
		}
		return $icon;
	}

	public static function deleteFolder($dir) { 
		$files = array_diff(scandir($dir), array('.','..')); 
		foreach ($files as $file) { 
			(is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
		} 
		rmdir($dir);
	}


}

?>