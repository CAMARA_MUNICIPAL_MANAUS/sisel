<?php
ini_set('default_charset', 'UTF-8');
class Connection {

	public $conn;

	public static function getConn()
	{
		return self::buildConnection();
	}

	public function __construct() {
		$this->conn = $this->buildConnection();
	}
 
	private static function buildConnection()
	{
		try {
			# $c = new PDO("mysql:host=mysql.inovadevweb.com.br;dbname=inovacaodweb;", "inovacaodweb", "17435382le");
			# $c = new PDO("mysql:host=localhost;dbname=idweb217_cmm;", "root", "");

			$env_host = getenv('DBHOST');
			$env_dbname = getenv('DBNAME');
			$env_dbuser = getenv('DBUSER');
			$env_dbpwd = getenv('DBPWD');
			
			$c = new PDO("mysql:host=$env_host;dbname=$env_dbname;", $env_dbuser, $env_dbpwd);


			$c->exec("set names utf8");
			return $c;
		} catch (PDOException $e) {
			echo "Falha na Conexão Com Banco de Dados";
			exit();
		}
	}

	public static function findAllByAttributes($tableName, $criteria, $debug){

		$select    = isset($criteria['select']) ? $criteria['select'] : '*';
		$condition = isset($criteria['condition']) ? $criteria['condition'] : 'true';
		$join 	   = isset($criteria['join']) ? $criteria['join'] : '';
		$order 	   = isset($criteria['order']) ? $criteria['order'] : $tableName.'.codigo';

		try {
			$sql="SELECT $select FROM $tableName $join WHERE $condition ORDER BY $order";
			if ($debug){echo $sql;}
			$stmt = self::getConn()->prepare($sql);
			$stmt->execute();
			$retorno = $stmt->fetchAll(PDO::FETCH_OBJ);
			return $retorno;

		}catch(SQLException $e){
			return $e->getMessage();
		}catch(Exception $e){
			return $e->getMessage();
		}
	}


	public static function findAll($tableName){
		try {
			$sql="SELECT * FROM $tableName";
			$stmt = self::getConn()->prepare($sql);
			$stmt->execute();
			$retorno = $stmt->fetchAll(PDO::FETCH_OBJ);
			return $retorno;
		
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public static function sql_insert($data, $tableName){

		$values_Sql = "";
		$campos_Sql = "";
		foreach ($data as $key => $value) {
			$campos_Sql.= "`".$key."`, ";
			$values_Sql.= ":".$key.", ";
		}

		return "INSERT INTO ".$tableName." (".substr($campos_Sql, 0, -2).") VALUES(".substr($values_Sql, 0, -2).")";

	}

	public static function sql_update($data, $tableName, $criteria){

		$campos_Sql = "";
		foreach ($data as $key => $value) {
			$campos_Sql.= $key." = :".$key.", ";
		}
		return "UPDATE ".$tableName." SET ".substr($campos_Sql, 0, -2)." WHERE $criteria";
	}

	public static function getFieldType($field, $tableName){
	

		$sql = "SELECT DISTINCT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE table_name = '".$tableName."' AND COLUMN_NAME = '".$field."'";
		$stmt = self::getConn()->prepare($sql);
		$stmt->execute();
		$type = $stmt->fetchAll(PDO::FETCH_OBJ);

		if (!empty($type)) {
			switch ($type[0]->DATA_TYPE) {
				case 'int':
					return PDO::PARAM_INT;
				break;
				
				default :
					return PDO::PARAM_STR;
				break;
			}
		}

	}

	public static function deleteByPk($pk, $tableName){
		$sql = "DELETE FROM $tableName WHERE `$tableName`.`codigo` = " . trim($pk);
		$stmt = self::getConn()->prepare($sql);
		return $stmt->execute();
	}

	public static function deleteByAttibutes($criteria, $tableName){
		$sql = "DELETE FROM $tableName WHERE $criteria";
		$stmt = self::getConn()->prepare($sql);
		return $stmt->execute();
	}

	public static function callSp($params, $spName, $debug = false){

		$params2 = array();

		if(is_array($params)){
			foreach (@$params as $value) {
				$params2[] = is_numeric($value) ? $value :  "'".$value."'";
			}
		}else{
			$params2 = $params;
		}

		$params = is_array($params2) ? implode(',', $params2) : $params2;
		$sql = "CALL $spName($params)";

		if ($debug) {
			echo $sql;
		}

		$stmt = self::getConn()->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_OBJ);
	}

	public static function teste($n){
		return $n;
	}
}

$conexao = new Connection;