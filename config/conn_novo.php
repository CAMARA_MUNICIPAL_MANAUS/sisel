<?php
set_time_limit(0);
class ConnectionSISEL extends FUN{

	public $conn;

	public static function getConn(){
		return self::buildConnection();
	}

	public function __construct() {
		$this->conn = $this->buildConnection();
	}

	public static function buildConnection(){
		try {			
			# $c = new PDO("mysql:host=localhost;dbname=idweb217_cmm;", "root", "");
            # $c = new PDO("mysql:host=mysql.inovadevweb.com.br;dbname=inovacaodweb;", "inovacaodweb", "17435382le");
            
			$env_host = getenv('DBHOST');
			$env_dbname = getenv('DBNAME');
			$env_dbuser = getenv('DBUSER');
			$env_dbpwd = getenv('DBPWD');
			
			$c = new PDO("mysql:host=$env_host;dbname=$env_dbname;", $env_dbuser, $env_dbpwd);

			$c->exec("set names utf8");

			return $c;
		} catch (PDOException $e) {
			print_r($e->getMessage());
			echo "<br/>Falha na Conexao Com Banco de Dados";
			exit();
		}
	}

	public static function findAllByAttributes($criteria, $debug, $tableName){

		$select   = isset($criteria['select']) ? $criteria['select'] : "*";
		$where 	  = isset($criteria['where']) ? "WHERE ".$criteria['where'] : "";
		$join 	  = isset($criteria['join']) ? $criteria['join'] : "";
		$group 	  = isset($criteria['group']) ? "GROUP BY ". $criteria['group'] : "";
		$order 	  = isset($criteria['order']) ? "ORDER BY ". $criteria['order'] : "";
		$unionall = isset($criteria['unionall']) ? $criteria['unionall'] : "";
		$join2 	  = isset($criteria['join2']) ? $criteria['join2'] : $join;
		$limit 	  = isset($criteria['limit']) ? $criteria['limit'] : "";

		try {
			if(!empty($unionall)){
				$sql = "SELECT $select FROM $tableName $join $where
						UNION ALL
						SELECT $select FROM $unionall $join2 $where $group $order";
			}else{
				$sql = "SELECT $select FROM $tableName $join $where $group $order";
			}

			if ($debug){echo $sql;}
			$stmt = self::getConn()->prepare($sql);
			$stmt->execute();
			$retorno = $stmt->fetchAll(PDO::FETCH_OBJ);
			return $retorno;

		}catch(SQLException $e){
			return $e->getMessage();
		}catch(Exception $e){
			return $e->getMessage();
		}
	}

	public static function table($tableName){
		try {
			$sql = "SELECT * FROM $tableName ORDER BY 1 ASC";
			$stmt = self::getConn()->prepare($sql);
			$stmt->execute();
			$retorno = $stmt->fetchAll(PDO::FETCH_OBJ);
			return $retorno;
		
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public static function sql($sql, $debug=false){
		if($debug==true){ echo $sql; }
		try {
			$stmt = self::getConn()->prepare($sql);
			$stmt->execute();
			$retorno = $stmt->fetchAll(PDO::FETCH_OBJ);
			return $retorno;

		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public static function sql_insert($data, $tableName){

		$values_Sql = "";
		$campos_Sql = "";
		foreach ($data as $key => $value) {
			$campos_Sql.= "".$key.", ";
			$values_Sql.= "'".$value."', ";
		}
		return "INSERT INTO ".$tableName." (".substr($campos_Sql, 0, -2).") VALUES(".substr($values_Sql, 0, -2).")";
	}

	public static function sql_update($data, $tableName, $criteria){

		$campos_Sql = "";
		foreach ($data as $key => $value) {
			$campos_Sql.= $key." = '".$value."', ";
		}
		return "UPDATE ".$tableName." SET ".substr($campos_Sql, 0, -2)." WHERE $criteria";
	}

	public static function getFieldType($field, $tableName, $value='0'){

		if($value != NULL){
			$sql = "SELECT DISTINCT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS
					WHERE table_name = '".$tableName."' AND COLUMN_NAME = '".$field."'";
			$stmt = self::getConn()->prepare($sql);
			$stmt->execute();
			$type = $stmt->fetchAll(PDO::FETCH_OBJ);

			if (!empty($type)) {
				switch ($type[0]->DATA_TYPE) {
					case 'int':
						return PDO::PARAM_INT;
					break;

					default :
						return PDO::PARAM_STR;
					break;
				}
			}
		}else{
			// echo ">> ".$value;
			return PDO::PARAM_INT;
		}

	}

	public static function deleteByPk($pk, $tableName, $campo="id", $debug=false){
		$sql = "DELETE FROM $tableName WHERE $tableName.$campo = '".trim($pk)."'";
		if($debug){ echo $sql; }
		$stmt = self::getConn()->prepare($sql);
		// return $stmt->execute();
		if($stmt->execute()){
            return 1; 
        }else{
        	$erro = $stmt->errorInfo();
            return $erro[2];
            // return 0;
        }
	}

	public static function deleteByAttibutes($criteria, $tableName){
		$sql = "DELETE FROM $tableName WHERE $criteria";
		$stmt = self::getConn()->prepare($sql);
		return $stmt->execute();
	}

	public static function callSp($params, $spName, $debug = false){

		$params2 = array();

		if(is_array($params)){
			foreach (@$params as $value) {
				if(empty($value)){
					$params2[] = 'null';
				}else{
					$params2[] = is_numeric($value) ? $value :  "'".$value."'";
				}
			}
		}else{
			$params2 = "'".$params."'";
		}

		$params = is_array($params2) ? implode(',', $params2) : $params2;
		$sql = "CALL $spName($params)";

		if ($debug) {
			echo $sql;
		}

		$stmt = self::getConn()->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_OBJ);
	}

}	