<?php
include_once 'conn.php';
ob_start();
session_start();

if( ( isset($_SESSION['usuario']['LOGIN']) && isset($_SESSION['usuario']['SENHA']) ) || ( isset($_POST['login']) && isset($_POST['senha']) ) ){

	$login 	= (isset($_SESSION['usuario']['LOGIN'])) ? $_SESSION['usuario']['LOGIN'] : $_POST['login'];
	$senha 	= (isset($_SESSION['usuario']['SENHA'])) ? $_SESSION['usuario']['SENHA'] : MD5($_POST['senha']);
	$ano 	= (isset($_SESSION['usuario']['ano_exercicio'])) ? $_SESSION['usuario']['ano_exercicio'] : $_POST['anoexercicio'];

	$sql = "SELECT u.*, COUNT(u.ID) as QTD, $ano as ano_exercicio
			FROM dqwuser u
			INNER JOIN dqwpermission p ON p.PROFILE = u.PROFILE
			WHERE u.LOGIN = :login
			  AND u.SENHA = :senha
			  AND :ano IN (SELECT ano FROM ano_exercicio)
			  AND u.STATUS = 1
			GROUP BY u.ID, u.LOGIN, u.NOME, u.PROFILE, u.`STATUS`, u.EMAIL
			HAVING COUNT(u.ID) > 0
			LIMIT 1";

	$stmt = $conexao->conn->prepare($sql);
	$stmt->bindValue(':login',$login);
	$stmt->bindValue(':senha',$senha);
	$stmt->bindValue(':ano',$ano);
	$stmt->execute();
	$usuario = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if(empty($usuario)){
		echo "<script>alert('Login inválido ou sem permissão!');</script>";
		echo "<script>location.href='php/login/';</script>";
		exit();
	}else{

		$sql = "SELECT * FROM empresa";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$empresa = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$sql2 = "SELECT dqwmenu.ID, IFNULL(dqwmenu.FATHER,0) as FATHER, ICON, HREF, CLASS,
				 		 TARGET, S, LEVEL, SYSTEM, dqwmenu.STATE, dqwmenu.TITLE, 
				 		 (SELECT count(D.ID) FROM dqwmenu D WHERE D.FATHER = dqwmenu.ID) AS QTYSUB
				FROM dqwmenu
				INNER JOIN dqwpermission ON dqwpermission.MENU = dqwmenu.ID
				INNER JOIN dqwuser ON dqwuser.PROFILE = dqwpermission.PROFILE
				WHERE dqwmenu.STATE = 1
				  AND dqwpermission.PROFILE = ".$usuario[0]["PROFILE"]."
				  AND (dqwuser.ID = ".$usuario[0]["ID"]." AND dqwuser.STATUS = 1)
				ORDER BY dqwmenu.S ASC";
				
		$stmt2 = $conexao->conn->prepare($sql2);
		$stmt2->execute();
		$menu = $stmt2->fetchAll(PDO::FETCH_ASSOC);

		$_SESSION['usuario'] = $usuario[0];
		$_SESSION['empresa'] = $empresa[0];
		$_SESSION['menu']    = $menu;

	}
}else{
	echo "<script>location.href='php/login/';</script>";
	exit();
}
?>