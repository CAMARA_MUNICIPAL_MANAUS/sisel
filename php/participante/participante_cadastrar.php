<script type="text/javascript" src="js/action/PARTICIPANTE/participante.js"></script>

<div class="col-lg-12 col-md-12">
	<h3 class="page-header">Cadastrar/Editar Participante</h3>
</div>

<style>
	.bloco:hover{
		background-color: #6cc2fb29;
	}
	.main{
		border-bottom: 1px solid #ccc;
		margin-bottom: 5px;
	}
</style>

<div class="col-lg-12 col-md-12">
		<? if(!isset($_GET['licitacaoparticipante'])){ ?>

			<form role='form' id="form-publicacao" method="GET">
				<input type="hidden" name="p" value="participante" />
				<input type="hidden" name="page" value="participante_cadastrar" />

				<div class='col-md-2'>
					<div class='form-group'>
						<label>Nº Processo</label>
						<? $auxiliar->licitacaoParticipante($_SESSION['usuario']['ano_exercicio']); ?>
					</div>
				</div>

				<div class='col-md-2'>
					<div class='form-group'>
						<label>&nbsp;</label>
						<input type='button' class='btn btn-success btn-block cadastrar' value="Iniciar Cadastro">
					</div>
				</div>
			</form>

		<? }else{ ?>

			<p><b>Selecione abaixo o participante para ser inserido no processo Nº <?= $_GET['licitacaoparticipante']; ?>.</b></p>
			<div class="row">
				<form role='form' id="form-publicacao">
					<div class='col-md-2'>
						<div class='form-group'>
							<label>Tipo Participante</label>
							<? $auxiliar->tipoparticipante(); ?>
						</div>
					</div>

					<div class='col-md-2'>
						<div class='form-group'>
							<label>Tipo Pessoa</label>
							<? $auxiliar->tipopessoa(); ?>
							<input type="hidden" id="licitacaoparticipante" name="licitacaoparticipante" value="<?= $_GET['licitacaoparticipante']; ?>"/>
						</div>
					</div>

					<div class='col-md-6'>
						<div class='form-group'>
							<label>Razão Social / Nome</label>
							<select name="razaosocial" class="razaosocial form-control">
								<option value="">Selecione um Participante</option>
							</select>
						</div>
					</div>

					<div class='col-md-2'>
						<div class='form-group'>
							<label>CPF / CNPJ</label>
							<input type="text" id="cpfcnpj" name="cpfcnpj" class="form-control" readonly="true" />
						</div>
					</div>

					<div class='col-md-1'>
						<div class='form-group'>
							<label>&nbsp;</label>
							<button type='button' class='btn btn-success btn-block btn-inserir' value="<?= $_GET['licitacaoparticipante']; ?>">Inserir</button>
						</div>
					</div>

					<div class='col-md-1'>
						<div class='form-group'>
							<label>&nbsp;</label>
							<a href="?p=participante" class="btn btn-danger btn-block">Voltar</a>
						</div>
					</div>
				</form>
			</div>

			<legend>Participantes Envolvidos</legend>

			<?php
				$sql = "SELECT DISTINCT f.razao_social, p.fornecedor_fk, p.tipoparticipante_fk, tp.tipoparticipante, f.cpfcnpj, f.id as idforn
						FROM participante_certidao p
						INNER JOIN fornecedor f ON f.id = p.fornecedor_fk
						INNER JOIN tipoparticipante tp ON tp.id = p.tipoparticipante_fk
						WHERE p.licitacao_fk = '".$_GET['licitacaoparticipante']."'
						ORDER BY f.nome_fantasia ASC";
				$stmt = $conexao->conn->prepare($sql);
				$stmt->execute();
				$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

				foreach ($retorno as $ln){
					echo "<div class='col-lg-12 main' data-fornecedor='".$ln['razao_social']."'>";
						echo "<div class='col-lg-6'>";
							echo "<h5><b><a href='?p=fornecedores&page=fornecedores_cadastrar&fornecedor=".$ln['idforn']."' target='_blank'>".$ln['razao_social']."</a></b></h5>";
						echo "</div>";
						echo "<div class='col-lg-2'>";
							echo "<p><b>".$funcoes->formatarDoc($ln['cpfcnpj'])."</b></p>";
						echo "</div>";
						echo "<div class='col-lg-2'>";
							echo "<p><b>".$ln['tipoparticipante']."</b></p>";
						echo "</div>";
						echo "<div class='col-lg-1 text-right'>";
								echo "<button type='button' class='btn btn-danger btn-excluir-fornecedor btn-circle' title='Remover Participante' data-fornecedor='".$ln['fornecedor_fk']."' data-licitacao='".$_GET['licitacaoparticipante']."'><i class='glyphicon glyphicon-remove'></i></button>";
						echo "</div>";

						echo "<div class='col-lg-12'>";
							echo "<form role='form' data-fornecedor='".$ln['fornecedor_fk']."' data-licitacao='".$_GET['licitacaoparticipante']."'>";
								
								$sql = "SELECT p.id, p.num_certidao, p.dt_emissao, p.dt_validade, p.dias, p.tipocertidao_fk, tc.certidao
										FROM participante_certidao p
										INNER JOIN tipocertidao tc ON tc.id = p.tipocertidao_fk
										WHERE p.licitacao_fk = '".$_GET['licitacaoparticipante']."'
										AND p.fornecedor_fk = ".$ln['fornecedor_fk']."
										ORDER BY p.tipocertidao_fk";
								$stmt = $conexao->conn->prepare($sql);
								$stmt->execute();
								$retorno2 = $stmt->fetchAll(PDO::FETCH_ASSOC);

								foreach ($retorno2 as $ln2) {
									echo "<div class='col-lg-6 col-md-6 bloco'>";
										echo "<div class='row'>";
											echo "<div class='col-lg-4 col-md-4 col-sm-4'>";
												echo "<div class='form-group'>";
													echo "<label><b>".$ln2['certidao']."</b></label>";
													echo "<input type='text' class='certidao form-control' data-id='".$ln2['id']."' data-certidao='".$ln2['tipocertidao_fk']."' value='".$ln2['num_certidao']."' maxlength='60'/>";
												echo "</div>";
											echo "</div>";

											echo "<div class='col-lg-4 col-md-4 col-sm-4'>";
												echo "<div class='form-group'>";
													echo "<label>".$ln2['certidao']." Emissão</label>";
													echo "<input type='date' class='certidao-data-emissao form-control data' data-id='".$ln2['id']."' data-certidao='".$ln2['tipocertidao_fk']."' value='".$ln2['dt_emissao']."' maxlength='10'/>";
												echo "</div>";
											echo "</div>";

											echo "<div class='col-lg-4 col-md-4 col-sm-4'>";
												echo "<div class='form-group'>";
													echo "<label>".$ln2['certidao']." Validade</label>";
													echo "<input type='date' class='certidao-data-validade form-control data' data-id='".$ln2['id']."' data-certidao='".$ln2['tipocertidao_fk']."' value='".$ln2['dt_validade']."' maxlength='10'/>";
												echo "</div>";
											echo "</div>";
										echo "</div>";
									echo "</div>";
								} // foreach

							echo "</form>";								
						echo "</div>";
					echo "</div>";
				} //fim foreach
			} // fim if ?>

	</div>
</div>

