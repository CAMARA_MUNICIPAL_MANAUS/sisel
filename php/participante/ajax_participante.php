<?php


if(isset($_POST['acao'])){
	
	include_once '../../config/conn.php';
	include_once '../../config/funcoesgerais.php';

	$funcoes  = new Funcoes;

	switch ($_POST['acao']) {
		case 'atualizarCertidao':

		/*print_r($_POST);
		exit();*/
			
			
			$sql = "UPDATE participante_certidao
						SET num_certidao = :certidao,
							dt_emissao = :emissao,
							dt_validade = :validade
						WHERE id = :codigo";
    		$stmt = $conexao->conn->prepare($sql);
    		
    		$stmt->bindValue(':codigo', $_POST['id']);
    		$stmt->bindValue(':certidao', $_POST['certidao']);
    		$stmt->bindValue(':emissao', $funcoes->dateBRparaUSMySql($_POST['certidao_emissao']));
    		$stmt->bindValue(':validade', $funcoes->dateBRparaUSMySql($_POST['certidao_validade']));

    		if($stmt->execute()){
    			echo 1;
    		}else{
    			echo 0;
    			print_r($stmt->errorinfo());
    		}
			
		break;

		case 'listarFornecedores':

			$sql = "SELECT f.id, trim(f.razao_social) as razao_social, formatarCpfCnpj(f.cpfcnpj) as cpfcnpj
					FROM fornecedor f
					WHERE f.id NOT IN (SELECT distinct p.fornecedor_fk
											FROM participante_certidao p
											WHERE p.licitacao_fk = '".$_POST['licitacao']."'
											AND p.fornecedor_fk)
					AND f.tipopessoa_fk = ".$_POST['tipopessoa']."
					ORDER BY trim(f.razao_social) ASC";
    		$stmt = $conexao->conn->prepare($sql);
    		$stmt->execute();

    		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

    		echo '<option value="">Selecione um Participante</option>';

    		foreach ($retorno as $ln) {
    			echo "<option value='".$ln['id']."' cpfcnpj='".$ln['cpfcnpj']."'>".$ln['razao_social']."</option>";
    		}

		break;

		case 'inserir':

			$sql = "INSERT INTO `participante_certidao`
						VALUES (null, :licitacaoparticipante, :razaosocial, :tipoparticipante, null, null, null, null, 1),
			 				   (null, :licitacaoparticipante, :razaosocial, :tipoparticipante, null, null, null, null, 2),
			 				   (null, :licitacaoparticipante, :razaosocial, :tipoparticipante, null, null, null, null, 3),
			 				   (null, :licitacaoparticipante, :razaosocial, :tipoparticipante, null, null, null, null, 4),
			 				   (null, :licitacaoparticipante, :razaosocial, :tipoparticipante, null, null, null, null, 5),
			 				   (null, :licitacaoparticipante, :razaosocial, :tipoparticipante, null, null, null, null, 6),
			 				   (null, :licitacaoparticipante, :razaosocial, :tipoparticipante, null, null, null, null, 7),
			 				   (null, :licitacaoparticipante, :razaosocial, :tipoparticipante, null, null, null, null, 99);";
    		$stmt = $conexao->conn->prepare($sql);
    		
    		$stmt->bindValue(':tipoparticipante', $_POST['tipoparticipante']);
    		$stmt->bindValue(':razaosocial', $_POST['razaosocial']);
    		$stmt->bindValue(':licitacaoparticipante', $_POST['licitacaoparticipante']);

    		if($stmt->execute()){
    			echo 1;
    		}else{
    			echo 0;
    		}
		break;

		case 'removerParticipante':

			$sql = "DELETE FROM participante_certidao WHERE fornecedor_fk = ".$_POST['fornecedor']." AND licitacao_fk = '".$_POST['licitacao']."'";
    		$stmt = $conexao->conn->prepare($sql);
    		if($stmt->execute()){
    			echo 1;
    		}else{
    			echo 0;
    			print_r($stmt->errorinfo());
    		}
		break;

		case 'excluirTodosParticipantes':

			$sql = "DELETE FROM participante_certidao WHERE licitacao_fk = '".$_POST['licitacao']."'";
    		$stmt = $conexao->conn->prepare($sql);
    		if($stmt->execute()){
    			echo 1;
    		}else{
    			echo 0;
    			print_r($stmt->errorinfo());
    		}

		break;
	}
}

?>