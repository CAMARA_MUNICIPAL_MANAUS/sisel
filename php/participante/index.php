<script type="text/javascript" src="js/action/PARTICIPANTE/participante.js"></script>

<!-- DATATABLES -->
<link href="plugin/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet" />
<link href="plugin/datatables-responsive/dataTables.responsive.css" rel="stylesheet" />

<!-- DATATABLES -->
<script src="plugin/datatables/js/jquery.dataTables.js"></script>
<script src="plugin/datatables-plugins/dataTables.bootstrap.js"></script>
<script src="plugin/datatables-responsive/dataTables.responsive.js"></script>

<div class="col-lg-12">
	<h3 class="page-header">Participante</h3>
	<input type="hidden" id="anoExercicio" name="anoExercicio" value="<?php echo $_SESSION['usuario']['ano_exercicio']; ?>"/>
</div>

<div class="col-lg-12">
	<a href="?p=participante&page=participante_cadastrar" class="btn btn-primary btn-sm novo"><i class="glyphicon glyphicon-plus"></i> Novo Participante</a>
	<br><br>
	<table class="table table-hover table-striped table-condensed" width="100%" id="tablecotacao">
		<thead>
			<tr>
				<th></th>
				<th>Nº Processo</th>
				<th>Participantes</th>
				<th>CPF/CNPJ</th>
				<th class='text-center'>Empenho</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?

				$sql = "SELECT p.id, p.licitacao_fk as licitacao,
							   GROUP_CONCAT(distinct concat('<a href=?p=fornecedores&page=fornecedores_cadastrar&fornecedor=',f.id, ' target=_blank>',f.razao_social,'</a>') ORDER BY f.razao_social ASC SEPARATOR '<br/>') as participantes,
							   GROUP_CONCAT(distinct concat(formatarCpfCnpj(f.cpfcnpj)) ORDER BY f.razao_social ASC SEPARATOR '<br/>') as cpfcnpj_participantes,
							   l.ano_exercicio, l.mes_empenho
						FROM participante_certidao p
						INNER JOIN fornecedor f ON f.id = p.fornecedor_fk
						INNER JOIN licitacao l ON l.id = p.licitacao_fk
						WHERE l.ano_exercicio = ".$_SESSION['usuario']['ano_exercicio']."
						GROUP BY p.licitacao_fk
						ORDER BY p.licitacao_fk ASC";
													
				$stmt = $conexao->conn->prepare($sql);
				$stmt->execute();
				$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

				$i = 1;
				foreach ($retorno as $ln) {
				?>
					<tr>
						<td>
							<a class='btn btn-primary btn-sm btn-visualizar btn-circle' target='_blank' href='?p=participante&page=participante_cadastrar&licitacaoparticipante=<?= $ln['licitacao']; ?>'><i class='glyphicon glyphicon-search'></i></a>
						</td>
						<td><?= $ln['licitacao']; ?></td>
						<td><?= $ln['participantes']; ?></td>
						<td><?= $ln['cpfcnpj_participantes']; ?></td>
						<td class='text-center'><?= $ln['mes_empenho'].$ln['ano_exercicio']; ?></td>
						<td>
							<button type='button' class='btn btn-danger btn-sm btn-excluir-todos-participantes btn-circle'
							data-licitacao="<?= $ln['licitacao'] ?>" title="Remover todos os participantes do processo Nº <?= $ln['licitacao']?>"><i class='glyphicon glyphicon-remove'></i></button>
						</td>
					</tr>
				<?
				}
			?>
		</tbody>
	</table>
</div>

<script>
	$(function(){
		$('#tablecotacao').DataTable({
	        responsive: true
	    });
	})
</script>