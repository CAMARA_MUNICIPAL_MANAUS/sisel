<script type="text/javascript" src="js/action/CONTRATO/contrato.js"></script>

<!-- DATATABLES -->
<link href="plugin/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet" />
<link href="plugin/datatables-responsive/dataTables.responsive.css" rel="stylesheet" />

<!-- DATATABLES -->
<script src="plugin/datatables/js/jquery.dataTables.js"></script>
<script src="plugin/datatables-plugins/dataTables.bootstrap.js"></script>
<script src="plugin/datatables-responsive/dataTables.responsive.js"></script>

<div class="col-lg-12 col-md-12 col-sm-12">
    <h3 class="page-header">Contratos</h3>
</div>

<div class="col-lg-12 col-md-12 col-sm-12">
    <a href="?p=contrato&page=contrato_cadastrar" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-plus"></i> Nova Contrato</a> <br>
    <br>
    <table class="table table-hover table-striped table-condensed" width="100%" id="tableContratos">
        <thead>
            <tr>
                <th></th>
                <th>Nº Contrato</th>
                <th>Nome do Contratado</th>
                <th>CPF/CNPJ</th>
                <th>Objetivo</th>
                <th class='text-right'>Valor</th>
                <th class='text-right'>Dt_Contrato</th>
                <th class='text-right'>Empenho</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $sql = "SELECT c.idcontrato, c.numcontrato, f.razao_social, f.cpfcnpj, c.objetivo, c.valor, c.dtcontrato, c.mesEmpenho, c.anoEmpenho
            FROM contrato c
            INNER JOIN fornecedor f ON f.id = c.fornecedor_fk
            WHERE c.ano_exercicio = ".$_SESSION['usuario']['ano_exercicio']."
            ORDER BY c.mesEmpenho, c.anoEmpenho ASC";                             
            $stmt = $conexao->conn->prepare($sql);
            $stmt->execute();
            $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $i = 1;
            foreach ($retorno as $ln) {
                echo "<tr>";
                    echo "<td>";
                        echo "<a class='btn btn-primary btn-sm btn-circle' href='?p=contrato&page=contrato_cadastrar&contrato=".$ln['idcontrato']."' id='{$ln['idcontrato']}'><i class='glyphicon glyphicon-search'></i></a>";
                    echo "</td>";
                    echo "<td>".$ln['numcontrato']."</td>";
                    echo "<td>".$ln['razao_social']."</td>";
                    echo "<td>".$funcoes->formatarDoc($ln['cpfcnpj'],'')."</td>";
                    echo "<td title='{$ln['objetivo']}' class='text-center'><i class='blue fa fa-info-circle'></i></td>";
                    echo "<td class='text-right'>R$ ".number_format($ln['valor'], 2, ',', '.')."</td>";
                    echo "<td class='text-right'>".$funcoes->dateUSparaBR($ln['dtcontrato'])."</td>";
                    echo "<td class='text-right'>".str_pad($ln['mesEmpenho'], 2, "0", STR_PAD_LEFT).$ln['anoEmpenho']."</td>";
                    echo "<td class='text-right'>";
                    if(file_exists("anexo/contrato/".$ln['numcontrato'].".pdf")){
                        echo "<a href='anexo/contrato/".$ln['numcontrato'].".pdf' class='btn btn-default' target='_blank'><i class='glyphicon glyphicon-file'></i></a>";
                        echo "&nbsp;";
                    }
                    echo "<button type='button' class='btn btn-danger btn-excluir btn-sm btn-circle' data-codigo='{$ln['idcontrato']}'><i class='glyphicon glyphicon-remove'></i></button>";
                    echo "</td>";
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>
</div>

<script>
    $(function(){
        $('#tableContratos').DataTable({
            responsive: true
        });
    })
</script>