<script type="text/javascript" src="js/action/CONTRATO/contrato.js?001"></script>

<?php
if(isset($_GET['contrato'])){
    $contrato = $funcoes->buscardados_campo("contrato", "idcontrato",$_GET['contrato']);
    $contrato = $contrato[0];

    $fornecedor = $funcoes->buscardados("fornecedor", $contrato['fornecedor_fk']);
    $fornecedor = $fornecedor[0];
}
?>

<div class="col-lg-12 col-md-12 col-sm-12">
    <h3 class="page-header">Cadastrar/Editar Contratos</h3>
</div>

<div class="col-lg-12 col-md-12 col-sm-12">
    <form role="form" method="post" action="?p=contrato&page=ajax_contrato" class="formContrato" enctype="multipart/form-data">

        <div class="col-lg-3 col-md-3 col-xs-2">
            <div class="form-group">
                <label class="label-obrigatorio">Tipo Pessoa:</label>
                <? $auxiliar->tipopessoa(@$fornecedor['tipopessoa_fk']); ?>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
            <div class="form-group">
                <label class="label-obrigatorio">Contratado: </label>

                <? if(!isset($_GET['contrato'])){ ?>

                <select id="contratado" name="contratado" class="form-control">
                    <option value="">Selecione o campo Tipo Pesoa</option>
                </select>

                <? }else{

                    $auxiliar->fornecedor($contrato['fornecedor_fk']);

                } ?>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <div class="form-group">
                <label class="label-obrigatorio">CPF/CNPJ:</label>
                <input type="text" id="cpfcnpj" name="cpfcnpj" readonly="readonly" class="form-control" value="<?= @$fornecedor['cpfcnpj'] ?>" />
            </div>
        </div>

        <!-- ### -->

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <div class="form-group">
                <label class="label-obrigatorio">Contrato decorrente de licitação:</label>
                <? $auxiliar->tipocontratodecorrente(@$contrato['tipocontratodecorrente_fk']); ?>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
            <div class="form-group">
                <label class="label-obrigatorio">Nº da Ata:</label> <a href='#' title="Informar se o campo 'Contrato decorrente de Licitação' for igual a (Adesão à ata de registro de preço)."><i class='glyphicon glyphicon-info-sign'></i></a>
                <?
                    /*if( @$contrato['numeroata'] != "" ){
                        $disa = "";
                    }else{
                        $disa = "disabled='true'";
                    }*/
                    ?>
                    <input type="text" id="numeroata" name="numeroata" maxlength="18" class="form-control" value="<?= @$contrato['numeroata'] ?>"/>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <div class="form-group">
                    <label class="label-obrigatorio">Tipo do contrato:</label>
                    <? $auxiliar->tipocontrato(@$contrato['tipocontrato_fk']); ?>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
                <div class="form-group">
                    <label class="label-obrigatorio">N&ordm; Contrato</label>
                    <input type="text" id="numcontrato" name="numcontrato" maxlength="18" class="form-control" value="<?= @$contrato['numcontrato'] ?>"/>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
                <div class="form-group">
                    <label class="label-obrigatorio">Receber Valor:</label>
                    <select name="recebeValor" id="recebeValor" class="form-control">
                        <option value="S" <? if(@$contrato['recebevalor'] == 'S'){ echo "selected"; } ?>>SIM</option>
                        <option value="N" <? if(@$contrato['recebevalor'] == 'N'){ echo "selected"; } ?>>N&Atilde;O</option>
                    </select>
                </div>
            </div>

            <div class="clearfix"></div>
            <!-- ### -->

            <?php
            if(isset($contrato['tipocontrato_fk']) && @$contrato['tipocontrato_fk'] == 2){
                $display = "block";
            }else{
                $display = "none";
            }
            ?>

            <div class="divAdtivo jumbotron" style="display: <?= $display; ?>">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
                    <div class="form-group">
                        <label for="">Nº do Contrato Superior</label> <a href='#' title="Caso o contrato seja Aditivo, deverá ser informado o contrato principal do objeto a que se referir (campo obrigatório para os contratos principais já informados por meio do Portal e-Contas - observando o ano de implantação do sistema). O número informado deverá coincidir, inclusive em seu formato, com o número do contrato informado ao Tribunal."><i class='glyphicon glyphicon-info-sign'></i></a>
                        <? $auxiliar->numContrato(@$contrato['contrato_pai']); ?>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
                    <div class="form-group">
                        <label for="">Tipo do Aditivo</label> <a href='#' title="Campo obrigatório sempre que o contrato for Aditivo."><i class='glyphicon glyphicon-info-sign'></i></a>
                        <? $auxiliar->tipoaditivo(@$contrato['tipodoaditivo']); ?>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
                    <div class="form-group">
                        <label for="">Tipo do Valor do Aditivo</label> <a href='#' title="Campo obrigatório sempre que o contrato for Aditivo."><i class='glyphicon glyphicon-info-sign'></i></a>
                        <? $auxiliar->tipovaloraditivo(@$contrato['tipoaditivo']); ?>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="label-obrigatorio">Nº Sequencial do Aditivo:</label>
                        <a href='#' title="Número sequêncial do Termo Aditivo ao Contrato. Exemplo:&#13;Primeiro termo aditivo ao Contrato 102/2015.&#13;Preencher '01'.&#13;Segundo termo aditivo ao Contrato 102/2015.&#13;Preencher '02'."><i class='glyphicon glyphicon-info-sign'></i></a>
                        <input type="text" id="sequencialaditivo" name="sequencialaditivo" maxlength="2" class="form-control sonums" value="<?= @$contrato['sequencialaditivo'] ?>"/>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label for="">CNPJ OU UG do Contrato Original</label> <a href='#' title="Campo obrigatório sempre que for aditivo e o contrato original tiver sido celebrado por outra UG."><i class='glyphicon glyphicon-info-sign'></i></a>
                        <input type="text" class="form-control sonums cnpj cnpjug" maxlength="14" name="cnpjug" value="<?= @$contrato['cnpjug'] ?>"/>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- ### -->

            <div class="clearfix"></div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
              <div class="form-group">
                  <label class="label-obrigatorio">Natureza do Objeto:</label>
                  <?php $auxiliar->tiponatureza(@$contrato['tiponatureza_fk']); ?>
              </div>
          </div>

          <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
            <div class="form-group">
                <label class="label-obrigatorio">Objeto:</label>
                <input name="obj" type="text" id="obj" class="form-control" value="<?= @$contrato['objetivo'] ?>"/>
            </div>
        </div>

        <div class="clearfix"></div>

        <!-- ### -->        

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
            <div class="form-group">
                <label class="label-obrigatorio">Ordenador:</label>
                <? $ordenador = (isset($contrato['responsavel_juridico'])) ? $contrato['responsavel_juridico'] : $_SESSION['empresa']['ordenador']; ?>
                <input type="text" name="ordernador" id="ordernador" class="form-control" value="<?= $ordenador ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
            <div class="form-group">
                <label>Moeda:</label>
                <?php $auxiliar->moeda(@$contrato['moeda_fk']); ?>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
            <div class="form-group">
                <label>Valor:</label>
                <input type="text" id="valor" name="valor" class="form-control dinheiro" value="<?= @number_format($contrato['valor'], 2, ',', '.'); ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
            <div class="form-group">
                <!-- <label>Licita&ccedil;&atilde;o:</label> -->
                <label>Nº Proc. de Compras:</label>
                <input type="text" id="licitacao" name="licitacao" class="form-control" maxlength="18" value="<?= @$contrato['licitacao_fk'] ?>"/>
            </div>
        </div>

        <!-- ### -->
        <div style='clear: both;'></div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
            <div class="form-group">
                <label class="label-obrigatorio">Dt. Ass. Contrato:</label>
                <input type="text" id="dtcontrato" name="dtcontrato" class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtcontrato']); ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
            <div class="form-group">
                <label>Prazos(dias):</label>
                <input type="text" id="dias" name="dias" class="form-control" readonly="readonly" />
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
            <div class="form-group">
                <label class="label-obrigatorio">Dt. Venc.:</label>
                <input type="text" id="dtvencimento" name="dtvencimento" class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtvencimento']) ?>"/>
            </div>
        </div>

        <!-- ### -->

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
            <div class="form-group">
                <label>N&ordm; DOE:</label>
                <input name="numdoe" type="text" id="numdoe" class="form-control" value="<?= @$contrato['num_doe'] ?>" />
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
            <div class="form-group">
                <label>Dt. Public.:</label>
                <input type="text" id="dtpublic" name="dtpublic" class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtpublicacao']) ?>" />
            </div>
        </div>   

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
            <div class="form-group">
                <label>Arq: Texto:</label>
                <input name="arqtexto" type="text" id="arqtexto" class="form-control" value="<?= @$contrato['nomearquivotexto'] ?>" />
            </div>
        </div>

        <?php

		// $arquivopdf = "d:/web/localuser/sisel-am/www/cmm/anexo/contrato/TACT10016-2014.pdf";
        $arquivopdf = "";
        if (file_exists($arquivopdf)){ ?>

        <div class="col-md-1">
         <div class="form-group">
            <label for="" class="control-label">&nbsp;</label>
            <a href="anexo/contrato/<?= @$contrato['numcontrato'] ?>.pdf" target="_blank" class="btn btn-info" title="Visualizar Arquivo"><i class="glyphicon glyphicon-search"></i></a>
        </div>
    </div>

    <? } ?>

    <div class="col-md-11">
        <div class="form-group">
            <label for="">Anexo Contrato <small>(Somente arquivos no formato PDF.)</small></label>
            <input type="file" name="contrato" />
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
        <div class="form-group">
            <label class="label-obrigatorio">Competência:</label>
            <?= $auxiliar->mes(@$contrato['mesEmpenho']); ?>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-4">
        <div class="form-group">
            <label>&nbsp;</label>
            <?= $auxiliar->anoexercicio(@$contrato['anoEmpenho']); ?>
        </div>
    </div>

    <div class="clearfix"></div>
    <!-- ############################################################################################################ -->
    <!-- ############################################################################################################ -->



    <div class="col-lg-5 col-md-5 col-sm-12">
        <legend>Empenhos</legend>
        <?php
        for ($i=1; $i <= 12; $i++) {

            $number_empenho = ($i == 1) ? "" : $i;

            echo "<div class='col-lg-6 col-md-12 col-sm-12 col-xs-3'>";
            echo "<div class='form-group'>";
            echo "<label>N&ordm; Empenho ".str_pad($i, 2, "0", STR_PAD_LEFT)."</label>";
            echo "<input name='e$i' type='text' id='e$i'  maxlength='10' class='form-control' value='".@$contrato['numempenho'.$number_empenho]."' />";
            echo "</div>";
            echo "</div>";
        }
        ?>
    </div>

    <div class="col-lg-7 col-md-7 col-sm-12">
        <legend>Certidões Negativas</legend>

        <div class="col-lg-6 col-md-6 col-xs-6">
            <div class="form-group">
                <label>INSS</label>
                <input name='cnd_inss' type='text' id='inss'  maxlength='60' class="form-control" value="<?= @$contrato['certificadoinss'] ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Emiss&atilde;o:</label>
                <input name='dt_emissao_inss' type='text' id='dte1' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtcertidaoinss']) ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Vencimento:</label>
                <input name='dt_validade_inss' type='text' id='dtv1' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtvalidadeinss']) ?>"/>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
                <label>FGTS:</label>
                <input name='cnd_fgts' type='text' id='fgts' maxlength='60'  class="form-control" value="<?= @$contrato['numcertidaofgts'] ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Emiss&atilde;o:</label>
                <input name='dt_emissao_fgts' type='text' id='dte2' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtcertidaofgts']) ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Vencimento:</label>
                <input name='dt_validade_fgts' type='text' id='dtv2' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtvalidadefgts']) ?>"/>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
                <label>Sec. Fazenda Estadual</label>
                <input name='cn_fazenda_estadual' type='text' id='sfe'  maxlength='60'  class="form-control" value="<?= @$contrato['numcertestmuncarat'] ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Emiss&atilde;o:</label>
                <input name='dt_emissao_secfe' type='text' id='dte3' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtcertestmuncarat']) ?>" />
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Vencimento:</label>
                <input name='dt_validade_secfe' type='text' id='dtv3' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtvalestmuncarat']) ?>" />
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
                <label>Sec. Fazenda Municipal</label>
                <input name='cnd_fazenda_municipal' type='text' id='sfm'  maxlength='60' class="form-control" value="<?= @$contrato['numcertidaofm'] ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Emiss&atilde;o:</label>
                <input name='dt_emissao_secfm' type='text' id='dte4' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtcertidaofm']) ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Vencimento:</label>
                <input name='dt_validade_secfm' type='text' id='dtv4' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtvalidadefm']) ?>"/>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
                <label>Sec. Fazenda Federal</label>
                <input name='cnd_fazenda_federal' type='text' id='sff' maxlength='60' class="form-control" value="<?= @$contrato['numcertidaoff'] ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Emiss&atilde;o:</label>
                <input name='dt_emissao_secff' type='text' id='dte5' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtcertidaoff']) ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Vencimento:</label>
                <input name='dt_validade_secff' type='text' id='dtv5' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtvalidadeff']) ?>"/>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
                <label>CNDT</label>
                <input name='tst' type='text' id='tst'  maxlength='60'  class="form-control" value="<?= @$contrato['numcertidaotst'] ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Emiss&atilde;o:</label>
                <input name='dt_emissao_tst' type='text' id='dte8' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtcertidaotst']) ?>"/>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Vencimento:</label>
                <input name='dt_validade_tst' type='text' id='dtv8' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtvalidadetst']) ?>"/>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
                <label>Outras Certid&otilde;es:</label>
                <input name='cnd_outros' type='text' id='oc'  maxlength='60'  class="form-control" value="<?= @$contrato['numcertidaooutros'] ?>" />
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Emiss&atilde;o:</label>
                <input name='dt_emissao_outras' type='text' id='dte6' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtcertidaooutros']) ?>" />
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Dt. Vencimento:</label>
                <input name='dt_validade_outras' type='text' id='dtv6' class="form-control data" value="<?= $funcoes->dateUSparaBR(@$contrato['dtvalidadeoutros']) ?>" />
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <? if(isset($_GET['contrato'])){ ?>
                <button type="button" class="btn btn-success btn-editar">Salvar</button>
                <input type="hidden" name="codigo" class="codigo" value="<?= @$_GET['contrato'] ?>" />
                <input type="hidden" name="acao" class="acao" value="editar" />
                <? }else{ ?>
                <button type="button" class="btn btn-success btn-inserir">Inserir</button>
                <input type="hidden" name="acao" class="acao" value="inserir" />
                <? } ?>

                <a href="?p=contrato" class="btn btn-danger">Cancelar</a>
            </div>
        </div>
    </div>
</form>
</div>