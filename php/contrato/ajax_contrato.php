<?php

if( isset($_POST['tipopessoa']) && !isset($_POST['acao']) ){
    include_once '../../config/conn.php';

	$sql = "SELECT f.id, f.razao_social, f.nome_fantasia, f.cpfcnpj FROM fornecedor f WHERE f.tipopessoa_fk = :tipopessoa ORDER BY trim(razao_social) ASC";
    $stmt = $conexao->conn->prepare($sql);
    $stmt->bindValue(':tipopessoa',$_POST['tipopessoa']);
    $stmt->execute();
    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo "<option value=''>Selecione um Fornecedor</option>";
    foreach ($retorno as $t) {
    	echo "<option cpfcnpj='{$t['cpfcnpj']}' value='{$t['id']}'>{$t['razao_social']}</option>";
    }
}

if(isset($_POST['acao'])){

    if( @$_FILES['contrato']['name'] != "" && @$_FILES['contrato']['type'] != "application/pdf"){
        echo "<script>alert('Seu arquivo não é do formato PDF. \nSuas informações serão salvas mas não faremos upload do anexo.')</script>";
    }else if( @$_FILES['contrato']['name'] != "" && @$_FILES['contrato']['type'] == "application/pdf"){

        move_uploaded_file($_FILES['contrato']['tmp_name'], "anexo/contrato/".utf8_decode($_POST['arqtexto']));

    }

    switch ($_POST['acao']) {
        case 'inserir':

            $insert = "INSERT INTO contrato (numcontrato, tipocontrato_fk, tipocontratodecorrente_fk, numeroata, fornecedor_fk, tiponatureza_fk, objetivo, responsavel_juridico, valor, recebevalor, moeda_fk, licitacao_fk, dtcontrato, dtvencimento, dtpublicacao, num_doe, nomearquivotexto, certificadoinss, dtcertidaoinss, dtvalidadeinss, numcertidaofgts, dtcertidaofgts, dtvalidadefgts, numcertestmuncarat, dtcertestmuncarat, dtvalestmuncarat, numcertidaofm, dtcertidaofm, dtvalidadefm, numcertidaoff, dtcertidaoff, dtvalidadeff, numcertidaotst, dtcertidaotst, dtvalidadetst, numcertidaooutros, dtcertidaooutros, dtvalidadeoutros, numempenho, numempenho2, numempenho3, numempenho4, numempenho5, numempenho6, numempenho7, numempenho8, numempenho9, numempenho10, numempenho11, numempenho12, contrato_pai, tipoaditivo, tipodoaditivo, sequencialaditivo, cnpjug, ano_exercicio, usuario, mesEmpenho, anoEmpenho)

                VALUES (:numcontrato, :tipocontrato_fk, :tipocontratodecorrente_fk, :numeroata, :fornecedor_fk, :tiponatureza_fk, :objetivo, :responsavel_juridico, :valor, :recebevalor, :moeda_fk, :licitacao_fk, :dtcontrato, :dtvencimento, :dtpublicacao, :num_doe, :nomearquivotexto, :certificadoinss, :dtcertidaoinss, :dtvalidadeinss, :numcertidaofgts, :dtcertidaofgts, :dtvalidadefgts, :numcertestmuncarat, :dtcertestmuncarat, :dtvalestmuncarat, :numcertidaofm, :dtcertidaofm, :dtvalidadefm, :numcertidaoff, :dtcertidaoff, :dtvalidadeff, :numcertidaotst, :dtcertidaotst, :dtvalidadetst, :numcertidaooutros, :dtcertidaooutros, :dtvalidadeoutros, :numempenho, :numempenho2, :numempenho3, :numempenho4, :numempenho5, :numempenho6, :numempenho7, :numempenho8, :numempenho9, :numempenho10, :numempenho11, :numempenho12, :contrato_pai, :tipoaditivo, :tipodoaditivo, :sequencialaditivo, :cnpjug, :ano_exercicio, :usuario, :mesEmpenho, :anoEmpenho)";

            $stmt = $conexao->conn->prepare($insert);
            $stmt->bindValue(':numcontrato', $_POST['numcontrato']);
            $stmt->bindValue(':tipocontrato_fk', $_POST['tipocontrato']);
            $stmt->bindValue(':tipocontratodecorrente_fk', $_POST['tipocontratodecorrente']);
            $stmt->bindValue(':numeroata', $_POST['numeroata']);
            $stmt->bindValue(':fornecedor_fk', $_POST['contratado']);
            $stmt->bindValue(':tiponatureza_fk', $_POST['tiponatureza']);
            $stmt->bindValue(':objetivo', $_POST['obj']);
            $stmt->bindValue(':responsavel_juridico', $_POST['ordernador']);
            $stmt->bindValue(':valor', $funcoes->formatarValorParaBanco($_POST['valor']));
            $stmt->bindValue(':recebevalor', $_POST['recebeValor']);
            $stmt->bindValue(':moeda_fk', $_POST['tipomoeda']);
            $stmt->bindValue(':licitacao_fk', $_POST['licitacao']);
            $stmt->bindValue(':dtcontrato', $funcoes->dateBRparaUSMySql($_POST['dtcontrato']));
            $stmt->bindValue(':dtvencimento', $funcoes->dateBRparaUSMySql($_POST['dtvencimento']));
            $stmt->bindValue(':dtpublicacao', $funcoes->dateBRparaUSMySql($_POST['dtpublic']));
            $stmt->bindValue(':num_doe', $_POST['numdoe']);
            $stmt->bindValue(':nomearquivotexto', $_POST['arqtexto']);
            $stmt->bindValue(':certificadoinss', $_POST['cnd_inss']);
            $stmt->bindValue(':dtcertidaoinss', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_inss']));
            $stmt->bindValue(':dtvalidadeinss', $funcoes->dateBRparaUSMySql($_POST['dt_validade_inss']));
            $stmt->bindValue(':numcertidaofgts', $_POST['cnd_fgts']);
            $stmt->bindValue(':dtcertidaofgts', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_fgts']));
            $stmt->bindValue(':dtvalidadefgts', $funcoes->dateBRparaUSMySql($_POST['dt_validade_fgts']));
            $stmt->bindValue(':numcertestmuncarat', $_POST['cn_fazenda_estadual']);
            $stmt->bindValue(':dtcertestmuncarat', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_secfe']));
            $stmt->bindValue(':dtvalestmuncarat', $funcoes->dateBRparaUSMySql($_POST['dt_validade_secfe']));
            $stmt->bindValue(':numcertidaofm', $_POST['cnd_fazenda_municipal']);
            $stmt->bindValue(':dtcertidaofm', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_secfm']));
            $stmt->bindValue(':dtvalidadefm', $funcoes->dateBRparaUSMySql($_POST['dt_validade_secfm']));
            $stmt->bindValue(':numcertidaoff', $_POST['cnd_fazenda_federal']);
            $stmt->bindValue(':dtcertidaoff', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_secff']));
            $stmt->bindValue(':dtvalidadeff', $funcoes->dateBRparaUSMySql($_POST['dt_validade_secff']));
            $stmt->bindValue(':numcertidaotst', $_POST['tst']);
            $stmt->bindValue(':dtcertidaotst', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_tst']));
            $stmt->bindValue(':dtvalidadetst', $funcoes->dateBRparaUSMySql($_POST['dt_validade_tst']));
            $stmt->bindValue(':numcertidaooutros', $_POST['cnd_outros']);
            $stmt->bindValue(':dtcertidaooutros', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_outras']));
            $stmt->bindValue(':dtvalidadeoutros', $funcoes->dateBRparaUSMySql($_POST['dt_validade_outras']));
            $stmt->bindValue(':numempenho', $_POST['e1']);
            $stmt->bindValue(':numempenho2', $_POST['e2']);
            $stmt->bindValue(':numempenho3', $_POST['e3']);
            $stmt->bindValue(':numempenho4', $_POST['e4']);
            $stmt->bindValue(':numempenho5', $_POST['e5']);
            $stmt->bindValue(':numempenho6', $_POST['e6']);
            $stmt->bindValue(':numempenho7', $_POST['e7']);
            $stmt->bindValue(':numempenho8', $_POST['e8']);
            $stmt->bindValue(':numempenho9', $_POST['e9']);
            $stmt->bindValue(':numempenho10', $_POST['e10']);
            $stmt->bindValue(':numempenho11', $_POST['e11']);
            $stmt->bindValue(':numempenho12', $_POST['e12']);
            $stmt->bindValue(':contrato_pai', $funcoes->validaValorRetornaNulo($_POST['contratoAdtivo']), PDO::PARAM_INT);
            $stmt->bindValue(':tipoaditivo', $funcoes->validaValorRetornaNulo($_POST['tipoaditivo']), PDO::PARAM_INT);
            $stmt->bindValue(':tipodoaditivo', $funcoes->validaValorRetornaNulo($_POST['tipoaditivo2']), PDO::PARAM_INT);
            $stmt->bindValue(':sequencialaditivo', $funcoes->validaValorRetornaNulo($_POST['sequencialaditivo']), PDO::PARAM_INT);
            $stmt->bindValue(':cnpjug', $funcoes->deixarSomenteNumeros($_POST['cnpjug']) );
            $stmt->bindValue(':ano_exercicio', $_SESSION['usuario']['ano_exercicio']);
            $stmt->bindValue(':usuario', $_SESSION['usuario']['id']);
            $stmt->bindValue(':mesEmpenho', $_POST['mes']);
            $stmt->bindValue(':anoEmpenho', $_POST['anoexercicio']);
            
            if($stmt->execute()){
                echo "<script>alert('Contrato inserido com sucesso!')</script>";
                echo "<script>location.href='?p=contrato'</script>";
            }else{
                echo "<script>alert('Falha ao inserir contrato, tente novamente mais tarde!')</script>";
                // echo "<script>history.back(1)';</script>";
                echo "<pre>";
                    // echo "Data: ".$funcoes->dateBRparaUSMySql($_POST['dt_emissao_inss']);
                    print_r($stmt->errorInfo());
                    // print_r($_POST);
                echo "</pre>";
            }


        break;

        case 'editar':
		/*
			print_r($_POST);
			
			echo $funcoes->formatarValorParaBanco($_POST['valor']);
			exit();
*/
            $update = "UPDATE contrato
                        SET numcontrato      = :numcontrato,
                        tipocontrato_fk      = :tipocontrato_fk,
                        tipocontratodecorrente_fk = :tipocontratodecorrente_fk,
                        numeroata            = :numeroata,
                        fornecedor_fk        = :fornecedor_fk,
                        tiponatureza_fk      = :tiponatureza_fk,
                        objetivo             = :objetivo,
                        responsavel_juridico = :responsavel_juridico,
                        valor                = :valor,
                        recebevalor          = :recebevalor,
                        moeda_fk             = :moeda_fk,
                        licitacao_fk         = :licitacao_fk,
                        dtcontrato           = :dtcontrato,
                        dtvencimento         = :dtvencimento,
                        dtpublicacao         = :dtpublicacao,
                        num_doe              = :num_doe,
                        nomearquivotexto     = :nomearquivotexto,
                        certificadoinss      = :certificadoinss,
                        dtcertidaoinss       = :dtcertidaoinss,
                        dtvalidadeinss       = :dtvalidadeinss,
                        numcertidaofgts      = :numcertidaofgts,
                        dtcertidaofgts       = :dtcertidaofgts,
                        dtvalidadefgts       = :dtvalidadefgts,
                        numcertestmuncarat   = :numcertestmuncarat,
                        dtcertestmuncarat    = :dtcertestmuncarat,
                        dtvalestmuncarat     = :dtvalestmuncarat,
                        numcertidaofm        = :numcertidaofm,
                        dtcertidaofm         = :dtcertidaofm,
                        dtvalidadefm         = :dtvalidadefm,
                        numcertidaoff        = :numcertidaoff,
                        dtcertidaoff         = :dtcertidaoff,
                        dtvalidadeff         = :dtvalidadeff,
                        numcertidaotst       = :numcertidaotst,
                        dtcertidaotst        = :dtcertidaotst,
                        dtvalidadetst        = :dtvalidadetst,
                        numcertidaooutros    = :numcertidaooutros,
                        dtcertidaooutros     = :dtcertidaooutros,
                        dtvalidadeoutros     = :dtvalidadeoutros,
                        numempenho           = :numempenho,
                        numempenho2          = :numempenho2,
                        numempenho3          = :numempenho3,
                        numempenho4          = :numempenho4,
                        numempenho5          = :numempenho5,
                        numempenho6          = :numempenho6,
                        numempenho7          = :numempenho7,
                        numempenho8          = :numempenho8,
                        numempenho9          = :numempenho9,
                        numempenho10         = :numempenho10,
                        numempenho11         = :numempenho11,
                        numempenho12         = :numempenho12,
                        contrato_pai         = :contrato_pai,
                        tipoaditivo          = :tipoaditivo,
                        tipodoaditivo        = :tipodoaditivo,
                        sequencialaditivo    = :sequencialaditivo,
                        cnpjug               = :cnpjug,
                        ano_exercicio        = :ano_exercicio,
                        usuario              = :usuario,
                        mesEmpenho           = :mesEmpenho,
                        anoEmpenho           = :anoEmpenho

                        WHERE idcontrato = :chave";

            $stmt = $conexao->conn->prepare($update);

            $stmt->bindValue(':numcontrato', $_POST['numcontrato']);
            $stmt->bindValue(':tipocontrato_fk', $_POST['tipocontrato']);
            $stmt->bindValue(':tipocontratodecorrente_fk', $_POST['tipocontratodecorrente']);
            $stmt->bindValue(':numeroata', $_POST['numeroata']);
            $stmt->bindValue(':fornecedor_fk', $_POST['contratado']);
            $stmt->bindValue(':tiponatureza_fk', $_POST['tiponatureza']);
            $stmt->bindValue(':objetivo', $_POST['obj']);
            $stmt->bindValue(':responsavel_juridico', $_POST['ordernador']);
            $stmt->bindValue(':valor', $funcoes->formatarValorParaBanco($_POST['valor']));
            $stmt->bindValue(':recebevalor', $_POST['recebeValor']);
            $stmt->bindValue(':moeda_fk', $_POST['tipomoeda']);
            $stmt->bindValue(':licitacao_fk', $_POST['licitacao']);
            $stmt->bindValue(':dtcontrato', $funcoes->dateBRparaUSMySql($_POST['dtcontrato']));
            $stmt->bindValue(':dtvencimento', $funcoes->dateBRparaUSMySql($_POST['dtvencimento']));
            $stmt->bindValue(':dtpublicacao', $funcoes->dateBRparaUSMySql($_POST['dtpublic']));
            $stmt->bindValue(':num_doe', $_POST['numdoe']);
            $stmt->bindValue(':nomearquivotexto', $_POST['arqtexto']);
            $stmt->bindValue(':certificadoinss', $_POST['cnd_inss']);
            $stmt->bindValue(':dtcertidaoinss', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_inss']));
            $stmt->bindValue(':dtvalidadeinss', $funcoes->dateBRparaUSMySql($_POST['dt_validade_inss']));
            $stmt->bindValue(':numcertidaofgts', $_POST['cnd_fgts']);
            $stmt->bindValue(':dtcertidaofgts', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_fgts']));
            $stmt->bindValue(':dtvalidadefgts', $funcoes->dateBRparaUSMySql($_POST['dt_validade_fgts']));
            $stmt->bindValue(':numcertestmuncarat', $_POST['cn_fazenda_estadual']);
            $stmt->bindValue(':dtcertestmuncarat', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_secfe']));
            $stmt->bindValue(':dtvalestmuncarat', $funcoes->dateBRparaUSMySql($_POST['dt_validade_secfe']));
            $stmt->bindValue(':numcertidaofm', $_POST['cnd_fazenda_municipal']);
            $stmt->bindValue(':dtcertidaofm', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_secfm']));
            $stmt->bindValue(':dtvalidadefm', $funcoes->dateBRparaUSMySql($_POST['dt_validade_secfm']));
            $stmt->bindValue(':numcertidaoff', $_POST['cnd_fazenda_federal']);
            $stmt->bindValue(':dtcertidaoff', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_secff']));
            $stmt->bindValue(':dtvalidadeff', $funcoes->dateBRparaUSMySql($_POST['dt_validade_secff']));
            $stmt->bindValue(':numcertidaotst', $_POST['tst']);
            $stmt->bindValue(':dtcertidaotst', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_tst']));
            $stmt->bindValue(':dtvalidadetst', $funcoes->dateBRparaUSMySql($_POST['dt_validade_tst']));
            $stmt->bindValue(':numcertidaooutros', $_POST['cnd_outros']);
            $stmt->bindValue(':dtcertidaooutros', $funcoes->dateBRparaUSMySql($_POST['dt_emissao_outras']));
            $stmt->bindValue(':dtvalidadeoutros', $funcoes->dateBRparaUSMySql($_POST['dt_validade_outras']));
            $stmt->bindValue(':numempenho', $_POST['e1']);
            $stmt->bindValue(':numempenho2', $_POST['e2']);
            $stmt->bindValue(':numempenho3', $_POST['e3']);
            $stmt->bindValue(':numempenho4', $_POST['e4']);
            $stmt->bindValue(':numempenho5', $_POST['e5']);
            $stmt->bindValue(':numempenho6', $_POST['e6']);
            $stmt->bindValue(':numempenho7', $_POST['e7']);
            $stmt->bindValue(':numempenho8', $_POST['e8']);
            $stmt->bindValue(':numempenho9', $_POST['e9']);
            $stmt->bindValue(':numempenho10', $_POST['e10']);
            $stmt->bindValue(':numempenho11', $_POST['e11']);
            $stmt->bindValue(':numempenho12', $_POST['e12']);
            $stmt->bindValue(':contrato_pai', $funcoes->validaValorRetornaNulo($_POST['contratoAdtivo']), PDO::PARAM_INT);
            $stmt->bindValue(':tipoaditivo', $funcoes->validaValorRetornaNulo($_POST['tipoaditivo']), PDO::PARAM_INT);
            $stmt->bindValue(':tipodoaditivo', $funcoes->validaValorRetornaNulo($_POST['tipoaditivo2']), PDO::PARAM_INT);
            $stmt->bindValue(':sequencialaditivo', $funcoes->validaValorRetornaNulo($_POST['sequencialaditivo']), PDO::PARAM_INT);
            $stmt->bindValue(':cnpjug', $funcoes->deixarSomenteNumeros($_POST['cnpjug']) );
            $stmt->bindValue(':ano_exercicio', $_SESSION['usuario']['ano_exercicio']);
            $stmt->bindValue(':usuario', $_SESSION['usuario']['id']);
            $stmt->bindValue(':mesEmpenho', $_POST['mes']);
            $stmt->bindValue(':anoEmpenho', $_POST['anoexercicio']);
            $stmt->bindValue(':chave', $_POST['codigo']);


            if($stmt->execute()){
                echo "<script>alert('Contrato alterado com sucesso!')</script>";
                echo "<script>location.href='?p=contrato'</script>";
            }else{
                echo "<script>alert('Falha ao alterar contrato, tente novamente mais tarde!')</script>";
                // echo "<script>history.back(1)';</script>";
                echo "<pre>";
                    print_r($stmt->errorInfo());
                    print_r($_POST);
                echo "</pre>";
            }

        break;

        case 'excluir':
            include_once '../../config/conn.php';

            $delete = "DELETE FROM contrato WHERE idcontrato = ".$_POST['codigo'];
            $stmt = $conexao->conn->prepare($delete);
            if($stmt->execute()){
                echo 1;
            }else{
                echo 0;
            }
        break;

        case 'buscarCertidoes':
            include_once '../../config/conn.php';

            $sql = "SELECT p.licitacao_fk, p.fornecedor_fk, p.num_certidao,
                            DATE_FORMAT(p.dt_emissao, '%d/%m/%Y') as dt_emissao,
                            DATE_FORMAT(p.dt_validade, '%d/%m/%Y') as dt_validade,
                            p.tipocertidao_fk
                    FROM participante_certidao p
                    WHERE p.licitacao_fk = '".$_POST['licitacao']."' 
                    AND p.fornecedor_fk = ".$_POST['fornecedor']."
                    AND p.num_certidao != ''";

            $stmt = $conexao->conn->prepare($sql);
            $stmt->execute();
            $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

            echo json_encode($retorno);
            
        break;
    }

}