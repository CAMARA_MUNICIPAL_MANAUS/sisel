<div class="modal fade" id="modalObra" role="dialog">
  <div class="modal-dialog modal-super-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Obras</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method='POST' role='form' class='formObras'>
            <div class="modal-body">
              <div class="row">
                <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>NOME PÚBLICO <i class="fa fa-info-circle blue" title="Nome do Bem público."></i></label>
                    <input type='text' name='obras[nome_publico]' id='obrasNome_publico' class='form-control required' maxlength='120' autocomplete="off" />
                  </div>
                </div>
                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Contrato <i class="fa fa-info-circle blue" title="Número do contrato original conforme informado no arquivo CONTRATO.REM."></i></label>
                    <select name='obras[contrato]' id='obrasContrato' class='form-control required select2'>
                      <?php
                      echo "<option value=''>Selecione</option>";
                      foreach (json_decode(CContrato::contrato()) as $c){
                        echo "<option value='".$c->numcontrato."' data-valor='".$c->valor."' data-licitacao='".$c->licitacao_fk."'>".$c->numcontrato."</option>";
                      } // foreach
                      ?>
                    </select>
                  </div>
                </div>
                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Licitação <i class="fa fa-info-circle blue" title="Número do processo licitatório. Preenchimento obrigatório se contrato for vinculado a uma licitação."></i></label>
                    <!-- <input type='text' name='obras[licitacao]' id='obrasLicitacao' class='form-control' maxlength='18' readonly=""/> -->
                    <select name='obras[licitacao]' id='obrasLicitacao' class='form-control required select2'>
                      <?php
                      echo "<option value=''>Selecione</option>";
                      foreach (json_decode(CLicitacao::licitacao()) as $l){
                        echo "<option value='".$l->id."' >".$l->id."</option>";
                      } // foreach
                      ?>
                    </select>
                  </div>
                </div>

                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Número da ART/RRT <i class="fa fa-info-circle blue" title="Número da ART (Anotação de Responsabilidade Técnica) ou RRT (Registro de Responsabilidade Técnica)."></i></label>
                    <input type='text' name='obras[art]' id='obrasArt' class='form-control required' maxlength='30'/>
                  </div>
                </div>

                <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Tipo da Obra</label>
                    <select name='obras[tipoobra]' id='obrasTipoobra' class='form-control required select2'>
                      <option value=''>Selecione</option>
                      <option value='1'>Edificações - Construção</option>
                      <option value='2'>Edificações - Reforma</option>
                      <option value='3'>Rodoviárias</option>
                      <option value='4'>Hídricas</option>
                      <option value='5'>Geração/Distribuição de Energia e Iluminação Pública</option>
                      <option value='6'>Aeroportuária /Portuária</option>
                      <option value='7'>Consultorias/Projetos/Gerenciamento</option>
                      <option value='8'>Obras de Artes</option>
                      <option value='9'>Outros tipos de obras </option>
                    </select>
                  </div>
                </div>
                <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Natureza da Obra</label>
                    <select name='obras[naturezaobra]' id='obrasNaturezaobra' class='form-control required select2'>
                      <option value=''>Selecione</option>
                      <option value='1'>Aeroporto</option>
                      <option value='2'>Água/Esgoto</option>
                      <option value='3'>Aterro / Resíduos Sólidos</option>
                      <option value='4'>Centro Social / Recreativo / Cultutal</option>
                      <option value='5'>Conjunto Habitacional</option>
                      <option value='6'>Delegacia/Posto de Polícia</option>
                      <option value='7'>Escola</option>
                      <option value='8'>Estádio</option>
                      <option value='9'>Estradas Vicinais/Ramais</option>
                      <option value='10'>Feira</option>
                      <option value='11'>Ginásio de Esporte/Campo de Futebol</option>
                      <option value='12'>Hospital</option>
                      <option value='13'>Penitenciária / Presídio</option>
                      <option value='14'>Praça</option>
                      <option value='15'>Rede de Distribuição Elétrica</option>
                      <option value='16'>Subestação Elétrica</option>
                      <option value='17'>Unidade Administrativa</option>
                      <option value='18'>Unidade de Saúde</option>
                      <option value='19'>Outros</option>
                    </select>
                  </div>
                </div>
                <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Situação da Obra <i class="fa fa-info-circle blue" title="Situação da obra na data do registro no Sistema e-Contas."></i></label>
                    <select name='obras[situacao_obra]' id='obrasSituacao_obra' class='form-control required select2'>
                      <option value=''>Selecione</option>
                      <option value='EX'>Em Execução</option>
                      <option value='FI'>Finalizada</option>
                      <option value='PR'>Prevista</option>
                      <option value='PA'>Paralisada</option>
                      <option value='OU'>Outros</option>
                    </select>
                  </div>
                </div>                

                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Origem Recurso</label>
                    <select name='obras[origem_recurso]' id='obrasOrigem_recurso' class='form-control required select2'>
                      <option value=''>Selecione</option>
                      <option value='1'>Próprio</option>
                      <option value='2'>Convênio Estadual</option>
                      <option value='3'>Convênio Federal </option>
                    </select>
                  </div>
                </div>

                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Custo da Obra (%) <i class="fa fa-info-circle blue" title="Percentual de custo da obra em relação ao total contratado. O somatório de todas as obras do contrato deve ser 100%."></i></label>
                    <input type='text' name='obras[custo_obra]' id='obrasCusto_obra' class='form-control dinheiro required' autocomplete="off"/>
                  </div>
                </div>
                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Valor Estimado <i class="fa fa-info-circle blue" title="Valor estimado da obra. O somatório de todas as obras deve ser igual ao valor do contrato."></i></label>
                    <input type='text' name='obras[valor_estimado]' id='obrasValor_estimado' class='form-control dinheiro required' autocomplete="off" />
                  </div>
                </div>
                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Número Convênio <i class="fa fa-info-circle blue" title="Número do convênio. Informar o código exatamente como o órgão repassador prestou contas ao TCE/AM,dos onvênios firmados,no arquivo TRANSFERENCIAVOLUNTARIA.REM, se convênio Estadual."></i></label>
                    <select name='obras[numero_convenio]' id='obrasNumero_convenio' class='form-control select2'>
                      <?php
                      echo "<option value=''>Selecione</option>";
                      foreach (json_decode(CTransferenciavoluntaria::transferenciavoluntaria()) as $tf){
                        echo "<option value='".$tf->ntransferenciavoluntaria."' data-ano='".$tf->anotransferencia."' data-convenente='".$tf->cnpjconvenente."'>".$tf->ntransferenciavoluntaria."</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Ano Convênio</label>
                    <input type='number' name='obras[ano_convenio]' id='obrasAno_convenio' class='form-control' maxlength='4' min='0' autocomplete="off" readonly="" />
                  </div>
                </div>
                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>CNPJ Concedente</label>
                    <input type='text' name='obras[cnpj_concedente]' id='obrasCnpj_concedente' class='form-control' maxlength='14' autocomplete="off" readonly="" />
                  </div>
                </div>
                <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Endereço</label>
                    <input type='text' name='obras[endereco]' id='obrasEndereco' class='form-control required' maxlength='120' autocomplete="off"/>
                  </div>
                </div>
                <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Bairro</label>
                    <input type='text' name='obras[bairro]' id='obrasBairro' class='form-control required' maxlength='30' autocomplete="off"/>
                  </div>
                </div>
                <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Municipio</label>
                    <input type='text' name='obras[municipio]' id='obrasMunicipio' class='form-control required' maxlength='30' autocomplete="off"/>
                  </div>
                </div>
                <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Latitude</label>
                    <input type='text' name='obras[latitude]' id='obrasLatitude' class='form-control required' maxlength='15' autocomplete="off"/>
                  </div>
                </div>
                <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Longitude</label>
                    <input type='text' name='obras[longitude]' id='obrasLongitude' class='form-control required' maxlength='15' autocomplete="off"/>
                  </div>
                </div>
                <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Ponto Georeferenciado</label>
                    <input type='text' name='obras[georeferenciado]' id='obrasGeoreferenciado' class='form-control required' maxlength='250' autocomplete="off"/>
                  </div>
                </div>

                <div class='col-lg-8 col-md-8 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Nome completo do Responsável Técnico 1</label>
                    <input type='text' name='obras[responsavel_nome_1]' id='obrasResponsavel_nome_1' class='form-control required' maxlength='120' autocomplete="off"/>
                  </div>
                </div>
                <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Registro do Responsável Técnico 1 (CREA ou CAU)</label>
                    <input type='text' name='obras[responsavel_registro_1]' id='obrasResponsavel_registro_1' class='form-control required' maxlength='15' autocomplete="off"/>
                  </div>
                </div>

                <div class='col-lg-8 col-md-8 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Nome completo do Responsável Técnico 2</label>
                    <input type='text' name='obras[responsavel_nome_2]' id='obrasResponsavel_nome_2' class='form-control' maxlength='120' autocomplete="off"/>
                  </div>
                </div>
                <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Registro do Responsável Técnico 2 (CREA ou CAU)</label>
                    <input type='text' name='obras[responsavel_registro_2]' id='obrasResponsavel_registro_2' class='form-control' maxlength='15' autocomplete="off"/>
                  </div>
                </div>

                <div class='col-lg-8 col-md-8 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Nome completo do Responsável Técnico 3</label>
                    <input type='text' name='obras[responsavel_nome_3]' id='obrasResponsavel_nome_3' class='form-control' maxlength='120'/>
                  </div>
                </div>
                <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Registro do Responsável Técnico 3 (CREA ou CAU)</label>
                    <input type='text' name='obras[responsavel_registro_3]' id='obrasResponsavel_registro_3' class='form-control' maxlength='15'/>
                  </div>
                </div>

                <div class='col-lg-8 col-md-8 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Nome completo do Responsável Técnico 4</label>
                    <input type='text' name='obras[responsavel_nome_4]' id='obrasResponsavel_nome_4' class='form-control' maxlength='120'/>
                  </div>
                </div>
                <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Registro do Responsável Técnico 4 (CREA ou CAU)</label>
                    <input type='text' name='obras[responsavel_registro_4]' id='obrasResponsavel_registro_4' class='form-control' maxlength='15'/>
                  </div>
                </div>

                <div class='col-lg-8 col-md-8 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Nome completo do Responsável Técnico 5</label>
                    <input type='text' name='obras[responsavel_nome_5]' id='obrasResponsavel_nome_5' class='form-control' maxlength='120'/>
                  </div>
                </div>
                <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Registro do Responsável Técnico 5 (CREA ou CAU)</label>
                    <input type='text' name='obras[responsavel_registro_5]' id='obrasResponsavel_registro_5' class='form-control' maxlength='15'/>
                  </div>
                </div>

                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Mes Competência</label>
                    <select name='obras[mesempenho]' id='obrasMesempenho' class='form-control required'>
                      <?php
                      echo "<option value=''>Selecione</option>";
                      foreach (json_decode(CAno_exercicio::mesempenho()) as $m){
                        echo "<option value='".$m->num."'>".$m->mes."</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Ano Competência</label>
                    <select name='obras[anoempenho]' id='obrasAnoempenho' class='form-control required'>
                      <?php
                      echo "<option value=''>Selecione</option>";
                      foreach (json_decode(CAno_exercicio::ano_exercicio()) as $a){
                        echo "<option value='".$a->ano."'>".$a->ano."</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div>

              </div>
            </div>
            <input type="hidden" name="codigo" />
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btnSalvar">Salvar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->