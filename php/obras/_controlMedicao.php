<div class="modal fade" id="modalObraMedicao" role="dialog">
  <div class="modal-dialog modal-super-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method='POST' role='form' class='formObrasMedicao'>
            <div class="modal-body">
              <div class="row">
                
                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Tipo de Medição</label>
                    <select name='obras_medicao[tipo_medicao]' class='form-control required select2'>
                      <option value=''>Selecione</option>
                      <option value='01'>Medição do contrato</option>
                      <option value='02'>Medição do aditivo</option>
                      <option value='03'>Meidção reajuste</option>
                      <option value='04'>Outras</option>
                    </select>
                  </div>
                </div>

                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Situação da Obra</label>
                    <select name='obras_medicao[situacao_obra]' class='form-control required select2'>
                      <option value=''>Selecione</option>
                      <option value='EX'>Em Execução</option>
                      <option value='FI'>Finalizada</option>
                      <option value='PR'>Prevista</option>
                      <option value='PA'>Paralisada</option>
                      <option value='OU'>Outros</option>
                    </select>
                  </div>
                </div>

                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Nota Fiscal</label>
                    <select name='obras_medicao[nota_fiscal]' class='form-control required select2'>
                      <?php
                      echo "<option value=''>Selecione</option>";
                      foreach (json_decode(CNotafiscal::obraMedicao()) as $n){
                        echo "<option value='".$n->id."'>".$n->nf." - ".$n->razao_social."</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div>

                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Início da Medição</label>
                    <input type='date' name='obras_medicao[inicio_medicao]' class='form-control required'/>
                  </div>
                </div>

                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Final da Medição</label>
                    <input type='date' name='obras_medicao[final_medicao]' class='form-control required'/>
                  </div>
                </div>

                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Data da Medição</label>
                    <input type='date' name='obras_medicao[data_medicao]' class='form-control required'/>
                  </div>
                </div>

                <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Observação</label>
                    <input type='text' name='obras_medicao[observacao]' class='form-control' maxlength='200'/>
                  </div>
                </div>

                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Valor da Medição</label>
                    <input type='text' name='obras_medicao[valor_medicao]' class='form-control required dinheiro'/>
                  </div>
                </div>

                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Mes Competência</label>
                    <select name='obras_medicao[mesempenho]' class='form-control required'>
                      <?php
                      echo "<option value=''>Selecione</option>";
                      foreach (json_decode(CAno_exercicio::mesempenho()) as $m){
                        echo "<option value='".$m->num."'>".$m->mes."</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div>

                <div class='col-lg-2 col-md-2 col-sm-6 col-xs-12'>
                  <div class='form-group'>
                    <label>Ano Competência</label>
                    <select name='obras_medicao[anoempenho]' class='form-control required'>
                      <?php
                      echo "<option value=''>Selecione</option>";
                      foreach (json_decode(CAno_exercicio::ano_exercicio()) as $a){
                        echo "<option value='".$a->ano."'>".$a->ano."</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2">
                  <div class="form-group">
                    <label for="">&nbsp;</label>
                    <input type="hidden" name="obras_medicao[obra_fk]" />
                    <input type="hidden" name="codigo" />
                    <button type="button" class="btn btn-primary btnAddMed form-control">SALVAR</button>
                  </div>
                </div>

              </div>
            </div>
          </form>

          <div class="col-lg-12 col-md-12 col-sm-12">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Medição</th>
                  <th>Situação</th>
                  <th>Nota Fiscal</th>
                  <th>Início Medição</th>
                  <th>Final Medição</th>
                  <th>Data Medição</th>
                  <th>Valor Medição</th>
                  <th>Observação</th>
                  <th>Competência</th>
                  <th></th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->