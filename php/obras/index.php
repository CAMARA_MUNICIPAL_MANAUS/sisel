<script type="text/javascript" src="js/action/OBRAS/obra.js?001"></script>
<link href="plugin/select2/css/minify/select2-bootstrap_select2.css" rel="stylesheet" />
<script src="plugin/select2/js/select2.min.js"></script>

<!-- DATATABLES -->
<link href="plugin/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet" />
<link href="plugin/datatables-responsive/dataTables.responsive.css" rel="stylesheet" />

<!-- DATATABLES -->
<script src="plugin/datatables/js/jquery.dataTables.js"></script>
<script src="plugin/datatables-plugins/dataTables.bootstrap.js"></script>
<script src="plugin/datatables-responsive/dataTables.responsive.js"></script>

<div class="col-lg-12 col-md-12 col-sm-12">
    <h3 class="page-header">Obras</h3>
</div>

<div class="col-lg-12 col-md-12 co-sm-12">
	<button class="btn btn-primary btnNewObra" data-toggle="modal" data-target="#modalObra"><i class="fa fa-plus"></i> OBRA</button>
	<br>
	<br>
</div>
<div class="col-lg-12 col-md-12 co-sm-12">
	<table class="table table-hover table-striped" id="tobra" width="100%">
		<thead>
			<tr>
				<th class='text-center'>SEQ</th>
				<th>Contrato</th>
				<th>Licitação</th>
				<th>Obra</th>
				<th>Valor R$</th>
				<th>Competência</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach (json_decode(CObras::obras()) as $o) {
				echo "<tr>";
					echo "<td class='text-center'>".str_pad($o->sequencial, "2","0",STR_PAD_LEFT)."</td>";
					echo "<td>".$o->contrato."</td>";
					echo "<td>".$o->licitacao."</td>";
					echo "<td>".$o->nome_publico."</td>";
					echo "<td>R$ ".$o->valor_estimado."</td>";
					echo "<td>".$o->anoempenho." ".str_pad($o->mesempenho, "2","0",STR_PAD_LEFT)."</td>";
					echo "<td class='text-right' data-id='".$o->id."'>";
						echo "<button type='button' class='btn btn-primary btn-circle btnMedicao' title='Registrar Medição'><i class='fa fa-list-ol'></i></button>&nbsp;";
						echo "<button type='button' class='btn btn-warning btn-circle btnEditarObra' title='Editar'><i class='fa fa-pencil'></i></button>&nbsp;";
						echo "<button type='button' class='btn btn-danger btn-circle btnRemoverObra' title='Remover'><i class='fa fa-remove'></i></button>";
					echo "</td>";
				echo "</tr>";
			}
			?>
		</tbody>
	</table>
</div>

<?php include_once "_controlObra.php"; ?>
<?php include_once "_controlMedicao.php"; ?>


<script>
    $(function(){
        $('#tobra').DataTable({
            responsive: true
        });
    })
</script>