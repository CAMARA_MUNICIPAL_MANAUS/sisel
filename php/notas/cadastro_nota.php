<?php
if(isset($_GET["nota"])){
	$sql = "SELECT n.*, f.id as codF, f.razao_social, f.cpfcnpj
			FROM notafiscal n
			INNER JOIN fornecedor f ON f.id = n.fornecedor_fk
			WHERE n.id=".$_GET["nota"];
    $stmt = $conexao->conn->prepare($sql);
    $stmt->execute();
    $nota = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $nota = $nota[0];
    $btn = "editar";
}else{
	$btn = "inserir";
}
?>

<script type="text/javascript" src="js/action/NOTA/nota.js"></script>

<div class="col-lg-12">
    <h3 class="page-header">Cadastrar / Editar Nota Fiscal</h3>
</div>

<div class="col-lg-12">
	<form action="?p=notas&page=ajax_nota" method="POST" class="formNotaFiscal">
		<div class='col-md-9'>
			<div class='form-group'>
				<label for="">Emitente</label>
				<? $auxiliar->fornecedor(@$nota['codF']); ?>
			</div>
		</div>
		<div class='col-md-3'>
			<div class='form-group'>
				<label for="">CPF/CNPJ</label>
				<input type='text' name='cpfcnpj' class='form-control' maxlength='120' placeholder='' value="<?= @$nota['cpfcnpj'] ?>" disabled="true"/>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class='col-md-3'>
			<div class='form-group'>
				<label for="">Nº Nota</label>
				<input type='number' name='notafiscal[nf]' id='notafiscalNf' class='form-control required' maxlength='20' min="0" value="<?= @$nota['nf']; ?>"/>
			</div>
		</div>
		<div class='col-md-3'>
			<div class='form-group'>
				<label for="">Serie</label>
				<input type='number' name='notafiscal[serienf]' id='notafiscalSerienf' class='form-control ' maxlength='8' min="0" placeholder='' value="<?= @$nota['serienf']; ?>"/>
			</div>
		</div>
		<div class='col-md-2'>
			<div class='form-group'>
				<label for="">Dt. Emissão</label>
				<input type='date' name='notafiscal[dtemissao]' id='notafiscalDtemissao' class='form-control '  value="<?= @$nota['dtemissao']; ?>"/>
			</div>
		</div>
		<div class='col-md-2'>
			<div class='form-group'>
				<label for="">Insc. Estadual</label>
				<input type='text' name='notafiscal[inscestadual_emitente]' id='notafiscalInscestadual_emitente' class='form-control required' maxlength='30' placeholder='' value="<?= @$nota['inscestadual_emitente']; ?>"/>
			</div>
		</div>
		<div class='col-md-2'>
			<div class='form-group'>
				<label for="">Insc. Municipal</label>
				<input type='text' name='notafiscal[inscmunicipal_emitente]' id='notafiscalInscmunicipal_emitente' class='form-control required' maxlength='30' placeholder='' value="<?= @$nota['inscmunicipal_emitente']; ?>"/>
			</div>
		</div>
		<div class='col-md-2 col-lg-2'>
			<div class='form-group'>
				<label for="">Municipio</label>
				<input type='text' name='notafiscal[municipio_emitente]' id='notafiscalMunicipio_emitente' class='form-control required' maxlength='30' placeholder='' value="<?= @$nota['municipio_emitente']; ?>"/>
			</div>
		</div>
		<div class='col-md-1 col-lg-1'>
			<div class='form-group'>
				<label for="">UF</label>
				<input type='text' name='notafiscal[uf_emitente]' id='notafiscalUf_emitente' class='form-control required' maxlength='2' placeholder='' value="<?= @$nota['uf_emitente']; ?>"/>
			</div>
		</div>
		<div class='col-md-3 col-lg-3'>
			<div class='form-group'>
				<label for="">Tipo da Nota Fiscal</label>
				<select name='notafiscal[tiponotafiscal]' class='form-control required'>
					<option value=''>Selecione</option>
					<option value='1' <?php if(@$nota['tiponotafiscal'] == 1){ echo "selected='true'"; } ?>>Sim, padrão Estadual ou SINIEF 07/05</option>
					<option value='2' <?php if(@$nota['tiponotafiscal'] == 2){ echo "selected='true'"; } ?>>Sim, chave de acesso municipal</option>
					<option value='3' <?php if(@$nota['tiponotafiscal'] == 3){ echo "selected='true'"; } ?>>Não</option>
				</select>
			</div>
		</div>
		<div class='col-md-6 col-lg-6'>
			<div class='form-group'>
				<label for="">Chave de Acesso</label>
				<input type='text' name='notafiscal[chave_acesso]' id='notafiscalChave_acesso' class='form-control ' maxlength='44' placeholder='' value='<?= @$nota['chave_acesso']; ?>'/>
			</div>
		</div>
		<div class='col-md-2 col-lg-2'>
			<div class='form-group'>
				<label for="">Valor Bruto</label>
				<input type='number' name='notafiscal[valor_bruto]' id='notafiscalValor_bruto' class='form-control required' placeholder='' step="0.01" value='<?= @$nota['valor_bruto']; ?>'/>
			</div>
		</div>
		<div class='col-md-2 col-lg-2'>
			<div class='form-group'>
				<label for="">Valor Desconto</label>
				<?php
				if(isset($nota['valor_bruto']) && isset($nota['valor_liquido'])){
					$valor = round(@$nota['valor_bruto']-@$nota['valor_liquido'],2);
				}else{
					$valor = "";
				}
				?>
				<input type='number' id='notafiscalValor_desconto' class='form-control' step="0.01" min="0" value='<?= $valor ?>'/>
			</div>
		</div>
		<div class='col-md-2 col-lg-2'>
			<div class='form-group'>
				<label for="">Valor Líquido</label>
				<input type='number' name='notafiscal[valor_liquido]' id='notafiscalValor_liquido' class='form-control required' placeholder='' min="0" step="0.01" value='<?= @$nota['valor_liquido']; ?>'/>
			</div>
		</div>
		<div class='col-md-6 col-lg-6'>
			<div class='form-group'>
				<label for="">Chave de Acesso Municipal</label>
				<input type='text' name='notafiscal[chave_acesso_municipal]' id='notafiscalChave_acesso_municipal' class='form-control' min="0" maxlength='60' placeholder='' value='<?= @$nota['chave_acesso_municipal']; ?>'/>
			</div>
		</div>
		<div class='col-md-2 col-lg-2'>
			<div class='form-group'>
				<label for="">Tipo da Nota</label>
				<select name='notafiscal[tipo_nota]' id='notafiscalTipo_nota' class='form-control required'>
					<option value=''>Selecione</option>
					<option value='1' <?php if(@$nota['tipo_nota'] == 1){ echo "selected='true'"; } ?>>Mercadoria</option>
					<option value='2' <?php if(@$nota['tipo_nota'] == 2){ echo "selected='true'"; } ?>>Serviço</option>
				</select>
			</div>
		</div>
		<div class="col-md-2">
            <div class="form-group">
                <label class="label-obrigatorio">Competência:</label>
                <?= $auxiliar->mes(@$nota['mes']); ?>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <label>&nbsp;</label>
                <?= $auxiliar->anoexercicio(@$nota['ano']); ?>
            </div>
        </div>
        <div class="clearfix"></div>

        <h3>Liquidação</h3>
		
		<?php
			for ($i=1; $i <= 8; $i++) { 
				echo "<div class='col-md-3 col-lg-3'>";
					echo "<div class='form-group'>";
						echo "<label for=''>Ano Empenho $i</label>";
						echo "<input type='text' name='notafiscal[anoempenho$i]' class='form-control ' maxlength='12' value='".@$nota["anoempenho$i"]."'/>";
					echo "</div>";
				echo "</div>";
				echo "<div class='col-md-3 col-lg-3'>";
					echo "<div class='form-group'>";
						echo "<label for=''>Empenho $i</label>";
						echo "<input type='text' name='notafiscal[empenho$i]' class='form-control ' maxlength='12' value='".@$nota["empenho$i"]."'/>";
					echo "</div>";
				echo "</div>";
				echo "<div class='col-md-3 col-lg-3'>";
					echo "<div class='form-group'>";
						echo "<label for=''>Nota de Lançamento $i</label>";
						echo "<input type='text' name='notafiscal[notalancamento$i]' class='form-control ' maxlength='5' value='".@$nota["notalancamento$i"]."'/>";
					echo "</div>";
				echo "</div>";
				echo "<div class='clearfix'></div>";
			}
		?>

        <div class="col-gl-2 col-md-2">
            <div class="form-group">
                <label>&nbsp;</label>
                <button type="button" class="btn btn-success btnSalvar">Salvar</button>
                <a href="?p=notas" class="btn btn-danger">Cancelar</a>
                <input type="hidden" name="acao" value="<?= $btn ?>">
                <input type="hidden" name="codigo" value="<?= @$nota['id'] ?>">
            </div>
        </div>
    </form>
</div>