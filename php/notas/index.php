<script src="js/action/NOTA/nota.js"></script>

<?php
$sql = "SELECT n.id, n.nf as NOTA_FISCAL, n.dtemissao AS DATA_EMISSAO, f.razao_social AS FORNECEDOR, n.mes,
                 f.cpfcnpj AS DOCUMENTO, n.municipio_emitente AS MUNICIPIO, n.valor_bruto AS VALOR_BRUTO, n.valor_liquido AS VALOR_LIQUIDO, CONCAT(LPAD(n.mes,2,0),n.ano) as COMPETENCIA,
                 IFNULL(n.empenho1,'') AS empenho1, IFNULL(n.empenho2,'') AS empenho2, IFNULL(n.empenho3,'') AS empenho3, IFNULL(n.empenho4,'') AS empenho4,
                 IFNULL(n.notalancamento1,'') AS notalancamento1, IFNULL(n.notalancamento2,'') AS notalancamento2,
                 IFNULL(n.notalancamento3,'') AS notalancamento3, IFNULL(n.notalancamento4,'') AS notalancamento4,
                 IFNULL(n.anoempenho1,'') AS anoempenho1, IFNULL(n.anoempenho2,'') AS anoempenho2, IFNULL(n.anoempenho3,'') AS anoempenho3,
                 IFNULL(n.anoempenho4,'') AS anoempenho4
        FROM notafiscal n
        INNER JOIN fornecedor f ON f.id = n.fornecedor_fk
        WHERE n.ano = ".$_SESSION['usuario']['ano_exercicio']."
        ORDER BY n.mes, n.nf ASC";
$stmt = $conexao->conn->prepare($sql);
$stmt->execute();
$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<!-- DATATABLES -->
<link href="plugin/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet" />
<link href="plugin/datatables-responsive/dataTables.responsive.css" rel="stylesheet" />

<!-- DATATABLES -->
<script src="plugin/datatables/js/jquery.dataTables.js"></script>
<script src="plugin/datatables-plugins/dataTables.bootstrap.js"></script>
<script src="plugin/datatables-responsive/dataTables.responsive.js"></script>

<div class="col-lg-12">
    <h3 class="page-header">Nota Fiscal</h3>
</div>

<div class="col-lg-12">
    <div class="col-lg-6 col-md-6">
        <a href="?p=notas&page=cadastro_nota" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-plus"></i> Nova Nota Fiscal</a>
    </div>
    
    <div class="col-lg-6 col-md-6 text-right">
        <form method='POST' target='_blank' action='php/excel.php' class='hidden-print'>
           <input type='hidden' name='file' value="<?php echo 'nota-fiscal-'.date('mdY_His') ?>" />
           <input type='hidden' name='data' value="<?php echo base64_encode(json_encode($retorno)) ; ?>"/>
           <button type="submit" class='btn btn-success'><i class='glyphicon glyphicon-download-alt'></i> Generate Excel</button>
        </form>
    </div>
    <br><br>
    
    <table class="table table-hover table-striped table-condensed" id="tt">
        <thead>
            <tr>
                <th></th>
                <th>Nº Nota</th>
                <th title="Data da Emissão">Emissão</th>
                <th>CPF/CNPJ</th>
                <th>Emitente</th>
                <th>Município</th>
                <th>Mês</th>
                <th class="text-right">Vl. Bruto</th>
                <th class="text-right">Vl. Líquido</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?

                $i = 1;
                foreach ($retorno as $ln) {
                    echo "<tr>";
                        echo "<td>";
                            echo "<a class='btn btn-primary btn-sm btn-circle' href='?p=notas&page=cadastro_nota&nota=".$ln['id']."'><i class='glyphicon glyphicon-search'></i></a>";
                        echo "</td>";
                        echo "<td>".$ln['NOTA_FISCAL']."</td>";
                        echo "<td style='min-width: 100px;'>".$funcoes->dateUSparaBR($ln['DATA_EMISSAO'])."</td>";
                        echo "<td style='min-width: 180px;'>".$funcoes->formatarDoc($ln['DOCUMENTO'],'')."</td>";
                        echo "<td>".$ln['FORNECEDOR']."</td>";
                        echo "<td>".$ln['MUNICIPIO']."</td>";
                        echo "<td>".$funcoes->mes($ln['mes'])."</td>";
                        echo "<td style='min-width: 120px;' class='text-right'>R$ ".number_format($ln['VALOR_BRUTO'], 2, ',', '.')."</td>";
                        echo "<td style='min-width: 120px;' class='text-right'>R$ ".number_format($ln['VALOR_LIQUIDO'], 2, ',', '.')."</td>";
                        echo "<td>";
                            echo "<form action='?p=notas&page=ajax_nota' method='post'>";
                                echo '<input type="hidden" name="acao" value="excluir">';
                                echo '<input type="hidden" name="codigo" value="'.$ln['id'].'">';
                                echo "<button class='btn btn-danger btn-sm btn-excluir btn-circle'><i class='glyphicon glyphicon-remove'></i></button>";
                            echo "</form>";
                        echo "</td>";
                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>
</div>

<script>
    $(function(){
        $('#tt').DataTable({
            responsive: true
        });
    })
</script>