<? include_once 'ajax_cadastar_editar_produto.php'; ?>
<script src="js/action/PRODUTO/produto.js"></script>

<!-- DATATABLES -->
<link href="plugin/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet" />
<link href="plugin/datatables-responsive/dataTables.responsive.css" rel="stylesheet" />

<!-- DATATABLES -->
<script src="plugin/datatables/js/jquery.dataTables.js"></script>
<script src="plugin/datatables-plugins/dataTables.bootstrap.js"></script>
<script src="plugin/datatables-responsive/dataTables.responsive.js"></script>

<div class="col-lg-12">
	<h3 class="page-header">Produtos</h3>
</div>

<div class="col-md-2">
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalProduto"><i class="glyphicon glyphicon-plus"></i> Novo Produto</button>
</div>
<div class="col-lg-12">
	<br>
	<table class="table table-hover table-striped" id="tableProdutos" width="100%">
		<thead>
			<tr>
				<th>Item</th>
				<th>Descrição</th>
				<th>Unidade</th>
				<th>Tipo</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?
				$sql = "select p.id, p.descricao, tp.tipoproduto, und.unidade
						from produto p
						inner join tipoproduto tp ON tp.id = p.tipoproduto_fk
						inner join tipounidade und ON und.id = p.unidade_fk
						order by p.descricao asc";
													
				$stmt = $conexao->conn->prepare($sql);
				$stmt->execute();
				$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

				$i = 1;
				foreach ($retorno as $ln) {

					echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>{$ln['descricao']}</td>";
						echo "<td>{$ln['unidade']}</td>";
						echo "<td>{$ln['tipoproduto']}</td>";
						echo "<td data-codigo='".$ln['id']."'>";
							echo "<button type='button' class='btn btn-danger btn-excluir btn-circle'><i class='glyphicon glyphicon-remove'></i></button>";
						echo "</td>";
					echo "</tr>";

					$i++;
				}
			?>
		</tbody>
	</table>
</div>

<!-- ########################### -->

<!-- Modal -->
<div class="modal fade" id="myModalProduto" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cadastrar / Editar Produto</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-lg-12 col-md-12 col-sm-12">
		      	<form action="?p=produto" method="POST" role="form" class="formProduto">
		      		<div class="col-lg-12 col-md-12">
		      			<div class="form-group">
		      				<label>Descrição</label>
		      				<input type="text" id="descricao" name="descricao" placeholder='Digite a descrição do produto' class='form-control required'/>
		      			</div>
		      		</div>
		      		<div class="col-md-4">
		      			<div class="form-group">
		      				<label>Unidade</label>
		      				<? $auxiliar->tipounidade(); ?>
		      			</div>
		      		</div>
		      		<div class="col-md-4">
		      			<div class="form-group">
		      				<label>Tipo de Produto</label>
		      				<? $auxiliar->tipoproduto(); ?>
		      			</div>
		      		</div>
		      		<div class="col-md-4">
		      			<div class="form-group">
		      				<label>&nbsp;</label>
		      				<button type="button" name="cadastrar" class="btn btn-success btn-cadastrar form-control">Cadastrar</button>
		      				<input type="hidden" id="acao" name="acao" value=""/>
		      			</div>
		      		</div>
		      	</form>
	      	</div>
      	</div>
      </div>
    </div>
  </div>
</div>

<script>
	$(function(){
		$('#tableProdutos').DataTable({
	        responsive: true
	    });
	})
</script>