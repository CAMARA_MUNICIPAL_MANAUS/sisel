<?

// print_r($_POST);

if(isset($_POST['buscarCpfCnpj'])){

	include_once '../../config/conn.php';

	$_POST['cpfcnpj'] = str_replace(".", "", str_replace("/", "", str_replace("-", "", $_POST['cpfcnpj'])));

    $sql = "SELECT f.id FROM fornecedor f WHERE f.cpfcnpj = :cpfcnpj";
    $stmt = $conexao->conn->prepare($sql);
    $stmt->bindValue(':cpfcnpj',$_POST['cpfcnpj']);
    $stmt->execute();
    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo sizeof($retorno);

}

if(isset($_POST['acao'])){

	include_once '../../config/conn.php';
	include_once '../../config/funcoesgerais.php';

	$funcoes  = new Funcoes;

	switch ($_POST['acao']) {
		case 'inserir':

			/*print_r($_POST);
			exit();*/
			
			$cpfcnpj = ($_POST['tipopessoa'] == 1) ? $_POST['cpf'] : $_POST['cnpj'];

                        $_POST['tipocnae'] = ($_POST['tipocnae'] == "") ? 2449 : $_POST['tipocnae'];

			$sql = "INSERT INTO `fornecedor`(nome_fantasia, razao_social, tipopessoa_fk, logradouro, bairro, numero, complemento, cep, cpfcnpj, telefone, insc_estadual, insc_municipal, ramoatividade, nomeresponsavel, cpfresponsavel, tipocnae_fk)

					VALUES(:nomeFantasia, :razaosocial, :tipopessoa, :logradouro, :bairro, :numero, :complemento, :cep, :cpfcnpj, :telefone, :inscEstadual, :inscMunicipal, :ramoAtividade, :nomeResponsavel, :cpfResponsavel, :tipocnae)";

		    $stmt = $conexao->conn->prepare($sql);
		    $stmt->bindValue(':nomeFantasia',$_POST['nomeFantasia']);
		    $stmt->bindValue(':razaosocial',$_POST['razaosocial']);
		    $stmt->bindValue(':tipopessoa',$_POST['tipopessoa']);
		    $stmt->bindValue(':logradouro',$_POST['logradouro']);
		    $stmt->bindValue(':bairro',$_POST['bairro']);
		    $stmt->bindValue(':numero',$_POST['numero']);
		    $stmt->bindValue(':complemento',$_POST['complemento']);
		    $stmt->bindValue(':cep',$funcoes->deixarSomenteNumeros($_POST['cep']));
		    $stmt->bindValue(':cpfcnpj',$funcoes->deixarSomenteNumeros($cpfcnpj));
		    $stmt->bindValue(':telefone',$_POST['telefone']);
		    $stmt->bindValue(':inscEstadual',$_POST['inscEstadual']);
		    $stmt->bindValue(':inscMunicipal',$_POST['inscMunicipal']);
		    $stmt->bindValue(':ramoAtividade',$_POST['ramoAtividade']);
		    $stmt->bindValue(':nomeResponsavel',$_POST['nomeResponsavel']);
		    $stmt->bindValue(':cpfResponsavel',$funcoes->deixarSomenteNumeros($_POST['cpfResponsavel']));
		    $stmt->bindValue(':tipocnae', $_POST['tipocnae']);
		    
		    if($stmt->execute()){
		    	echo 1;
		    }else{
		    	echo 0;
		    	print_r($stmt->errorInfo());
		    }
		break;

		case 'editar':

			/*print_r($_POST);
			exit();*/
			
			$cpfcnpj = ($_POST['tipopessoa'] == 1) ? $_POST['cpf'] : $_POST['cnpj'];

                        $_POST['tipocnae'] = ($_POST['tipocnae'] == "") ? 2449 : $_POST['tipocnae'];

			$sql = "UPDATE `fornecedor`
						SET nome_fantasia = :nomeFantasia,
							razao_social = :razaosocial,
							tipopessoa_fk = :tipopessoa,
							logradouro = :logradouro,
							bairro = :bairro,
							numero = :numero,
							complemento = :complemento,
							cep = :cep,
							cpfcnpj = :cpfcnpj,
							telefone = :telefone,
							insc_estadual = :inscEstadual,
							insc_municipal = :inscMunicipal,
							ramoatividade = :ramoAtividade,
							nomeresponsavel = :nomeResponsavel,
							cpfresponsavel = :cpfResponsavel,
							tipocnae_fk = :tipocnae
						WHERE id = :codigo";

		    $stmt = $conexao->conn->prepare($sql);
		    $stmt->bindValue(':nomeFantasia',$_POST['nomeFantasia']);
		    $stmt->bindValue(':razaosocial',$_POST['razaosocial']);
		    $stmt->bindValue(':tipopessoa',$_POST['tipopessoa']);
		    $stmt->bindValue(':logradouro',$_POST['logradouro']);
		    $stmt->bindValue(':bairro',$_POST['bairro']);
		    $stmt->bindValue(':numero',$_POST['numero']);
		    $stmt->bindValue(':complemento',$_POST['complemento']);
		    $stmt->bindValue(':cep',$funcoes->deixarSomenteNumeros($_POST['cep']));
		    $stmt->bindValue(':cpfcnpj',$funcoes->deixarSomenteNumeros($cpfcnpj));
		    $stmt->bindValue(':telefone',$_POST['telefone']);
		    $stmt->bindValue(':inscEstadual',$_POST['inscEstadual']);
		    $stmt->bindValue(':inscMunicipal',$_POST['inscMunicipal']);
		    $stmt->bindValue(':ramoAtividade',$_POST['ramoAtividade']);
		    $stmt->bindValue(':nomeResponsavel',$_POST['nomeResponsavel']);
		    $stmt->bindValue(':cpfResponsavel',$funcoes->deixarSomenteNumeros($_POST['cpfResponsavel']));
		    $stmt->bindValue(':tipocnae', $_POST['tipocnae']);

		    $stmt->bindValue(':codigo',$_POST['codigo']);

		    if($stmt->execute()){
		    	echo 1;
		    }else{
		    	echo 0;
		    	print_r($stmt->errorInfo());
		    }

		break;

		case 'excluir':
			// print_r($_POST);
			$sql = "CALL sp_deletarFornecedor(".$_POST['codigo'].")";
			$stmt = $conexao->conn->prepare($sql);

			if($stmt->execute()){
		    	echo 1;
		    }else{
		    	echo 0;
		    	print_r($stmt->errorInfo());
		    }
		break;
	}
}


?>	