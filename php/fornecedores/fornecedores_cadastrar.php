<script type="text/javascript" src="js/action/FORNECEDOR/fornecedor.js"></script>

<?php
if(isset($_GET['fornecedor'])){
	$fornecedor = $funcoes->buscardados("fornecedor", $_GET['fornecedor']);
	$fornecedor = $fornecedor[0];
}
?>

<div class="col-lg-12 col-md-12 col-sm-12">
	<h3 class="page-header">Cadastrar/Editar Fornecedores</h3>
</div>

<div class="col-lg-12 col-md-12 col-sm-12">
	<form role="form" method="post" action="">
		
		<h4>Documentação</h4>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<label class="label-obrigatorio">Pessoa</label>
					<?php echo $auxiliar->tipopessoa(@$fornecedor['tipopessoa_fk']); ?>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="label-obrigatorio">Nome Completo / Razão Social</label>
					<input type="text" name="razaosocial" class="form-control razaosocial" maxlength="100" autocomplete="off" value="<?= @$fornecedor['razao_social']?>" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="">Nome Fantasia</label>
					<input type="text" name="nomeFantasia" class="form-control nomeFantasia" value="<?= @$fornecedor['nome_fantasia']?>" readonly />
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label class="label-obrigatorio">CPF/CNPJ</label>
					<input type="text" name="cpf" class="form-control cpfcnpj cpf" style='display:none;' value="<?= @$fornecedor['cpfcnpj']?>" />
					<input type="text" name="cnpj" class="form-control cpfcnpj cnpj" style='display:none;' value="<?= @$fornecedor['cpfcnpj']?>" />
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="">Ramo de Atividade</label>
					<input type="text" name="ramoAtividade" class="ramoAtividade form-control" value="<?= @$fornecedor['ramoatividade']?>" />
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label for="">Inscrição Municipal</label>
					<input type="text" name="inscMunicipal" class="inscMunicipal form-control" value="<?= @$fornecedor['insc_municipal']?>" />
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label for="">Inscrição Estadual</label>
					<input type="text" name="inscEstadual" class="inscEstadual form-control" value="<?= @$fornecedor['insc_estadual']?>" />
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="">Nome do Responsável</label>
					<input type="text" name="nomeResponsavel" class="nomeResponsavel form-control" value="<?= @$fornecedor['nomeresponsavel']?>" />
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label for="">CPF do Responsável</label>
					<input type="text" name="cpfResponsavel" class="form-control cpfResponsavel cpf" value="<?= @$fornecedor['cpfresponsavel']?>" />
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label for="">Telefone</label>
					<input type="text" name="telefone" class="form-control telefone celular" value="<?= @$fornecedor['telefone']?>" />
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="">Ativiade Principal</label>
					<?php echo $auxiliar->cnae(@$fornecedor['tipocnae_fk']); ?>
				</div>
			</div>
		</div>

		<h4>Endereço</h4>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label for="">Logradouro</label>
					<input type="text" name="logradouro" class="form-control logradouro" maxlength="100" value="<?= @$fornecedor['logradouro']?>" />
				</div>
			</div>

			<div class="col-md-1">
				<div class="form-group">
					<label for="">Nº</label>
					<input type="text" name="numero" class="form-control numero" maxlength="10" value="<?= @$fornecedor['numero']?>" />
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label for="">Bairro</label>
					<input type="text" name="bairro" class="form-control bairro" maxlength="80" value="<?= @$fornecedor['bairro']?>" />
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label for="">Complemento</label>
					<input type="text" name="complemento" class="form-control complemento" maxlength="100" value="<?= @$fornecedor['complemento']?>" />
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label for="">CEP</label>
					<input type="text" name="cep" class="form-control cep" value="<?= @$fornecedor['cep']?>" />
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12 col-md-12">
				<? if(isset($_GET['fornecedor'])){ ?>
				<button type="button" class="btn btn-success btn-salvar" value="editar" data-alert="alterado">Salvar</button>
				&nbsp;
				<input type="hidden" id="codigo" name="codigo" value="<?= $_GET['fornecedor']; ?>" />
				<? }else{ ?>
				<button type="button" class="btn btn-success btn-inserir" value="inserir" data-alert="inserido">Inserir</button>
				<? } ?>
				<a href="?p=fornecedores" class="btn btn-danger">Cancelar</a>
			</div>
		</div>
	</form>
</div>