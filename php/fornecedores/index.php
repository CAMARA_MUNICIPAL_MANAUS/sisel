<script type="text/javascript" src="js/action/FORNECEDOR/fornecedor.js"></script>

<!-- DATATABLES -->
<link href="plugin/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet" />
<link href="plugin/datatables-responsive/dataTables.responsive.css" rel="stylesheet" />

<!-- DATATABLES -->
<script src="plugin/datatables/js/jquery.dataTables.js"></script>
<script src="plugin/datatables-plugins/dataTables.bootstrap.js"></script>
<script src="plugin/datatables-responsive/dataTables.responsive.js"></script>

<div class="col-lg-12">
	<h3 class="page-header">Fornecedores</h3>
</div>

<div class="col-lg-12">
	<a href="?p=fornecedores&page=fornecedores_cadastrar" class="btn btn-primary novo"><i class="glyphicon glyphicon-plus"></i> Novo Fornecedor</a>
		<br><br>
		<table class="table table-hover table-striped table-condensed" id="tt">
			<thead>
				<tr>
					<th></th>
					<th>Razão Social</th>
					<th>CPF/CNPJ</th>
					<!-- <th>Responsável</th>
					<th>Telefone</th> -->
					<th>Endereço</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?
					$sql = "SELECT f.id, f.razao_social, formatarCpfCnpj(f.cpfcnpj) as cpfcnpj, f.nomeresponsavel, f.telefone, f.logradouro
							FROM fornecedor f
							ORDER BY trim(f.razao_social) ASC";
														
					$stmt = $conexao->conn->prepare($sql);
					$stmt->execute();
					$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

					$i = 1;
					foreach ($retorno as $ln) {
						echo "<tr>";
							echo "<td class='text-right'>";
								echo "<a class='btn btn-primary visualizar btn-sm btn-circle' href='?p=fornecedores&page=fornecedores_cadastrar&fornecedor={$ln['id']}'><i class='glyphicon glyphicon-search'></i></a>";
									echo "&nbsp;";
							echo "</td>";
							echo "<td>&nbsp;".$ln['razao_social']."</td>";
							echo "<td>".$ln['cpfcnpj']."</td>";
							# echo "<td>".$ln['nomeresponsavel']."</td>";
							# echo "<td>".$ln['telefone']."</td>";
							echo "<td>".$ln['logradouro']."</td>";
							echo "<td class='text-right'>";
								echo "<button type='button' class='btn btn-danger btn-excluir btn-sm btn-circle' data-codigo='".$ln['id']."'><i class='glyphicon glyphicon-remove'></i></button>";
							echo "</td>";
						echo "</tr>";
					}
				?>
			</tbody>
		</table>
	</div>
</div>

<script>
    $(function(){
        $('#tt').DataTable({
            responsive: true
        });
    })
</script>