<?
class MContrato extends ConnectionSISEL{

	public static function getTableName(){
		return 'contrato';
	}

	public static function findAll($tableName=''){
		return parent::findAll(self::getTableName());
	}

	public static function findAllByAttributes($criteria, $debug=false, $tableName=''){
		return parent::findAllByAttributes($criteria, $debug, self::getTableName());
	}

	public static function save($data, $id = ''){

		$conn = parent::getConn();

		if($id == ''){
			$sql = parent::sql_insert($data[self::getTableName()], self::getTableName());
		}else{
			$sql = parent::sql_update($data[self::getTableName()], self::getTableName(), 'id = '.$id);
		}

		$stmt = $conn->prepare($sql);

		$conn->beginTransaction();

		if($stmt->execute()){
			$conn->commit();
			if($id == ''){
				$lastId = $conn->lastInsertId();
				return $lastId;
			}else{
				return 1;
			}
		}else{
			$conn->rollBack();
			print_r($conn->errorInfo());
			return 0;
			// echo 'id: '$id;
		}
	}

	public static function excluir($id){
		return parent::deleteByPk($id, self::getTableName());
	}

} ?>