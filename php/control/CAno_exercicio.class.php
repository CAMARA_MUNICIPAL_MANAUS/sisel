<?
class CAno_exercicio extends MAno_exercicio{

	public static function ano_exercicio(){
		$r = parent::findAllByAttributes(
			array(
				"order" => "ano DESC"
			), false
		);
		return json_encode($r);
	}

	public static function mesempenho(){
		$mes = array(
			array("num" => "01", "mes" => "JANEIRO", "abr" => "JAN"),
			array("num" => "02", "mes" => "FEVEREIRO", "abr" => "FEV"),
			array("num" => "03", "mes" => "MARÇO", "abr" => "MAR"),
			array("num" => "04", "mes" => "ABRIL", "abr" => "ABR"),
			array("num" => "05", "mes" => "MAIO", "abr" => "MAI"),
			array("num" => "06", "mes" => "JUNHO", "abr" => "JUN"),
			array("num" => "07", "mes" => "JULHO", "abr" => "JUL"),
			array("num" => "08", "mes" => "AGOSTO", "abr" => "AGO"),
			array("num" => "09", "mes" => "SETEMBRO", "abr" => "SET"),
			array("num" => "10", "mes" => "OUTUBRO", "abr" => "OUT"),
			array("num" => "11", "mes" => "NOVEMBRO", "abr" => "NOV"),
			array("num" => "12", "mes" => "DEZEMBRO", "abr" => "DEZ")
		);

		return json_encode($mes);
	}


} ?>