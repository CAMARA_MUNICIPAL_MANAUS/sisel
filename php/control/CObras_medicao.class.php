<?
class CObras_medicao extends MObras_medicao{

	public static function medicao($obra){
		$r = parent::findAllByAttributes(
			array(
				"select" => "obras_medicao.id, tipo_medicao, situacao_obra, observacao, inicio_medicao, final_medicao, data_medicao, valor_medicao, nota_fiscal, obras_medicao.mesempenho, obras_medicao.anoempenho, n.id idnf, n.nf, f.razao_social, n.serienf",
				"join" => "INNER JOIN notafiscal n ON n.id = obras_medicao.nota_fiscal
						   INNER JOIN fornecedor f ON f.id = n.fornecedor_fk",
				"where" => "obras_medicao.obra_fk = $obra",
				"order" => "obras_medicao.mesempenho desc, obras_medicao.anoempenho desc"
			), false
		);
		return json_encode($r);
	}

	public static function savee($dados){

		$dados = FUN::serializeToArray($dados);

		$dados["obras_medicao"]["valor_medicao"] = str_replace(",", ".", str_replace(".", "", $dados["obras_medicao"]["valor_medicao"]));
		
		# print_r($dados);
		# ENVIAR PARA SALVAR
		if(!isset($dados["codigo"])){
			return parent::save($dados);
		}else{
			$codigo = $dados["codigo"];
			unset($dados["codigo"]);

			return parent::save($dados, $codigo);
		}
	}

	public static function excluirr($codigo){
		return parent::excluir($codigo);
	}


} ?>