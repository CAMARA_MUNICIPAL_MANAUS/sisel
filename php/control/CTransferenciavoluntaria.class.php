<?
class CTransferenciavoluntaria extends MTransferenciavoluntaria{

	public static function transferenciavoluntaria(){
		$r = parent::findAllByAttributes(
			array(
				"order" => "ano_empenho desc, mes_empenho desc"
			), false
		);
		return json_encode($r);
	}

	public static function savee($dados){
		$codigo = $dados["codigo"];
		unset($dados["codigo"]);
		
		# ENVIAR PARA SALVAR
		if($codigo == ""){
			return parent::save($dados);
		}else{
			return parent::save($dados, $codigo);
		}
	}

	public static function excluir($codigo){
		return parent::excluir($codigo);
	}


} ?>