<?
class CLicitacao extends MLicitacao{

	public static function licitacao($licitacao=""){
		$where = ($licitacao != "") ? "id = $licitacao" : "1=1";
		$r = parent::findAllByAttributes(
			array(
				"where" => $where,
				"order" => "ano_exercicio desc, mes_empenho desc"
			), false
		);
		return json_encode($r);
	}

	public static function savee($dados){

		$dados = FUN::serializeToArray($dados);	
		
		# print_r($dados);
		# ENVIAR PARA SALVAR
		if(!isset($dados["codigo"])){
			return parent::save($dados);
		}else{
			$codigo = $dados["codigo"];
			unset($dados["codigo"]);

			return parent::save($dados, $codigo);
		}
	}

	public static function remove($codigo){
		return parent::excluir($codigo);
	}


} ?>