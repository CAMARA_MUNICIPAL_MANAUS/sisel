<?
class CNotafiscal extends MNotafiscal{

	public static function notafiscal(){
		$r = parent::findAllByAttributes(
			array(
				"order" => "ano desc, mes desc"
			), false
		);
		return json_encode($r);
	}

	public static function obraMedicao(){
		$r = parent::findAllByAttributes(
			array(
				"select" => "notafiscal.id, LPAD(notafiscal.nf,6,'0') as nf, f.razao_social",
				"join" => "inner join fornecedor f On f.id = notafiscal.fornecedor_fk",
				"order" => "notafiscal.nf ASC"
			), false
		);
		return json_encode($r);
	}



	public static function savee($dados){
		$codigo = $dados["codigo"];
		unset($dados["codigo"]);
		
		# ENVIAR PARA SALVAR
		if($codigo == ""){
			return parent::save($dados);
		}else{
			return parent::save($dados, $codigo);
		}
	}

	public static function excluir($codigo){
		return parent::excluir($codigo);
	}


} ?>