<?
class CObras extends MObras{

	public static function obras($obra=""){
		$where = ($obra != "") ? "id = $obra" : "1=1";
		$r = parent::findAllByAttributes(
			array(
				"where" => $where,
				"order" => "anoempenho desc, mesempenho desc"
			), false
		);
		return json_encode($r);
	}

	public static function savee($dados){

		$dados = FUN::serializeToArray($dados);
		$dados["obras"]["valor_estimado"] = str_replace(",", ".", str_replace(".", "", $dados["obras"]["valor_estimado"]));
		
		# print_r($dados);
		# ENVIAR PARA SALVAR
		if(!isset($dados["codigo"])){
			return parent::save($dados);
		}else{
			$codigo = $dados["codigo"];
			unset($dados["codigo"]);

			return parent::save($dados, $codigo);
		}
	}

	public static function remove($codigo){
		return parent::excluir($codigo);
	}


} ?>