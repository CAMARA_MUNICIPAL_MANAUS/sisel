<?php
include_once '../../config/conn.php';
include_once '../../config/tabela_auxiliar.php';
$auxiliar = new Auxiliar;
?>
<html>
<head>
	<title>SISEL</title>	
	<meta charset="UTF-8">		
	<link rel="stylesheet" href="../../css/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href="../../css/login.css">

</head>
<body>
<div class="background"></div>
<div id="wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="dvLogin">

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
					<img src="../../img/logo.png" width="260" />
				</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
					<h5>CAMARA MUNICIPAL DE MANAUS</h5>
				</div>

				<form action="../../" method="POST" class="formLogin">
					<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
						<div class="form-group divlogin">
							<label for=""><i class="glyphicon glyphicon-user"></i> Usuario</label>
							<input type="text" name="login" class="form-control required" maxlength="20">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="form-group divsenha">
							<label for=""><i class="glyphicon glyphicon-asterisk"></i> Senha</label>
							<input type="password" name="senha" class="form-control required" maxlength="20">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="form-group divsenha">
							<label for=""><i class="glyphicon glyphicon-calendar"></i> Ano</label>
							<?php $auxiliar->anoexercicio(); ?>
						</div>
					</div>
					<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<label for=""><i class="glyphicon glyphicon-home"></i> Unidade Gestora</label>
							<select name="unidade" class="form-control required">
								<option value="">Selecione</option>
								<option value="cmm">CMM - Câmara Municipal de Manaus</option>
								<option value="fecmm">FECMM - Fundo Especial da Câmara Municipal de Manaus</option>
							</select>
						</div>
					</div> -->
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block btnSign">Entrar</button>
							<small>v 0.0.1</small>
						</div>
					</div>
				</form>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<script src="../../js/jquery.min.js"></script>
<!-- <script src="../../js/jquery-ui.min.js"></script> -->
<script src="../../js/login.js"></script>
</body>
</html>	