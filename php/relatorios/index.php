<?php
$relatorio[1] = array("id" => "1", "report" => "Contrato", "link" => "contrato");
$relatorio[2] = array("id" => "2", "report" => "Licitação", "link" => "licitacao");
$relatorio[3] = array("id" => "3", "report" => "Licitação, Itens, Cotação e Participantes", "link" => "licitacao_itens_cotacao");
$relatorio[4] = array("id" => "4", "report" => "Participante", "link" => "participante");
$relatorio[5] = array("id" => "5", "report" => "Adesão", "link" => "adesao");
$relatorio[6] = array("id" => "6", "report" => "Transferência Voluntária", "link" => "transfvol");
$relatorio[7] = array("id" => "7", "report" => "Nota Fiscal", "link" => "notafiscal");
?>

<div class="col-lg-12 hidden-print">
	<h3 class="page-header">Relatórios</h3>
</div>

<div class="col-lg-12 hidden-print">
	<div class="row">
		<!-- <form action="?p=relatorios&page=visualizar" method="post" role="form" target="_blank"> -->
		<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" role="form" onsubmit="return validarForm('.formReport')" class='formReport'>
			
			<div class="col-lg-4">
				<div class="form-group">
					<label for="">Relatório</label>
					<select name="relatorio" class="form-control relatorio required">
						<option value='' selected=selected>Selecione um Relatório</option>
						<?php
						foreach ($relatorio as $r){
							$selected = (@$_POST["relatorio"] == $r["id"]) ? "selected" : "";
							echo "<option value='".$r["id"]."' $selected>".$r["report"]."</option>";
						}
						?>
					</select>
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Mês Empenho</label>
					<? $auxiliar->mes(@$_POST["mes"]); ?>
				</div>
			</div>

			<div class="col-lg-1">
				<div class="form-group">
					<label for="">Ano</label>
					<input type="text" value="<?= $_SESSION['usuario']['ano_exercicio'] ?>" class="form-control ano required" readonly="true" name="ano"/>
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">&nbsp;</label>
					<button type="submit" class="form-control btn btn-success gerar-relatorio">Gerar Relatório</button>
				</div>
			</div>
			
			<?php if(isset($_POST["relatorio"])){ ?>
				<div class="col-lg-2">
					<div class="form-group">
						<label for="">&nbsp;</label>
						<button type="button" class="form-control btn btn-primary" onclick="window.print();">Imprimir</button>
					</div>
				</div>
			<?php } ?>

		</form>
	</div>
</div>

<div class="col-md-6 col-md-offset-3 text-center visible-print-block">
	<img src="img/empresa/brasao.png" alt="">
	<p>
		<b>ESTADO DO AMAZONAS</b><br/>
		<b><? print_r($_SESSION["empresa"]["nome_empresa"]) ?></b><br/>
		<b>DIRETORIA FINANCEIRA</b>
	</p>
</div>
<?php
if(isset($_POST["relatorio"])){
	include_once 'tiporelatorio/'.$relatorio[$_POST["relatorio"]]["link"].'.php';
}
?>
<div class="col-lg-12 text-right visible-print-block">Manaus, <?= date('d') ?> de <?= $funcoes->mes(date('m')) ?> de <?= date('Y') ?></div>