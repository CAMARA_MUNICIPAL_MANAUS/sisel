<style>
	.dvRel{
		padding: 5px;
	}
	table tbody tr td, table thead tr th{
		font-size: 11px !important;
	}
</style>

<div class="col-lg-12">

	<h4>Relatório de Transferência Voluntária - <?= $funcoes->mes($_POST['mes']); ?> de <?= $_POST['ano'] ?></h4>


	<table class="table table-hover table-stripped table-bordered">
		<thead>
			<tr>
				<th>Nº Transf. Voluntária</th>
				<th>Tipo</th>
				<th>CNPJ do Convenente</th>
				<th>Data da Celebração</th>
				<th class='text-center'>Objeto</th>
			</tr>
		</thead>
		<tbody>
			<?

				$sql = "select t.ntransferenciavoluntaria, tt.descricao, t.cnpjconvenente, t.datacelebracao, t.objeto, t.mes_empenho, t.ano_exercicio
						from transferenciavoluntaria t
						inner join tipotransferenciavoluntaria tt ON tt.id = t.tipotransferencia_fk
						WHERE t.mes_empenho = ".$_POST['mes']." AND t.ano_empenho = ".$_POST['ano']."
						ORDER BY t.ntransferenciavoluntaria ASC";
			    $stmt = $conexao->conn->prepare($sql);
			    $stmt->execute();
			    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

			    if(sizeof($retorno) == 0){
			    	echo "<tr><td colspan='5'>Nenhum resultado encontrado.</td></tr>";
			    	exit();
			    }

			    foreach ($retorno as $ln) {
			    	
					echo "<tr>";
						echo "<td>".$ln['ntransferenciavoluntaria']."</td>";
						echo "<td>".$ln['descricao']."</td>";
						echo "<td>".$ln['cnpjconvenente']."</td>";
						echo "<td class='text-center'>".$funcoes->dateUSparaBR($ln['datacelebracao'])."</td>";
						echo "<td class='text-center'>".$ln['objeto']."</td>";
					echo "</tr>";

			    }

			?>
		</tbody>
	</table>
</div>