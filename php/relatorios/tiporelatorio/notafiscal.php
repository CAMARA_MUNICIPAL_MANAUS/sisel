<style> .dvRel{ padding: 5px; } </style>

<div class="col-lg-12">

	<h4>Relatório de Nota Fiscal - <?= $funcoes->mes($_POST['mes']); ?> de <?= $_POST['ano'] ?></h4>


	<table class="table table-hover table-stripped">
		<thead>
			<tr>
				<th>NOTA FISCAL</th>
				<th>DATA DE EMISSÃO</th>
				<th>RAZÃO SOCIAL</th>
				<th>TIPO</th>
				<th class='text-right'>VALOR LÍQUIDO</th>
				<th class='text-right'>VALOR BRUTO</th>
			</tr>
		</thead>
		<tbody>
			<?
				$sql = "SELECT n.nf, n.dtemissao, f.razao_social,
							   IF(n.tipo_nota=1,'MERCADORIA','SERVIÇO') AS tiponota,
							   n.valor_liquido, n.valor_bruto,
							   IF(LENGTH(TRIM(empenho1)) > 0, concat(n.empenho1,'|',n.notalancamento1,'|',n.anoempenho1), null) as emp1,
							   IF(LENGTH(TRIM(empenho2)) > 0, concat(n.empenho2,'|',n.notalancamento2,'|',n.anoempenho2), null) as emp2,
							   IF(LENGTH(TRIM(empenho3)) > 0, concat(n.empenho3,'|',n.notalancamento3,'|',n.anoempenho3), null) as emp3,
							   IF(LENGTH(TRIM(empenho4)) > 0, concat(n.empenho4,'|',n.notalancamento4,'|',n.anoempenho4), null) as emp4,
							   IF(LENGTH(TRIM(empenho5)) > 0, concat(n.empenho5,'|',n.notalancamento5,'|',n.anoempenho5), null) as emp5,
							   IF(LENGTH(TRIM(empenho6)) > 0, concat(n.empenho6,'|',n.notalancamento6,'|',n.anoempenho6), null) as emp6,
							   IF(LENGTH(TRIM(empenho7)) > 0, concat(n.empenho7,'|',n.notalancamento7,'|',n.anoempenho7), null) as emp7,
							   IF(LENGTH(TRIM(empenho8)) > 0, concat(n.empenho8,'|',n.notalancamento8,'|',n.anoempenho8), null) as emp8
						FROM notafiscal n
						INNER JOIN fornecedor f ON f.id = n.fornecedor_fk
						INNER JOIN tiponotafiscal tn on tn.id = n.tiponotafiscal
						WHERE n.mes = ".$_POST['mes']." and n.ano = ".$_POST['ano'];
			    $stmt = $conexao->conn->prepare($sql);
			    $stmt->execute();
			    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

			    if(sizeof($retorno) == 0){
			    	echo "<tr><td colspan='6'>Nenhum resultado encontrado.</td></tr>";
			    	exit();
			    }

			    foreach ($retorno as $ln) {
			    	
					echo "<tr>";
						echo "<td>".$ln['nf']."</td>";
						echo "<td>".$funcoes->dateUSparaBR($ln['dtemissao'])."</td>";
						echo "<td>".$ln['razao_social']."</td>";
						echo "<td>".$ln['tiponota']."</td>";
						echo "<td class='text-right'>R$ ".number_format($ln['valor_liquido'], 2, ',', '.')."</td>";
						echo "<td class='text-right'>R$ ".number_format($ln['valor_bruto'], 2, ',', '.')."</td>";
					echo "</tr>";
					echo "<tr>";
						echo "<td colspan='1'></td>";
						echo "<td colspan='3'>";
							echo "<table class='table table-striped table-bordered'>";
								echo "<thead>";
									echo "<tr>";
										echo "<th>EMPENHO</th>";
										echo "<th>NOTA DE LANÇAMENTO</th>";
										echo "<th>ANO EMPENHO</th>";
									echo "</tr>";
								echo "</thead>";
								echo "<tbody>";
									for ($i=1; $i<=8; $i++){
										if(!empty($ln["emp".$i])){
											$d = explode("|", $ln["emp".$i]);
											echo "<tr>";
												echo "<td>".$d[0]."</td>";
												echo "<td>".$d[1]."</td>";
												echo "<td>".$d[2]."</td>";
											echo "</tr>";
										} // fim if
									}
								echo "</tbody>";
							echo "</table>";
						echo "</td>";
						echo "<td colspan='2'></td>";
					echo "</tr>";

			    }

			?>
		</tbody>
	</table>
</div>