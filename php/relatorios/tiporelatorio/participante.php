<style> .dvRel{ padding: 5px; } </style>

<div class="col-lg-12">

	<h4>Relatório de Participante - <?= $funcoes->mes($_POST['mes']); ?> de <?= $_POST['ano'] ?></h4>


	<table class="table table-hover table-stripped">
		<thead>
			<tr>
				<th>Nº do Processo</th>
				<th>Participantes</th>
				<th>CPF/CNPJ</th>
			</tr>
		</thead>
		<tbody>
			<?

				$sql = "SELECT p.licitacao_fk,
							GROUP_CONCAT(DISTINCT f.razao_social ORDER BY f.razao_social ASC SEPARATOR '<br/>') as fornecedor,
		 					GROUP_CONCAT(DISTINCT formatarCpfCnpj(f.cpfcnpj) ORDER BY f.razao_social ASC SEPARATOR '<br/>') as cpfcnpj
						FROM participante_certidao p
						INNER JOIN fornecedor f 	ON f.id = p.fornecedor_fk
						INNER JOIN tipopessoa tp 	ON tp.id = f.tipopessoa_fk
						INNER JOIN licitacao l 		ON l.id = p.licitacao_fk
						WHERE l.mes_empenho = ".$_POST['mes']."
						AND l.ano_empenho = ".$_POST['ano']."
						GROUP BY p.licitacao_fk";
			    $stmt = $conexao->conn->prepare($sql);
			    $stmt->execute();
			    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

			    if(sizeof($retorno) == 0){
			    	echo "<tr><td colspan='3'>Nenhum resultado encontrado.</td></tr>";
			    	exit();
			    }

			    foreach ($retorno as $ln) {
			    	
					echo "<tr>";
						echo "<td>".$ln['licitacao_fk']."</td>";
						echo "<td>".$ln['fornecedor']."</td>";
						echo "<td>".$ln['cpfcnpj']."</td>";
					echo "</tr>";

			    }

			?>
		</tbody>
	</table>
</div>