<style>
	.dvRel{
		padding: 5px;
	}
</style>

<div class="col-lg-12">

	<h4>Relatório de Licitação - <?= $funcoes->mes($_POST['mes']); ?> de <?= $_POST['ano'] ?></h4>


	<table class="table table-hover table-stripped">
		<thead>
			<tr>
				<th>Nº do Processo</th>
				<th>Modalidade</th>
				<th>Objeto</th>
				<th class='text-right'>Valor</th>
				<th class='text-right'>Nº Edital</th>
				<th class='text-right'>Public. Edital</th>
			</tr>
		</thead>
		<tbody>
			<?

				$sql = "SELECT l.id, m.descricao, l.objetivo, l.valor_bloqueado, l.numero_edital, l.dt_publicacao
						FROM licitacao l
						INNER JOIN tipomodalidade m ON m.id = l.modalidade_fk
						WHERE l.mes_empenho = ".$_POST['mes']."
						AND l.ano_empenho = ".$_POST['ano'];
			    $stmt = $conexao->conn->prepare($sql);
			    $stmt->execute();
			    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

			    if(sizeof($retorno) == 0){
			    	echo "<tr><td colspan='6'>Nenhum resultado encontrado.</td></tr>";
			    	exit();
			    }

			    foreach ($retorno as $ln) {
			    	
					echo "<tr>";
						echo "<td>".$ln['id']."</td>";
						echo "<td>".$ln['descricao']."</td>";
						echo "<td>".$ln['objetivo']."</td>";
						echo "<td class='text-right'>R$ ".number_format($ln['valor_bloqueado'], 2, ',', '.')."</td>";
						echo "<td class='text-right'>".$ln['numero_edital']."</td>";
						echo "<td class='text-right'>".$funcoes->dateUSparaBR($ln['dt_publicacao'])."</td>";
					echo "</tr>";

			    }

			?>
		</tbody>
	</table>
</div>