<style>
	.dvRel{
		padding: 5px;
	}
	table tbody tr td, table thead tr th{
		font-size: 11px !important;
	}
</style>

<div class="col-lg-12">

	<h5>Relatório de Contrato - <?= $funcoes->mes($_POST['mes']); ?> de <?= $_POST['ano'] ?></h5>


	<table class="table table-hover table-stripped table-bordered">
		<thead>
			<tr>
				<th>Nº do Contrato</th>
				<th>Contratado</th>
				<th>Objeto</th>
				<th>Valor</th>
				<th class='text-center'>Data do Contrato</th>
				<th class='text-center'>Licitação</th>
			</tr>
		</thead>
		<tbody>
			<?

				$sql = "SELECT c.numcontrato, f.razao_social, c.objetivo, c.dtcontrato, c.licitacao_fk, c.valor
						FROM contrato c
						INNER JOIN fornecedor f ON f.id = c.fornecedor_fk
						WHERE c.mesEmpenho = ".$_POST['mes']." AND c.anoEmpenho = ".$_POST['ano']."
						ORDER BY c.dtcontrato ASC";
			    $stmt = $conexao->conn->prepare($sql);
			    $stmt->execute();
			    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

			    if(sizeof($retorno) == 0){
			    	echo "<tr><td colspan='5'>Nenhum resultado encontrado.</td></tr>";
			    	exit();
			    }

			    foreach ($retorno as $ln) {
			    	
					echo "<tr>";
						echo "<td>".$ln['numcontrato']."</td>";
						echo "<td>".$ln['razao_social']."</td>";
						echo "<td>".$ln['objetivo']."</td>";
						echo "<td>R$ ".number_format($ln['valor'], 2, ',', '.')."</td>";
						echo "<td class='text-center'>".$funcoes->dateUSparaBR($ln['dtcontrato'])."</td>";
						echo "<td class='text-center'>".$ln['licitacao_fk']."</td>";
					echo "</tr>";

			    }

			?>
		</tbody>
	</table>
</div>