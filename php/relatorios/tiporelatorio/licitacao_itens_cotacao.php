<style>
	.dvRel{ padding: 5px; }

	ul li{
		list-style-type:decimal;
	}

	@media print {
		@page {size: portrait;}
	}
</style>

<div class="col-lg-12">

	<h4>Relatório Completo de Licitação - <?= $funcoes->mes($_POST['mes']); ?> de <?= $_POST['ano'] ?></h4>
	<hr/>
	
	<?

		$sql = "SELECT l.id, l.objetivo, l.valor_bloqueado
				FROM licitacao l
				WHERE l.mes_empenho = ".$_POST['mes']."
				AND l.ano_empenho = ".$_POST['ano'];
	    $stmt = $conexao->conn->prepare($sql);
	    $stmt->execute();
	    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

	    if(sizeof($retorno) == 0){
	    	echo "<tr><td colspan='3'>Nenhum resultado encontrado.</td></tr>";
	    	exit();
	    }

	    foreach ($retorno as $lic) {
	    	
			echo "<legend style='font-size: 20px;'>".$lic['id']." <small>(".$lic['objetivo']." - R$ ".number_format($lic['valor_bloqueado'], 2, ',', '.').")</small></legend>";

			$sql = "SELECt i.id, p.descricao, i.quantidade, si.`status`
					FROM itemlicitacao i
					INNER JOIN produto p ON p.id = i.produto_fk
					LEFT JOIN status_item si ON si.id = i.status_item_fk
					WHERE i.licitacao_fk = '".$lic['id']."'
					ORDER BY i.sequencial";
		    $stmt = $conexao->conn->prepare($sql);
		    $stmt->execute();
		    $retorno2 = $stmt->fetchAll(PDO::FETCH_ASSOC);

		    echo "<ul>";
			    foreach ($retorno2 as $item) {
			    	echo "<li><i>".$item['descricao']." ( Qtd: ".$item['quantidade']." ) | Status: ".$item['status']."</i></li>";

			    	$sql = "SELECT trim(f.razao_social) as razao_social, c.valor_total, tp.tipovalor, tr.tiporesultado
							FROM cotacao c
							INNER JOIN fornecedor f ON f.id = c.participante_fk
							INNER JOIN tipovalor tp ON tp.id = c.tipovalor_fk
							INNER JOIN tiporesultado tr ON tr.id = c.tiporesultado_fk
							WHERE c.itemlicitacao_fk = ".$item['id']."
							ORDER BY trim(f.razao_social) ASC;";
				    $stmt = $conexao->conn->prepare($sql);
				    $stmt->execute();
				    $retorno3 = $stmt->fetchAll(PDO::FETCH_ASSOC);

				    	echo "<table class='table table-stripped table-hover'>";
				    		echo "<thead>";
				    			echo "<tr>";
				    				echo "<th >Fornecedor</th>";
				    				echo "<th style='width: 100px;'>Valor Total</th>";
				    				echo "<th style='width: 100px; text-align: center'>Tipo de Valor</th>";
				    				echo "<th style='width: 100px; text-align: center'>Resultado</th>";
				    			echo "</tr>";
				    		echo "</thead>";
				    		echo "<tbody>";
				    			foreach ($retorno3 as $cot) {
					    			echo "<tr>";
					    				echo "<td>".$cot['razao_social']."</td>";
					    				echo "<td style='width: 100px;'>R$ ".number_format($cot['valor_total'], 2, ',', '.')."</td>";
					    				echo "<td style='width: 100px; text-align: center'>".$cot['tipovalor']."</td>";
					    				echo "<td style='width: 100px; text-align: center'>".$cot['tiporesultado']."</td>";
					    			echo "</tr>";
				    			}
				    		echo "</tbody>";
				    	echo "</table>";

			    }
		    echo "</ul>";



	    }

	?>

</div>