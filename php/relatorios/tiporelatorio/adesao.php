<style>
	.dvRel{
		padding: 5px;
	}
	table tbody tr td, table thead tr th{
		font-size: 11px !important;
	}
</style>

<div class="col-lg-12">

	<h4>Relatório de Adesão - <?= $funcoes->mes($_POST['mes']); ?> de <?= $_POST['ano'] ?></h4>


	<table class="table table-hover table-stripped table-bordered">
		<thead>
			<tr>
				<th>Nº Ata</th>
				<th>Nº Processo de Compra</th>
				<th>Processo Licitatório</th>
				<th>Data da Adesão</th>
				<th class='text-center'>Fornecedor</th>
				<th class='text-center'>CPf/CNPJ</th>
			</tr>
		</thead>
		<tbody>
			<?

				$sql = "SELECT a.numprocessocompra as `numata`, a.numata as `numprocessocompra`, a.numprocesso_licitatorio, a.dt_adesao, f.nome_fantasia,
							f.cpfcnpj, f.razao_social, a.mes_empenho, a.ano_empenho
						FROM adesao a
						INNER JOIN fornecedor f ON f.id = a.fornecedor_fk
						WHERE a.mes_empenho = ".$_POST['mes']." AND a.ano_empenho = ".$_POST['ano']."
						ORDER BY a.numprocessocompra ASC";
			    $stmt = $conexao->conn->prepare($sql);
			    $stmt->execute();
			    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

			    if(sizeof($retorno) == 0){
			    	echo "<tr><td colspan='5'>Nenhum resultado encontrado.</td></tr>";
			    	exit();
			    }

			    foreach ($retorno as $ln) {
			    	
					echo "<tr>";
						echo "<td>".$ln['numata']."</td>";
						echo "<td>".$ln['numprocessocompra']."</td>";
						echo "<td>".$ln['numprocesso_licitatorio']."</td>";
						echo "<td class='text-center'>".$funcoes->dateUSparaBR($ln['dt_adesao'])."</td>";
						echo "<td class='text-center'>".$ln['razao_social']."</td>";
						echo "<td class='text-center'>".$ln['cpfcnpj']."</td>";
					echo "</tr>";

					/*echo "<tr>";
						echo "<td colspan='6'>";
							echo "<table class='table table-hover table-stripped table-bordereds'>";
								echo "<thead>";
									echo "<tr>";
										echo "<th>Nº</th>";
										echo "<th>Produto</th>";
										echo "<th>Qtd</th>";
										echo "<th>Val. Unit.</th>";
										echo "<th>Val. Total</th>";
									echo "</tr>";
								echo "</thead>";
									
								echo "<tbody>";
								echo "</tbody>";
							echo "</table>";
						echo "</td>";
					echo "</tr>";*/

			    }

			?>
		</tbody>
	</table>
</div>