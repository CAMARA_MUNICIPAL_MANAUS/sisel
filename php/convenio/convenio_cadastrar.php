<script type="text/javascript" src="js/cadastro_convenio.js"></script>

<?php

if(isset($_GET['convenio'])){
	$convenio = $funcoes->buscardados("convenio", $_GET['convenio']);
    $conv = $convenio[0];

   /*echo "<pre>";
    print_r($conv);
    echo "</pre>";
*/
}else{
	$ordenador = $_SESSION['empresa']['ordenador'];
}


?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Convênio Cadastrar</h3>
	</div>
	<div class="panel-body">
		<form action="">

			<div class="col-lg-6">
				<div class="form-group">
					<label for="">Tipo de Convênio</label>
					<? $auxiliar->tipoconvenio(@$conv['tipoconvenio_fk']); ?>
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Nº Convênio</label>
					<input type="text" class="form-control numconvenio" maxlength="16" name="numconvenio" value="<?= @$conv['numconvenio'] ?>" />
				</div>
			</div>

			<div class="col-lg-4">
				<div class="form-group">
					<label for="">Esfera</label>
					<? $auxiliar->tipoesfera(@$conv['esfera_fk']); ?>
				</div>
			</div>

			<div class="col-lg-6">
				<div class="form-group">
					<label for="">Objeto</label>
					<input type="text" class="form-control objeto" name="objeto" maxlength="300"  value="<?= @$conv['objconvenio'] ?>" />
				</div>
			</div>

			<div class="col-lg-5">
				<div class="form-group">
					<label for="">Ordenador</label>
					<input type="text" class="form-control ordenador" name="ordenador" value="<?= @$ordenador; ?>"/>
				</div>
			</div>

			<div class="col-lg-1">
				<div class="form-group">
					<label for="">Nº DOE</label>
					<input type="text" class="form-control ndoe" name="ndoe" value="<?= @$conv['numdoem'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Recebe Valor</label>
					<select name="recebevalor" class="recebevalor form-control">
						<option value="S" <? if(@$conv['recebervalor'] == 'S'){ echo "selected=selected"; } ?>>SIM</option>
						<option value="N" <? if(@$conv['recebervalor'] == 'N'){ echo "selected=selected"; } ?>>NÃO</option>
					</select>
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Moeda</label>
					<?= $auxiliar->moeda(@$conv['moeda']); ?>
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Valor</label>
					<input type="text" class="form-control dinheiro" name="dinheiro" maxlength="16" value="<?= @$conv['valor'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Data Assinatura</label>
					<input type="text" class="form-control dtassinatura data" name="dtassinatura" value="<?= $funcoes->dateUSparaBR(@$conv['dtassinatura']); ?>"/>
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Data Vencimento</label>
					<input type="text" class="form-control dtvencimento data" name="dtvencimento" value="<?= $funcoes->dateUSparaBR(@$conv['dtvencimento']); ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Data Publicação</label>
					<input type="text" class="form-control dtpublicacao data" name="dtpublicacao" value="<?= $funcoes->dateUSparaBR(@$conv['dtpublicacao']); ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Nº Ofício</label>
					<input type="text" class="form-control noficio" name="noficio" value="<?= @$conv['numoficio'] ?>"/>
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Data Ofício</label>
					<input type="text" class="form-control dtoficio data" name="dtoficio" value="<?= $funcoes->dateUSparaBR(@$conv['dtoficio']); ?>"/>
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Arquivo Texto</label>
					<input type="text" class="form-control arqtexto" name="arqtexto" readonly="" value="<?= @$conv['nomearquivotexto'] ?>" />
				</div>
			</div>

			<div class="clearfix"></div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Empenho 1</label>
					<input type="text" class="form-control empenho1" name="empenho1" maxlength="10" value="<?= @$conv['numempenho1'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Empenho 2</label>
					<input type="text" class="form-control empenho2" name="empenho2" maxlength="10" value="<?= @$conv['numempenho2'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Empenho 3</label>
					<input type="text" class="form-control empenho3" name="empenho3" maxlength="10" value="<?= @$conv['numempenho3'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Empenho 4</label>
					<input type="text" class="form-control empenho4" name="empenho4" maxlength="10" value="<?= @$conv['numempenho4'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Empenho 5</label>
					<input type="text" class="form-control empenho5" name="empenho5" maxlength="10" value="<?= @$conv['numempenho5'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Empenho 6</label>
					<input type="text" class="form-control empenho6" name="empenho6" maxlength="10" value="<?= @$conv['numempenho6'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Empenho 7</label>
					<input type="text" class="form-control empenho7" name="empenho7" maxlength="10" value="<?= @$conv['numempenho7'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Empenho 8</label>
					<input type="text" class="form-control empenho8" name="empenho8" maxlength="10" value="<?= @$conv['numempenho8'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Empenho 9</label>
					<input type="text" class="form-control empenho9" name="empenho9" maxlength="10" value="<?= @$conv['numempenho9'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Empenho 10</label>
					<input type="text" class="form-control empenho10" name="empenho10" maxlength="10" value="<?= @$conv['numempenho10'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Empenho 11</label>
					<input type="text" class="form-control empenho11" name="empenho11" maxlength="10" value="<?= @$conv['numempenho11'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Empenho 12</label>
					<input type="text" class="form-control empenho12" name="empenho12" maxlength="10" value="<?= @$conv['numempenho12'] ?>" />
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Mês Empenho</label>
					<? $auxiliar->mes(@$conv['mes']); ?>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Ano Empenho</label>
					<? $auxiliar->anoexercicio(@$conv['ano']); ?>
				</div>
			</div>

			<div class="clearfix"></div>

			<div class="col-lg-1">
				<div class="form-group">
					<label for="">&nbsp;</label>
					<? if(isset($_GET['convenio'])){ ?>
						<button type="button" class="btn btn-success form-control btn-editar" codigo="<?= $_GET['convenio'] ?>" acao="editar">Editar</button>
					<? }else{ ?>
						<button type="button" class="btn btn-success form-control btn-inserir" acao="inserir">Inserir</button>
					<? } ?>
				</div>
			</div>
			<div class="col-lg-1">
				<div class="form-group">
					<label for="">&nbsp;</label>
					<a href="?p=convenio&page=convenio" class="btn btn-danger form-control">Cancelar</a>
				</div>
			</div>

		</form>
	</div>
</div>