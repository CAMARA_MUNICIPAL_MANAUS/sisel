<?php

if(isset($_POST['acao'])){

	include_once '../../config/conn.php';
	include_once '../../config/validarsessao.php';
	include_once '../../config/funcoesgerais.php';

	$funcoes  = new Funcoes;


	switch ($_POST['acao']) {
		
		case 'inserir':

			$insert = "INSERT INTO convenio (numconvenio, tipoconvenio_fk, esfera_fk, recebervalor, objconvenio, respjuridico, moeda, valor, dtassinatura, dtvencimento, numdoem, dtpublicacao,
											 numoficio, dtoficio, nomearquivotexto, ano, mes, numempenho, numempenho2, numempenho3, numempenho4, numempenho5, numempenho6,
											 numempenho7, numempenho8, numempenho9, numempenho10, numempenho11, numempenho12, ano_exercicio, usuario)

						VALUES (:numconvenio, :tipoconvenio, :tipoesfera, :recebevalor, :objeto, :ordenador, :tipomoeda, :dinheiro, :dtassinatura, :dtvencimento, :ndoe, :dtpublicacao,
								:noficio, :dtoficio, :arqtexto, :ano, :mes, :empenho1, :empenho2, :empenho3, :empenho4, :empenho5, :empenho6, 
								:empenho7, :empenho8, :empenho9, :empenho10, :empenho11, :empenho12, :ano_exercicio, :usuario)";

		    $stmt = $conexao->conn->prepare($insert);

		    $stmt->bindValue(':numconvenio', $_POST['numconvenio']);
		    $stmt->bindValue(':tipoconvenio', $_POST['tipoconvenio']);
		    $stmt->bindValue(':tipoesfera', $_POST['tipoesfera']);
		    $stmt->bindValue(':recebevalor', $_POST['recebevalor']);
		    $stmt->bindValue(':objeto', $_POST['objeto']);
		    $stmt->bindValue(':ordenador', $_POST['ordenador']);
		    $stmt->bindValue(':tipomoeda', $_POST['tipomoeda']);
		    $stmt->bindValue(':dinheiro', $funcoes->formatarValorParaBanco($_POST['dinheiro']));
		    $stmt->bindValue(':dtassinatura', $funcoes->dateBRparaUSMySql($_POST['dtassinatura']));
		    $stmt->bindValue(':dtvencimento', $funcoes->dateBRparaUSMySql($_POST['dtvencimento']));
		    $stmt->bindValue(':ndoe', $_POST['ndoe']);
		    $stmt->bindValue(':dtpublicacao', $funcoes->dateBRparaUSMySql($_POST['dtpublicacao']));
		    $stmt->bindValue(':noficio', $_POST['noficio']);
		    $stmt->bindValue(':dtoficio', $funcoes->dateBRparaUSMySql($_POST['dtoficio']));
		    $stmt->bindValue(':arqtexto', $_POST['arqtexto']);
		    $stmt->bindValue(':mes', $_POST['mes']);
		    $stmt->bindValue(':ano', $_POST['anoexercicio']);
		    $stmt->bindValue(':empenho1', $_POST['empenho1']);
		    $stmt->bindValue(':empenho2', $_POST['empenho2']);
		    $stmt->bindValue(':empenho3', $_POST['empenho3']);
		    $stmt->bindValue(':empenho4', $_POST['empenho4']);
		    $stmt->bindValue(':empenho5', $_POST['empenho5']);
		    $stmt->bindValue(':empenho6', $_POST['empenho6']);
		    $stmt->bindValue(':empenho7', $_POST['empenho7']);
		    $stmt->bindValue(':empenho8', $_POST['empenho8']);
		    $stmt->bindValue(':empenho9', $_POST['empenho9']);
		    $stmt->bindValue(':empenho10', $_POST['empenho10']);
		    $stmt->bindValue(':empenho11', $_POST['empenho11']);
		    $stmt->bindValue(':empenho12', $_POST['empenho12']);
		    $stmt->bindValue(':ano_exercicio', $_SESSION['usuario']['ano_exercicio']);
		    $stmt->bindValue(':usuario', $_SESSION['usuario']['id']);
		    
		    if($stmt->execute()){
		    	echo 1;
		    }else{
		    	echo 0;
		    	print_r($stmt->errorInfo());
		    }
			
		break;

		case 'editar':

			$update = "UPDATE convenio 
							SET numconvenio = :numconvenio,
								tipoconvenio_fk = :tipoconvenio,
								esfera_fk = :tipoesfera,
								recebervalor = :recebevalor,
								objconvenio = :objeto,
								respjuridico = :ordenador,
								moeda = :tipomoeda,
								valor = :dinheiro,
								dtassinatura = :dtassinatura,
								dtvencimento = :dtvencimento,
								numdoem = :ndoe,
								dtpublicacao = :dtpublicacao,
								numoficio = :noficio,
								dtoficio = :dtoficio,
								nomearquivotexto = :arqtexto,
								ano = :ano,
								mes = :mes,
								numempenho = :empenho1,
								numempenho2 = :empenho2,
								numempenho3 = :empenho3,
								numempenho4 = :empenho4,
								numempenho5 = :empenho5,
								numempenho6 = :empenho6,
								numempenho7 = :empenho7,
								numempenho8 = :empenho8,
								numempenho9 = :empenho9,
								numempenho10 = :empenho10,
								numempenho11 = :empenho11,
								numempenho12 = :empenho12,
								ano_exercicio = :ano_exercicio,
								usuario = :usuario
							WHERE id = :codigo";

		    $stmt = $conexao->conn->prepare($update);

		    $stmt->bindValue(':numconvenio', $_POST['numconvenio']);
		    $stmt->bindValue(':tipoconvenio', $_POST['tipoconvenio']);
		    $stmt->bindValue(':tipoesfera', $_POST['tipoesfera']);
		    $stmt->bindValue(':recebevalor', $_POST['recebevalor']);
		    $stmt->bindValue(':objeto', $_POST['objeto']);
		    $stmt->bindValue(':ordenador', $_POST['ordenador']);
		    $stmt->bindValue(':tipomoeda', $_POST['tipomoeda']);
		    $stmt->bindValue(':dinheiro', $funcoes->formatarValorParaBanco($_POST['dinheiro']));
		    $stmt->bindValue(':dtassinatura', $funcoes->dateBRparaUSMySql($_POST['dtassinatura']));
		    $stmt->bindValue(':dtvencimento', $funcoes->dateBRparaUSMySql($_POST['dtvencimento']));
		    $stmt->bindValue(':ndoe', $_POST['ndoe']);
		    $stmt->bindValue(':dtpublicacao', $funcoes->dateBRparaUSMySql($_POST['dtpublicacao']));
		    $stmt->bindValue(':noficio', $_POST['noficio']);
		    $stmt->bindValue(':dtoficio', $funcoes->dateBRparaUSMySql($_POST['dtoficio']));
		    $stmt->bindValue(':arqtexto', $_POST['arqtexto']);
		    $stmt->bindValue(':ano', $_POST['anoexercicio']);
		    $stmt->bindValue(':mes', $_POST['mes']);
		    $stmt->bindValue(':empenho1', $_POST['empenho1']);
		    $stmt->bindValue(':empenho2', $_POST['empenho2']);
		    $stmt->bindValue(':empenho3', $_POST['empenho3']);
		    $stmt->bindValue(':empenho4', $_POST['empenho4']);
		    $stmt->bindValue(':empenho5', $_POST['empenho5']);
		    $stmt->bindValue(':empenho6', $_POST['empenho6']);
		    $stmt->bindValue(':empenho7', $_POST['empenho7']);
		    $stmt->bindValue(':empenho8', $_POST['empenho8']);
		    $stmt->bindValue(':empenho9', $_POST['empenho9']);
		    $stmt->bindValue(':empenho10', $_POST['empenho10']);
		    $stmt->bindValue(':empenho11', $_POST['empenho11']);
		    $stmt->bindValue(':empenho12', $_POST['empenho12']);
		    $stmt->bindValue(':ano_exercicio', $_SESSION['usuario']['ano_exercicio']);
		    $stmt->bindValue(':usuario', $_SESSION['usuario']['id']);
		    $stmt->bindValue(':codigo', $_POST['coigo']);
		    
		    if($stmt->execute()){
		    	echo 1;
		    }else{
		    	echo 0;
		    	print_r($stmt->errorInfo());
		    	print_r($_POST);
		    }
			
		break;

	}

}


?>