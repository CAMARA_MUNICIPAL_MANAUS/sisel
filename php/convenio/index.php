<script type="text/javascript" src="js/cadastro_convenio.js"></script>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Convênio</h3>
	</div>
	<div class="panel-body">
		<div class="col-md-2">
			<a href="?p=convenio&page=convenio_cadastrar" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Novo Convênio</a>
		</div>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>Nº Convênio</th>
					<th>Nº DOE</th>
					<th>Objeto</th>
					<th class="text-right">Valor</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?
					$sql = "SELECT c.id, c.numconvenio, c.numdoem, c.objconvenio, ROUND(c.valor) as valor
							FROM convenio c
							WHERE c.ano_exercicio = ".$_SESSION['usuario']['ano_exercicio'];
														
					$stmt = $conexao->conn->prepare($sql);
					$stmt->execute();
					$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

					$i = 1;
					foreach ($retorno as $ln) {
						echo "<tr>";
							
							echo "<td>".$ln['numconvenio']."</td>";
							echo "<td>".$ln['numdoem']."</td>";
							echo "<td>".$ln['objconvenio']."</td>";
							echo "<td class='text-right'>".number_format($ln['valor'], 2, ',', '.')."</td>";
							
							echo "<td class='text-right'>";
								echo "<a href='?p=convenio&page=convenio_cadastrar&convenio=".$ln['id']."' class='btn btn-primary visualizar'><i class='glyphicon glyphicon-search'></i></a>";
									echo "&nbsp;";
								echo "<button type='button' class='btn btn-danger excluir' data-id='{$ln['id']}'><i class='glyphicon glyphicon-remove'></i></button>";
							echo "</td>";
						echo "</tr>";
					}
				?>
			</tbody>
		</table>
	</div>
</div>