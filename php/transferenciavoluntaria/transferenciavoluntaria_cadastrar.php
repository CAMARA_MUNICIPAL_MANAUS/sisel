<script type="text/javascript" src="js/action/transferenciavoluntaria/transferenciavoluntaria.js"></script>


<?php
if(isset($_GET['transv'])){
    $transf = $funcoes->buscardados_campo('transferenciavoluntaria', 'ntransferenciavoluntaria', $_GET['transv']);

    $conv = $funcoes->buscardados_campo('convenentetv', 'ntransferenciavoluntaria', $_GET['transv']);

    $forn = $funcoes->buscardados_campo('fornecedor', 'cpfcnpj', $transf[0]['cnpjconvenente']);

    $conv[0]['data_inicio'] = $funcoes->dateUSparaBR($conv[0]['data_inicio']);

    // print_r($transf);
}
?>
<div class="alert alert-dismissible alert-warning text-center">  <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->  NESSE ARQUIVO SÓ SERÃO INFORMADAS TODAS AS <strong>TRANSFERÊNCIAS VOLUNTÁRIAS</strong> NAS QUAIS O ÓRGÃO ATUE COMO CONCEDENTE/REPASSADOR.</div>
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Convenente TV</h3>
    </div>
    <div class="panel-body">
        <form action="">
            
            <div class="col-lg-2">
                <div class="form-group">
                    <label for="">CNPJ do Convenente</label>
                    <input type="text" class="form-control cnpj" name="cnpj-convenentetv" value="<?= @$transf[0]['cnpjconvenente']; ?>" disabled>
                </div>
            </div>

            <div class="col-lg-5">
                <div class="form-group">
                    <label for="">Razão Social</label>
                    <input type="text" class="form-control" name="razaosocial-convenentetv" disabled value="<?= @$forn[0]['razao_social']; ?>">
                </div>
            </div>

            <div class="col-lg-5">
                <div class="form-group">
                    <label for="">Nome Fantasia</label>
                    <input type="text" class="form-control" name="nomefantaia-convenentetv" disabled value="<?= @$forn[0]['nome_fantasia']; ?>">
                </div>
            </div>

            <div class="col-lg-2">
                <div class="form-group">
                    <label for="">Início da Atividade</label>
                    <input type="text" class="form-control data" name="inicio-convenentetv" value="<?= @$conv[0]['data_inicio']; ?>">
                </div>
            </div>

            <div class="col-lg-8">
                <div class="form-group">
                    <label for="">CNAE</label>
                    <?php echo $auxiliar->cnae(@$conv[0]['cnae']); ?>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="form-group">
                    <label for="">Esfera</label>
                    <?php echo $auxiliar->tipoesfera(@$conv[0]['esfera']); ?>
                </div>
            </div>

        </form>
    </div>
</div>

<br/>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Transferência Voluntária</h3>
    </div>
    <div class="panel-body">
        <form id="cadastrar_transferenciavoluntaria" action="" role="form">

            <div class="col-lg-2">
                <div class="form-group">
                    <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='CNPJ do Convenente/OSC (Organização da Sociedade Civil).'></i> CNPJ do Convenente</label>
                    <input type="text" class="form-control cnpj required" name="cnpjconvenente" value="<?= @$transf[0]['cnpjconvenente']; ?>">
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label class="label-obrigatorio">Tipo da Transferência / Aditivo</label>
                    <? $auxiliar->tipotransferenciavoluntaria(@$transf[0]['tipotransferencia_fk']); ?>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label class="control-label label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Compreende o número único emitido pelo concedente/repasador que deverá ser o identificados do instrumento, sendo composto preferencialmente por números. Não podendo haver repetição da numeração em outros exercícios.'></i> Nº da Transf. Voluntária/Aditivo</label>
                    <input type="text" class="form-control required" name="ntransferenciavoluntaria" maxlength="20" placeholder="Ex.: TCL0001-2015" value="<?= @$transf[0]['ntransferenciavoluntaria']; ?>">
                </div>
            </div>


            <div class="col-lg-2">
                <div class="form-group">
                    <label class=""><i class='glyphicon glyphicon-info-sign blue' title='Informar obrigatoriamente o Número da Transferência Superior quando for um aditivo.'></i> Nº da Transf. Vol. Superior</label>
                    <input type="text" class="form-control" name="ntransferenciavoluntariasuperior" maxlength="20" value="<?= @$transf[0]['ntransferenciavoluntariasuperior']; ?>">
                </div>
            </div>

            <div class="col-lg-2">
                <div class="form-group">
                    <label class="control-label label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Ano em que o ato foi celebrado.'></i> Ano da Transferência</label>
                    <input type="text" class="form-control sonums required" name="anotransferencia" maxlength="4" value="<?= @$transf[0]['anotransferencia']; ?>">
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Informar a data em que foi publicado o extrato do instrumento da transferência voluntária / aditivo em periódico oficial da concedente. Deve ser igual ou posterior à data de celebração.'></i> Atividade Principal</label>
                    <? $auxiliar->tipoatividadeprincipal($transf[0]['tipoatividadeprincipal_fk']); ?>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="form-group">
                    <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Data de assinatura do instrumento de Transferência Voluntária / Aditivo'></i> Data de Celebração</label>
                    <input type="text" class="form-control data required" name="datacelebracao" value="<?= @$funcoes->dateUSparaBR($transf[0]['datacelebracao']); ?>">
                </div>
            </div>

            <div class="col-lg-2">
                <div class="form-group">
                    <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Informar data válida destacada em cláusula do instrumento de transferência / aditivo, que deverá ser anterior ou igual a data atual.'></i> Data Inicio da Vigência</label>
                    <input type="text" class="form-control data required" name="datainiciovigencia" value="<?= @$funcoes->dateUSparaBR($transf[0]['datainiciovigencia']); ?>">
                </div>
            </div>

            <div class="col-lg-2">
                <div class="form-group">
                    <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Informar data válida destacada em cláusula do instrumento de transferência / aditivo, que deverá ser igual ou posterior a de início da vigência.'></i> Data Fim da Vigência</label>
                    <input type="text" class="form-control data required" name="datafimvigencia" value="<?= @$funcoes->dateUSparaBR($transf[0]['datafimvigencia']); ?>">
                </div>
            </div>

            <div class="col-lg-2">
                <div class="form-group">
                    <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Informar a data em que foi publicado o extrato do instrumento da transferência voluntária / aditivo em periódico oficial da concedente. Deve ser igual ou posterior à data de celebração.'></i> Data de Publicação</label>
                    <input type="text" class="form-control data required" name="datapublicacao" value="<?= @$funcoes->dateUSparaBR($transf[0]['datapublicacao']); ?>">
                </div>
            </div>


            <div class="col-lg-12">
                <div class="form-group">
                    <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Transcrever em texto, de forma clara e precisa, qual o objeto da transferência conforme definido no instrumento celebrado. Se Aditivo, descrever com clareza a alteração que o termo aditivo realizou.'></i> Objeto</label>
                    <input type="text" class="form-control required" name="objeto" maxlength="300" value="<?= @$transf[0]['objeto']; ?>"/>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Código do Banco.'></i> Banco</label>
                    <? $auxiliar->tipobanco(@$transf[0]['banco']); ?>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Informar a Agência Bancária.'></i> Agência Bancária</label>
                    <input type="text" class="form-control required" name="agenciabancaria" maxlength="7" value="<?= @$transf[0]['agenciabancaria']; ?>"/>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Informar a Conta Corrente para movimentação dos recursos financeiros decorrentes da transferência.'></i> Conta Bancária</label>
                    <input type="text" class="form-control" name="contabancaria" maxlength="14" value="<?= @$transf[0]['contabancaria']; ?>"/>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Informar o valor de repasse previsto no instrumento celebrado. No caso de aditivo, informar o valor atualizado de repasse. Se não houver alteração, informar o valor original do repasse.'></i> Valor do Repasse</label>
                    <input type="text" class="form-control dinheiro required" name="valorrepasse" maxlength="18" value="<?= @number_format($transf[0]['valorrepasse'], 2, ',', '.'); ?>"/>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label class="label-obrigatorio">Tipo da Contrapartida</label>
                    <? $auxiliar->tipocontrapartida(@$transf[0]['tipocontrapartida_fk']); ?>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="form-group">
                    <label class=""><i class='glyphicon glyphicon-info-sign blue' title='Se o tipo da contrapartida for Financeira, Econômica ou Financeira e Econômica, descrever a parcela de colaboração do convenente para a consecução do objeto.'></i> Contrapartida</label>
                    <input type="text" class="form-control" name="contrapartida" maxlength="300" value="<?= @$transf[0]['contrapartida']; ?>"/>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label class=""><i class='glyphicon glyphicon-info-sign blue' title='Informar o valor da contrapartida que cabe ao recebedor da Transferência Voluntária, caso seja uma contrapartida financeira. No caso de aditivo, informar o valor atualizado da contrapartida, caso não haja alteração, informar o valor original da contrapartida.'></i> Valor Contrapartida Financeira</label>
                    <input type="text" class="form-control dinheiro" name="valorcontrapartidafinanceira" maxlength="18" value="<?= @number_format($transf[0]['valorcontrapartidafinanceira'], 2, ',', '.'); ?>"/>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label class=""><i class='glyphicon glyphicon-info-sign blue' title='Informar o valor da contrapartida que cabe ao recebedor da Transferência Voluntária, caso seja uma contrapartida Econômica, assim entendido em bens e serviços economicamente mensuráveis. No caso de aditivo, informar o valor atualizado da contrapartida, caso não haja alteração, informar o valor original da contrapartida.'></i> Valor Contrapartida Econômica</label>
                    <input type="text" class="form-control dinheiro" name="valorcontrapartidaeconomica" maxlength="18" value="<?= @number_format($transf[0]['valorcontrapartidaeconomica'], 2, ',', '.'); ?>"/>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group"> 
                    <label>Mês do Empenho</label>
                    <?php $auxiliar->mes($transf[0]['mes_empenho']); ?>
                </div>
            </div>

            <div class="col-md-1">
                <div class="form-group"> 
                    <label class="label-obrigatorio" title="Ano do Empenho">Ano do Emp.</label>
                    <input name="anoEmpenho" type="text" id="anoEmpenho" class="form-control sonums required" maxlength="4" value="<?= @$transf[0]['ano_empenho']; ?>" />
                </div>
            </div>

            <div class="col-lg-6">
                <!-- <fieldset> -->
                    <legend><h4>Responsável pela Fiscalização / Gestor</h4></legend>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="label-obrigatorio">Nome</label>
                            <input type="text" class="form-control required" name="nomefiscalizador" maxlength="50" value="<?= @$transf[0]['nomefiscalizador']; ?>" />
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="label-obrigatorio">Email</label>
                            <input type="text" class="form-control required" name="emailfiscalizador" maxlength="60" value="<?= @$transf[0]['emailfiscalizador']; ?>" />
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Número do CPF do responsável que será designado pela concedente/repassador dentre servidores efetivos e indicado em cláusula específica do instrumento de transferência.'></i> CPF Responsável</label>
                            <input type="text" class="form-control cpf required" name="cpfresponsavelfiscalizador" value="<?= @$transf[0]['cpfresponsavelfiscalizador']; ?>" />
                        </div>
                    </div>
                    
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Designar o cargo ou função que o responsável pela fiscalização ocupa no quadro funcional da concedente/repassador.'></i> Cargo/Função</label>
                            <input type="text" class="form-control required" name="cargoresponsavelfiscalizador" maxlength="50" value="<?= @$transf[0]['cargoresponsavelfiscalizador']; ?>"/>
                        </div>
                    </div>
                <!-- </fieldset> -->
            </div>

            <div class="col-lg-6">
                <!-- <fieldset> -->
                    <legend><h4>Responsável pela Execução / Dirigente</h4></legend>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="label-obrigatorio"><i class='glyphicon glyphicon-info-sign blue' title='Nome do Responsável pela execução/dirigente'></i> Nome</label>
                            <input type="text" class="form-control required" name="nomeexecucao" maxlength="50" value="<?= @$transf[0]['nomeexecucao']; ?>" />
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="label-obrigatorio">Email</label>
                            <input type="text" class="form-control required" name="emailexecucao" maxlength="60" value="<?= @$transf[0]['emailexecucao']; ?>" />
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="label-obrigatorio">CPF</label>
                            <input type="text" class="form-control cpf required" name="cpfexecucao" value="<?= @$transf[0]['cpfexecucao']; ?>" />
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="label-obrigatorio">Cargo/Função</label>
                            <input type="text" class="form-control required" name="cargoexecucao" maxlength="50" value="<?= @$transf[0]['cargoexecucao']; ?>" />
                        </div>
                    </div>
                <!-- </fieldset> -->
            </div>
            
            <div class="col-lg-6">
                <!-- <fieldset class="col-lg-6"> -->
                    <legend><h4>Responsável pela Assinatura - Orgão Repassador</h4></legend>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="label-obrigatorio">Nome</label>
                            <input type="text" class="form-control required" name="nomerepassador" maxlength="50" value="<?= @$transf[0]['nomerepassador']; ?>" />
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="label-obrigatorio">CPF</label>
                            <input type="text" class="form-control cpf required" name="cpfrepassador" value="<?= @$transf[0]['cpfrepassador']; ?>" />
                        </div>
                    </div>
                <!-- </fieldset> -->
            </div>

            <div class="col-lg-6">
                <!-- <fieldset class="col-lg-6"> -->
                    <legend><h4>Responsável pela Assinatura - Convenente/OSC</h4></legend>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="label-obrigatorio">Nome</label>
                            <input type="text" class="form-control required" name="nomeassinaturaconvenente" maxlength="50" value="<?= @$transf[0]['nomeassinaturaconvenente']; ?>" />
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="label-obrigatorio">CPF</label>
                            <input type="text" class="form-control cpf required" name="cpfconvenente" value="<?= @$transf[0]['cpfconvenente']; ?>" />
                        </div>
                    </div>
                <!-- </fieldset> -->
            </div>

            <div class="col-lg-12">
                <?
                if(isset($_GET['transv'])){
                    echo "<button type='button' class='btn btn-success btn-alterar' codigo='".$_GET['transv']."'>Editar</button>";
                        echo "&nbsp;";
                    echo "<button type='button' class='btn btn-danger'>Cancelar</button>";
                }else{
                    echo "<button type='button' class='btn btn-success btn-inserir'>Cadastrar</button>";
                }
                ?>
            </div>
        </form>
    </div>
</div>

<? if(isset($_GET['transv'])){ ?>
<br/>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Empenho</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <form action="" role="form" id="formempenho">
                    
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="label-obrigatorio">Nota de Empenho</label>
                            <input type="text" class="form-control required" name="notaempenho" placeholder="Nota de Emp." maxlength="10" />
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label class="label-obrigatorio">Ano de Emp.</label>
                            <input type="text" class="form-control required" name="anoempenho" placeholder="Ano de Emp" maxlength="4"/>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="label-obrigatorio" title="Tipo de Participação da Transferência Voluntária">Part. Transf. Vol.</label>
                            <?php $auxiliar->tipoparticipacaotransferenciavol(); ?>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label class="label-obrigatorio" title="Ano da Transferência">Ano da Tran.</label>
                            <input type="text" class="form-control required" name="anotransferencia" placeholder="Ano da Transf." />
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label class="label-obrigatorio" title="CNPJ do concedente da Transferência Voluntária">CNPJ</label>
                            <input type="text" class="form-control cnpj required" name="cnpjconcedente" placeholder="CNPJ" />
                        </div>
                    </div>

                    <button type='button' class='btn btn-success btn-inserir-empenho' acao="inserirEmpenho" codigo='<?= @$transf[0]['ntransferenciavoluntaria']; ?>' pk="">Inserir</button>
                    
                    <button type='button' class='btn btn-danger btn-cancelar-empenho' disabled>Cancelar</button>
                </form>
            </div>
        </div>

        <div class="col-lg-12 empenho"></div>
    </div>
</div>

<br/>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Certidão (Convenente)</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <form action="" role="form" id="formcertidao">

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="label-obrigatorio">Tipo de Certidão</label>
                            <?php $auxiliar->tipocertidao(); ?>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label class="label-obrigatorio">Nº de Certidão</label>
                            <input type="text" class="form-control required" name="numero_certidao" />
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label class="label-obrigatorio">Data da Certidão</label>
                            <input type="text" class="form-control data required" name="data_certidao" />
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label class="label-obrigatorio">Data da Validade</label>
                            <input type="text" class="form-control data required" name="data_validade" />
                        </div>
                    </div>

                    <div class="col-lg-1">
                        <label for="">&nbsp;</label>
                        <button type='button' class='btn btn-success form-control btn-inserir-certidao' acao="inserirCertidao" codigo='<?= @$transf[0]['ntransferenciavoluntaria']; ?>' pk="">Inserir</button>
                    </div>

                    <div class="col-lg-1">
                        <label for="">&nbsp;</label>
                        <button type='button' class='btn btn-danger form-control btn-cancelar-certidao' disabled>Cancelar</button>
                    </div>

                </form>
            </div>
        </div>

        <div class="col-lg-12 certidao"></div>
    </div>
</div>
</div>

<script>
    $(function(){
        carregarEmpenho();
        carregarCertidao();
    })
</script>

<? } ?>