<!-- <script type="text/javascript" src="js/action/TRANSFERENCIAVOLUNTARIA/transferenciavoluntaria.js"></script> -->
<script type="text/javascript" src="js/action/TRANSFERENCIAVOLUNTARIA/transferenciavoluntaria2.js"></script>

<div class="col-lg-12">
	<h3 class="page-header">Transferência Voluntária</h3>
</div>

<div class="col-lg-12">
	<a href="?p=transferenciavoluntaria&page=transferenciavoluntaria_cadastrar" class="btn btn-primary btn-sm novo"><i class="glyphicon glyphicon-plus"></i> Nova Transferência</a>

	<table class="table table-hover table-stripped">
		<thead>
			<tr>
				<th></th>
				<th title="Número da Transferência Voluntária">Nº Transf. Vol.</th>
				<th>Objeto</th>
				<th class='text-right'>Valor do Repasse</th>
				<th class='text-right' title="Ano da Transferência">Ano da Transf.</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				$sql = "SELECT t.id, t.ntransferenciavoluntaria, t.objeto, t.valorrepasse, t.anotransferencia
						FROM transferenciavoluntaria t
						WHERE t.ano_exercicio = ".$_SESSION['usuario']['ano_exercicio']."
						ORDER BY id ASC";
													
				$stmt = $conexao->conn->prepare($sql);
				$stmt->execute();
				$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

				if(sizeof($retorno) > 0){
					foreach ($retorno as $t) {
						echo "<tr>";
							echo "<td class='text-center'>";
								echo "<a class='btn btn-primary btn-sm visualizar btn-circle' href='?p=transferenciavoluntaria&page=transferenciavoluntaria_cadastrar&transv={$t['ntransferenciavoluntaria']}'><i class='glyphicon glyphicon-search'></i></a>";
							echo "</td>";
							echo "<td>".$t['ntransferenciavoluntaria']."</td>";
							echo "<td>".$t['objeto']."</td>";
							echo "<td class='text-right'>R$ ".number_format($t['valorrepasse'], 2, ',', '.')."</td>";
							echo "<td class='text-right'>".$t['anotransferencia']."</td>";
							echo "<td class='text-center'>";
								echo "<button type='button' class='btn btn-danger btn-sm excluir-transferencia btn-circle' data-licitacao='{$t['id']}'><i class='glyphicon glyphicon-remove'></i></button>";
							echo "</td>";
						echo "</tr>";
					}
				}else{
					echo "<tr><td colspan='5'>Nenhum Resultado encontrado.</td></tr>";
				}
			?>
		</tbody>
	</table>
</div>