<?php

include_once '../../config/conn.php';
include_once '../../config/validarsessao.php';
include_once '../../config/funcoesgerais.php';

$funcoes = new Funcoes;

?>

<table class='table table-hover table-stripped'>
    <thead>
        <tr>
            <th>Tipo Certidão</th>
            <th>Nº Certidão</th>
            <th>Data Certidão</th>
            <th>Data Validade.</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php

            $sql = "SELECT c.id, t.descricao, c.numero_certidao, c.data_certidao, c.data_validade
					FROM certidaotv c
					INNER JOIN tipocertidao2 t ON t.id = c.tipocertidao_fk
					WHERE c.ntransferenciavoluntaria = '".$_GET['ntransferenciavoluntaria']."'";
                                                
            $stmt = $conexao->conn->prepare($sql);
            $stmt->execute();
            $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($retorno as $ln){
                echo "<tr>";
                    echo "<td>".$ln['descricao']."</td>";
                    echo "<td>".$ln['numero_certidao']."</td>";
                    echo "<td>".$funcoes->dateUSparaBR($ln['data_certidao'])."</td>";
                    echo "<td>".$funcoes->dateUSparaBR($ln['data_validade'])."</td>";
                    echo "<td class='text-right'>";
                        echo "<button type='button' class='btn btn-warning btn-editar-certidao' codigo='".$ln['id']."'><i class='glyphicon glyphicon-pencil'></i></button>";
                        // echo "&nbsp;";
                        // echo "<button type='button' class='btn btn-danger btn-excluir-certidao' codigo='".$ln['id']."'><i class='glyphicon glyphicon-remove'></i></button>";
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </tbody>
</table>