<?php

include_once '../../config/conn.php';
include_once '../../config/validarsessao.php';
include_once '../../config/funcoesgerais.php';

$funcoes = new Funcoes;

/*print_r($_POST);
exit();*/

switch ($_POST['acao']) {
	
	case 'inserir':
		
		foreach ($_POST as $key => $value){
			$data["transferenciavoluntaria"][$key] = $value;
		}

		$data["transferenciavoluntaria"]['datacelebracao'] 				= $funcoes->dateBRparaUSMySql($data["transferenciavoluntaria"]['datacelebracao']);
		$data["transferenciavoluntaria"]['datainiciovigencia'] 			= $funcoes->dateBRparaUSMySql($data["transferenciavoluntaria"]['datainiciovigencia']);
		$data["transferenciavoluntaria"]['datafimvigencia'] 			= $funcoes->dateBRparaUSMySql($data["transferenciavoluntaria"]['datafimvigencia']);
		$data["transferenciavoluntaria"]['datapublicacao'] 				= $funcoes->dateBRparaUSMySql($data["transferenciavoluntaria"]['datapublicacao']);

		$data["transferenciavoluntaria"]['cnpjconvenente'] 				= $funcoes->deixarSomenteNumeros($data["transferenciavoluntaria"]['cnpjconvenente']);
		$data["transferenciavoluntaria"]['cpfresponsavelfiscalizador'] 	= $funcoes->deixarSomenteNumeros($data["transferenciavoluntaria"]['cpfresponsavelfiscalizador']);
		$data["transferenciavoluntaria"]['cpfexecucao'] 				= $funcoes->deixarSomenteNumeros($data["transferenciavoluntaria"]['cpfexecucao']);
		$data["transferenciavoluntaria"]['cpfrepassador'] 				= $funcoes->deixarSomenteNumeros($data["transferenciavoluntaria"]['cpfrepassador']);
		$data["transferenciavoluntaria"]['cpfconvenente'] 				= $funcoes->deixarSomenteNumeros($data["transferenciavoluntaria"]['cpfconvenente']);

		$data["transferenciavoluntaria"]['ano_exercicio'] 				= $_SESSION['usuario']['ano_exercicio'];
		$data["transferenciavoluntaria"]['mes_empenho']   				= $data["transferenciavoluntaria"]['mes'];
		$data["transferenciavoluntaria"]['ano_empenho']   				= $data["transferenciavoluntaria"]['anoEmpenho'];

		$data["transferenciavoluntaria"]['valorrepasse'] 				= $funcoes->formatarValorParaBanco($data["transferenciavoluntaria"]['valorrepasse']);

		$data["transferenciavoluntaria"]['tipotransferencia_fk'] 		= $data["transferenciavoluntaria"]['tipotransferenciavoluntaria'];
		$data["transferenciavoluntaria"]['tipoatividadeprincipal_fk'] 	= $data["transferenciavoluntaria"]['tipoatividadeprincipal'];
		$data["transferenciavoluntaria"]['tipocontrapartida_fk'] 		= $data["transferenciavoluntaria"]['tipocontrapartida'];

		if($data["transferenciavoluntaria"]['contrapartida'] == ""){ unset($data["transferenciavoluntaria"]['contrapartida']); }

		if($data["transferenciavoluntaria"]['valorcontrapartidafinanceira'] == ""){
			unset($data["transferenciavoluntaria"]['valorcontrapartidafinanceira']);
		}else{
			$data["transferenciavoluntaria"]['valorcontrapartidafinanceira'] = $funcoes->formatarValorParaBanco($funcoes->validaValorRetornaNulo($data["transferenciavoluntaria"]['valorcontrapartidafinanceira']));
		}

		if($data["transferenciavoluntaria"]['valorcontrapartidaeconomica'] == ""){
			unset($data["transferenciavoluntaria"]['valorcontrapartidaeconomica']);
		}else{
			$data["transferenciavoluntaria"]['valorcontrapartidaeconomica'] = $funcoes->formatarValorParaBanco($funcoes->validaValorRetornaNulo($data["transferenciavoluntaria"]['valorcontrapartidaeconomica']));
		}


		unset($data["transferenciavoluntaria"]['tipotransferenciavoluntaria']);
		unset($data["transferenciavoluntaria"]['tipoatividadeprincipal']);
		unset($data["transferenciavoluntaria"]['tipocontrapartida']);
		unset($data["transferenciavoluntaria"]['mes']);
		unset($data["transferenciavoluntaria"]['anoEmpenho']);
		unset($data["transferenciavoluntaria"]['acao']);

		// print_r($data["transferenciavoluntaria"]);
		//exit();

		$conn = $conexao->getConn();

		$sql = $conexao->sql_insert($data['transferenciavoluntaria'], "transferenciavoluntaria");
		$stmt = $conn->prepare($sql);
		foreach ($data['transferenciavoluntaria'] as $key => $value){
			// echo "'".$value."',";
			$stmt->bindValue(":$key", $value, $conexao->getFieldType($key, "transferenciavoluntaria"));
		}

		if($stmt->execute()){
			echo 1;
			// echo $lastInsertId = $conn->lastInsertId();
		}else{
			echo 0;
			// print_r($conn->errorInfo());
		}

	break;

	case 'editar':
		
		foreach ($_POST as $key => $value){
			$data["transferenciavoluntaria"][$key] = $value;
		}

		$data["transferenciavoluntaria"]['datacelebracao'] 				= $funcoes->dateBRparaUSMySql($data["transferenciavoluntaria"]['datacelebracao']);
		$data["transferenciavoluntaria"]['datainiciovigencia'] 			= $funcoes->dateBRparaUSMySql($data["transferenciavoluntaria"]['datainiciovigencia']);
		$data["transferenciavoluntaria"]['datafimvigencia'] 			= $funcoes->dateBRparaUSMySql($data["transferenciavoluntaria"]['datafimvigencia']);
		$data["transferenciavoluntaria"]['datapublicacao'] 				= $funcoes->dateBRparaUSMySql($data["transferenciavoluntaria"]['datapublicacao']);

		$data["transferenciavoluntaria"]['cnpjconvenente'] 				= $funcoes->deixarSomenteNumeros($data["transferenciavoluntaria"]['cnpjconvenente']);
		$data["transferenciavoluntaria"]['cpfresponsavelfiscalizador'] 	= $funcoes->deixarSomenteNumeros($data["transferenciavoluntaria"]['cpfresponsavelfiscalizador']);
		$data["transferenciavoluntaria"]['cpfexecucao'] 				= $funcoes->deixarSomenteNumeros($data["transferenciavoluntaria"]['cpfexecucao']);
		$data["transferenciavoluntaria"]['cpfrepassador'] 				= $funcoes->deixarSomenteNumeros($data["transferenciavoluntaria"]['cpfrepassador']);
		$data["transferenciavoluntaria"]['cpfconvenente'] 				= $funcoes->deixarSomenteNumeros($data["transferenciavoluntaria"]['cpfconvenente']);

		$data["transferenciavoluntaria"]['ano_exercicio'] 				= $_SESSION['usuario']['ano_exercicio'];
		$data["transferenciavoluntaria"]['mes_empenho']   				= $data["transferenciavoluntaria"]['mes'];
		$data["transferenciavoluntaria"]['ano_empenho']   				= $data["transferenciavoluntaria"]['anoEmpenho'];

		$data["transferenciavoluntaria"]['valorrepasse'] 				= $funcoes->formatarValorParaBanco($data["transferenciavoluntaria"]['valorrepasse']);

		$data["transferenciavoluntaria"]['tipotransferencia_fk'] 		= $data["transferenciavoluntaria"]['tipotransferenciavoluntaria'];
		$data["transferenciavoluntaria"]['tipoatividadeprincipal_fk'] 	= $data["transferenciavoluntaria"]['tipoatividadeprincipal'];
		$data["transferenciavoluntaria"]['tipocontrapartida_fk'] 		= $data["transferenciavoluntaria"]['tipocontrapartida'];

		if($data["transferenciavoluntaria"]['contrapartida'] == ""){ unset($data["transferenciavoluntaria"]['contrapartida']); }

		if($data["transferenciavoluntaria"]['valorcontrapartidafinanceira'] == ""){
			unset($data["transferenciavoluntaria"]['valorcontrapartidafinanceira']);
		}else{
			$data["transferenciavoluntaria"]['valorcontrapartidafinanceira'] = $funcoes->formatarValorParaBanco($funcoes->validaValorRetornaNulo($data["transferenciavoluntaria"]['valorcontrapartidafinanceira']));
		}

		if($data["transferenciavoluntaria"]['valorcontrapartidaeconomica'] == ""){
			unset($data["transferenciavoluntaria"]['valorcontrapartidaeconomica']);
		}else{
			$data["transferenciavoluntaria"]['valorcontrapartidaeconomica'] = $funcoes->formatarValorParaBanco($funcoes->validaValorRetornaNulo($data["transferenciavoluntaria"]['valorcontrapartidaeconomica']));
		}


		$codigo = $data["transferenciavoluntaria"]['codigo'];

		unset($data["transferenciavoluntaria"]['tipotransferenciavoluntaria']);
		unset($data["transferenciavoluntaria"]['tipoatividadeprincipal']);
		unset($data["transferenciavoluntaria"]['tipocontrapartida']);
		unset($data["transferenciavoluntaria"]['mes']);
		unset($data["transferenciavoluntaria"]['anoEmpenho']);
		unset($data["transferenciavoluntaria"]['acao']);
		unset($data["transferenciavoluntaria"]['codigo']);

		$conn = $conexao->getConn();

		$sql = $conexao->sql_update($data['transferenciavoluntaria'], "transferenciavoluntaria", "ntransferenciavoluntaria = '$codigo'");
		$stmt = $conn->prepare($sql);
		foreach ($data['transferenciavoluntaria'] as $key => $value){
			// echo "'".$value."',";
			$stmt->bindValue(":$key", $value, $conexao->getFieldType($key, "transferenciavoluntaria"));
		}

		if($stmt->execute()){
			echo $codigo;
		}else{
			echo 0;
			// print_r($conn->errorInfo());
		}

	break;

	case 'inserirEmpenho':
		
		$_POST['ntransferenciavoluntaria'] 			= $_POST['codigo'];
		$_POST['cnpjconcedente'] 					= $funcoes->deixarSomenteNumeros($_POST['cnpjconcedente']);
		$_POST['tipoparticipacaotransferencia_pk'] 	= $_POST['tipoparticipacaotransferenciavol'];

		unset($_POST['codigo']);
		unset($_POST['acao']);
		unset($_POST['pk']);
		unset($_POST['tipoparticipacaotransferenciavol']);

		foreach ($_POST as $key => $value){
			$data["transferenciavoluntariaempenho"][$key] = $value;
		}

		// print_r($data);

		$conn = $conexao->getConn();

		$sql = $conexao->sql_insert($data['transferenciavoluntariaempenho'], "transferenciavoluntariaempenho");
		$stmt = $conn->prepare($sql);
		foreach ($data['transferenciavoluntariaempenho'] as $key => $value){
			// echo "'".$value."',";
			$stmt->bindValue(":$key", $value, $conexao->getFieldType($key, "transferenciavoluntariaempenho"));
		}

		if($stmt->execute()){
			echo 1;
		}else{
			echo 0;
			// print_r($conn->errorInfo());
		}
	break;

	case 'editarEmpenho':
		// print_r($_POST);

		$_POST['ntransferenciavoluntaria'] 			= $_POST['codigo'];
		$_POST['cnpjconcedente'] 					= $funcoes->deixarSomenteNumeros($_POST['cnpjconcedente']);
		$_POST['tipoparticipacaotransferencia_pk'] 	= $_POST['tipoparticipacaotransferenciavol'];

		$codigo = $_POST['pk'];

		unset($_POST['codigo']);
		unset($_POST['acao']);
		unset($_POST['pk']);
		unset($_POST['tipoparticipacaotransferenciavol']);

		foreach ($_POST as $key => $value){
			$data["transferenciavoluntariaempenho"][$key] = $value;
		}

		$conn = $conexao->getConn();

		$sql = $conexao->sql_update($data['transferenciavoluntariaempenho'], "transferenciavoluntariaempenho", "id = ".$codigo);
		$stmt = $conn->prepare($sql);
		foreach ($data['transferenciavoluntariaempenho'] as $key => $value){
			$stmt->bindValue(":$key", $value, $conexao->getFieldType($key, "transferenciavoluntariaempenho"));
		}

		if($stmt->execute()){
			echo 1;
		}else{
			echo 0;
			// print_r($conn->errorInfo());
		}

	break;

	case 'buscarEmpenho':
		$conn = $conexao->getConn();

		$sql = "SELECT * FROM transferenciavoluntariaempenho WHERE id = ".$_POST['codigo'];
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo json_encode($retorno);
	break;

	case 'excluirEmpenho':
		$conn = $conexao->getConn();

		$sql = "DELETE FROM transferenciavoluntariaempenho WHERE id = ".$_POST['codigo'];
		$stmt = $conn->prepare($sql);
		$stmt->execute();
	break;

	case 'excluirTransferencia':
		$conn = $conexao->getConn();

		$sql = "DELETE FROM transferenciavoluntaria WHERE id = ".$_POST['codigo'];
		$stmt = $conn->prepare($sql);
		$stmt->execute();
	break;

	case 'buscarConvenenteTv':
		$conn = $conexao->getConn();

		$sql = "SELECT removerAcento(nome_fantasia) as nome_fantasia, removerAcento(razao_social) as razao_social FROM fornecedor WHERE cpfcnpj = ".$funcoes->deixarSomenteNumeros($_POST['cnpj']);
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		echo json_encode($retorno);
	break;

	case 'inserirConvenenteTv':

		unset($_POST['acao']);

		$data['convenentetv']['ntransferenciavoluntaria'] = $_POST['ntransf'];
		$data['convenentetv']['data_inicio'] = $funcoes->dateBRparaUSMySql($_POST['data']);
		$data['convenentetv']['cnae'] = $_POST['cnae'];
		$data['convenentetv']['esfera'] = $_POST['esfera'];

		$conn = $conexao->getConn();

		$sql = $conexao->sql_insert($data['convenentetv'], "convenentetv");
		$stmt = $conn->prepare($sql);
		foreach ($data['convenentetv'] as $key => $value){
			$stmt->bindValue(":$key", $value, $conexao->getFieldType($key, "convenentetv"));
		}
		$stmt->execute();

	break;

	case 'editarConvenenteTv':

		unset($_POST['acao']);

		$data['convenentetv']['ntransferenciavoluntaria'] = $_POST['ntransf'];
		$data['convenentetv']['data_inicio'] = $funcoes->dateBRparaUSMySql($_POST['data']);
		$data['convenentetv']['cnae'] = $_POST['cnae'];
		$data['convenentetv']['esfera'] = $_POST['esfera'];

		$conn = $conexao->getConn();

		$sql = $conexao->sql_update($data['convenentetv'], "convenentetv", "ntransferenciavoluntaria = ".$_POST['codigo']);
		$stmt = $conn->prepare($sql);
		foreach ($data['convenentetv'] as $key => $value){
			$stmt->bindValue(":$key", $value, $conexao->getFieldType($key, "convenentetv"));
		}

		$stmt->execute();

	break;

	case 'buscarCertidao':
		$conn = $conexao->getConn();

		$sql = "SELECT * FROM certidaotv WHERE id = ".$_POST['codigo'];
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$retorno[0]['data_certidao'] = $funcoes->dateUSparaBR($retorno[0]['data_certidao']);
		$retorno[0]['data_validade'] = $funcoes->dateUSparaBR($retorno[0]['data_validade']);

		echo json_encode($retorno);
	break;

	case 'inserirCertidao':

		$_POST['tipocertidao_fk'] 			= $_POST['tipocertidao'];
		$_POST['ntransferenciavoluntaria'] 	= $_POST['codigo'];
		$_POST['data_certidao'] 			= $funcoes->dateBRparaUSMySql($_POST['data_certidao']);
		$_POST['data_validade'] 			= $funcoes->dateBRparaUSMySql($_POST['data_validade']);

		unset($_POST['codigo']);
		unset($_POST['acao']);
		unset($_POST['pk']);
		unset($_POST['tipocertidao']);

		foreach ($_POST as $key => $value){
			$data["certidaotv"][$key] = $value;
		}

		$conn = $conexao->getConn();

		$sql = $conexao->sql_insert($data['certidaotv'], "certidaotv");
		$stmt = $conn->prepare($sql);
		foreach ($data['certidaotv'] as $key => $value){
			// echo "'".$value."',";
			$stmt->bindValue(":$key", $value, $conexao->getFieldType($key, "certidaotv"));
		}

		if($stmt->execute()){
			echo 1;
		}else{
			echo 0;
			// print_r($conn->errorInfo());
		}

	break;

	case 'editarCertidao':

		$_POST['tipocertidao_fk'] 			= $_POST['tipocertidao'];
		$_POST['ntransferenciavoluntaria'] 	= $_POST['codigo'];
		$_POST['data_certidao'] 			= $funcoes->dateBRparaUSMySql($_POST['data_certidao']);
		$_POST['data_validade'] 			= $funcoes->dateBRparaUSMySql($_POST['data_validade']);

		$pk = $_POST['pk'];

		unset($_POST['codigo']);
		unset($_POST['acao']);
		unset($_POST['pk']);
		unset($_POST['tipocertidao']);

		foreach ($_POST as $key => $value){
			$data["certidaotv"][$key] = $value;
		}

		$conn = $conexao->getConn();

		$sql = $conexao->sql_update($data['certidaotv'], "certidaotv", "id = '$pk'");
		$stmt = $conn->prepare($sql);
		foreach ($data['certidaotv'] as $key => $value){
			$stmt->bindValue(":$key", $value, $conexao->getFieldType($key, "certidaotv"));
		}

		if($stmt->execute()){
			echo 1;
		}else{
			echo 0;
			// print_r($conn->errorInfo());
		}

	break;
	
}





?>