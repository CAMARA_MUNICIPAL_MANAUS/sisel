<?php

include_once '../../config/conn.php';
include_once '../../config/validarsessao.php';
include_once '../../config/funcoesgerais.php';

$funcoes = new Funcoes;

?>

<table class='table table-hover table-stripped'>
    <thead>
        <tr>
            <th>Nota</th>
            <th>Ano Empenho</th>
            <th>Uni. Orc.</th>
            <th>Part. Transf.</th>
            <th>Transf. Vol.</th>
            <th>Ano Transf.</th>
            <th>CNPJ</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php

            $sql = "SELECT t.id, t.notaempenho, t.anoempenho,
                             (SELECT e.unidade_orcamentaria FROM empresa e) as unidorc,
                             tpt.descricao as descricaoo, ttv.descricao,
                             t.anotransferencia, t.cnpjconcedente
                    FROM transferenciavoluntariaempenho t
                    INNER JOIN tipoparticipacaotransferenciavol tpt ON tpt.id = t.tipoparticipacaotransferencia_pk
                    INNER JOIN transferenciavoluntaria tv           ON tv.ntransferenciavoluntaria = t.ntransferenciavoluntaria
                    INNER JOIN tipotransferenciavoluntaria ttv      ON ttv.id = tv.tipotransferencia_fk
                    WHERE t.ntransferenciavoluntaria = '".$_GET['ntransferenciavoluntaria']."'";
                                                
            $stmt = $conexao->conn->prepare($sql);
            $stmt->execute();
            $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);


            foreach ($retorno as $ln){
                echo "<tr>";
                    echo "<td>".$ln['notaempenho']."</td>";
                    echo "<td>".$ln['anoempenho']."</td>";
                    echo "<td>".$ln['unidorc']."</td>";
                    echo "<td>".$ln['descricaoo']."</td>";
                    echo "<td>".$ln['descricao']."</td>";
                    echo "<td>".$ln['anotransferencia']."</td>";
                    echo "<td>".$ln['cnpjconcedente']."</td>";
                    echo "<td class='text-right'>";
                        echo "<button type='button' class='btn btn-warning btn-editar-empenho' codigo='".$ln['id']."'><i class='glyphicon glyphicon-pencil'></i></button>";
                        echo "&nbsp;";
                        echo "<button type='button' class='btn btn-danger btn-excluir-empenho' codigo='".$ln['id']."'><i class='glyphicon glyphicon-remove'></i></button>";
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </tbody>
</table>