<script src="js/action/adesao/adesao.js"></script>

<div class="col-lg-12">
	<h3 class="page-header">Adesão de Item de Adesão</h3>
</div>

<div class="col-lg-12">
	<a href="?p=adesao&page=adesao_cadastrar" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-plus"></i> Nova Adesão</a>
	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th>Nº Processo de Compras</th>
				<th>Nº Ata</th>
				<th>Nº Proc. Licitatório</th>
				<th>Nº DOE</th>
				<th>Nº Ofício</th>
				<th>Adesão a Ata</th>
				<th>Fornecedor</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?
                $sql = "SELECT a.numprocessocompra, a.numata, a.numprocesso_licitatorio, a.noficio, a.numdoe, a.dt_adesao, f.razao_social
						FROM adesao a
						INNER JOIN fornecedor f ON f.id = a.fornecedor_fk
						WHERE a.ano_exercicio = '".$_SESSION['usuario']['ano_exercicio']."'
						ORDER BY a.numprocessocompra ASC";
                $stmt = $conexao->conn->prepare($sql);
                $stmt->execute();
                $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

                $i = 1;
                foreach ($retorno as $ln) {
                    echo "<tr>";
                        echo "<td>".$ln['numprocessocompra']."</td>";
                        echo "<td>".$ln['numata']."</td>";
                        echo "<td>".$ln['numprocesso_licitatorio']."</td>";
                        echo "<td>".$ln['numdoe']."</td>";
                        echo "<td>".$ln['noficio']."</td>";
                        echo "<td>".$funcoes->dateUSparaBR($ln['dt_adesao'],'')."</td>";
                        echo "<td>".$ln['razao_social']."</td>";
                    	echo "<td class='text-center'>";
                            echo "<a title='Editar' class='btn btn-warning btn-sm btn-circle' href='?p=adesao&page=adesao_cadastrar&processocompras=".$ln['numprocessocompra']."'><i class='fa fa-pencil'></i></a>";
                        echo "</td>";
                        echo "<td class='text-right'>";
                            echo "<button type='button' title='Remover' class='btn btn-danger btn-circle btn-sm btn-excluir' data-codigo='{$ln['numprocessocompra']}'><i class='glyphicon glyphicon-remove'></i></button>";
                        echo "</td>";
                    echo "</tr>";
                }
            ?>
		</tbody>
	</table>
</div>