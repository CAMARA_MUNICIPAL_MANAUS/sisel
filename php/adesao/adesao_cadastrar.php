<script src="js/action/adesao/adesao.js"></script>

<?
if(isset($_GET['processocompras'])){
	$adesaoAta = $funcoes->buscardados_campo("adesao", "numprocessocompra",$_GET['processocompras']);
	$adesaoAta = $adesaoAta[0];
}
?>

<div class="col-lg-12">
	<h3 class="page-header">Adesão</h3>
</div>

<div class="col-lg-12">
	<form role="form" class="formAdesao">
		<div class="col-lg-5">
			<div class="form-group">
				<label for="">Órgão Gerenciador do Registro de Preço</label>
				<input type="text" class="form-control" name="ger-orgao" id="ger-orgao" maxlength="200" value="<?= @$adesaoAta['orgao'] ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">CNPJ</label>
				<input type="text" class="form-control cnpj" name="ger-cnpj" id="ger-cnpj" maxlength="18"  value="<?= @$adesaoAta['cnpj'] ?>"/>
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Esfera</label>
				<select name="ger-esfera" id="ger-esfera" class="form-control">
					<option value="1" <? if(@$adesaoAta['esfera'] == 1){echo "selected"; } ?>>Federal</option>
					<option value="2" <? if(@$adesaoAta['esfera'] == 2){echo "selected"; } ?>>Estadual</option>
					<option value="3" <? if(@$adesaoAta['esfera'] == 3){echo "selected"; } ?>>Municipal</option>
				</select>
			</div>
		</div>

		<div class="col-lg-3">
			<div class="form-group">
				<label for="">UF</label>
				<select name="ger-uf" id="ger-uf" class="form-control">
					<option value="AC" <? if( @$adesaoAta['uf'] == "AC" ) { echo "selected"; } ?> >Acre - AC</option>
					<option value="AL" <? if( @$adesaoAta['uf'] == "AL" ) { echo "selected"; } ?> >Alagoas - AL</option>
					<option value="AM" <? if( @$adesaoAta['uf'] == "AM" ) { echo "selected"; } ?> >Amazonas - AM</option>
					<option value="AP" <? if( @$adesaoAta['uf'] == "AP" ) { echo "selected"; } ?> >Amapá - AP</option>
					<option value="BA" <? if( @$adesaoAta['uf'] == "BA" ) { echo "selected"; } ?> >Bahia - BA</option>
					<option value="CE" <? if( @$adesaoAta['uf'] == "CE" ) { echo "selected"; } ?> >Ceará - CE</option>
					<option value="DF" <? if( @$adesaoAta['uf'] == "DF" ) { echo "selected"; } ?> >Distrito Federal - DF</option>
					<option value="ES" <? if( @$adesaoAta['uf'] == "ES" ) { echo "selected"; } ?> >Espírito Santo - ES</option>
					<option value="GO" <? if( @$adesaoAta['uf'] == "GO" ) { echo "selected"; } ?> >Goiás - GO</option>
					<option value="MA" <? if( @$adesaoAta['uf'] == "MA" ) { echo "selected"; } ?> >Maranhão - MA</option>
					<option value="MG" <? if( @$adesaoAta['uf'] == "MG" ) { echo "selected"; } ?> >Minas Gerais - MG</option>
					<option value="MS" <? if( @$adesaoAta['uf'] == "MS" ) { echo "selected"; } ?> >Mato Grosso do Sul - MS</option>
					<option value="MT" <? if( @$adesaoAta['uf'] == "MT" ) { echo "selected"; } ?> >Mato Grosso - MT</option>
					<option value="PA" <? if( @$adesaoAta['uf'] == "PA" ) { echo "selected"; } ?> >Pará - PA</option>
					<option value="PB" <? if( @$adesaoAta['uf'] == "PB" ) { echo "selected"; } ?> >Paraíba - PB</option>
					<option value="PE" <? if( @$adesaoAta['uf'] == "PE" ) { echo "selected"; } ?> >Pernambuco - PE</option>
					<option value="PI" <? if( @$adesaoAta['uf'] == "PI" ) { echo "selected"; } ?> >Piauí - PI</option>
					<option value="PR" <? if( @$adesaoAta['uf'] == "PR" ) { echo "selected"; } ?> >Paraná - PR</option>
					<option value="RJ" <? if( @$adesaoAta['uf'] == "RJ" ) { echo "selected"; } ?> >Rio de Janeiro - RJ</option>
					<option value="RN" <? if( @$adesaoAta['uf'] == "RN" ) { echo "selected"; } ?> >Rio Grande do Norte - RN</option>
					<option value="RO" <? if( @$adesaoAta['uf'] == "RO" ) { echo "selected"; } ?> >Rondônia - RO</option>
					<option value="RR" <? if( @$adesaoAta['uf'] == "RR" ) { echo "selected"; } ?> >Roraima - RR</option>
					<option value="RS" <? if( @$adesaoAta['uf'] == "RS" ) { echo "selected"; } ?> >Rio Grande do Sul - RS</option>
					<option value="SC" <? if( @$adesaoAta['uf'] == "SC" ) { echo "selected"; } ?> >Santa Catarina - SC</option>
					<option value="SE" <? if( @$adesaoAta['uf'] == "SE" ) { echo "selected"; } ?> >Sergipe - SE</option>
					<option value="SP" <? if( @$adesaoAta['uf'] == "SP" ) { echo "selected"; } ?> >São Paulo - SP</option>
					<option value="TO" <? if( @$adesaoAta['uf'] == "TO" ) { echo "selected"; } ?> >Tocantins - TO</option>
				</select>
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Tipo de Adesão</label>
				<? $auxiliar->tipoata(@$adesaoAta['tipoadesao']); ?>
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<!-- <label for="">Nº Proceso de Compras</label>-->
				<label for="">Nº Ata</label>
				<input type="text" class="form-control processo-compra required" name="processo-compra" maxlength="18" value="<?= @$adesaoAta['numprocessocompra']; ?>"/>
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<!-- <label for="">Nº Ata</label> -->
				<label for="">Nº Proceso de Compras</label>
				<input type="text" class="form-control num-ata required" name="num-ata" maxlength="18" value="<?= @$adesaoAta['numata']; ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Nº Empenho 01</label>
				<input type="text" class="form-control nempenho1" name="nempenho1" maxlength="10" value="<?= @$adesaoAta['numempenho']; ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Nº Empenho 02</label>
				<input type="text" class="form-control nempenho2" name="nempenho2" maxlength="10" value="<?= @$adesaoAta['numempenho2']; ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Nº Empenho 03</label>
				<input type="text" class="form-control nempenho3" name="nempenho3" maxlength="10" value="<?= @$adesaoAta['numempenho3']; ?>" />
			</div>
		</div>

		<div class="col-lg-4">
			<div class="form-group">
				<label for="">Fornecedor</label>
				<? $auxiliar->fornecedor(@$adesaoAta['fornecedor_fk']); ?>
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Processo Licitatório</label>
				<input type="text" class="form-control processo-licitatorio requried" name="processo-licitatorio" maxlength="18" value="<?= @$adesaoAta['numprocesso_licitatorio']; ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Nº Empenho 04</label>
				<input type="text" class="form-control nempenho4" name="nempenho4" maxlength="10" value="<?= @$adesaoAta['numempenho4']; ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Nº Empenho 05</label>
				<input type="text" class="form-control nempenho5" name="nempenho5" maxlength="10" value="<?= @$adesaoAta['numempenho5']; ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Nº Empenho 06</label>
				<input type="text" class="form-control nempenho6" name="nempenho6" maxlength="10" value="<?= @$adesaoAta['numempenho6']; ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Nº DOE</label>
				<input type="text" class="form-control ndoe" name="ndoe" maxlength="6" value="<?= @$adesaoAta['numdoe']; ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Data Publicação DOE</label>
				<input type="text" class="form-control publicacaoDoe data" name="publicacaoDoe" maxlength="10" value="<?= @$funcoes->dateUSparaBR($adesaoAta['dt_pulblicacaodoe']); ?>"/>
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Data da Validade da Ata</label>
				<input type="text" class="form-control validadeAta data" name="validadeAta" maxlength="10" value="<?= @$funcoes->dateUSparaBR($adesaoAta['dt_validade']); ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Nº Empenho 07</label>
				<input type="text" class="form-control nempenho7" name="nempenho7" maxlength="10" value="<?= @$adesaoAta['numempenho7']; ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Nº Empenho 08</label>
				<input type="text" class="form-control nempenho8" name="nempenho8" maxlength="10" value="<?= @$adesaoAta['numempenho8']; ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Nº Empenho 09</label>
				<input type="text" class="form-control nempenho9" name="nempenho9" maxlength="10" value="<?= @$adesaoAta['numempenho9']; ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Data de Adesão</label>
				<input type="text" class="form-control dtadesao data" name="dtadesao" maxlength="10" value="<?= @$funcoes->dateUSparaBR($adesaoAta['dt_adesao']); ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Nº de Ofício</label>
				<input type="text" class="form-control noficio" name="noficio" maxlength="10" value="<?= @$adesaoAta['noficio']; ?>" />
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Mês Competência</label>
				<? $auxiliar->mes(@$adesaoAta['mes_empenho']); ?>
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">Ano Competência</label>
				<? $auxiliar->anoexercicio(@$adesaoAta['ano_empenho']); ?>
			</div>
		</div>

		<input type="hidden" class="form-control" id="usuario_id" name="usuario_id" value="<?= $_SESSION['usuario']['id'] ?>" />
		<input type="hidden" class="form-control" id="usuario_ano_exercicio" name="usuario_ano_exercicio" value="<?= $_SESSION['usuario']['ano_exercicio'] ?>" />

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">&nbsp;</label>
				<? if(isset($_GET['processocompras'])){ ?>
				<button type="button" class="btn btn-success btn-editar form-control" data-adesao="<?= $_GET['processocompras'] ?>">Editar</button>
				<? }else{ ?>
				<button type="button" class="btn btn-success btn-inserir form-control">Inserir</button>
				<? } ?>
			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<label for="">&nbsp;</label>
				<a href="?p=adesao" class="btn btn-danger form-control">Voltar</a>
			</div>
		</div>

	</form>
</div>

<? if(isset($_GET['processocompras'])){ ?>

<!-- #################################################################################################### -->
<!-- #####################      ############# -->
<!-- #################################################################################################### -->
<div class="ccol-lg-12 col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Item de Adesão / <?= $_GET['processocompras']; ?></h3>
		</div>
		<div class="panel-body">
			<form role="form">
				<div class="col-lg-1">
					<div class="form-group">
						<label for="">Quantidade</label>
						<input type="text" name="qtd" class="form-control qtd sonums" maxlength="5" />
					</div>
				</div>

				<div class="col-lg-2">
					<div class="form-group">
						<label for="">Valor Unitário</label>
						<input type="text" name="vlunit" class="form-control vlunit dinheiro" />
					</div>
				</div>

				<div class="col-lg-7">
					<div class="form-group">
						<label for="">Produto</label>
						<?=  $auxiliar->produto(); ?>
					</div>
				</div>

				<div class="col-lg-1">
					<div class="form-group">
						<label for="">&nbsp;</label>
						<button type="button" class="btn btn-success btn-inserir-item form-control">Inserir</button>
						<button type="button" class="btn btn-success btn-editar-item form-control invisivel">Editar</button>
						<input type="hidden" id="idprocessocompras" name="idprocessocompras" value="<?= @$adesaoAta['id']; ?>"/>
						<input type="hidden" id="processocompras" name="processocompras" value="<?= @$_GET['processocompras']; ?>"/>
					</div>
				</div>

				<div class="col-lg-1">
					<div class="form-group">
						<label for="">&nbsp;</label>
						<button type="button" class="btn btn-danger btn-cancelar-item form-control">Cancelar</button>
					</div>
				</div>

			</form>


			<div class="col-lg-12">
				<table class="table table-hover tabler-stripped">
					<thead>
						<tr>
							<th>Nº</th>
							<th>Produto</th>
							<th>Qtd</th>
							<th class='text-right'>Valor Unit</th>
							<th class='text-right'>Valor Total</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?
							$sql = "SELECT i.id ,(@cont := @cont + 1) as seq, a.numprocessocompra, i.adesao_fk, i.quantidade, i.valor_unitario, p.descricao, (i.valor_unitario * i.quantidade) as valor_total
									FROM (SELECT @cont:= 0) cont, itemadesaoata i
									INNER JOIN produto p ON p.id = i.produto_fk
									INNER JOIN adesao a  ON a.id = i.adesao_fk
									WHERE a.numprocessocompra = '".$_GET['processocompras']."'
									ORDEr BY seq;";

							$stmt = $conexao->conn->prepare($sql);
		                    $stmt->execute();
		                    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		                    foreach ($retorno as $ln){
		                    	echo "<tr>";
		                    		echo "<td>".$ln['seq']."</td>";
		                    		echo "<td>".$ln['descricao']."</td>";
		                    		echo "<td>".$ln['quantidade']."</td>";
		                    		echo "<td class='text-right'>R$ ".number_format($ln['valor_unitario'], 2, ',', '.')."</td>";
		                    		echo "<td class='text-right'>R$ ".number_format(($ln['valor_total']), 2, ',', '.')."</td>";
		                    		echo "<td class='text-right'>";
		                    			echo "<button class='btn btn-primary btn-alterar-item' title='Editar'><i class='glyphicon glyphicon-pencil'></i></button>";
		                    				echo "&nbsp;";
		                    			echo "<button class='btn btn-danger btn-excluir-item' title='Excluir' data-id='".$ln['id']."'><i class='glyphicon glyphicon-remove'></i></button>";
		                    		echo "</td>";
		                    	echo "</tr>";
		                    }
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<? } ?>