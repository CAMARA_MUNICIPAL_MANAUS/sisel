<?php

if($_POST['acao']){

	include_once '../../config/conn.php';
	include_once '../../config/funcoesgerais.php';

	$funcoes  = new Funcoes;

	switch ($_POST['acao']){

		case 'inserir':

			$tipoata 				= $_POST['tipoata'];
			$processo_compra 		= $_POST['processo-compra'];
			$num_ata 				= $_POST['num-ata'];
	    	$contratado 			= $_POST['contratado'];
	    	$processo_licitatorio 	= $_POST['processo-licitatorio'];
	    	$ndoe 					= $_POST['ndoe'];
	    	$publicacaoDoe 			= $funcoes->dateBRparaUS($_POST['publicacaoDoe']);
	    	$validadeAta 			= $funcoes->dateBRparaUS($_POST['validadeAta']);
			$nempenho1 				= $_POST['nempenho1'];
	    	$nempenho2 				= $_POST['nempenho2'];
	    	$nempenho3 				= $_POST['nempenho3'];
	    	$nempenho4 				= $_POST['nempenho4'];
	    	$nempenho5 				= $_POST['nempenho5'];
	    	$nempenho6 				= $_POST['nempenho6'];
	    	$nempenho7 				= $_POST['nempenho7'];
	    	$nempenho8 				= $_POST['nempenho8'];
	    	$nempenho9 				= $_POST['nempenho9'];
	    	$dtadesao 				= $funcoes->dateBRparaUS($_POST['dtadesao']);
	    	$noficio 				= $_POST['noficio'];
	    	$mes 					= $_POST['mes'];
	    	$ano 					= $_POST['anoexercicio'];
	    	$usuario 				= $_POST['usuario_id'];
	    	$anoExercicio 			= $_POST['usuario_ano_exercicio'];
	    	$orgao 					= $_POST["ger-orgao"];
	    	$cnpj 					= $_POST["ger-cnpj"];
	    	$esfera 				= $_POST["ger-esfera"];
	    	$uf 					= $_POST["ger-uf"];

			$sql = "INSERT INTO `adesao` (`tipoadesao`, `numprocessocompra`, `numata`, `numprocesso_licitatorio`, `numdoe`, `dt_pulblicacaodoe`, `dt_validade`, `dt_adesao`, `noficio`, `fornecedor_fk`, `numempenho`, `numempenho2`, `numempenho3`, `numempenho4`, `numempenho5`, `numempenho6`, `numempenho7`, `numempenho8`, `numempenho9`, `mes_empenho`, `ano_empenho`, `usuario`, `ano_exercicio`, `orgao`, `cnpj`, `esfera`, `uf`)

				VALUES (:tipoata, :processo_compra, :num_ata, :processo_licitatorio, :ndoe, :publicacaoDoe, :validadeAta, :dtadesao, :noficio, :contratado, :nempenho1, :nempenho2, :nempenho3, :nempenho4, :nempenho5, :nempenho6, :nempenho7, :nempenho8, :nempenho9, :mes, :ano, :usuario, :anoExercicio, :orgao, :cnpj, :esfera, :uf)";

	    	$stmt = $conexao->conn->prepare($sql);

	    	$stmt->bindValue(':tipoata',$tipoata);
	    	$stmt->bindValue(':processo_compra',$processo_compra);
	    	$stmt->bindValue(':num_ata',$num_ata);
	    	$stmt->bindValue(':processo_licitatorio',$processo_licitatorio);
	    	$stmt->bindValue(':ndoe',$ndoe);
	    	$stmt->bindValue(':publicacaoDoe',$publicacaoDoe);
	    	$stmt->bindValue(':validadeAta',$validadeAta);
	    	$stmt->bindValue(':dtadesao',$dtadesao);
	    	$stmt->bindValue(':noficio',$noficio);
	    	$stmt->bindValue(':contratado',$contratado);
	    	$stmt->bindValue(':nempenho1',$nempenho1);
	    	$stmt->bindValue(':nempenho2',$nempenho2);
	    	$stmt->bindValue(':nempenho3',$nempenho3);
	    	$stmt->bindValue(':nempenho4',$nempenho4);
	    	$stmt->bindValue(':nempenho5',$nempenho5);
	    	$stmt->bindValue(':nempenho6',$nempenho6);
	    	$stmt->bindValue(':nempenho7',$nempenho7);
	    	$stmt->bindValue(':nempenho8',$nempenho8);
	    	$stmt->bindValue(':nempenho9',$nempenho9);
	    	$stmt->bindValue(':mes',$mes);
	    	$stmt->bindValue(':ano',$ano);
	    	$stmt->bindValue(':usuario',$usuario);
	    	$stmt->bindValue(':anoExercicio',$anoExercicio);
	    	$stmt->bindValue(':orgao',$orgao);
	    	$stmt->bindValue(':cnpj',$cnpj);
	    	$stmt->bindValue(':esfera',$esfera);
	    	$stmt->bindValue(':uf',$uf);

		    if($stmt->execute()){
		    	echo 1;
		    }else{
		    	print_r($stmt->errorinfo());
		    }

		break;

		case 'editar':

		/*print_r($_POST);
		exit();*/

			$tipoata 				= $_POST['tipoata'];
			$processo_compra 		= $_POST['processo-compra'];
			$num_ata 				= $_POST['num-ata'];
	    	$contratado 			= $_POST['contratado'];
	    	$processo_licitatorio 	= $_POST['processo-licitatorio'];
	    	$ndoe 					= $_POST['ndoe'];
	    	$publicacaoDoe 			= $funcoes->dateBRparaUS($_POST['publicacaoDoe']);
	    	$validadeAta 			= $funcoes->dateBRparaUS($_POST['validadeAta']);
			$nempenho1 				= $_POST['nempenho1'];
	    	$nempenho2 				= $_POST['nempenho2'];
	    	$nempenho3 				= $_POST['nempenho3'];
	    	$nempenho4 				= $_POST['nempenho4'];
	    	$nempenho5 				= $_POST['nempenho5'];
	    	$nempenho6 				= $_POST['nempenho6'];
	    	$nempenho7 				= $_POST['nempenho7'];
	    	$nempenho8 				= $_POST['nempenho8'];
	    	$nempenho9 				= $_POST['nempenho9'];
	    	$dtadesao 				= $funcoes->dateBRparaUS($_POST['dtadesao']);
	    	$noficio 				= $_POST['noficio'];
	    	$mes 					= $_POST['mes'];
	    	$ano 					= $_POST['anoexercicio'];
	    	$usuario 				= $_POST['usuario_id'];
	    	$anoExercicio 			= $_POST['usuario_ano_exercicio'];
	    	$orgao 					= $_POST["ger-orgao"];
	    	$cnpj 					= $_POST["ger-cnpj"];
	    	$esfera 				= $_POST["ger-esfera"];
	    	$uf 					= $_POST["ger-uf"];

			$sql = "UPDATE adesao
						SET tipoadesao = :tipoata,
							numprocessocompra = :processo_compra,
							numata = :num_ata,
							numprocesso_licitatorio = :processo_licitatorio,
							numdoe = :ndoe,
							dt_pulblicacaodoe = :publicacaoDoe,
							dt_validade = :validadeAta,
							dt_adesao = :dtadesao,
							noficio = :noficio,
							fornecedor_fk = :contratado,
							numempenho = :nempenho1,
							numempenho2 = :nempenho2,
							numempenho3 = :nempenho3,
							numempenho4 = :nempenho4,
							numempenho5 = :nempenho5,
							numempenho6 = :nempenho6,
							numempenho7 = :nempenho7,
							numempenho8 = :nempenho8,
							numempenho9 = :nempenho9,
							mes_empenho = :mes,
							ano_empenho = :ano,
							usuario = :usuario,
							ano_exercicio = :anoExercicio,
							orgao = :orgao,
							cnpj = :cnpj,
							esfera = :esfera,
							uf = :uf
					WHERE numprocessocompra = '".$_POST['getAdesao']."'";

	    	$stmt = $conexao->conn->prepare($sql);

	    	$stmt->bindValue(':tipoata',$tipoata);
	    	$stmt->bindValue(':processo_compra',$processo_compra);
	    	$stmt->bindValue(':num_ata',$num_ata);
	    	$stmt->bindValue(':processo_licitatorio',$processo_licitatorio);
	    	$stmt->bindValue(':ndoe',$ndoe);
	    	$stmt->bindValue(':publicacaoDoe',$publicacaoDoe);
	    	$stmt->bindValue(':validadeAta',$validadeAta);
	    	$stmt->bindValue(':dtadesao',$dtadesao);
	    	$stmt->bindValue(':noficio',$noficio);
	    	$stmt->bindValue(':contratado',$contratado);
	    	$stmt->bindValue(':nempenho1',$nempenho1);
	    	$stmt->bindValue(':nempenho2',$nempenho2);
	    	$stmt->bindValue(':nempenho3',$nempenho3);
	    	$stmt->bindValue(':nempenho4',$nempenho4);
	    	$stmt->bindValue(':nempenho5',$nempenho5);
	    	$stmt->bindValue(':nempenho6',$nempenho6);
	    	$stmt->bindValue(':nempenho7',$nempenho7);
	    	$stmt->bindValue(':nempenho8',$nempenho8);
	    	$stmt->bindValue(':nempenho9',$nempenho9);
	    	$stmt->bindValue(':mes',$mes);
	    	$stmt->bindValue(':ano',$ano);
	    	$stmt->bindValue(':usuario',$usuario);
	    	$stmt->bindValue(':anoExercicio',$anoExercicio);
	    	$stmt->bindValue(':orgao',$orgao);
	    	$stmt->bindValue(':cnpj',$cnpj);
	    	$stmt->bindValue(':esfera',$esfera);
	    	$stmt->bindValue(':uf',$uf);

		    if($stmt->execute()){
		    	echo 1;
		    }else{
		    	print_r($stmt->errorinfo());
		    }
		
		break;

		case 'excluir':

			$sql = "DELETE FROM adesao WHERE numprocessocompra = '".$_POST['nprocesso']."'";
			$stmt = $conexao->conn->prepare($sql);
			if($stmt->execute()){
		    	echo 1;
		    }else{
		    	print_r($stmt->errorinfo());
		    }

		break;

		case 'inserirItem':
			// print_r($_POST);

			$sql = "INSERT INTO itemadesaoata (adesao_fk, quantidade, valor_unitario, produto_fk)
					VALUES (:adesao_fk, :quantidade, :valor_unitario, :produto_fk)";

			$stmt = $conexao->conn->prepare($sql);

	    	$stmt->bindValue(':adesao_fk',$_POST['idprocessocompras']);
	    	$stmt->bindValue(':quantidade',$_POST['qtd']);
	    	$stmt->bindValue(':valor_unitario',$funcoes->formatarValorParaBanco($_POST['vlunit']));
	    	$stmt->bindValue(':produto_fk',$_POST['produto']);

	    	if($stmt->execute()){
		    	echo 1;
		    }else{
		    	print_r($stmt->errorinfo());
		    }

		break;

		case 'excluirItem':
		
			$delete = "DELETE FROM itemadesaoata WHERE id = ".$_POST['id'];

			$stmt = $conexao->conn->prepare($delete);

			if($stmt->execute()){
		    	echo 1;
		    }else{
		    	print_r($stmt->errorinfo());
		    }

		break;
		
	}
}


?>