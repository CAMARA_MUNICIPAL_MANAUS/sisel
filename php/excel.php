<?php
// $_POST["data"] = json_decode($_POST["data"]);
$_POST["data"] = json_decode(base64_decode($_POST["data"]));

if (PHP_SAPI == 'cli'){
	die('This example should only be run from a Web Browser');
}

include_once "../plugin/PHPExcel/Classes/PHPExcel.php";

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
/*$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
			->setLastModifiedBy("Maarten Balliauw")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");*/


//TITLE
$letra = "A";
foreach ($_POST["data"][0] as $title => $value) {
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letra."1", $title);
	$objPHPExcel->getActiveSheet()->getColumnDimension($letra)->setAutoSize(true);
	$letra++;
}
$letra--;	

// Miscellaneous glyphs, UTF-8
$linha = 2;

foreach ($_POST["data"] as $data) {
	$coluna = "A";
	foreach ($data as $d) {
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($coluna.$linha, $d);
		// echo $coluna.$linha." - ".$d;
		$coluna++;
	}
	$linha++;
}

// Rename worksheet
// $objPHPExcel->getActiveSheet()->setTitle('Simple');
$objPHPExcel->getActiveSheet()->getStyle("A1:".$letra."1")->getFont()->setBold(true);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$_POST["file"].'.xls"');
header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>