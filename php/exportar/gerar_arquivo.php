<?php
if(isset($_POST['mes']) && isset($_POST['ano']) ){

	$sql = "SELECT * FROM exportacao ORDER BY sp ASC";
	$stmt = $conexao->conn->prepare($sql);
	$stmt->execute();
	$r = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$html = "<div>";

		$html .= "<ul class='nav nav-pills' role='tablist'>";
		$active = "active";
		foreach ($r as $sp) {
			$spp = str_replace(".", "", strtolower($sp["sp"]));
			$html .= "<li class='$active'><a href='#$spp' aria-controls='$spp' role='tab' data-toggle='tab'>".strtoupper($sp["sp"])."</a></li>";
			$active="";
		}
		$html .= "</ul>";
		$html .= "<hr>";

		$html .= "<div class='tab-content'>";
			$active = "active";
			foreach ($r as $arquivo) {
				$spp = str_replace(".", "", strtolower($arquivo["sp"]));
				$html .= "<div role='tabpanel' class='tab-pane $active' id='$spp'>";
					unlink("php/exportar2/".$arquivo["sp"]);
					$fp = fopen("php/exportar2/".$arquivo["sp"], "a");

					$sql = "CALL `".$arquivo["sp"]."`('".$_POST['ano']."', '".$_POST['mes']."')";
					$stmt = $conexao->conn->prepare($sql);
					$stmt->execute();
					$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

					$table = (isset($retorno[0])) ? "table" : "";

					$html .= "<div class='col-lg-12'>";
						$html .= "<h4>".strtoupper($arquivo["sp"])."</h4>";
						$html .= "<div class='row table-responsive'>";
							$html .= "<table class='$table table-striped table-hover table-bordered' width='100%'>";
								$html .= "<thead>";
									if(isset($retorno[0])){
										$html .= "<tr>";
											$html .= "<th>#</th>";
											foreach ($retorno[0] as $head => $value) {
												$html .= "<th>$head</th>";
											}
										$html .= "</tr>";
									}else{
										$html .= "<div class='alert alert-dismissible alert-warning'>";
											$html .= "<p>Nenhum registro encontrato.</p>";
										$html .= "</div>";
									}
								$html .= "</thead>";
								$html .= "<tbody>";
									$contador = 1;
									foreach ($retorno as $k => $lista) {
										$html .= "<tr>";
											$html .= "<td>".$contador++."</td>";
											foreach ($lista as $key => $v) {
												fwrite($fp,$v);
												$html .= "<td>".$v."</td>";
											}
										fwrite($fp,"\r\n");
										$html .= "</tr>";
									}
									fclose($fp);
								$html .= "</tbody>";
							$html .= "</table>";
						$html .= "</div>";
					$html .= "</div>";
				$html .= "</div>";
				$active = "";
			}
		$html .= "</div>";
	$html .= "</div>";

	$zip = new ZipArchive;
    if ($zip->open("php/exportar2/MES.zip") === TRUE) {
		foreach ($r as $arquivo) {
        	$zip->addFile('php/exportar2/'.$arquivo["sp"],$arquivo["sp"]);
        }

        $zip->close();
        echo "<label>&nbsp;</label>";
       	echo "<a class='btn btn-primary form-control' href='php/exportar2/MES.zip' target='_blank'><i class='fa fa-download'></i> Clique aqui para baixar o arquivo - ".$_POST['mes']."/".$_POST['ano']."</a>";

    } else {
    	echo "<script>alert('Falha ao Criar os Arquivos')</script>";
    }


} // FIM IF GERAL
?>