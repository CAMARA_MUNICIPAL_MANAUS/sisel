<script>

	$(function(){
		$(".gerarArquivo").on('click', function(){
			var mes = $("#mes").val();

			if(mes == ''){
				alert('Selecione o mês!');
				return false;
			}

			$("form").submit();

		})
	})

</script>

<!-- DATATABLES -->
<link href="plugin/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet" />
<link href="plugin/datatables-responsive/dataTables.responsive.css" rel="stylesheet" />

<!-- DATATABLES -->
<script src="plugin/datatables/js/jquery.dataTables.js"></script>
<script src="plugin/datatables-plugins/dataTables.bootstrap.js"></script>
<script src="plugin/datatables-responsive/dataTables.responsive.js"></script>

<div class="col-lg-12">
	<h3 class="page-header">Exportação TCE</h3>
</div>

<div class="col-lg-6">
		<form action="" role="form" method="POST">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="">Mês</label>
					<?= $auxiliar->mes(@$_POST["mes"]); ?>
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Ano</label>
					<input type="text" id="ano" name="ano" class='form-control' value="<?= $_SESSION['usuario']['ano_exercicio']; ?>" readonly/>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="form-group">
					<label for="">&nbsp;</label>
					<button type="button" class="btn btn-success form-control gerarArquivo">Gerar Arquivo</button>
				</div>
			</div>
		</form>
</div>
<div class="col-lg-6">
	<? include_once 'gerar_arquivo.php'; ?>
</div>

<div class="col-lg-12">
	<?php
	if(isset($html)){
		echo $html;
	}
	?>
</div>

<script>
	$(function(){
		$(".table").DataTable({
	        responsive: true,
	    });
	})
</script>