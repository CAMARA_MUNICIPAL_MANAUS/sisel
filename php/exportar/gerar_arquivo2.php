<?
if(isset($_POST['mes']) && isset($_POST['ano']) ){

	$fp = fopen("php/gerarArquivo/CONTRATO.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `contrato.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {
			fwrite($fp,$ln['unidade_orcamentaria']); #    | CÓDIGO DA UNIDADE ORÇAMENTARIA
			fwrite($fp,$ln['numcontrato']); #             | NÚMERO DO CONTRATO OU ADITIVO
			fwrite($fp,$ln['valor']); #                   | VALOR DO CONTRATO OU ADITIVO
			fwrite($fp,$ln['dtcontrato']); #              | DATA DE ASINATURA DO CONTRATO OU ADITIVO
			fwrite($fp,$ln['objcontrato']); #             | DESCRIÇÃO DO OBJETO
			fwrite($fp,$ln['numlicitacao']); #            | NÚMERO DO PROCESSO
			fwrite($fp,$ln['moeda']); #                   | CÓDIGO DE MOEDA
			fwrite($fp,$ln['tipopessoa_fk']); #           | TIPO JURIDICO
			fwrite($fp,$ln['cpfcnpj']); #                 | CNPJ OU CPF
			fwrite($fp,$ln['nomecontratado']); #          | NOME DO CONTRATADO
			fwrite($fp,$ln['dtvencimento']); #            | DATA DE VENCIMENTO
			fwrite($fp,$ln['numdoe']); #                  | NÚMERO DO DIÁRIO
			fwrite($fp,$ln['dtpublicacao']); #            | DATA DE PUBLICAÇÃO
			fwrite($fp,$ln['recebevalor']); #             | RECEBE VALOR

			fwrite($fp,$ln['numcertificadoinss']);
			fwrite($fp,$ln['dtcertidaoinss']);
			fwrite($fp,$ln['dtvalidadeinss']);

			fwrite($fp,$ln['numcertidaofgts']);
			fwrite($fp,$ln['dtcertidaofgts']);
			fwrite($fp,$ln['dtvalidadefgts']);

			fwrite($fp,$ln['numcertestmuncarat']);
			fwrite($fp,$ln['dtcertestmuncarat']);
			fwrite($fp,$ln['dtvalestmuncarat']);

			fwrite($fp,$ln['numcertidaofm']);
			fwrite($fp,$ln['dtcertidaofm']);
			fwrite($fp,$ln['dtvalidadefm']);

			fwrite($fp,$ln['numcertidaoff']);
			fwrite($fp,$ln['dtcertidaoff']);
			fwrite($fp,$ln['dtvalidadeff']);

			fwrite($fp,$ln['numcertidaotst']);
			fwrite($fp,$ln['dtcertidaotst']);
			fwrite($fp,$ln['dtvalidadetst']);

			fwrite($fp,$ln['numcertidaooutros']);
			fwrite($fp,$ln['dtcertidaooutros']);
			fwrite($fp,$ln['dtvalidadeoutros']);

			fwrite($fp,$ln['tipoajustecontrato']); #      | TIPO DO CONTRATO
			fwrite($fp,$ln['contratoSuperior']); #        | NÚMERO DO CONTRATO SUPERIOR
			fwrite($fp,$ln['sequencialaditivo']); #       | SEQUENCIAL DO ADITIVO
			fwrite($fp,$ln['tipo_adtivo']); #             | TIPO DO ADITIVO
			fwrite($fp,$ln['tipo_do_adtivo']); #          | TIPO DO VALOR DO ADITIVO
			fwrite($fp,$ln['cnpjug']); #                  | CNOJ OU UG
			fwrite($fp,$ln['tipo_contratodecorrente']); # | CONTRATO DECORRENTE DE LICITAÇÃO
			fwrite($fp,$ln['tipo_natureza']); #           | NATUREZA DO OBJETO
			fwrite($fp,$ln['numeroata']); #               | NÚMERO DA ATA
			fwrite($fp,$ln['competencia']); #             | COMPETÊNCIA (AAAAMMDD)

			fwrite($fp,"\r\n");
		}
		
		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);

	} // FIM CONTRATO REM

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/ITEMADESAOATA.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `ItemAdesaoAta.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp,$ln['unidade_orcamentaria']); #| CÓD UNIDADE ORCAMENTARIA
			fwrite($fp,$ln['numProcessoCompra']);
			fwrite($fp,$ln['numAta']);
			fwrite($fp,$ln['quantidade']);
			fwrite($fp,$ln['sequencial']);
			fwrite($fp,$ln['valor']);
			fwrite($fp,$ln['unidade']);
			fwrite($fp,$ln['descricaoItem']);
			fwrite($fp,$ln['controleItemLote']);
			fwrite($fp,$ln['cpfcnpj']);

			fwrite($fp,"\r\n");
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/ADESAOATALICITACAO.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `AdesaoAtaLicitacao.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp,$ln['unidade_orcamentaria']); #| CÓD UNIDADE ORCAMENTARIA
			fwrite($fp,$ln['processoCompras']); #| PROCESSO DE COMPRA
			fwrite($fp,$ln['numata']); #| NUMERO DA ATA
			fwrite($fp,$ln['licitacao']); #| PROCESSO LICITATÓRIO
			fwrite($fp,$ln['dt_pulblicacaodoe']); #| DATA DE PUBLICAÇÃO
			fwrite($fp,$ln['dt_validade']); #| DATA DA VALIDADE DA ATA
			fwrite($fp,$ln['numdoe']); #| NÚM DO DIÁRIO OFICIL
			fwrite($fp,$ln['dt_adesao']); #| DATA DE ADESÃO
			fwrite($fp,$ln['tipoadesao']); #| TIPO DE ADESÃO
			fwrite($fp,$ln['cpfcnpj']); #| CNPJ DO ORGÃO GERENCIADOR
			fwrite($fp,$ln['esfera']); #| ESFERA DO ORGÃO
			fwrite($fp,$ln['uforgao']); #| UF DO ORGÃO
			fwrite($fp,$ln['empenho']); #| EMPENHO

			fwrite($fp,"\r\n");
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/LICITACAO.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `licitacao.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp,$ln['unidade_orcamentaria']);
			fwrite($fp,$ln['numprocesso']);
			fwrite($fp,$ln['modalidade']);
			fwrite($fp,$ln['tipodalicitacao']);
			fwrite($fp,$ln['tiponatureza']);
			fwrite($fp,$ln['tiponaturezaproc']);
			fwrite($fp,$ln['objeto']);
			fwrite($fp,$ln['tiporegime']);
			fwrite($fp,$ln['valor']);
			fwrite($fp,$ln['sigla']);
			fwrite($fp,$ln['empenho']);

			fwrite($fp,"\r\n");
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/ITEMLICITACAO.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `ItemLicitacao.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp,$ln['numprocesso']);
			fwrite($fp,$ln['numedital']);
			fwrite($fp,$ln['dtpublicacao']);
			fwrite($fp,$ln['sequencial']);
			fwrite($fp,$ln['descricao']);
			fwrite($fp,$ln['quantidade']);
			fwrite($fp,$ln['dtassinatura']);
			fwrite($fp,$ln['dtpublicacao']);
			fwrite($fp,$ln['unidade']);
			fwrite($fp,$ln['statusitem']);
			fwrite($fp,$ln['controleitemlote']);

			fwrite($fp,"\r\n");
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/COTACAO.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `cotacao.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp,$ln['tipovalor_fk']);
			fwrite($fp,$ln['numprocesso']);
			fwrite($fp,$ln['numedital']);
			fwrite($fp,$ln['dtpublicacao']);
			fwrite($fp,$ln['tipopessoa_fk']);
			fwrite($fp,$ln['cpfcnpj']);
			fwrite($fp,$ln['sequencial']);
			fwrite($fp,$ln['valor_unitario']);
			fwrite($fp,$ln['tiporesultado_fk']);
			fwrite($fp,$ln['quantidade']);
			fwrite($fp,$ln['controle']);

			fwrite($fp,"\r\n");
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 
	
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/LICITACAOHISTORICO.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `LicitacaoHistorico.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp,$ln['unidade_orcamentaria']);
			fwrite($fp,$ln['numprocesso']);
			fwrite($fp,$ln['numedital']);
			fwrite($fp,$ln['dtpublicacao']);
			fwrite($fp,$ln['numdiario']);
			fwrite($fp,$ln['dtlimite']);

			fwrite($fp,"\r\n");
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/LICITACAODISPENSA.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `LicitacaoDispensa.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp,$ln['unidade_orcamentaria']);
			fwrite($fp,$ln['numprocesso']);
			fwrite($fp,$ln['data_processo']);
			fwrite($fp,$ln['tipo_procedimento']);
			fwrite($fp,$ln['tiponatureza']);
			fwrite($fp,$ln['objeto']);
			fwrite($fp,$ln['justificativa']);
			fwrite($fp,$ln['razao']);
			fwrite($fp,$ln['data_publicacao']);
			fwrite($fp,$ln['veiculo_publicacao']);
			fwrite($fp,$ln['valor']);
			fwrite($fp,$ln['controleItemLote']);
			fwrite($fp,$ln['empenho']);

			fwrite($fp,"\r\n");
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/ITEMLICITACAODISPENSA.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `ItemLicitacaoDispensa.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp,$ln['numprocesso']);
			fwrite($fp,$ln['tipopessoa_fk']);
			fwrite($fp,$ln['cpfcnpj']);
			fwrite($fp,$ln['fornecedor']);
			fwrite($fp,$ln['descricao_item']);
			fwrite($fp,$ln['quantidade']);
			fwrite($fp,$ln['controle']);
			fwrite($fp,$ln['unidade']);
			fwrite($fp,$ln['valor_unitario']);

			fwrite($fp,"\r\n");
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/CERTIDAO.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `certidao.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp,$ln['numproceso']);
			fwrite($fp,$ln['tipo_processo']);
			fwrite($fp,$ln['cpfcnpj']);
			fwrite($fp,$ln['tipocertidao']);
			fwrite($fp,$ln['tipopessoa']);
			fwrite($fp,$ln['certidao']);
			fwrite($fp,$ln['emissao']);
			fwrite($fp,$ln['validade']);

			fwrite($fp,"\r\n");
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/CERTIDAODISPENSA.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `certidaoDispensa.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp,$ln['numproceso']);
			fwrite($fp,$ln['tipo_processo']);
			fwrite($fp,$ln['cpfcnpj']);
			fwrite($fp,$ln['tipocertidao']);
			fwrite($fp,$ln['tipopessoa']);
			fwrite($fp,$ln['certidao']);
			fwrite($fp,$ln['emissao']);
			fwrite($fp,$ln['validade']);

			fwrite($fp,"\r\n");
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/PARTICIPANTELICITACAO.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `ParticipanteLicitacao.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp,$ln['numprocesso']);
			fwrite($fp,$ln['cpfcnpj']);
			fwrite($fp,$ln['tipopessoa']);
			fwrite($fp,$ln['raza_social']);
			fwrite($fp,$ln['tipoparticipante_fk']);
			fwrite($fp,$ln['cgcconsorcio']);
			fwrite($fp,$ln['convidado']);

			fwrite($fp,"\r\n");
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/CONTRATOEMPENHO.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `ContratoEmpenho.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			$numcontrato 	= $ln['numcontrato'];
			$anoEmpenho 	= $ln['anoEmpenho'];
			$unidadeOrc 	= $ln['unidade_orcamentaria'];

			for ($i=1; $i <= 12; $i++) { 
				if(trim($ln["numempenho$i"]) != ''){

					fwrite($fp,$numcontrato);
					fwrite($fp,$ln["numempenho$i"]);
					fwrite($fp,$anoEmpenho);
					fwrite($fp,$unidadeOrc);

					fwrite($fp,"\r\n");
				}
			}
			
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/ADESAOATAEMPENHO.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `AdesaoAtaEmpenho.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			$numprocessocompra 	= $ln['numprocessocompra'];
			$numata 			= $ln['numata'];
			$ano_empenho 		= $ln['ano_empenho'];
			$unidadeOrc 		= $ln['unidade_orcamentaria'];

			for ($i=1; $i <= 9; $i++) { 
				if(trim($ln["numempenho$i"]) != ''){

					fwrite($fp,$numprocessocompra);
					fwrite($fp,$numata);
					fwrite($fp,$ln["numempenho$i"]);
					fwrite($fp,$ano_empenho);
					fwrite($fp,$unidadeOrc);

					fwrite($fp,"\r\n");
				}
			}
			
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/LICITACAOEMPENHO.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `LicitacaoEmpenho.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			$numprocesso 	= $ln['numprocesso'];
			$ano_empenho 	= $ln['ano_empenho'];
			$unidadeOrc 	= $ln['unidade_orcamentaria'];
			$tipo_processo 	= $ln['tipo_processo'];

			for ($i=1; $i <= 12; $i++) { 
				if(trim($ln["numempenho$i"]) != ''){

					fwrite($fp,$numprocesso);
					fwrite($fp,$ln["numempenho$i"]);
					fwrite($fp,$ano_empenho);
					fwrite($fp,$unidadeOrc);
					fwrite($fp,$tipo_processo);

					fwrite($fp,"\r\n");
				}
			}
			
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/LICITACAOEMPENHODISPENSA.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `LicitacaoEmpenhoDispensa.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			$numprocesso 	= $ln['numprocesso'];
			$ano_empenho 	= $ln['ano_empenho'];
			$unidadeOrc 	= $ln['unidade_orcamentaria'];
			$tipo_processo 	= $ln['tipo_processo'];

			for ($i=1; $i <= 12; $i++) { 
				if(trim($ln["numempenho$i"]) != ''){

					fwrite($fp,$numprocesso);
					fwrite($fp,$ln["numempenho$i"]);
					fwrite($fp,$ano_empenho);
					fwrite($fp,$unidadeOrc);
					fwrite($fp,$tipo_processo);

					fwrite($fp,"\r\n");
				}
			}
			
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/PUBLICACAO.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `publicacao.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp,$ln['numprocesso']);
			fwrite($fp,$ln['tdpublicacao']);
			fwrite($fp,$ln['comunicacao']);
			
			fwrite($fp,"\r\n");
			
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/TRANSFERENCIAVOLUNTARIA.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `TransferenciaVoluntaria.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp, $ln['unidade_orcamentaria']);
			fwrite($fp, $ln['cnpjconvenente']);
			fwrite($fp, $ln['ntransferenciavoluntaria']);
			fwrite($fp, $ln['tipotransferencia_fk']);
			fwrite($fp, $ln['ntransferenciavoluntariasuperior']);
			fwrite($fp, $ln['anotransferencia']);
			fwrite($fp, $ln['datacelebracao']);
			fwrite($fp, $ln['datainiciovigencia']);
			fwrite($fp, $ln['datafimvigencia']);
			fwrite($fp, $ln['datapublicacao']);
			fwrite($fp, $ln['tipoatividadeprincipal_fk']);
			fwrite($fp, $ln['objetivo']);
			fwrite($fp, $ln['banco']);
			fwrite($fp, $ln['agenciabancaria']);
			fwrite($fp, $ln['contabancaria']);
			fwrite($fp, $ln['valorrepasse']);
			fwrite($fp, $ln['tipocontrapartida_fk']);
			fwrite($fp, $ln['contrapartida']);
			fwrite($fp, $ln['valorcontrapartidafinanceira']);
			fwrite($fp, $ln['valorcontrapartidaeconomica']);
			fwrite($fp, $ln['nomefiscalizador']);
			fwrite($fp, $ln['emailfiscalizador']);
			fwrite($fp, $ln['cpfresponsavelfiscalizador']);
			fwrite($fp, $ln['cargoresponsavelfiscalizador']);
			fwrite($fp, $ln['nomeexecucao']);
			fwrite($fp, $ln['emailexecucao']);
			fwrite($fp, $ln['cpfexecucao']);
			fwrite($fp, $ln['cargoexecucao']);
			fwrite($fp, $ln['nomerepassador']);
			fwrite($fp, $ln['cpfrepassador']);
			fwrite($fp, $ln['nomeassinaturaconvenente']);
			fwrite($fp, $ln['cpfconvenente']);
			fwrite($fp, $ln['competencia']);

			fwrite($fp,"\r\n");
			
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/TRANSFERENCIAVOLUNTARIAEMPENHO.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `TransferenciaVoluntariaEmpenho.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {

			fwrite($fp,$ln['cnpjconvenente']);
			
			fwrite($fp, $ln['ntransferenciavoluntaria']);
			fwrite($fp, $ln['notaempenho']);
			fwrite($fp, $ln['anoempenho']);
			fwrite($fp, $ln['unidorc']);
			fwrite($fp, $ln['tipoparticipacaotransferencia_pk']);
			fwrite($fp, $ln['tipoinstrumento_pk']);
			fwrite($fp, $ln['anotransferencia']);
			fwrite($fp, $ln['cnpjconcedente']);

			fwrite($fp,"\r\n");
			
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################

	$fp = fopen("php/gerarArquivo/CONVENENTETV.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `ConvenenteTv.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {
			
			fwrite($fp,$ln['cnpjconvenente']);
			fwrite($fp,$ln['razao_social']);
			fwrite($fp,$ln['nome_fantasia']);
			fwrite($fp,$ln['data_inicio']);
			fwrite($fp,$ln['cnae']);
			fwrite($fp,$ln['esfera']);

			fwrite($fp,"\r\n");
			
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	
	$fp = fopen("php/gerarArquivo/CERTIDAOTV.REM", "w");
	if ($fp == false) {
	   die("Impossivel criar arquivo.");
	} else {

		$sql = "CALL `CertidaoTv.rem`('".$_POST['ano']."', '".$_POST['mes']."')";
		$stmt = $conexao->conn->prepare($sql);
		$stmt->execute();
		$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($retorno as $ln) {
			
			fwrite($fp,$ln['cnpjconvenente']);
			fwrite($fp,$ln['tipotransferencia_fk']);
			fwrite($fp,$ln['ntransferenciavoluntaria']);
			fwrite($fp,$ln['anotransferencia']);
			fwrite($fp,$ln['tipocertidao_fk']);
			fwrite($fp,$ln['numero_certidao']);
			fwrite($fp,$ln['data_certidao']);
			fwrite($fp,$ln['data_validade']);

			fwrite($fp,"\r\n");
			
		}

		// EXECUTADO APÓS O LOOP PARA FECHAR O ARQUIVO
		fclose($fp);
	} // FIM IF 

	###################################################################################################################################################################################################
	###################################################################################################################################################################################################
	###################################################################################################################################################################################################


	// PROCESSO PARA FAZER DOWNLAOD DO ARQUIVO ZIP PARA PRESTAÇÃO DE CONTAS

	
          
    //GERA O NOVO NOME DO ARUIVO COM MES E ANO
	$nomeArquivoNovo = str_replace("ç", "c", $funcoes->mes($_POST["mes"]))."-".$_POST["ano"].".zip";
    

    //RENOMEIA O ARQUIVO PARA SER BAIXADO
    $nomeArquivoBanco = "SELECT arquivo_exportacao FROM empresa LIMIT 1";
	$stmt = $conexao->conn->prepare($nomeArquivoBanco);
	$stmt->execute();
	$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$nomeArquivoBanco = $retorno[0]['arquivo_exportacao'];

    //RENOMEIA O ARQUIVO ANTIGO
    rename("php/gerarArquivo/$nomeArquivoBanco", "php/gerarArquivo/$nomeArquivoNovo");

    
    
    //INSERI O NOVO NOME DO ARQUIVO NO BANCO
    $alterarnomeBanco = "UPDATE empresa SET arquivo_exportacao = '$nomeArquivoNovo' WHERE id > 0";
	$stmt = $conexao->conn->prepare($alterarnomeBanco);
	$stmt->execute();
	$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    $zip = new ZipArchive;
    if ($zip->open("php/gerarArquivo/$nomeArquivoNovo") === TRUE) {

          $zip->addFile('php/gerarArquivo/ADESAOATAEMPENHO.REM','ADESAOATAEMPENHO.REM');

          $zip->addFile('php/gerarArquivo/ADESAOATALICITACAO.REM','ADESAOATALICITACAO.REM');

          $zip->addFile('php/gerarArquivo/CERTIDAO.REM','CERTIDAO.REM');

          $zip->addFile('php/gerarArquivo/CERTIDAODISPENSA.REM','CERTIDAODISPENSA.REM');
          
          $zip->addFile('php/gerarArquivo/CERTIDAOTV.REM','CERTIDAOTV.REM');
          
          $zip->addFile('php/gerarArquivo/CONTRATO.REM','CONTRATO.REM');
          
          $zip->addFile('php/gerarArquivo/CONTRATOEMPENHO.REM','CONTRATOEMPENHO.REM');

          $zip->addFile('php/gerarArquivo/LICITACAODISPENSA.REM','LICITACAODISPENSA.REM');

          $zip->addFile('php/gerarArquivo/ITEMLICITACAODISPENSA.REM','ITEMLICITACAODISPENSA.REM');

          $zip->addFile('php/gerarArquivo/CONVENENTETV.REM','CONVENENTETV.REM');
          
          $zip->addFile('php/gerarArquivo/COTACAO.REM','COTACAO.REM');
          
          $zip->addFile('php/gerarArquivo/LICITACAOHISTORICO.REM','LICITACAOHISTORICO.REM');
          
          $zip->addFile('php/gerarArquivo/ITEMADESAOATA.REM','ITEMADESAOATA.REM');
          
          $zip->addFile('php/gerarArquivo/ITEMLICITACAO.REM','ITEMLICITACAO.REM');
          
          $zip->addFile('php/gerarArquivo/LICITACAO.REM','LICITACAO.REM');
          
          $zip->addFile('php/gerarArquivo/LICITACAOEMPENHO.REM','LICITACAOEMPENHO.REM');

          $zip->addFile('php/gerarArquivo/LICITACAOEMPENHODISPENSA.REM','LICITACAOEMPENHODISPENSA.REM');
          
          $zip->addFile('php/gerarArquivo/PARTICIPANTELICITACAO.REM','PARTICIPANTELICITACAO.REM');
          
          $zip->addFile('php/gerarArquivo/PUBLICACAO.REM','PUBLICACAO.REM');

          $zip->addFile('php/gerarArquivo/TRANSFERENCIAVOLUNTARIA.REM','TRANSFERENCIAVOLUNTARIA.REM');

          $zip->addFile('php/gerarArquivo/TRANSFERENCIAVOLUNTARIAEMPENHO.REM','TRANSFERENCIAVOLUNTARIAEMPENHO.REM');

        $zip->close();

        // echo "php/gerarArquivo/$nomeArquivoNovo";
        echo "<label>&nbsp;</label>";
       echo "<a class='btn btn-primary form-control' href='php/gerarArquivo/$nomeArquivoNovo' target='_blank'>Clique aqui para baixar o arquivo</a>";
       // echo "<script>window.open('php/gerarArquivo/$nomeArquivoNovo','_blank');</script>";
    } else {
    	echo "<script>alert('Falha ao Criar os Arquivos')</script>";
    }

} // FIM IF GERAL
?>