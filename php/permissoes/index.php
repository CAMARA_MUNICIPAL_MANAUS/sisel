<script src="js/action/DQWMENU/dqwmenu.js?001"></script>
<?php
if (@sizeof($_POST["DQWMENU"]) > 0){
	$r = CDQWPERMISSION::change($_POST);
}
?>

<style>
	.formMenuPermission ul{
		padding: 0px 0px;
		margin: 0px;
		list-style-type: none;
	}

	.formMenuPermission .liMenu{
		vertical-align: middle;
		padding: 4px 0px;
		border-top: 1px solid #e5e5e5;
	}

	span[data-level="1"]{ font-weight: bold; }
	.state-menu{ float: right; }

	.formMenuPermission ul.segundo{ margin-left: 20px; }
	.formMenuPermission ul.terceiro{ margin-left: 40px; }
	.formMenuPermission ul.quarto{ margin-left: 60px; }
	.formMenuPermission ul.quinto{ margin-left: 80px; }
</style>

<div class="col-lg-12">
	<h4 class="page-header"><i class="glyphicon glyphicon-lock"></i> <b>Permissions</b></h4>
	<div class="row">
		<div class="col-lg-6 co-md-6">
			<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST" role="form" class="hidden-print formProfile" onsubmit="return validarForm('.formProfile')">

				<div class="col-lg-4 col-md-4 col-sm-6">
					<div class="form-group">
						<label>System</label>
						<select name="SYSTEM" class="form-control required">
							<option value="">-</option>
							<?php
							foreach (CDQWSYSTEM::system() as $s){
								$select = ($s->ID == @$_POST["SYSTEM"]) ? "selected=true" : "";
								echo "<option value='".$s->ID."' $select>".$s->SYSTEM."</option>";
							}
							?>
						</select>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<div class="form-group">
						<label>Profile</label>
						<select name="DQWPERMISSION[PROFILE]" class="form-control required">
							<option value="">-</option>
							<?php
							foreach (CDQWPROFILE::filter("PROFILE") as $o){
								$select = ($o->ID == @$_POST["DQWPERMISSION"]["PROFILE"]) ? "selected=true" : "";
								echo "<option value='".$o->ID."' $select>".$o->PROFILE."</option>";
							}
							?>
						</select>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4">
					<div class="form-group">
						<label for="">&nbsp;</label>
						<button type="submit" class="btn btn-primary form-control"><i class="glyphicon glyphicon-search"></i></button>
					</div>
				</div>
			</form>

		<?php
		if(isset($_POST["DQWPERMISSION"]["PROFILE"])){
			$menuPermission =  json_decode(CDQWMENU::listMenu($_POST["DQWPERMISSION"]["PROFILE"], $_POST["SYSTEM"]),true);

			echo "<form method='POST' action='".$_SERVER['REQUEST_URI']."' role='form' class='formMenuPermission'>";

				echo "<div class='col-lg-12'>";
					echo "<ul>";
						$mp = CDQWMENU::menuDynamicPermission($menuPermission, 1);
					echo "</ul>";
				echo "</div>";

				echo "<input type='hidden' name='DQWPERMISSION[PROFILE]' value='".$_POST["DQWPERMISSION"]["PROFILE"]."'>";
				echo "<input type='hidden' name='SYSTEM' value='".$_POST["SYSTEM"]."'>";
				echo "<button type='submit' class='btn btn-primary'>Save Changes</button>";
			echo "</form>";
		}
		?>
		</div>

		<div class="col-lg-6 co-md-6">
			<?php
			if(isset($_POST["DQWPERMISSION"]["PROFILE"])){
				$prof = CDQWPROFILE::findAllByAttributes(array("where" => "ID = ".$_POST["DQWPERMISSION"]["PROFILE"]));
			
				echo "<h4 class='page-header'>".$prof[0]->PROFILE."</h4>";

				$usuarios = CDQWUSER::usuarios($_POST["DQWPERMISSION"]["PROFILE"]);

				echo "<table class='table table-hover table-striped'>";
					echo "<thead>";
						echo "<tr>";
							echo "<th>No</th>";
							echo "<th>NAME</th>";
							echo "<th>STATE</th>";
						echo "</tr>";
					echo "</thead>";
					echo "<tbody>";
						foreach ($usuarios as $u) {
							echo "<tr>";
								echo "<td>".$u->EMP_NO."</td>";
								echo "<td>".$u->EMP_NAME."</td>";
								echo "<td>".$u->STATE."</td>";
							echo "</tr>";
						}
					echo "</tbody>";
				echo "<table>";

			} ?>
		</div>
	</div>
</div>