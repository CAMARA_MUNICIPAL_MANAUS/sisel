<script type="text/javascript" src="js/action/EMPRESA/empresa.js"></script>

<?
$empresa = $funcoes->buscardados('empresa', $_SESSION['empresa']['id']);
$empresa = $empresa[0];
?>

<div class="col-lg-12 col-md-12 col-sm-12">
	<h3 class="page-header">Configurações Gerais</h3>
</div>

<div class="col-lg-12 col-md-12 col-sm-12">
	<form action="?p=configuracao&page=gerais" role="form" method="post" class="formempresa">
		<h4>Empresa</h4>
		<div class="row">
			<div class="col-lg-7">
				<div class="form-group">
					<label for="">Orgão:</label>
					<input type="text" name="empresa" class="form-control" value="<?= $empresa['nome_empresa']; ?>" readonly="true"/>
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">CNPJ:</label>
					<input type="text" name="cnpj" class="form-control cnpj" value="<?= $empresa['cnpj']; ?>" readonly="true"/>
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">Telefone:</label>
					<input type="text" name="telefone" class="form-control" value="<?= $empresa['telefone']; ?>" readonly="true"/>
				</div>
			</div>

			<div class="col-lg-1">
				<div class="form-group">
					<label for="">Unid. Orc.:</label>
					<input type="text" name="unidade_orcamentaria" class="form-control" value="<?= $empresa['unidade_orcamentaria']; ?>" readonly="true"/>
				</div>
			</div>
		</div>

		<h4>Endereço</h4>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<label for="">Logradouro</label>
					<input type="text" class="form-control" name="logradouro" value="<?= $empresa['logradouro']; ?>" readonly="true">
				</div>
			</div>
			<div class="col-lg-1">
				<div class="form-group">
					<label for="">Número</label>
					<input type="text" class="form-control" name="numero" value="<?= $empresa['numero']; ?>" readonly="true">
				</div>
			</div>

			<div class="col-lg-2">
				<div class="form-group">
					<label for="">CEP</label>
					<input type="text" class="form-control" name="cep" value="<?= $empresa['cep']; ?>" readonly="true">
				</div>
			</div>
		</div>

		<h4>Comissão de Licitação</h4>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<label for="">Ordenador</label>
					<input type="text" class="form-control" name="ordenador" value="<?= $empresa['ordenador']; ?>" readonly="true">
				</div>
			</div>

			<div class="col-lg-8">
				<div class="form-group">
					<label for="">Presidente</label>
					<input type="text" class="form-control" name="presidente" value="<?= $empresa['presidente']; ?>" readonly="true">
				</div>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<label for="">Função Presidente</label>
					<input type="text" class="form-control" name="funcao_presidente" value="<?= @$empresa['funcao_presidente']; ?>" readonly="true">
				</div>
			</div>

			<div class="col-lg-8">
				<div class="form-group">
					<label for="">Vice Presidente</label>
					<input type="text" class="form-control" name="vice_presidente" value="<?= $empresa['vice_presidente']; ?>" readonly="true">
				</div>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<label for="">Função Vice Presidente</label>
					<input type="text" class="form-control" name="funcao_vicepresidente" value="<?= @$empresa['funcao_vicepresidente']; ?>" readonly="true">
				</div>
			</div>

			<div class="col-lg-4">
				<div class="form-group">
					<label for="">Membro 1</label>
					<input type="text" class="form-control" name="membro1" value="<?= @$empresa['membro1']; ?>" readonly="true">
				</div>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<label for="">Membro 2</label>
					<input type="text" class="form-control" name="membro2" value="<?= @$empresa['membro2']; ?>" readonly="true">
				</div>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<label for="">Membro 3</label>
					<input type="text" class="form-control" name="membro3" value="<?= @$empresa['membro3']; ?>" readonly="true">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-1">
				<div class="form-group">
					<button type="button" class="btn btn-success btn-editar" name="editar">Editar</button>
					<button type="button" class="btn btn-success btn-salvar" name="salvar" style='display: none'>Salvar</button>
					<input type="hidden" name="codigo" value="<?= @$empresa['id']; ?>"/>
					<input type="hidden" name="acao" value="editar"/>
				</div>
			</div>
			<div class="col-lg-1">
				<div class="form-group">
					<button type="button" class="btn btn-danger btn-cancelar" name="cancelar">Cancelar</button>
				</div>
			</div>
		</div>
	</form>
</div>

<?

if(isset($_POST['acao']) && @$_POST['acao'] == 'editar'){

	$empresa 				= $_POST['empresa'];
	$logradouro 			= $_POST['logradouro'];
	$cep 					= $_POST['cep'];
	$numero 				= $_POST['numero'];
	$telefone 				= $_POST['telefone'];
	$ordenador 				= $_POST['ordenador'];
	$presidente 			= $_POST['presidente'];
	$funcao_presidente 		= $_POST['funcao_presidente'];
	$vice_presidente 		= $_POST['vice_presidente'];
	$funcao_vicepresidente 	= $_POST['funcao_vicepresidente'];
	$membro1 				= $_POST['membro1'];
	$membro2 				= $_POST['membro2'];
	$membro3 				= $_POST['membro3'];
	$unidade_orcamentaria 	= $_POST['unidade_orcamentaria'];

	$codigo = $_POST['codigo'];

	$sql = "UPDATE empresa

	SET nome_empresa = '$empresa',
	logradouro = '$logradouro',
	cep = '$cep',
	numero = '$numero',
	telefone = '$telefone',
	ordenador = '$ordenador',
	presidente = '$presidente',
	funcao_presidente = '$funcao_presidente',
	vice_presidente = '$vice_presidente',
	funcao_vicepresidente = '$funcao_vicepresidente',
	membro1 = '$membro1',
	membro2 = '$membro2',
	membro3 = '$membro3',
	unidade_orcamentaria = '$unidade_orcamentaria'
	WHERE id = $codigo";

	$stmt = $conexao->conn->prepare($sql);
	if($stmt->execute()){
		echo "<script>location.href='?p=configuracao&page=gerais';</script>";
	}else{
		echo "<script>location.href='?p=configuracao&page=gerais';</script>";
	}

}

?>