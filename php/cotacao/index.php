<script src="js/action/cotacao/cotacao.js"></script>

<!-- DATATABLES -->
<link href="plugin/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet" />
<link href="plugin/datatables-responsive/dataTables.responsive.css" rel="stylesheet" />

<!-- DATATABLES -->
<script src="plugin/datatables/js/jquery.dataTables.js"></script>
<script src="plugin/datatables-plugins/dataTables.bootstrap.js"></script>
<script src="plugin/datatables-responsive/dataTables.responsive.js"></script>

<input type="hidden" id="anoExercicio" name="anoExercicio" value="<?php echo $_SESSION['usuario']['ano_exercicio']; ?>"/>

<div class="col-lg-12">
	<h3 class="page-header">Cotação</h3>
</div>

<div class="col-lg-12">
	<div class="row">
		<div class="col-lg-2 col-md-2 col-sm-2">
			<a href="?p=cotacao&page=cotacao_cadastrar" class="btn btn-primary btn-sm novo form-control"><i class="glyphicon glyphicon-plus"></i> Nova Cotação</a>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2">
			<a href="?p=cotacao&page=cotacao_cadastrar&visualizar=visualizar" class="btn btn-primary btn-sm novo form-control"><i class="glyphicon glyphicon-search"></i> Visualizar Cotações</a>
		</div>
	</div>

	<br>
	
	<table class="table table-hover table-striped table-condensed" id="tt">
		<thead>
			<tr>
				<th>Nº Processo</th>
				<th>Fornecedor</th>
				<th>CPF/CNPJ</th>
				<th class='text-right'>Valor</th>
				<th class='text-center'>Resultado</th>
				<th class='text-right'></th>
			</tr>
		</thead>
		<tbody>
			<?
				// print_r($_SESSION);

				$sql = "SELECT c.id, i.licitacao_fk, p.descricao, c.itemlicitacao_fk,
								 ROUND(c.valor_total,2) as valor, c.tipovalor_fk, c.tiporesultado_fk,
								 f.nome_fantasia, f.razao_social, formatarCpfCnpj(f.cpfcnpj) as cpfcnpj, f.insc_estadual, f.insc_municipal
						FROM cotacao c
						INNER JOIN itemlicitacao i  ON i.id = c.itemlicitacao_fk
						INNER JOIN produto p 		ON p.id = i.produto_fk
						INNER JOIN licitacao l 		ON l.id = i.licitacao_fk
						INNER JOIN fornecedor f 	ON f.id = c.participante_fk
						WHERE l.ano_exercicio = ".$_SESSION['usuario']['ano_exercicio'];
													
				$stmt = $conexao->conn->prepare($sql);
				$stmt->execute();
				$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

				$i = 1;
				foreach ($retorno as $ln) {
				?>
					<tr>
						<td><?= $ln['licitacao_fk'] ?></td>

						<td title='Razão Social: <?= $ln["razao_social"]."\n" ?>CPF/CNPJ: <?= $funcoes->formatarDoc($ln["cpfcnpj"])."\n" ?>Insc. Estadual: <?= $ln["insc_estadual"]."\n" ?>Insc. Municipal: <?= $ln["insc_municipal"] ?>'>
							<?= $ln['razao_social']; ?>
						</td>
						<td><?= $ln['cpfcnpj'] ?></td>

						<td class='text-right'>
							<?= "R$ ".number_format($ln['valor'], 2, ',', '.'); ?>
						</td>
						
						<td class='text-center'>
							<?
							if($ln['tiporesultado_fk'] == 'V'){
								echo "<img src='img/up.fw.png' width='20' title='Vencedor'/>";
							}else{
								echo "<img src='img/down.fw.png' width='20' title='Perdedor'/>";
							}
							?>
						</td>

						<td class='text-right'>
							<!-- <a class='btn btn-primary visualizar' href=''><i class='glyphicon glyphicon-search'></i></a>
								&nbsp; -->
							<button type='button' class='btn btn-danger btn-sm excluir-licitacao' data-licitacao='<?= $ln['id']; ?>'><i class='glyphicon glyphicon-remove'></i></button>
						</td>
					</tr>
				<?
				}
			?>
		</tbody>
	</table>
</div>

<script>
    $(function(){
        $('#tt').DataTable({
            responsive: true
        });
    })
</script>