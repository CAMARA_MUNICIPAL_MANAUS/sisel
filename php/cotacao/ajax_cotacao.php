<?

if(isset($_POST['acao'])){

	include_once '../../config/conn.php';
    include_once '../../config/funcoesgerais.php';

    $funcoes  = new Funcoes;

	switch ($_POST['acao']) {

		case 'buscarItens':

			$sql = "SELECT i.id, i.sequencial, i.quantidade, p.descricao, u.unidade, i.controle_itemlote
					FROM itemlicitacao i
					LEFT JOIN tipounidade u ON u.id = i.unidade_fk
					LEFT JOIN produto p ON p.id = i.produto_fk
					WHERE i.licitacao_fk = '".$_POST['licitacao']."'
					ORDER BY i.sequencial ASC";
			
			$stmt = $conexao->conn->prepare($sql);
			$stmt->execute();
			$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

			echo '<option value="">Selecione primerio o Nº do Processo</option>';

			foreach ($retorno as $ln){
				echo "<option value='{$ln['id']}' data-qtd='{$ln['quantidade']}' data-tipoproduto='{$ln['tipoproduto']}'
							data-controle='{$ln['controle_itemlote']}' data-unidade='{$ln['unidade']}'>".$ln['sequencial'].' - '.$ln['descricao']."</option>";
			}

		break;

		case 'buscarParticipante':

			$sql = "SELECT DISTINCT p.fornecedor_fk, f.razao_social, tp.tipopessoa
					FROM participante_certidao p
					INNER JOIN fornecedor f ON f.id = p.fornecedor_fk
					LEFT JOIN tipopessoa tp ON tp.id = f.tipopessoa_fk
					WHERE p.licitacao_fk = '".$_POST['licitacao']."'
					AND p.fornecedor_fk NOT IN (SELECT c.participante_fk FROM cotacao c WHERE c.itemlicitacao_fk = ".$_POST['item'].")";

			$stmt = $conexao->conn->prepare($sql);
			$stmt->execute();
			$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

			echo '<option value="">Selecione um participante</option>';

			foreach ($retorno as $p) {
				echo "<option value='".$p['fornecedor_fk']."' data-pessoa='".$p['tipopessoa']."'>".$p['razao_social']."</option>";
			}

		break;

		case 'inserirCotacao':

			// print_r($_POST);

			$licitacao 		= $_POST['licitacao']; /* => CC0003-2014 */
		    $itemLicitacao 	= $_POST['itemLicitacao']; /* => 47 */
		    $participante 	= $_POST['participante']; /* => 139 */
		    $vlunit 		= $funcoes->formatarValorParaBanco($_POST['vlunit']); /* => 10.000,00 */
		    $vltotal 		= $funcoes->formatarValorParaBanco($_POST['vltotal']); /* => 10.000,00 */
		    $tipovalor 		= $_POST['tipovalor']; /* => E */
		    $tiporesultado 	= $_POST['tiporesultado']; /* => V */

		    $sql = "INSERT INTO cotacao (participante_fk, itemlicitacao_fk, valor_unitario, valor_total, tipovalor_fk, tiporesultado_fk)
		    		VALUES(:participante, :item, :vlunit, :vltotal, :tipovalor, :resultado)";

		    $stmt = $conexao->conn->prepare($sql);
			$stmt->bindValue(':participante', $participante);
			$stmt->bindValue(':item', $itemLicitacao);
			$stmt->bindValue(':vlunit', $vlunit);
			$stmt->bindValue(':vltotal', $vltotal);
			$stmt->bindValue(':tipovalor', $tipovalor);
			$stmt->bindValue(':resultado', $tiporesultado);

			if($stmt->execute()){
				echo 1;
			}else{
				print_r($stmt->errorInfo());
			}

		break;

		case 'listarCotacao':

			$buscaItem = "SELECT i.id, i.sequencial, i.quantidade, p.descricao
							FROM itemlicitacao i 
							INNER JOIN produto p ON p.id = i.produto_fk
							WHERE i.licitacao_fk = '".$_POST['licitacao']."'
							ORDER BY i.sequencial ASC;";
			$stmt = $conexao->conn->prepare($buscaItem);
			$stmt->execute();
			$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

			if(sizeof($retorno) == 0){
				echo "Nenhuma item inserido para o processo nº ".$_POST['licitacao'];
				exit();
			}else{

				echo "<p><i>*Segue abaixo os itens do processo ".$_POST['licitacao'].".</i></p>";

			}

			foreach ($retorno as $item) {
				echo "<legend>".$item['sequencial']." - ".$item['descricao']." <small>(Qtd: ".$item['quantidade'].")</small></legend>";

				$buscarCotacaoDesseItem = "SELECT f.razao_social, c.valor_total, tv.tipovalor, tr.tiporesultado, c.id
											FROM cotacao c
											INNER JOIN fornecedor f ON f.id = c.participante_fk
											INNER JOIN tipovalor tv ON tv.id = c.tipovalor_fk
											INNER JOIN tiporesultado tr ON tr.id = c.tiporesultado_fk
											WHERE c.itemlicitacao_fk = ".$item['id']."
											ORDER BY f.razao_social ASC";
				
				$stmt = $conexao->conn->prepare($buscarCotacaoDesseItem);
				$stmt->execute();
				$resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);

					if(sizeof($resultado) == 0){

						echo "Nenhuma cotação encontrada para este item.";

					}else{
						echo "<table class='table table-hover table-stripped'>";
							echo "<thead>";
								echo "<tr>";
									echo "<th>Nº</th>";
									echo "<th style='min-width: 300px;'>Fornecedor</th>";
									echo "<th class='text-right'>Valor Unit.</th>";
									echo "<th class='text-right'>Valor Total</th>";
									echo "<th class='text-right'>Valor</th>";
									echo "<th class='text-right'>Resultado</th>";
									echo "<th class='text-right'></th>";
								echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
								$cont = 1;
								foreach ($resultado as $cot) {
									echo "<tr>";
										echo "<td>".$cont++."</td>";
										echo "<td>".$cot['razao_social']."</td>";
										echo "<td class='text-right'>".number_format($cot['valor_total']/$item['quantidade'], 2, ',', '.')."</td>";
										echo "<td class='text-right'>".number_format($cot['valor_total'], 2, ',', '.')."</td>";
										echo "<td class='text-right'>".$cot['tipovalor']."</td>";
										echo "<td class='text-right'>".$cot['tiporesultado']."</td>";
										echo "<td class='text-right'><button type='button' class='btn btn-danger excluir-licitacao' data-licitacao='".$cot['id']."'><i class='glyphicon glyphicon-remove'></i></button></td>";
									echo "</tr>";
								}
							echo "</tbody>";
						echo "</table>";
					}
			}

		break;


		case 'excluirCotacao':

			$excluir = "DELETE FROM cotacao WHERE id = ".$_POST['licitacao'];
			$stmt = $conexao->conn->prepare($excluir);
			
			if($stmt->execute()){
				echo 1;
			}else{
				print_r($stmt->errorInfo());
			}

		break;

	}

}

?>