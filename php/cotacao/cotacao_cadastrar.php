<script src="js/action/COTACAO/cotacao.js"></script>

<div class="col-lg-12">
	<h3 class="page-header">Cotação</h3>
</div>

<div class="col-lg-12">
	<form role='form' id="form-publicacao">
		<div class='col-md-2'>
			<div class='form-group'>
				<label>Nº Processo</label>
				<? $auxiliar->licitacao($_SESSION['usuario']['ano_exercicio']); ?>
			</div>
		</div>

		<?php
			if(isset($_GET['visualizar'])){
				echo "<p style='color: red;'>**Selecione o nº do processo para visualizar todas as cotações.</p>";
			}else{
		?>

		<div class='col-md-6'>
			<div class='form-group'>
				<label>Item da Licitação</label>
				<select name="itemLicitacao" class="form-control itemLicitacao">
					<option value="">Selecione primerio o Nº do Processo</option>
				</select>
			</div>
		</div>

		<div class='col-md-1'>
			<div class='form-group'>
				<label>QTD</label>
				<input type="text" name="quantidade" class="form-control quantidade" disabled="true"/>
			</div>
		</div>

		<div class='col-md-2'>
			<div class='form-group'>
				<label>Controle Item Lote</label>
				<input type="text" name="controleItemLote" class="form-control controleItemLote" disabled="true"/>
			</div>
		</div>
		<div class='col-md-1'>
			<div class='form-group'>
				<label>Unidade</label>
				<input type="text" name="unidade" class="form-control unidade" disabled="true"/>
			</div>
		</div>

		<div class='col-md-1'>
			<div class='form-group'>
				<label title="Tipo Pessoa">Pessoa</label>
				<input type="text" name="tipopessoa" class="form-control tipopessoa" disabled="true"/>
			</div>
		</div>

		<div class='col-md-4'>
			<div class='form-group'>
				<label>Participante</label>
				<select name="participante" class="form-control participante">
					<option value="">Selecione um participante</option>
				</select>
			</div>
		</div>

		<div class='col-md-2'>
			<div class='form-group'>
				<label>Valor Unit.</label>
				<input type="text" name="vlunit" class="form-control vlunit" readonly="true"/>
			</div>
		</div>

		<div class='col-md-2'>
			<div class='form-group'>
				<label>Valor Total.</label>
				<input type="text" name="vltotal" class="form-control vltotal dinheiro"/>
			</div>
		</div>

		<div class='col-md-2'>
			<div class='form-group'>
				<label>Tipo Valor</label>
				<? $auxiliar->tipovalor(); ?>
			</div>
		</div>

		<div class='col-md-1'>
			<div class='form-group'>
				<label>Resultado</label>
				<? $auxiliar->tiporesultado(); ?>
			</div>
		</div>

		<div class='col-md-2'>
			<div class='form-group'>
				<label>&nbsp;</label>
				<button type='button' class='btn btn-success cadastrar btn-inserir'>Inserir</button>
			</div>
		</div>

		<? } ?>
	</form>

	<div class="col-lg-12 listarCotacao"></div> <!-- LISTA COTAÇÃO A PARTIR DA SELEÇÃO DO Nº DO PROCESO -->
	
</div>