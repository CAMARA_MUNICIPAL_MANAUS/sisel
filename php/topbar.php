<div class="topbar">
    
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="text-center brand">
            <a class="" href="?"><img src="img/logo_menu_2.png" width="192"></a>
        </div>
    </div>

    <button type='button' class="btn btn-primary btnMenuHide hidden-xs"><i class="fa fa-bars"></i></button>

    <ul class="nav navbar-top-links navbar-right hidden-xs">
        <li class="dropdown">
            <strong style="color: #ffffff;"><?php echo $_SESSION['empresa']['nome_empresa']; ?></strong>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <?php $fullname = explode(" ", $_SESSION['usuario']['NOME']); ?>
                <img src="img/user.png" width="20px" /> <b><?php echo $fullname[0]; ?></b>
                <!-- <i class="fa fa-caret-down"></i> -->
            </a>
            <!-- <ul class="dropdown-menu dropdown-user">
                <li><a href="php/view/login/logout.php"><i class="fa fa-sign-out"></i> Sair</a></li>
            </ul> -->
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->
</div>