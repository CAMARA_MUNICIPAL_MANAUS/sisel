<div class="modal fade effect-8" id="userProfile" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><label for=""><?php echo $lang['btnUserProfile']; ?></label></h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <form action="" method="POST" role="form" class="formUserProfile">
                  <div class="col-lg-4 col-md-4">
                     <div class="form-group">
                        <label for=""><?php echo $lang['userProfile_fieldRegUser']; ?></label>
                        <input type="text" id="dqwuserEmpno" name="dqwuser[EMP_NO]" class="form-control" autocomplete="off" value="<?php echo $_SESSION["eip"]["user"]->EMP_NO; ?>" disabled="true"/>
                     </div>
                  </div>

                  <div class="col-lg-8 col-lg-md-8">
                     <div class="form-group">
                        <label for=""><?php echo $lang['userProfile_fieldName']; ?></label>
                        <input type="text" id="dqwuserEmpname" name="dqwuser[EMP_NAME]" class="form-control" autocomplete="name" value="<?php echo $_SESSION["eip"]["user"]->EMP_NAME; ?>"/>
                     </div>
                  </div>

                  <div class="col-lg-4 col-md-4">
                     <div class="form-group">
                        <label for=""><?php echo $lang['userProfile_fieldProfile']; ?></label>
                        <input type="text" class="form-control" title="<?php echo $_SESSION["eip"]["user"]->NAMEPROFILE; ?>" value="<?php echo $_SESSION["eip"]["user"]->NAMEPROFILE; ?>" disabled="true"/>
                     </div>
                  </div>

                  <div class="col-lg-8 col-lg-md-8">
                     <div class="form-group">
                        <label for=""><?php echo $lang['userProfile_fieldEmail']; ?></label>
                        <input type="text" id="dqwuserEmail" name="dqwuser[EMAIL]" class="form-control" autocomplete="email" value="<?php echo $_SESSION["eip"]["user"]->EMAIL; ?>"/>
                     </div>
                  </div>

                  <div class="col-lg-4">
                     <div class="form-group">
                        <label for=""><?php echo $lang['userProfile_fieldPass']; ?></label>
                        <input type="password" id="dqwuserPassword" name="dqwuser[PASSWORD]" class="form-control" maxlength="15" autocomplete="off"/>
                     </div>
                  </div>

                  <div class="col-lg-4">
                     <div class="form-group">
                        <label for=""><?php echo $lang['userProfile_fieldConfirmPass']; ?></label>
                        <input type="password" id="dqwuserPassword2" name="dqwuser[PASSWORD2]" class="form-control" maxlength="15" autocomplete="off"/>
                     </div>
                  </div>

                  <div class="col-lg-4">
                     <div class="form-group">
                        <label for=""><?php echo $lang['userProfile_fieldLanguage']; ?></label>
                        <select id="dqwuserLang" name="dqwuser[LANG]" class="form-control required">
                           <?php
                           $langSelect = array(
                              array("l" => "EN", "lang" => "ENGLISH"),
                              array("l" => "CH", "lang" => "CHINESE"),
                              array("l" => "PT", "lang" => "PORTUGUESE"),
                           );
                           foreach ($langSelect as $l){
                              $selectedLang = ($_SESSION["eip"]["user"]->LANG == $l["l"]) ? "selected" : "";
                              echo "<option value='".$l["l"]."' $selectedLang>".$l["lang"]."</option>";
                           }
                           ?>
                        </select>
                     </div>
                  </div>

                  <input type="hidden" id="dqwuserId" name="ID" value="<?php echo $_SESSION["eip"]["user"]->ID; ?>"/>
               </form>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary btnSave"><?php echo $lang['btnSave']; ?></button>
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $lang['btnClose']; ?></button>
         </div>
      </div>
   </div>
</div>



<script>
   $(function(){

      $("#dqwuserPassword, #dqwuserPassword2").on("keyup", function(){
         var p1 = $("#dqwuserPassword").val();
         var p2 = $("#dqwuserPassword2").val();

      // VERIFICA SE OS DOIS CAMPOS INFORMADOS PELO USUARIO AO ALTERAR SENHA SÃO IGUAIS
      if(p1 && p2){
         if(p1 == p2){
            $(".btnSave").removeClass("disabled");
         }else{
            alert2("danger", lang('msgDiffPass'));
            $(".btnSave").addClass("disabled");
         }
      }else{
         $(".btnSave").addClass("disabled");
      }
   });

      $(".btnSave").on("click", function(){
         if(validarForm(".formUserProfile") == true){
            var s = $(".formUserProfile").serializeArray();
            $.ajax({
               url: "webservice.php?",
               data: {funcao: "Cdqwuser::savee", dados: {s: s} },
               type: 'POST',
               success: function(data){
                  if(data == 1){
                     $(this).closest('.modal-footer').find("button:eq(1)").click();
                     alert2("success", lang('msgSuccess'));
                  }else{
                     alert2("danger", lang('msgFailSave'));
                  }
               }
            });
         }
      });

   })
</script>