<!-- DATATABLES -->
<link href="plugin/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet" />
<link href="plugin/datatables-responsive/dataTables.responsive.css" rel="stylesheet" />

<!-- DATATABLES -->
<script src="plugin/datatables/js/jquery.dataTables.js"></script>
<script src="plugin/datatables-plugins/dataTables.bootstrap.js"></script>
<script src="plugin/datatables-responsive/dataTables.responsive.js"></script>

<!-- INICIO DO CONTEÚDO DA PÁGINA -->

<div class="col-lg-12">
	<h4 class="page-header"><i class="glyphicon glyphicon-user"></i> <b>User management</b></h4>
	<button type="button" class="btn btn-primary btn-sm btnAddUser" data-toggle="modal" data-target="#addUser"><i class="fa fa-plus"></i> Add User</button>
	<br><br>
	<div class="row">
		<div class="col-lg-12">
			<table class="table table-stripped table-hover" id="table-users">
				<thead>
					<tr>
						<th>No</th>
						<th>NAME</th>
						<th>E-MAIL</th>
						<th>PROFILE</th>
						<th>STATE</th>
						<th>LAST_UPD</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach (CDQWUSER::usuarios() as $u) {
						echo "<tr>";
							echo "<td>".$u->EMP_NO."</td>";
							echo "<td>".$u->EMP_NAME."</td>";
							echo "<td>".$u->EMAIL."</td>";
							echo "<td>".$u->PROFILE."</td>";
							echo "<td>".$u->STATE."</td>";
							echo "<td>".$u->LAST_UPD."</td>";
							echo "<td class='text-right'>";
								echo "<button type='button' class='btn btn-warning btn-circle btnEditUser' title='Edit' data-toggle='modal' data-target='#addUser' code='".$u->ID."' profile='".$u->IDPROFILE."'><i class='glyphicon glyphicon-pencil'></i></button>";
								echo "&nbsp;";
								echo "<button type='button' class='btn btn-danger btn-circle btnRemoveUser' title='Remove'><i class='glyphicon glyphicon-remove'></i></button>";
							echo "</td>";
						echo "</tr>";
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php include_once "_controlUser.php"; ?>

<script>
	$(function(){
		$('#table-users').DataTable({
	        responsive: true
	    });
	})
</script>