<div class="modal fade" id="userValidation" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close btnClose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">User Validation</h4>
         </div>
         <div class="modal-body">
            <div class="row">
				<form method="POST" class="formLogin">
					<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
						<div class="form-group divlogin">
							<label for=""><i class="glyphicon glyphicon-user"></i> Login</label>
							<input type="text" name="EMP_NO" id="EMP_NO" class="form-control required" style="text-transform:uppercase" autofocus="true">
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
						<div class="form-group divsenha">
							<label for=""><i class="glyphicon glyphicon-asterisk"></i> Password</label>
							<input type="password" name="PASSWORD" id="PASSWORD" class="form-control required">
						</div>
					</div>
				</form>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary btnValidate">Validate</button>
            <button type="button" class="btn btn-default btnClose" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>


