<script src="js/action/DQWUSER/dqwuser.js"></script>
<!-- Modal -->
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $lang['manageFlow_tableColUsers']; ?></h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <form action="" method="POST" role="form" class="formInsertUser">
                  <div class="col-lg-3">
                     <div class="form-group">
                        <label for=""><span class="red">*</span> No.</label>
                        <!-- <input type="text" name="DQWUSER[EMP_NO]" class="form-control required"/> -->
                        <div class="input-group">
                           <input type="text" name="DQWUSER[EMP_NO]" class="form-control required">
                           <span class="input-group-btn">
                              <button class="btn btn-default btn-sm btnFindNo" type="button"><i class="fa fa-search"></i></button>
                           </span>
                        </div>
                     </div>
                  </div>

                  <div class="col-lg-9">
                     <div class="form-group">
                        <label for="">Profile</label>
                        <select name="DQWUSER[PROFILE]" class="form-control required" disabled="true">
                           <option value="">-</option>
                           <?php
                           $profile = ($_SESSION["eip"]["user"]->PROFILE != 1) ? explode("-", $_SESSION["eip"]["user"]->NAMEPROFILE) : "";
                           $profile = (is_array($profile)) ? trim($profile[0]) : "";
                           foreach (CDQWPROFILE::filter("PROFILE", $profile) as $o){
                              echo "<option value='".$o->ID."'>".$o->PROFILE."</option>";
                           }
                           ?>
                        </select>
                     </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-12">
                     <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="DQWUSER[EMP_NAME]" class="form-control required" disabled="true"/>
                     </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-12">
                     <div class="form-group">
                        <label for="">E-mail</label>
                        <input type="text" name="DQWUSER[EMAIL]" class="form-control" disabled="true"/>
                     </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-12">
                     <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="DQWUSER[PASSWORD]" class="form-control required" disabled="true"/>
                     </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-12">
                     <div class="form-group">
                        <label for="">Confirm Password</label>
                        <input type="password" name="DQWUSER[PASSWORD2]" class="form-control required" disabled="true"/>
                     </div>
                  </div>

                  <input type="hidden" name="ID"/>
               </form>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary btnSaveInsertUser disabled">Save changes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>