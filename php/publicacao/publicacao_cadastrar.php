<?
if(isset($_POST['id'])){
	include_once '../../config/conn.php';

    $sql = "select * from publicacao where id = {$_POST['id']}";
    $stmt = $conexao->conn->prepare($sql);
    $stmt->execute();
    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // print_r($retorno);
    echo json_encode($retorno[0]);

    exit();
}

if(isset($_POST['Cadastrar'])){
	include_once '../../config/conn.php';
	include_once '../../config/funcoesgerais.php';

	$funcoes = new Funcoes;

	
	$sql = "INSERT INTO `publicacao` (`licitacao_fk`, `dt_publicacao`, `veiculo_comunicacao`)
			VALUES (:licitacao, :dtpublicacao, :veiculo)";
    $stmt = $conexao->conn->prepare($sql);
    $stmt->bindValue(':licitacao', $_POST['licitacao']);
    $stmt->bindValue(':dtpublicacao', $_POST['dtpublicacao']);
    $stmt->bindValue(':veiculo', $_POST['veiculo']);
    if($stmt->execute()){
    	echo 1;
    }else{
    	print_r($stmt->errorInfo());
    	echo 0;
    }
	exit();
}

if(isset($_POST['Editar'])){
	include_once '../../config/conn.php';
	include_once '../../config/funcoesgerais.php';

	$funcoes = new Funcoes;

	$sql = "UPDATE `publicacao` 
				SET `licitacao_fk` = :licitacao,
				    `dt_publicacao` = :dtpublicacao,
				    `veiculo_comunicacao` = :veiculo
				WHERE id = :id";
    $stmt = $conexao->conn->prepare($sql);
    $stmt->bindValue(':licitacao', $_POST['licitacao']);
    $stmt->bindValue(':dtpublicacao', $_POST['dtpublicacao']);
    $stmt->bindValue(':veiculo', $_POST['veiculo']);
    $stmt->bindValue(':id', $_POST['idpub']);
    if($stmt->execute()){
    	echo 1;
    }else{
    	print_r($stmt->errorInfo());
    	echo 0;
    }
	exit();
}

if(isset($_POST['excluir'])){
	include_once '../../config/conn.php';
	
	$sql = "DELETE FROM publicacao WHERE id = :id";
    $stmt = $conexao->conn->prepare($sql);
    $stmt->bindValue(':id', $_POST['publicacao']);
    if($stmt->execute()){
    	echo 1;
    }else{
    	print_r($stmt->errorInfo());
    	echo 0;
    }
	exit();
}
?>