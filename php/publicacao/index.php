<script type="text/javascript" src="js/action/PUBLICACAO/publicacao.js"></script>
<? include_once 'php/publicacao/publicacao_cadastrar.php'; ?>

<!-- DATATABLES -->
<link href="plugin/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet" />
<link href="plugin/datatables-responsive/dataTables.responsive.css" rel="stylesheet" />

<!-- DATATABLES -->
<script src="plugin/datatables/js/jquery.dataTables.js"></script>
<script src="plugin/datatables-plugins/dataTables.bootstrap.js"></script>
<script src="plugin/datatables-responsive/dataTables.responsive.js"></script>

<link href="plugin/select2/css/minify/select2-bootstrap_select2.css" rel="stylesheet" />
<script src="plugin/select2/js/select2.min.js"></script>

<div class="col-lg-12 col-md-12 col-sm-12">
    <h3 class="page-header">Publicação</h3>
</div>

<div class="col-lg-12 col-md-12 col-sm-12">

	<button type="button" class="btn btn-primary novo btn-sm" data-toggle="modal" data-target="#myModalPublicacao"><i class="glyphicon glyphicon-plus"></i> Nova Publicação</button>
	<br><br>

	<table class="table table-hover table-striped" id="tablePublicacao" width="100%">
		<thead>
			<tr>
				<th></th>
				<th>Nº Processo</th>
				<th>Data da Publicação</th>
				<th>Veículo de Comunicação</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?
				$sql = "SELECT p.*
						FROM publicacao p
						INNER JOIN licitacao l On l.id = p.licitacao_fk
						WHERE l.ano_exercicio = ".$_SESSION['usuario']['ano_exercicio']."
						ORDER BY licitacao_fk ASC;";
													
				$stmt = $conexao->conn->prepare($sql);
				$stmt->execute();
				$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

				$i = 1;
				foreach ($retorno as $ln) {
					echo "<tr>";
						echo "<td class='text-left'>";
							echo "<a class='btn btn-primary btn-sm visualizar btn-circle' id='{$ln['id']}'><i class='glyphicon glyphicon-search'></i></a>";
						echo "</td>";
						echo "<td data-licitacao='".$ln['licitacao_fk']."'>".$ln['licitacao_fk']."</td>";
						echo "<td data-dt_publicacao='".$ln['dt_publicacao']."'>".$funcoes->dateUSparaBR($ln['dt_publicacao'])."</td>";
						echo "<td data-veiculo='".$ln['veiculo_comunicacao']."'>".$ln['veiculo_comunicacao']."</td>";
						echo "<td class='text-right'>";
							echo "<button type='button' class='btn btn-danger btn-sm excluir btn-circle' data-id='{$ln['id']}'><i class='glyphicon glyphicon-remove'></i></button>";
						echo "</td>";
					echo "</tr>";
				}
			?>
		</tbody>
	</table>
</div>

<!-- ########################### -->

<!-- Modal -->
<div class="modal fade" id="myModalPublicacao" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cadastrar/Editar Publicação</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-lg-12 col-md-12 col-sm-12">
      			<form role='form' id="form-publicacao">
      				<div class='col-lg-4 col-md-4'>
      					<div class='form-group'>
      						<label>Nº Processo</label>
      						<? $auxiliar->licitacao($_SESSION['usuario']['ano_exercicio']); ?>
      					</div>
      				</div>
      				<div class='col-lg-4 col-md-4'>
      					<div class='form-group'>
      						<label>Data de Publicação</label>
      						<input type="date" id="dtpublicacao" name="dtpublicacao" class="form-control" />
      					</div>
      				</div>
      				<div class='col-lg-12 col-md-12'>
      					<div class='form-group'>
      						<label>Veículo de Comunicação</label>
      						<input type="text" id="veiculo" name="veiculo" class="form-control" maxlength="200" />
      					</div>
      				</div>
      				<div class='col-lg-2 col-md-2'>
      					<div class='form-group'>
      						<label>&nbsp;</label>
      						<button type='button' class='btn btn-success cadastrar' data-id=""></button>
      					</div>
      				</div>
      			</form>
	      	</div>
      	</div>
      </div>
    </div>
  </div>
<!-- ########################### -->

<script>
	$(function(){
		$('#tablePublicacao').DataTable({
	        responsive: true
	    });
	})
</script>