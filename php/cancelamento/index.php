<div class="col-lg-12">
    <h3 class="page-header">Cancelamento Nota Fiscal</h3>
</div>

<div class="col-lg-12">
    
    <a href="?p=cancelamento&page=cadastrocancelamento" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Novo Cancelamento de Nota</a>
    <br>
    <br>
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>Nº Nota</th>
                <th>Dt. Emissão</th>
                <th>CPF/CNPJ</th>
                <th>Emitente</th>
                <th>Dt. Cancelamento</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?
                $sql = "SELECT n.id, n.nf, n.dtemissao, n.documento, n.emitente, n.dtcancelamento
                        FROM cancelamento n
                        WHERE n.ano = ".$_SESSION['usuario']['ano_exercicio']."
                        ORDER BY n.nf ASC";                                    
                $stmt = $conexao->conn->prepare($sql);
                $stmt->execute();
                $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

                $i = 1;
                foreach ($retorno as $ln) {
                    echo "<tr>";
                        echo "<td>".$ln['nf']."</td>";
                        echo "<td>".$ln['dtemissao']."</td>";
                        echo "<td>".$funcoes->formatarDoc($ln['documento'],'')."</td>";
                        echo "<td>".$ln['emitente']."</td>";
                        echo "<td>".$ln['dtcancelamento']."</td>";
                        echo "<td class='text-right'>";
                            echo "<form action='?p=cancelamento&page=ajax_cancelamento' method='post'>";
                                echo '<input type="hidden" name="acao" value="excluir">';
                                echo '<input type="hidden" name="codigo" value="'.$ln['id'].'">';
                                echo "<a class='btn btn-primary' href='?p=cancelamento&page=cadastrocancelamento&nota=".$ln['id']."'><i class='glyphicon glyphicon-search'></i></a>";
                                echo "&nbsp;";
                            echo "<button class='btn btn-danger btn-excluir'><i class='glyphicon glyphicon-remove'></i></button>";
                            echo "</form>";
                        echo "</td>";
                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>
</div>