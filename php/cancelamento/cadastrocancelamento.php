<?php
if(isset($_GET["nota"])){
	$nota = $funcoes->buscardados_campo("cancelamento", "id", $_GET['nota']);
    $nota = $nota[0];
    $btn = "editar";
}else{
	$btn = "inserir";
}
?>

<div class="col-lg-12">
    <h3 class="page-header">Cadastrar/Editar Nota Fiscal</h3>
</div>

<div class="col-lg-12">
	<form action="?p=cancelamento&page=ajax_cancelamento" method="POST" form="formCancelamento">
		<div class='col-md-3 col-lg-3'>
			<div class='form-group'>
				<label for="">CPF/CNPJ</label>
				<input type='number' name='cancelamento[documento]' id='cancelamentoDocumento' class='form-control required' maxlength='14' placeholder='' value='<?= @$nota['documento']; ?>'/>
			</div>
		</div>
		<div class='col-md-9 col-lg-9'>
			<div class='form-group'>
				<label for="">Emitente</label>
				<input type='text' name='cancelamento[emitente]' id='cancelamentoDocumento' class='form-control required' maxlength='14' placeholder='' value='<?= @$nota['emitente']; ?>'/>
			</div>
		</div>
		<div class='col-md-3 col-lg-3'>
			<div class='form-group'>
				<label for="">Nº Nota</label>
				<input type='number' name='cancelamento[nf]' id='cancelamentoNf' class='form-control required' maxlength='20' placeholder='' value='<?= @$nota['nf']; ?>'/>
			</div>
		</div>
		<div class='col-md-3 col-lg-3'>
			<div class='form-group'>
				<label for="">Serie</label>
				<input type='number' name='cancelamento[serie]' id='cancelamentoSerie' class='form-control ' maxlength='8' placeholder='' value='<?= @$nota['serie']; ?>'/>
			</div>
		</div>
		<div class='col-md-3 col-lg-3'>
			<div class='form-group'>
				<label for="">Dt. Emissão</label>
				<input type='date' name='cancelamento[dtemissao]' id='cancelamentoDtemissao' class='form-control' value='<?= @$nota['dtemissao']; ?>'/>
			</div>
		</div>
		<div class='col-md-3 col-lg-3'>
			<div class='form-group'>
				<label for="">Dt. Cancelamento</label>
				<input type='date' name='cancelamento[dtcancelamento]' id='cancelamentoDtcancelamento' class='form-control required'  value='<?= @$nota['dtcancelamento']; ?>'/>
			</div>
		</div>
		<div class='col-md-2 col-lg-2'>
			<div class='form-group'>
				<label for="">Tipo da Nota</label>
				<select name='cancelamento[tiponota]' id='cancelamentoTiponota' class='form-control required'>
					<option value=''>Selecione</option>
					<option value='1' <?php if(@$nota['tiponota'] == 1){ echo "selected='true'"; } ?>>Mercadoria</option>
					<option value='2' <?php if(@$nota['tiponota'] == 2){ echo "selected='true'"; } ?>>Serviço</option>
				</select>
			</div>
		</div>
		
		<div class='col-md-10 col-lg-10'>
			<div class='form-group'>
				<label for="">Motivo</label>
				<input type='text' name='cancelamento[motivo]' id='cancelamentoMotivo' class='form-control required' maxlength='120' placeholder='' value='<?= @$nota['motivo']; ?>'/>
			</div>
		</div>
		<div class="col-md-2">
            <div class="form-group">
                <label class="label-obrigatorio">Competência:</label>
                <?= $auxiliar->mes(@$nota['mes']); ?>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <label>&nbsp;</label>
                <?= $auxiliar->anoexercicio(@$nota['ano']); ?>
            </div>
        </div>
		<div class="clearfix"></div>
        <div class="col-gl-2 col-md-2">
            <div class="form-group">
                <label>&nbsp;</label>
                <button type="" class="btn btn-success btnSalvar">Salvar</button>
                <a href="?p=cancelamento" class="btn btn-danger">Cancelar</a>
                <input type="hidden" name="acao" value="<?= $btn ?>">
                <input type="hidden" name="codigo" value="<?= @$nota['id'] ?>">
            </div>
        </div>
	</form>
</div>

<!-- <script>
	$(function(){
		$(".btnSalvar").on("click", function(){
			if(validarForm(".formCancelamento") == true){
				$(".formCancelamento").submit();
			} // fim if
		});
	})
</script> -->