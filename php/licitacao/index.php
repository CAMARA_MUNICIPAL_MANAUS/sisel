<script type="text/javascript" src="js/action/LICITACAO/licitacao.js"></script>

<input type="hidden" id="anoExercicio" name="anoExercicio" value="<?php echo $_SESSION['usuario']['ano_exercicio']; ?>"/>

<div class="col-lg-12 col-md-12">
	<h3 class="page-header">Licitação</h3>
</div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left">
	<label for="">&nbsp;</label>
	<a href="?p=licitacao&page=licitacao_cadastrar" class="btn btn-primary novo form-control"><i class="glyphicon glyphicon-plus"></i> Nova Licitação</a>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
	<div class="form-group">
		<label>Mês de Empenho</label>
		<select class="form-control buscarempenho">
			<?php
			echo "<option value=''>Todos</option>";
			for ($i=1; $i <= 12 ; $i++) {
				echo "<option value='$i'>".$funcoes->mes($i)."</option>";
			}
			?>
		</select>
	</div>
</div>

<div class="col-lg-12 col-md-12">
	<table class="table table-hover table-striped table-condensed">
		<thead>
			<tr>
				<th></th>
				<th>Nº Processo</th>
				<th>Objetivo</th>
				<th class='text-right'>Valor</th>
				<th class='text-right'>Nº Edital</th>
				<th class='text-right'>Public. Edital</th>
				<th class='text-right'></th>
			</tr>
		</thead>
		<tbody>
			<?
			$sql = "SELECT id, objetivo, valor_bloqueado, numero_edital, date_format(dt_publicacao,'%d/%m/%Y') as dt_publicacao
			FROM licitacao
			WHERE licitacao.ano_exercicio = ".$_SESSION['usuario']['ano_exercicio']."
			ORDER BY id ASC";

			$stmt = $conexao->conn->prepare($sql);
			$stmt->execute();
			$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$i = 1;
			foreach ($retorno as $ln) {
				echo "<tr>";
					echo "<td class='text-right'>";
						echo "<a class='btn btn-primary btn-sm visualizar btn-circle' href='?p=licitacao&page=licitacao_cadastrar&licitacao={$ln['id']}'><i class='glyphicon glyphicon-search'></i></a>";
					echo "</td>";
					echo "<td>".$ln['id']."</td>";
					echo "<td title='{$ln['objetivo']}'>";
					if( strlen($ln['objetivo']) > 60){ echo substr($ln['objetivo'], 0,60).'...'; }else{ echo $ln['objetivo']; }
					echo "</td>";
					echo "<td class='text-right'> R$ ".number_format($ln['valor_bloqueado'], 2, ',', '.')."</td>";
					echo "<td class='text-right'>".$ln['numero_edital']."</td>";
					echo "<td class='text-right'>".$ln['dt_publicacao']."</td>";
					echo "<td class='text-right'>";
						echo "<button type='button' class='btn btn-danger btn-sm excluir-licitacao btn-circle' data-licitacao='{$ln['id']}'><i class='glyphicon glyphicon-remove'></i></button>";
					echo "</td>";
				echo "</tr>";
			}
			?>
		</tbody>
	</table>
</div>