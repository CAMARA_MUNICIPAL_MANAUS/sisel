<?php
include_once 'ajax_licitacao.php';

if(isset($_GET['licitacao'])){
    $licitacao = $funcoes->buscardados('licitacao', $_GET['licitacao']);

    $ordenador = $licitacao[0]['responsavel'];

    $lici = explode("-", $licitacao[0]['id']);

    $n_licitacao  = $lici[0];
    $anolicitacao = $lici[1];

    // ESSE CAMPO SERVE COMO PARAMETRO PARA O EDITAR VIA AJAX. O SCRIPT PEGA A LICITAÇÃO QUE ESTÁ NO GET E ENVIA PARA O UPDATE
    echo "<input type='hidden' value='{$_GET['licitacao']}' id='getlicitacao' />";

}else{
    $anolicitacao = $_SESSION['usuario']['ano_exercicio'];
    $n_licitacao  = '';
    $ordenador    = $_SESSION['empresa']['ordenador'];
}
?>

<script type="text/javascript" src="js/action/LICITACAO/licitacao.js"></script>

<link href="plugin/select2/css/minify/select2-bootstrap_select2.css" rel="stylesheet" />
<script src="plugin/select2/js/select2.min.js"></script>

<div class="col-lg-12 col-md-12 col-sm-12">
    <h3 class="page-header">Cadastrar/Editar Licitação</h3>
</div>
    
<div class="col-lg-12 col-md-12 col-sm-12">
        <form role="form" method="post" action="">

            <!-- ROM -->
            <div class="col-md-12">
                <div class="form-group">
                    <label>Modalidade</label> 
                    <?php $auxiliar->tipomodalidade(@$licitacao[0]['modalidade_fk']); ?>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label>Tipo de Licitação</label> <a href='#' title="Importante: Este campo não deve ser preenchido quando a modalide for: CONCURSO, DISPENSA OU INEXIGIBILIDADE."><i class='glyphicon glyphicon-info-sign'></i></a>
                    <?php $auxiliar->tipolicitacao(@$licitacao[0]['tipo_licitacao_fk']); ?>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label>Natureza do Objeto</label> <a href='#' title="Importante: Este campo não deve ser preenchido quando a modalide for: CONCURSO."><i class='glyphicon glyphicon-info-sign'></i></a>
                    <?php $auxiliar->tiponaturezalicitacao(@$licitacao[0]['tipo_natureza_fk']); ?>
                </div>
            </div>

            <div class="col-md-1">
                <div class="form-group">
                    <label>Nº Processo</label>
                    <input type="text" name="numprocesso" id="numprocesso" value="<?= @$n_licitacao; ?>" maxlength="8" class="form-control sonums"/>
                </div>
            </div>

            <div class="col-md-1">
                <div class="form-group">
                    <label>&nbsp;</label>
                    <input type="text" id="anolicitacao" value="<?= @$anolicitacao; ?>" maxlength="5" class="form-control sonums" />
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label>Tipo de Aquisição</label>
                    <?php $auxiliar->tipoitemlote(@$licitacao[0]['tipo_itemlote_fk']); ?>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label>Valor Previsto para Despesa</label>
                    <input type="text" name="vrBloq" value="<?= @number_format(@$licitacao[0]['valor_bloqueado'], 2, ',', '.'); ?>" class="form-control dinheiro"/>
                </div>
            </div>

            <!-- FIM ROM -->

            <div class="clearfix"></div>

            <div class="col-md-3">
                <div class="form-group">
                    <label>Natureza do Procedimento</label>
                    <?php $auxiliar->tiponaturezaprocedimento(@$licitacao[0]['tipo_naturezaprocedimento']); ?>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label>Regime de Execução - Obras</label>
                    <?php $auxiliar->tiporegimeexecucao(@$licitacao[0]['tipo_regime_fk']); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>Razão da escolha do fornecedor ou executante</label>

                    <? $disabled = (@$licitacao[0]['razao'] != "") ? "" : "disabled=true"; ?>
                    <input type="text" id="razao" name="razao" value="<?= @$licitacao[0]['razao']; ?>" class="form-control" <?= @$disabled ?> />
                </div>
            </div>

            <!-- ROM -->
            <div class="col-md-6">
                <div class="form-group">
                    <label>Objeto</label>
                    <input name="obj" type="text" id="obj" value="<?= @$licitacao[0]['objetivo']; ?>" class="form-control" placeholder="Digite aqui o objetivo desta licitação" maxlength='300'/>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                        <label>N&ordm; Empenho 01</label>
                        <input name="e1" type="text" id="e1"  maxlength="10" class="form-control" value="<?= @$licitacao[0]['numempenho']; ?>"/>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label>N&ordm; Empenho 05</label>
                    <input name="e5" type="text" id="e5"  maxlength="10" class="form-control" value="<?= @$licitacao[0]['numempenho5']; ?>"/>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label>N&ordm; Empenho 09</label>
                    <input name="e9" type="text" id="e9"  maxlength="10" class="form-control" value="<?= @$licitacao[0]['numempenho9']; ?>"/>
                </div>
            </div>
            <!-- FIM ROM -->

            <!-- ROM -->
            <div class="col-md-4">
                <div class="form-group">
                    <label>Ordenador</label>
                    <input name="ordenador" type="text" id="ordenador" class="form-control" value="<?= @$ordenador; ?>" />
                </div>
            </div>

            <div class="col-md-1">
                <div class="form-group">
                    <label>Nº Edital</label>
                    <input type="text" name="numEdital" id="numEdital" value="<?= @$licitacao[0]['numero_edital']; ?>" class="form-control"/>
                </div>
            </div>

            <div class="col-md-1">
                <div class="form-group">
                    <label>N&ordm; Di&aacute;rio</label>
                    <input type="text" name="numdiario" id="numdiario" value="<?= @$licitacao[0]['numero_diario']; ?>" class="form-control"/>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label>N&ordm; Empenho 02</label>
                    <input name="e2" type="text" id="e2"  maxlength="10" class="form-control" value="<?= @$licitacao[0]['numempenho2']; ?>"/>
                </div>
            </div>
        
            <div class="col-md-2">
                <div class="form-group">
                    <label>N&ordm; Empenho 06</label>
                    <input name="e6" type="text" id="e6"  maxlength="10" class="form-control" value="<?= @$licitacao[0]['numempenho6']; ?>"/>
                </div>
            </div>
        
            <div class="col-md-2">
                <div class="form-group">
                    <label>N&ordm; Empenho 10</label>
                    <input name="e10" type="text" id="e10" maxlength="10" class="form-control" value="<?= @$licitacao[0]['numempenho10']; ?>"/>
                </div>
            </div>
            <!-- FIM ROM -->
        
            <!-- ROM -->
        
            <div class="col-md-2">
                <div class="form-group"> 
                    <label>Public Edital <a href='#' title="Se houver mais de uma publicação de edital, referente ao mesmo processo licitatório, dentro da mesma competência, deverão ser enviadas somente as informações de último edital de licitação publicado."><i class='glyphicon glyphicon-info-sign'></i></a></label>
                    <input type="text" name="dtpublic" id="dtpublic" value="<?= @$funcoes->dateUSparaBR(@$licitacao[0]['dt_publicacao']); ?>" maxlength="10" class="form-control data"/>
                </div>
            </div>
        
            <div class="col-md-2">
                <div class="form-group"> 
                    <label>Dias</label>
                    <input type="text" name="dias" id="dias" value="<?= @$licitacao[0]['dias']; ?>"  maxlength="4" readonly="readonly" class="form-control"/>
                </div>
            </div>
        
            <div class="col-md-2">
                <div class="form-group"> 
                    <label>Dt. Limte Proposta</label>
                    <input type="text" name="dtLimite" id="dtLimite" value="<?= @$funcoes->dateUSparaBR(@$licitacao[0]['dt_limite']); ?>" class="form-control data"/>
                </div>
            </div>
        
            <div class="col-md-2">
                <div class="form-group">
                    <label>N&ordm; Empenho 03</label>
                    <input name="e3" type="text" id="e3" maxlength="10" class="form-control" value="<?= @$licitacao[0]['numempenho3']; ?>"/>
                </div>
            </div>
        
            <div class="col-md-2">
                <div class="form-group">
                    <label>N&ordm; Empenho 07</label>
                    <input name="e7" type="text" id="e7"  maxlength="10" class="form-control" value="<?= @$licitacao[0]['numempenho7']; ?>"/>
                </div>
            </div>
        
            <div class="col-md-2">
                <div class="form-group">
                    <label>N&ordm; Empenho 11</label>
                    <input name="e11" type="text" id="e11" maxlength="10" class="form-control" value="<?= @$licitacao[0]['numempenho11']; ?>"/>
                </div>
            </div>
            <!-- FIM ROW -->
        
            <!-- ROW -->           
        
            <div class="col-md-3">
                <div class="form-group"> 
                    <label>Arq. Texto:</label>
                    <input name="arq" type="text" id="arq" value="<?= @$licitacao[0]['nome_arquivo']; ?>" class="form-control" />
                </div>
            </div>
        
            <div class="col-md-2">
                <div class="form-group"> 
                    <label>Mês Competência</label>
                    <?php $auxiliar->mes(@$licitacao[0]['mes_empenho']); ?>
                </div>
            </div>
        
            <div class="col-md-1">
                <div class="form-group"> 
                    <label title="Ano da Competência">Ano</label>
                    <input name="anoEmpenho" type="text" id="anoEmpenho" class="form-control sonums" maxlength="4" value="<?= @$licitacao[0]['ano_empenho']; ?>" />
                </div>
            </div>
        
        
            
            <div class="col-md-2">
                <div class="form-group">
                    <label>N&ordm; Empenho 04</label>
                    <input name="e4" type="text" id="e4"  maxlength="10" class="form-control" value="<?= @$licitacao[0]['numempenho4']; ?>"/>
                </div>
            </div>
        
            <div class="col-md-2">
                <div class="form-group">
                    <label>N&ordm; Empenho 08</label>
                    <input name="e8" type="text" id="e8"  maxlength="10" class="form-control" value="<?= @$licitacao[0]['numempenho8']; ?>"/>
                </div>
            </div>
        
            <div class="col-md-2">
                <div class="form-group">
                    <label>N&ordm; Empenho 12</label>
                    <input name="e12" type="text" id="e12"  maxlength="10" class="form-control" value="<?= @$licitacao[0]['numempenho12']; ?>"/>
                </div>
            </div>
            
            <div class="col-md-2">
                <? if(isset($_GET['licitacao'])){ ?>
                    <button type="button" class='btn btn-success btn-block editar-licitacao'>Editar</button>
                <? }else{ ?>
                    <button type="button" class='btn btn-success btn-block inserir-licitacao'>Inserir</button>
                <? } ?>
            </div>
            <div class="col-md-2">
                <a href="?p=licitacao" class='btn btn-danger btn-block'>Voltar</a>
            </div>
        
        </form>
            </div>
        </div>
        
        <? if(isset($_GET['licitacao'])){ ?>
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
        <h3 class="panel-title">Dotação da Licitação</h3>
            </div>
            <div class="panel-body"> 
        <form action="" method="post">
            <div class="col-md-1">
                <div class="form-group" style="font-size: 11px;">
                    <label title="Cat. Econ&ocirc;mica">Cat. Econ.</label>
                    <input name="c1" type="text" id="c1" maxlength="1" class="form-control sonums"/>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="font-size: 11px;">
                    <label title="Grupo da Natureza">G. da Nat.</label>
                    <input name="c2" type="text" id="c2" maxlength="1" class="form-control sonums"/>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="font-size: 11px;">
                    <label title="Modalidade de Aplica&ccedil;&atilde;o">M. da Apl.</label>
                    <input name="c3" type="text" id="c3" maxlength="2" class="form-control sonums"/>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="font-size: 11px;">
                    <label title="Elemento">Elemento</label>
                    <input name="c4" type="text" id="c4" maxlength="2" class="form-control sonums"/>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="font-size: 11px;">
                    <label title="Unid. Or&ccedil;ament&aacute;ria">Unid. Or.</label>
                    <input name="c5" type="text" id="c5" maxlength="6" class="form-control sonums"/>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="font-size: 11px;">
                    <label title="Fonte de Recursos">Fonte. Re.</label>
                    <input name="c6" type="text" id="c6" maxlength="10" class="form-control sonums"/>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="font-size: 11px;">
                    <label title="A&ccedil;&atilde;o do Governo">Ação Gov.</label>
                    <input name="c7" type="text" id="c7" maxlength="7" class="form-control sonums"/>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="font-size: 11px;">
                    <label title="Sub-Fun&ccedil;&atilde;o">Sub F.</label>
                    <input name="c8" type="text" id="c8" maxlength="3" class="form-control sonums"/>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="font-size: 11px;">
                    <label title="Fun&ccedil;&atilde;o">Função</label>
                    <input name="c9" type="text" id="c9" maxlength="2" class="form-control sonums"/>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="font-size: 11px;">
                    <label title="Programa">Programa</label>
                    <input name="c10" type="text" id="c10" maxlength="4" class="form-control sonums"/>
                </div>
            </div>
        
            <div class="col-md-1">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    <button type="button" id="inserir-dotacao" class='btn btn-success form-control'>Inserir</button>
                    <button type="button" id="salvar-dotacao" class='btn btn-success form-control' style="display:none;">Editar</button>
                </div>
            </div> 
            <div class="col-md-1">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    <button type="button" id="cancelar-edicao" class='btn btn-danger form-control' disabled="disabled">Cancelar</button>
                </div>
            </div>           
        </form>
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th title="Categoria Econômica">C.E.</th>
                    <th title="Grupo da Natureza">G.N.</th>
                    <th title="Modalidade de Aplicação">M.A.</th>
                    <th title="Elemento">Elem.</th>
                    <th title="Unidade Orçamentária">U.O.</th>
                    <th title="Fonte de Recursos">F.R</th>
                    <th title="Ação do Governo">A.G</th>
                    <th title="Sub-Função">S.F.</th>
                    <th title="Função">Fu.</th>
                    <th title="Programa">Prog.</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody class="tbody-dotacao">
                <?php
                if(isset($_GET['licitacao'])){
        
                    $dados = $funcoes->buscardados_campo('dotacao', 'licitacao_fk', $_GET['licitacao']);
        
                    $num = sizeof(@$dados);
        
                    if(@$num > 0){
                        foreach (@$dados as $d) {
                            echo "<tr id='{$d['id']}'>";
                                echo "<td>".$d['categoria_economica']."</td>";
                                echo "<td>".$d['grupo_natureza']."</td>";
                                echo "<td>".$d['modalidade_aplicacao']."</td>";
                                echo "<td>".$d['elemento']."</td>";
                                echo "<td>".$d['unidade_orcamentaria']."</td>";
                                echo "<td>".$d['fonte_recurso']."</td>";
                                echo "<td>".$d['acao_governo']."</td>";
                                echo "<td>".$d['sub_funcao']."</td>";
                                echo "<td>".$d['funcao']."</td>";
                                echo "<td>".$d['programa']."</td>";
                                echo "<td>";
                                    echo "<button type='button' class='btn btn-warning btn-sm editar-dotacao btn-circle' idDotacao='{$d['id']}'><i class='glyphicon glyphicon-edit'></i></button>";
                                echo "</td>";
                                echo "<td>";
                                    echo "<button type='button' class='btn btn-danger btn-sm remover-dotacao btn-circle' idDotacao='{$d['id']}' data-licitacao='{$_GET['licitacao']}'><i class='glyphicon glyphicon-remove'></i></button>";
                                echo "</td>";
                            echo "</tr>";
                        }
                    }else{
                        echo "<tr><td colspan='11'>Nenhuma Dotação Cadastrada</td></tr>";
                    }
        
                }else{
                    echo "<tr><td colspan='11'>Nenhuma Dotação Cadastrada</td></tr>";
                }
                ?>
            </tbody>
        </table>
            </div>
        </div>
            <? } ?>
        
        <!--    ADICIONAR ITEM --><!--    ADICIONAR ITEM -->
        <!--    ADICIONAR ITEM --><!--    ADICIONAR ITEM -->
        <!--    ADICIONAR ITEM --><!--    ADICIONAR ITEM -->
        <!--    ADICIONAR ITEM --><!--    ADICIONAR ITEM -->
        <!--    ADICIONAR ITEM --><!--    ADICIONAR ITEM -->
        <!--    ADICIONAR ITEM --><!--    ADICIONAR ITEM -->
        
        
        <? if(isset($_GET['licitacao'])){ ?>
        <br/>
        <div class="panel panel-default item-licitacao">
            <div class="panel-heading">
        <h3 class="panel-title">Item da Licitação</h3>
            </div>
            <div class="panel-body">    
        
        <div class="col-md-12">
            <button type="button" class='btn btn-primary novoitem' data-toggle="modal" data-target="#myModalItemLicitacao"><i class="glyphicon glyphicon-plus"></i> Adicionar Item</button>
        </div>
        
        <div class="row"><div class="col-md-12"></div></div>
        
        <div class="col-md-12">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th title="Sequencial">Seq.</th>
                        <th>Descrição</th>
                        <th>QTD</th>
                        <th>Unidade</th>
                        <th>Publicação</th>
                        <th>Assinatura</th>
                        <th>Item/Lote</th>
                        <th>Status</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?
                    $sql = "select i.id, i.licitacao_fk, i.sequencial, i.quantidade, p.descricao, i.dt_publicacao, i.dt_assinatura, s.`status`, u.unidade, i.controle_itemlote, p.id as idproduto, u.id as idunidade, s.id as idstatus
                    from itemlicitacao i 
                    left join produto p        ON p.id = i.produto_fk
                    left join status_item s    ON s.id = i.status_item_fk
                    left join tipounidade u    ON u.id = i.unidade_fk
                    where i.licitacao_fk = '{$_GET['licitacao']}'";
                    $stmt = $conexao->conn->prepare(@$sql);
                    $stmt->execute();
                    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    /*echo "<pre>";
                    print_r($retorno);
                    echo "</pre>";*/
                    foreach (@$retorno as $item) {
        
                        echo "<tr data-id='".$item['id']."' data-licitacao='".$item['licitacao_fk']."'>";
                            echo "<td data-sequencial='".$item['sequencial']."'>".$item['sequencial']."</td>";
                            echo "<td data-produto='".$item['idproduto']."'>".$item['descricao']."</td>";
                            echo "<td data-qtd='".$item['quantidade']."'>".$item['quantidade']."</td>";
                            echo "<td data-unidade='".$item['idunidade']."'>".$item['unidade']."</td>";
                            echo "<td data-dt_publicacao='".$item['dt_publicacao']."'>".$funcoes->dateUSparaBR(@$item['dt_publicacao'])."</td>";
                            echo "<td data-dt_assinatura='".$item['dt_assinatura']."'>".$funcoes->dateUSparaBR(@$item['dt_assinatura'])."</td>";
                            echo "<td data-itemlote='".$item['controle_itemlote']."'>".$item['controle_itemlote']."</td>";
                            echo "<td data-status='".$item['idstatus']."'>".$item['status']."</td>";
                            echo "<td>";
                                echo "<button type='button' class='btn btn-warning editar-item btn-sm btn-circle'><i class='glyphicon glyphicon-edit'></i></button>";
                            echo "</td>";
                            echo "<td>";
                                echo "<button type='button' class='btn btn-danger excluir-item btn-sm btn-circle'><i class='glyphicon glyphicon-remove'></i></button>";
                            echo "</td>";
                        echo "</tr>";
        
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <? } ?>
            </div>
        </div>



<!-- ####################### -->
<!-- ####################### -->
<!-- ####################### -->

<!-- Modal -->
<div class="modal fade" id="myModalItemLicitacao" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cadastrar/Editar Item de Licitação</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <form role='form' class="formItemLicitacao">
                    <div class='col-md-12'>
                        <div class='form-group'>
                            <label>Descrição do Produto</label>
                            <select name="descricao" id="descricao" class="form-control select2">
                                <!-- autocomplete_forn -->
                                <option value="" selected="selected">Selecione</option>
                                <?
                                $sql = "SELECT * FROM produto p ORDER BY p.descricao";
                                $stmt = $conexao->conn->prepare($sql);
                                $stmt->execute();
                                $produto = $stmt->fetchAll(PDO::FETCH_ASSOC);

                                foreach ($produto as $prod) {
                                    echo "<option value='{$prod['id']}' unidade='{$prod['unidade_fk']}'>{$prod['descricao']}</option>";
                                }
                                ?>    
                            </select>
                        </div>
                    </div>

                    <div class='col-md-3'>
                        <div class='form-group'>
                            <label>Dt. Public. Homologa</label>
                            <input type='date' name='dtpubhomologa' id="dtpubhomologa" class='form-control data' />
                        </div>
                    </div>
                    <div class='col-md-3'>
                        <div class='form-group'>
                            <label>Dt. Ass. Homologação</label>
                            <input type='date' name='dtassinatura' id="dtassinatura" class='form-control data' />
                        </div>
                    </div>

                    <div class='col-md-3'>
                        <div class='form-group'>
                            <label>Controle Item Lote</label>
                            <input type='text' name='controle' id="controle" class='form-control' maxlength="10" />
                        </div>
                    </div>

                    <div class='col-md-3'>
                        <div class='form-group'>
                            <label>Status do Item</label>
                            <?php $auxiliar->statusitem(); ?>
                        </div>
                    </div>

                    <div class='col-md-3'>
                        <div class='form-group'>
                            <label>Sequencial</label>
                            <input type='text' name='sequencial' id="sequencial" class='form-control sonums' />
                        </div>
                    </div>
                    <div class='col-md-3'>
                        <div class='form-group'>
                            <label>QTD</label>
                            <input type='text' name='qtd' id='qtd' class='form-control sonums' maxlength="10" />
                        </div>
                    </div>
                    <div class='col-md-3'>
                        <div class='form-group'>
                            <label>Unidade</label>
                            <?php $auxiliar->tipounidade(); ?>
                        </div>
                    </div>

                    <div class='col-md-2'>
                        <div class='form-group'>
                            <label>&nbsp;</label>
                            <button type="button" class='form-control btn btn-success cadastrar-item' data-id=""></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<!-- ####################### -->
<!-- ####################### -->
<!-- ####################### -->