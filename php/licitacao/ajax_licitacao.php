<?php
if(isset($_POST['cadastrarlicitacao'])){

    include_once '../../config/conn.php';
    include_once '../../config/validarsessao.php';
    include_once '../../config/funcoesgerais.php';

    $funcoes = new Funcoes;

    $ano_exercicio = $_SESSION['usuario']['ano_exercicio'];

    $_POST['dt_publicacao'] = $funcoes->dateBRparaUS($_POST['dt_publicacao']);
    $_POST['dt_limite'] = $funcoes->dateBRparaUS($_POST['dt_limite']);

    $sql = "INSERT INTO licitacao (id, modalidade_fk, tipo_itemlote_fk, tipo_licitacao_fk, tipo_natureza_fk, tipo_regime_fk, tipo_naturezaprocedimento, objetivo, razao, valor_bloqueado, numero_edital, numero_diario,
                                     dt_publicacao, dias, dt_limite, responsavel, ano_empenho, nome_arquivo,
                                     numempenho, numempenho2, numempenho3, numempenho4, numempenho5, numempenho6, numempenho7,
                                     numempenho8, numempenho9, numempenho10, numempenho11, numempenho12,
                                     ano_exercicio, mes_empenho, usuario, data_criacao)

                             VALUES (:numprocesso, :molidade, :tipo_itemlote, :tipolicitacao, :tiponaturezalicitacao, :tiporegimeexecucao, :tiponaturezaprocedimento, :objetivo, :razao, :valor_bloqueado, :numero_edital, :numero_diario, :dt_publicacao,
                                     :dias, :dt_limite, :ordernador, :anoEmpenho, :arq, :empenho1, :empenho2, :empenho3, :empenho4, :empenho5, :empenho6,
                                     :empenho7, :empenho8, :empenho9, :empenho10, :empenho11, :empenho12,
                                     $ano_exercicio, :mesEmpenho, 19, NOW())";

        $stmt = $conexao->conn->prepare($sql);

        $stmt->bindValue(':molidade', $_POST['molidade']);
        $stmt->bindValue(':numprocesso', $_POST['numprocesso']);
        $stmt->bindValue(':tipo_itemlote', $_POST['tipo_itemlote']);

        $stmt->bindValue(':tipolicitacao', $_POST['tipolicitacao']);
        $stmt->bindValue(':tiponaturezalicitacao', $_POST['tiponaturezalicitacao']);
        $stmt->bindValue(':tiporegimeexecucao', $_POST['tiporegimeexecucao']);
        $stmt->bindValue(':tiponaturezaprocedimento', $_POST['tiponaturezaprocedimento']);
        $stmt->bindValue(':razao', $_POST['razao']);

        $stmt->bindValue(':valor_bloqueado', $funcoes->formatarValorParaBanco($_POST['valor_bloqueado']));
        $stmt->bindValue(':objetivo', $_POST['objetivo']);
        $stmt->bindValue(':ordernador', $_POST['ordernador']);
        $stmt->bindValue(':numero_edital', $_POST['numero_edital']);
        $stmt->bindValue(':numero_diario', $_POST['numero_diario']);
        $stmt->bindValue(':dt_publicacao', $_POST['dt_publicacao']);
        $stmt->bindValue(':dias', $_POST['dias']);
        $stmt->bindValue(':dt_limite', $_POST['dt_limite']);
        $stmt->bindValue(':mesEmpenho', $_POST['mesEmpenho']);
        $stmt->bindValue(':anoEmpenho', $_POST['anoEmpenho']);
        $stmt->bindValue(':arq', $_POST['arq']);
        $stmt->bindValue(':empenho1', $_POST['empenho1']);
        $stmt->bindValue(':empenho2', $_POST['empenho2']);
        $stmt->bindValue(':empenho3', $_POST['empenho3']);
        $stmt->bindValue(':empenho4', $_POST['empenho4']);
        $stmt->bindValue(':empenho5', $_POST['empenho5']);
        $stmt->bindValue(':empenho6', $_POST['empenho6']);
        $stmt->bindValue(':empenho7', $_POST['empenho7']);
        $stmt->bindValue(':empenho8', $_POST['empenho8']);
        $stmt->bindValue(':empenho9', $_POST['empenho9']);
        $stmt->bindValue(':empenho10', $_POST['empenho10']);
        $stmt->bindValue(':empenho11', $_POST['empenho11']);
        $stmt->bindValue(':empenho12', $_POST['empenho12']);
        
        if($stmt->execute()){
            echo 1;
        }else{
            $sql = "SELECT * FROM licitacao WHERE id = '{$_POST['numprocesso']}'";
            $stmt = $conexao->conn->prepare($sql);
            $stmt->execute();
            $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $sizeof = sizeof($retorno);
            if($sizeof > 0){
                echo 2;
            }else{                
                print_r($_POST);
                print_r($stmt->errorInfo());
            }
        }

        // print_r($stmt->errorInfo());
    exit();
}

if(isset($_POST['editarlicitacao'])){

    // print_r($_POST);

    include_once '../../config/conn.php';
    include_once '../../config/validarsessao.php';
    include_once '../../config/funcoesgerais.php';

    $funcoes = new Funcoes;

    $ano_exercicio = $_SESSION['usuario']['ano_exercicio'];


    $_POST['dt_publicacao'] = $funcoes->dateBRparaUS($_POST['dt_publicacao']);
    $_POST['dt_limite'] = $funcoes->dateBRparaUS($_POST['dt_limite']);

        // não existe nenhuma licitação com o mesmo número e o update vai ser feito

        $stmt->bindValue(':tipolicitacao', $_POST['tipolicitacao']);
        $stmt->bindValue(':tiponaturezalicitacao', $_POST['tiponaturezalicitacao']);
        $stmt->bindValue(':tiporegimeexecucao', $_POST['tiporegimeexecucao']);
        $stmt->bindValue(':razao', $_POST['razao']);

        $sql = "UPDATE licitacao
                SET id               = '".$_POST['numprocesso']."',
                    modalidade_fk    = '".$_POST['molidade']."',
                    tipo_itemlote_fk = '".$_POST['tipo_itemlote']."',
                    
                    tipo_licitacao_fk   = '".$_POST["tipolicitacao"]."',
                    tipo_natureza_fk    = '".$_POST["tiponaturezalicitacao"]."',
                    tipo_regime_fk      = '".$_POST["tiporegimeexecucao"]."',
                    tipo_naturezaprocedimento = '".$_POST["tiponaturezaprocedimento"]."',

                    objetivo         = '".$_POST['objetivo']."',

                    razao            = '".$_POST["razao"]."',

                    valor_bloqueado  = '".$funcoes->formatarValorParaBanco($_POST['valor_bloqueado'])."',
                    numero_edital    = '".$_POST['numero_edital']."',
                    numero_diario    = '".$_POST['numero_diario']."',
                    dt_publicacao    = '".$_POST['dt_publicacao']."',
                    dias             = '".$_POST['dias']."',
                    dt_limite        = '".$_POST['dt_limite']."',
                    responsavel      = '".$_POST['ordernador']."',
                    ano_empenho      = '".$_POST['anoEmpenho']."',
                    nome_arquivo     = '".$_POST['arq']."',
                    numempenho       = '".$_POST['empenho1']."',
                    numempenho2      = '".$_POST['empenho2']."',
                    numempenho3      = '".$_POST['empenho3']."',
                    numempenho4      = '".$_POST['empenho4']."',
                    numempenho5      = '".$_POST['empenho5']."',
                    numempenho6      = '".$_POST['empenho6']."',
                    numempenho7      = '".$_POST['empenho7']."',
                    numempenho8      = '".$_POST['empenho8']."',
                    numempenho9      = '".$_POST['empenho9']."',
                    numempenho10     = '".$_POST['empenho10']."',
                    numempenho11     = '".$_POST['empenho11']."',
                    numempenho12     = '".$_POST['empenho12']."',
                    ano_exercicio    = $ano_exercicio,
                    mes_empenho      = '".$_POST['mesEmpenho']."',
                    usuario          = 19
                WHERE id = '{$_POST['getlicitacao']}'";

        $stmt = $conexao->conn->prepare($sql);
        
        if($stmt->execute()){
            echo 1;
        }else{

            if(trim($_POST['numprocesso']) != trim($_POST['getlicitacao'])){
                $sql = "SELECT * FROM licitacao WHERE id = '{$_POST['numprocesso']}'";
                $stmt = $conexao->conn->prepare($sql);
                $stmt->execute();
                $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);
                
                $sizeof = sizeof($retorno);
                if($sizeof > 1){
                    echo 2;
                }else{                
                    echo 0;
                }
            }else{
                echo 0;
				print_r($_POST);
                print_r($stmt->errorInfo());
            }

        }

    exit();
}

if (isset($_POST['excluirlicitacao'])) {

    include_once '../../config/conn.php';

    $licitacao = trim($_POST['licitacao']);

    /*print_r($_POST);
    exit();*/

    $sql = "CALL sp_deletarLicitacao('$licitacao')";
    $stmt = $conexao->conn->prepare($sql);
    
    if($stmt->execute()){
        echo 1;
    }else{
        echo 0;
    }

    exit();
}

if(isset($_POST['cadastraritem']) || isset($_POST['editaritem'])){
    // ESSA FUNÇÃO SERVE CADASTRAR E EDITAR ITEM DA LICITAÇÃO

    include_once '../../config/conn.php';
    include_once '../../config/funcoesgerais.php';

    $funcoes = new Funcoes;

    // CRIA AS VARIÁVEIS COM VALORES
    $numprocesso        = $_POST['numprocesso'];
    $sequencial         = $_POST['sequencial'];
    $quantidade         = $_POST['quantidade'];
    $descricao          = $_POST['descricao'];
    $dtpubhomologa      = $_POST['dtpubhomologa'];
    $dtassinatura       = $_POST['dtassinatura'];
    $tipounidade        = $_POST['tipounidade'];
    $statusitem         = $_POST['statusitem'];
    $controle           = $_POST['controle'];


    if(isset($_POST['cadastraritem'])){
        $sql = "INSERT INTO itemlicitacao (licitacao_fk, sequencial, quantidade, produto_fk, dt_publicacao,
                                             dt_assinatura, controle_itemlote, unidade_fk, status_item_fk)
                                     VALUES ('$numprocesso', $sequencial, $quantidade, $descricao, '$dtpubhomologa',
                                             '$dtassinatura', '$controle', $tipounidade, $statusitem)";
    }else{
        $sql = "UPDATE itemlicitacao
                    SET licitacao_fk = '$numprocesso',
                        sequencial = $sequencial,
                        quantidade = $quantidade,
                        produto_fk = $descricao,
                        dt_publicacao = '$dtpubhomologa',
                        dt_assinatura = '$dtassinatura',
                        controle_itemlote = '$controle',
                        unidade_fk = $tipounidade,
                        status_item_fk = $statusitem
                WHERE id = {$_POST['idItem']}";
    }

    $stmt = $conexao->conn->prepare($sql);
    
    // SE A QUERY FOR EXECUTADA COM SUCESSO RETORNA 1, SE NÃO VAI RETORNAR 0
    if($stmt->execute()){
        echo 1;
    }else{
        echo 0;
    }

    exit();
}

if(isset($_POST['exluiritem'])){

    include_once '../../config/conn.php';

    /*print_r($_POST);
    exit();*/

    $deletar = "DELETE FROM itemlicitacao WHERE id = ".$_POST['iditem'];
    $stmt = $conexao->conn->prepare($deletar);
    
    if($stmt->execute()){
        echo 1;
    }else{
        echo 0;
    }
    exit();
}


if(isset($_POST['iditem'])){
    
    include_once '../../config/conn.php';

    $sql = "select i.id, i.licitacao_fk, i.sequencial, i.quantidade, i.produto_fk,
         date_format(i.dt_publicacao,'%d/%m/%Y') as 'dt_publicacao',
         date_format(i.dt_assinatura,'%d/%m/%Y') as 'dt_assinatura',
         i.unidade_fk, i.status_item_fk, p.descricao, i.controle_itemlote
        from itemlicitacao i
        inner join produto p ON p.id = i.produto_fk
        where i.id = {$_POST['iditem']}";
    $stmt = $conexao->conn->prepare($sql);
    $stmt->execute();
    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($retorno[0]);

    exit();
}

if(isset($_POST['dotacao'])){

    include_once '../../config/conn.php';

    $categoria_economica  = $_POST['c1'];
    $grupo_natureza       = $_POST['c2'];
    $modalidade_aplicacao = $_POST['c3'];
    $elemento             = $_POST['c4'];
    $unidade_orcamentaria = $_POST['c5'];
    $fonte_recurso        = $_POST['c6'];
    $acao_governo         = $_POST['c7'];
    $sub_funcao           = $_POST['c8'];
    $funcao               = $_POST['c9'];
    $programa             = $_POST['c10'];

    $numprocesso          = $_POST['numprocesso'];

    $sql = "INSERT INTO dotacao (licitacao_fk, categoria_economica, grupo_natureza, modalidade_aplicacao, elemento,
                                        unidade_orcamentaria, fonte_recurso, acao_governo, sub_funcao, funcao, programa)              
                VALUES (:numprocesso, :categoria_economica, :grupo_natureza, :modalidade_aplicacao, :elemento, :unidade_orcamentaria,
                               :fonte_recurso, :acao_governo, :sub_funcao, :funcao, :programa)";
    $stmt = $conexao->conn->prepare($sql);
    $stmt->bindValue(':numprocesso', $numprocesso);
    $stmt->bindValue(':categoria_economica', $categoria_economica);
    $stmt->bindValue(':grupo_natureza', $grupo_natureza);
    $stmt->bindValue(':modalidade_aplicacao', $modalidade_aplicacao);
    $stmt->bindValue(':elemento', $elemento);
    $stmt->bindValue(':unidade_orcamentaria', $unidade_orcamentaria);
    $stmt->bindValue(':fonte_recurso', $fonte_recurso);
    $stmt->bindValue(':acao_governo', $acao_governo);
    $stmt->bindValue(':sub_funcao', $sub_funcao);
    $stmt->bindValue(':funcao', $funcao);
    $stmt->bindValue(':programa', $programa);

    if($stmt->execute()){


        $sql = "select * from dotacao WHERE licitacao_fk = :dado";
        $stmt = $conexao->conn->prepare($sql);
        $stmt->bindValue(':dado',$numprocesso);
        $stmt->execute();
        $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $num = sizeof($retorno);

        if($num > 0){
            foreach ($retorno as $d) {
                echo "<tr id='{$d['id']}'>";
                    echo "<td>".$d['categoria_economica']."</td>";
                    echo "<td>".$d['grupo_natureza']."</td>";
                    echo "<td>".$d['modalidade_aplicacao']."</td>";
                    echo "<td>".$d['elemento']."</td>";
                    echo "<td>".$d['unidade_orcamentaria']."</td>";
                    echo "<td>".$d['fonte_recurso']."</td>";
                    echo "<td>".$d['acao_governo']."</td>";
                    echo "<td>".$d['sub_funcao']."</td>";
                    echo "<td>".$d['funcao']."</td>";
                    echo "<td>".$d['programa']."</td>";
                    echo "<td class='text-right'>";
                        echo "<button type='button' class='btn btn-warning editar-dotacao' idDotacao='{$d['id']}'><i class='glyphicon glyphicon-edit'></i></button>";
                            echo "&nbsp;&nbsp;";
                        echo "<button type='button' class='btn btn-danger remover-dotacao' idDotacao='{$d['id']}'><i class='glyphicon glyphicon-remove'></i></button>";
                    echo "</td>";
                echo "</tr>";
            }
        }else{
            echo "<tr><td colspan='11'>Nenhuma Dotação Cadastrada</td></tr>";
        }

    }else{
        // print_r($stmt->errorinfo());
        echo 0;
    } // fim if

    exit();
} // fim if

if(isset($_POST['cancelardotacao'])){

    include_once '../../config/conn.php';

    $sql = "select * from dotacao WHERE licitacao_fk = :dado";
    $stmt = $conexao->conn->prepare($sql);
    $stmt->bindValue(':dado',$_POST['numprocesso']);
    $stmt->execute();
    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $num = sizeof($retorno);

    if($num > 0){
        foreach ($retorno as $d) {
            echo "<tr id='{$d['id']}'>";
                echo "<td>".$d['categoria_economica']."</td>";
                echo "<td>".$d['grupo_natureza']."</td>";
                echo "<td>".$d['modalidade_aplicacao']."</td>";
                echo "<td>".$d['elemento']."</td>";
                echo "<td>".$d['unidade_orcamentaria']."</td>";
                echo "<td>".$d['fonte_recurso']."</td>";
                echo "<td>".$d['acao_governo']."</td>";
                echo "<td>".$d['sub_funcao']."</td>";
                echo "<td>".$d['funcao']."</td>";
                echo "<td>".$d['programa']."</td>";
                echo "<td class='text-right'>";
                    echo "<button type='button' class='btn btn-warning editar-dotacao btn-circle' idDotacao='{$d['id']}'><i class='glyphicon glyphicon-edit'></i></button>";
                        echo "&nbsp;&nbsp;";
                    echo "<button type='button' class='btn btn-danger remover-dotacao btn-circle' idDotacao='{$d['id']}'><i class='glyphicon glyphicon-remove'></i></button>";
                echo "</td>";
            echo "</tr>";
        }
    }else{
        echo "<tr><td colspan='11'>Nenhuma Dotação Cadastrada</td></tr>";
    }

    exit();

}

if(isset($_POST['deletardotacao'])){

    include_once '../../config/conn.php';

    $sql = "DELETE FROM dotacao WHERE id = {$_POST['iddotacao']}";
    $stmt = $conexao->conn->prepare($sql);
    
    if($stmt->execute()){

        $sql = "select * from dotacao WHERE licitacao_fk = :dado";
        $stmt2 = $conexao->conn->prepare($sql);
        $stmt2->bindValue(':dado',$_POST['numprocesso']);
        $stmt2->execute();
        $retorno = $stmt2->fetchAll(PDO::FETCH_ASSOC);

        $num = sizeof($retorno);

        if($num > 0){
            foreach ($retorno as $d) {
                echo "<tr id='{$d['id']}'>";
                    echo "<td>".$d['categoria_economica']."</td>";
                    echo "<td>".$d['grupo_natureza']."</td>";
                    echo "<td>".$d['modalidade_aplicacao']."</td>";
                    echo "<td>".$d['elemento']."</td>";
                    echo "<td>".$d['unidade_orcamentaria']."</td>";
                    echo "<td>".$d['fonte_recurso']."</td>";
                    echo "<td>".$d['acao_governo']."</td>";
                    echo "<td>".$d['sub_funcao']."</td>";
                    echo "<td>".$d['funcao']."</td>";
                    echo "<td>".$d['programa']."</td>";
                    echo "<td class='text-right'>";
                        echo "<button type='button' class='btn btn-warning editar-dotacao btn-circle' idDotacao='{$d['id']}'><i class='glyphicon glyphicon-edit'></i></button>";
                            echo "&nbsp;&nbsp;";
                        echo "<button type='button' class='btn btn-danger remover-dotacao btn-circle' idDotacao='{$d['id']}' data-licitacao='{$_POST['numprocesso']}'><i class='glyphicon glyphicon-remove'></i></button>";
                    echo "</td>";
                echo "</tr>";
            }
        }else{
            echo "<tr><td colspan='11'>Nenhuma Dotação Cadastrada</td></tr>";
        }
    }else{
        echo 0;
    }
    exit();

}

if(isset($_POST['pesLic'])){
    // PESQUISAR LICITAÇÃO POR MÊS DE EMPENHO
    
    /*print_r($_POST); */

    include_once '../../config/conn.php';

    if($_POST['empenho'] != ''){
        $where = "AND licitacao.mes_empenho = LPAD('".$_POST['empenho']."','2','0')";
    }else{
        $where = "";
    }

    $sql = "SELECT id, objetivo, valor_bloqueado, numero_edital, date_format(dt_publicacao,'%d/%m/%Y') as dt_publicacao
            FROM licitacao
            WHERE licitacao.ano_exercicio = ".$_POST['anoexercicio']."
            $where       
            ORDER BY id ASC";
                                        
    // exit();
    $stmt = $conexao->conn->prepare($sql);
    $stmt->execute();
    $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $i = 1;
    if(!empty($retorno)){
        foreach ($retorno as $ln) {
            echo "<tr>";
                echo "<td>".$ln['id']."</td>";
                echo "<td title='{$ln['objetivo']}'>";
                    if( strlen($ln['objetivo']) > 60){ echo substr($ln['objetivo'], 0,60).'...'; }else{ echo $ln['objetivo']; }
                echo "</td>";
                echo "<td class='text-right'> R$ ".number_format($ln['valor_bloqueado'], 2, ',', '.')."</td>";
                echo "<td class='text-right'>".$ln['numero_edital']."</td>";
                echo "<td class='text-right'>".$ln['dt_publicacao']."</td>";
                echo "<td class='text-right'>";
                    echo "<a class='btn btn-primary visualizar' href='?p=licitacao&page=licitacao_cadastrar&licitacao={$ln['id']}'><i class='glyphicon glyphicon-search'></i></a>";
                        echo "&nbsp;";
                    echo "<button type='button' class='btn btn-danger excluir-licitacao' data-licitacao='{$ln['id']}'><i class='glyphicon glyphicon-remove'></i></button>";
                echo "</td>";
            echo "</tr>";
        }
    }else{
        echo 'Nenhuma Licitação cadastrada para este mês!';
    }
    exit();
}

?>