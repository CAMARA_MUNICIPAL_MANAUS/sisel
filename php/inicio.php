<?php include_once 'plugin/Fusioncharts/fusioncharts.php'; ?>
<script src="plugin/Fusioncharts/js/fusioncharts.js"></script>

<div class="col-lg-12 col-md-12 col-sm-12">
	<h3 class="page-header">Bem vindo!</h3>
</div>

<div class='col-lg-12 col-md-12 col-sm-12'>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><b>Despesas por Fornecedor - <?= $_SESSION['usuario']['ano_exercicio']; ?> (Top 20)</b></h3>
		</div>
		<div class="panel-body">
		<?php
			$arrData = array(
				"chart" => array(
					/* "caption" => "Teste titulo do grafico",
					"subCaption" => "sub titulo", */
					"xAxisName" => "20 Fornecedores que tiveram as maiores despesas junto a empresa.",
					"yAxisName" => "Valor em Real",
					"numberPrefix" => "R$ ", // Moeda (prefixo)
					// "paletteColors" => "#0075c2", // Define se todos serão da mesma cor ou cores diferente
					"bgColor" => "#ffffff",
					"borderAlpha" => "20",
					"canvasBorderAlpha" => "0",
					"usePlotGradientColor" => "0", // Define se as cores serão chapas ou de forma gradiente
					"plotBorderAlpha" => "10",
					"placeValuesInside" => "1",
					"rotatevalues" => "1",
					"labelDisplay" => "", // Deixar a label na vertical ou horizontal. Para deixar horizontal => Rotate, se não apenas remova
					"valueFontColor" => "#000000", // Define a cor dos valores
					"showXAxisLine" => "1",
					"xAxisLineColor" => "#999999",
					"divlineColor" => "#999999",
					"divLineIsDashed" => "1",
					"showAlternateHGridColor" => "0",
					"subcaptionFontSize" => "14",
					"subcaptionFontBold" => "2",
					"formatNumberScale" => "1" // Define se o valor em exibido em K,M ou sem formatação
					)
				);

			$sql = "CALL `sp_gastos_por_fornecedor`('{$_SESSION['usuario']['ano_exercicio']}')";
			$stmt = $conexao->conn->prepare($sql);
			$stmt->execute();
			$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

			// print_r($retorno);

			$arrData['data'] = array(); 
			foreach ($retorno as $value) {
				array_push($arrData['data'],
					array(
						'label' => $value['nome_fantasia'],
						'value' => $value['valor']
						)
					);
			}

			$jsonEncodedData = json_encode($arrData);

			$chart = new FusionCharts("Column2D", "myFirstChart2" , '100%', 300, "chart-2", "json", $jsonEncodedData);
			$chart->render();
		?>

		<div id="chart-2"></div>

		</div>
	</div>
</div>

<div class='col-lg-6 col-md-6 col-sm-12'>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><b>Despesas por Mês - <?= $_SESSION['usuario']['ano_exercicio']; ?> (Licitação)</b></h3>
		</div>
		<div class="panel-body">
			
		<?php
			$arrData = array(
				"chart" => array(
					/* "caption" => "Teste titulo do grafico",
					"subCaption" => "sub titulo", */
					"xAxisName" => "Mês",
					"yAxisName" => "Valor em Real",
					"numberPrefix" => "R$ ",
					// "paletteColors" => "#0075c2", // Define se todos serão da mesma cor ou cores diferente
					"bgColor" => "#ffffff",
					"borderAlpha" => "20",
					"canvasBorderAlpha" => "0",
					"usePlotGradientColor" => "0", // Define se as cores serão chapas ou de forma gradiente
					"plotBorderAlpha" => "10",
					"placeValuesInside" => "1",
					"rotatevalues" => "1",
					"valueFontColor" => "#000000", // Define a cor dos valores
					"showXAxisLine" => "1",
					"xAxisLineColor" => "#999999",
					"divlineColor" => "#999999",
					"divLineIsDashed" => "1",
					"showAlternateHGridColor" => "0",
					"subcaptionFontSize" => "14",
					"subcaptionFontBold" => "2",
					"formatNumberScale" => "1" // Define se o valor em exibido em K,M ou sem formatação
					)
				);

			$sql = "CALL `sp_gastos_por_mes`('{$_SESSION['usuario']['ano_exercicio']}')";
			$stmt = $conexao->conn->prepare($sql);
			$stmt->execute();
			$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$arrData['data'] = array();
			foreach ($retorno as $value) {
				array_push($arrData['data'],
					array(
						'label' => $value['mes'],
						'value' => $value['valor']
						)
					);
			}

			$jsonEncodedData = json_encode($arrData);

			$chart = new FusionCharts("Column2D", "myFirstChart" , '100%', 300, "chart-1", "json", $jsonEncodedData);
			$chart->render();
		?>

		<div id="chart-1"></div>

		</div>
	</div>
</div>

<div class='col-lg-6 col-md-6 col-sm-12'>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><b>Despesas por Modalidade - <?= $_SESSION['usuario']['ano_exercicio']; ?></b></h3>
		</div>
		<div class="panel-body">
			
		<?php
			$arrData = array(
				"chart" => array(
					/* "caption" => "Teste titulo do grafico",
					"subCaption" => "sub titulo", */
					"xAxisName" => "Mês",
					"yAxisName" => "Valor em Real",
					"numberPrefix" => "R$ ",
					// "paletteColors" => "#0075c2", // Define se todos serão da mesma cor ou cores diferente
					"bgColor" => "#ffffff",
					"borderAlpha" => "20",
					"canvasBorderAlpha" => "0",
					"usePlotGradientColor" => "0", // Define se as cores serão chapas ou de forma gradiente
					"plotBorderAlpha" => "10",
					"placeValuesInside" => "1",
					"rotatevalues" => "1",
					"valueFontColor" => "#000000", // Define a cor dos valores
					"showXAxisLine" => "1",
					"xAxisLineColor" => "#999999",
					"divlineColor" => "#999999",
					"divLineIsDashed" => "1",
					"showAlternateHGridColor" => "0",
					"subcaptionFontSize" => "14",
					"subcaptionFontBold" => "2",
					"formatNumberScale" => "1" // Define se o valor em exibido em K,M ou sem formatação
					)
				);

			$sql = "CALL `sp_gastos_por_modalidade`('{$_SESSION['usuario']['ano_exercicio']}')";
			$stmt = $conexao->conn->prepare($sql);
			$stmt->execute();
			$retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);

			// print_r($retorno);

			$arrData['data'] = array();
			foreach ($retorno as $value) {
				array_push($arrData['data'],
					array(
						'label' => $value['descricao'],
						'value' => $value['valor']
						)
					);
			}

			$jsonEncodedData = json_encode($arrData);

			$chart = new FusionCharts("Column2D", "tipoproduto2" , '100%', 300, "tipoproduto", "json", $jsonEncodedData);
			$chart->render();
		?>

		<div id="tipoproduto"></div>

		</div>
	</div>
</div>