  <nav class="navbar navbar-inverse">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">
          <div onclick="location.href='index.php'" class="logot"><img src="img/logo_menu.png" width="100"></div>
      </a>
  </div>


  <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">

      <li><a href="?"><i class='glyphicon glyphicon-home'></i> Início</a></li>

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class='flaticon-licitacao2'></i>Processos<span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
          
          <li><a href="?p=contrato&page=contrato">Contratos</a></li>
            
            <li class="divider"></li>

          <li><a href="?p=licitacao&page=licitacao">Licitação e Item</a></li>
          <li><a href="?p=publicacao&page=publicacao">Publicação</a></li>
          <li><a href="?p=participante&page=participante">Participante e Certidão</a></li>
          <li><a href="?p=cotacao&page=cotacao">Cotação</a></li>
            
            <li class="divider"></li>

          <li><a href="?p=transferenciavoluntaria&page=transferenciavoluntaria">Transf. Voluntária</a></li>
            
            <li class="divider"></li>

          <li><a href="?p=adesao&page=adesao">Adesão e Item de Adesão</a></li>

      </ul>
  </li>
  <li><a href="?p=exportar&page=exportar"><i class='flaticon-exportar'></i>Exportar TCE</a></li>


  <li><a href="?p=relatorios&page=relatorios"><i class='flaticon-relatorios'></i>Relatórios</a></li>

  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class='flaticon-configuracoes'></i>Configurações<span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
      <li><a href="?p=configuracao&page=gerais">Gerais</a></li>
      <li><a href="?p=fornecedores&page=fornecedores">Fornecedores</a></li>
      <li><a href="?p=produto&page=produto">Produtos</a></li>
  </ul>
</li>
<li><a href="#"><i class='flaticon-usuarios'></i>Usuários</a></li>

</ul>
<ul class="nav navbar-nav navbar-right">
  <li><a id="btnOpenDialog" href="php/login/logout.php"><i class='flaticon-sair'></i>Sair</a></li>
</ul>
</div>
</div>

<div class="panel panel-default">
  <div class="panel-body">
    <div class="container">
      <div class='col-md-12'>
        <form class="form-horizontal" id="" name="" method="POST" action="">
          <div class="col-md-2">
            <input class="form-control" name="" type="text" placeholder="Digite o nº da Licitação" autocomplete="off"/>
        </div>
        <div class="col-md-4">
            <button class="btn btn-success" onclick="alert('Desculpe pelo transtorno, ainda estamos em desenvolvimento. Até quarta-feira(26/08/2015) todas as funcionalidades do sistema estarão funcionando normalmente.\n\nContinue cadastrando normalmente as outras informações.')"><i class='flaticon-pesquisar'></i>Buscar</button>
            &nbsp;
            <button name="" type="button" class="btn btn-primary" onclick="alert('Desculpe pelo transtorno, ainda estamos em desenvolvimento. Até quarta-feira(26/08/2015) todas as funcionalidades do sistema estarão funcionando normalmente.\n\nContinue cadastrando normalmente as outras informações.')"><i class='flaticon-busca-avanca'></i>Pesquisa Avançada</button>
        </div>
        <div class="col-md-6">
            <?php
            $nome_completo  = $_SESSION['usuario']['nome'];
            $ano_exercicio  = $_SESSION['usuario']['ano_exercicio'];
            $empresa_nome   = $_SESSION['empresa']['nome_empresa'];
            ?>
            <input class="form-control" value="Usuário: <?= $nome_completo; ?> | Exercício: <?= $ano_exercicio; ?> | <?= $empresa_nome; ?>" type="text" disabled="disabled">
        </div>
    </form>
</div>
</div>
</div>
</div>
</nav>