<?php
date_default_timezone_set('america/manaus');

include_once 'config/Fun.php';
include_once 'config/conn_novo.php';

date_default_timezone_set('america/manaus');

// ARUQIVO COM FUNÇÕES COMUNS PARA OS CONTROL
$fun = new FUN();

// CONNECTIONS
$sisel = new ConnectionSISEL();

// IMPORTAÇÃO DE CLASSES
function my_autoload ($pClassName) {
    include_once('./php'.DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.'M'.substr($pClassName, 1).".class.php");
    include_once('./php'.DIRECTORY_SEPARATOR.'control'.DIRECTORY_SEPARATOR.'C'.substr($pClassName, 1).".class.php");
}
spl_autoload_register("my_autoload");

// VERIFICA SE TEM ALGUMA FUNÇÃO SENDO CHAMADA VIA POST OU GET
// TAMBÉM É UTILIZADO PARA RECEBER PARAMETRO VIA JSON

# print_r($_POST);

if(isset($_POST["funcao"]) || isset($_GET["funcao"])){
	if(isset($_POST["funcao"])){
		if(isset($_FILES) && sizeof($_FILES) > 0){
			echo call_user_func_array($_POST["funcao"], array($_POST, $_FILES));
		}else{
			echo call_user_func_array($_POST["funcao"], $_POST["dados"]);
		}
	}else{
		echo call_user_func_array($_GET["funcao"], $_GET["dados"]);
	}
	exit();
}

?>